Maia Installation Guide
=======================

Requirements
============
Progress 10.2B +
Stomp (https://bitbucket.org/jmls/stomp)

Optional
========
Windows 32 (Optional, for UI demos)
DevExpress DxV2 controls  (Optional, for UI demos)

Basic installation for eclipse
==============================

Unzip the source file package into either the top-level directory of your project, or a sub-directory of your choice
Ensure that the directory is in your propath
Compile dotr/Maia
Select the OE Editor view
Select Openedge->Tools->Customization Editor
Select Menu / Toolbar Entries

Press the Add Button
Name: Maia
Icon: (Choose an icon)
ToolTip: Starts Maia
Program Name: dotr/Maia/OEABuild.p
Parameter: object
Tick the �Send file name of the current selection�
Tick the "run persistent" option
Select �Show on menu and toolbar�

Press the Add Button
Name: Maia Scaffolding
Icon: (Choose an icon)
ToolTip: Starts Maia
Program Name: dotr/Maia/Scaffold.p
Parameter: object
Tick the �Send file name of the current selection�
Tick the "run persistent" option
Select �Show on menu and toolbar�

Select File->Save all
Press the refresh button (top right, tooltip is �refresh customization options�
If all is well, there should be a new button on your OEA toolbar. 

for a new project, you need to go select the folder that you want to build a new maia 
 project in. Press the Maia Scaffolding toolbar button and select the tables that you
 want to build maia build files for.
  
for examples of a build file, see sports._maia

select either a build file or _Maia project and press the Maia toolbar button

Basic installation for other
============================

Unzip the source file package into either the top-level directory of your project, or a sub-directory of your choice
Ensure that the directory is in your propath
Compile dotr/Maia
run dotr/Maia/OEABuild.p ("build file" / "_Maia" directory)
