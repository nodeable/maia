class Enum._Maia.Colour inherits dotr.Maia.Enum:
  method public override void init():
    super:init().
		DataType(dotr.Maia.Enum.DataTypeEnum:GetByValue("integer")).
		Property(NewProp("Red"):InitialValue("1")).
		Property(NewProp("Green"):InitialValue("2")).
		Property(NewProp("Blue"):InitialValue("3")).
  end method.
end class.