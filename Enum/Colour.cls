/*
Copyright (c) 2012, Julian Lyndon-Smith (julian+maia@dotr.com)
http://www.dotr.com
All rights reserved.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction except as noted below, including without limitation
the rights to use,copy, modify, merge, publish, distribute,
and/or sublicense, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

 The Software and/or source code cannot be copied in whole and
 sold without meaningful modification for a profit.

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.

 Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in
 the documentation and/or other materials provided with
 the distribution.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

using Progress.Lang.*.
using Enum.*.

routine-level on error undo, throw.

class Enum.Colour :
  def public property name        as char no-undo get . private set .
  def public property description as char no-undo get . private set .
  
  def public property value as integer no-undo get . private set .
  
  def public static property Red as Colour no-undo
    get():
      if not valid-object(Enum.Colour:Red) then Enum.Colour:Red = new Colour('Red',1).
      return Enum.Colour:Red.
    end get . private set .
  
  def public static property Green as Colour no-undo
    get():
      if not valid-object(Enum.Colour:Green) then Enum.Colour:Green = new Colour('Green',2).
      return Enum.Colour:Green.
    end get . private set .
  
  def public static property Blue as Colour no-undo
    get():
      if not valid-object(Enum.Colour:Blue) then Enum.Colour:Blue = new Colour('Blue',3).
      return Enum.Colour:Blue.
    end get . private set .

  constructor static Colour():
  end method.

  constructor private Colour(p_Name as char,p_Value as integer):
    this-object(p_Name,p_Value,p_Name).
  end constructor.

  constructor private Colour(p_Name as char,p_Value as integer,p_Desc as char):
    assign this-object:name        = p_Name
           this-object:value       = p_Value
           this-object:description = p_Desc.
  end constructor.
  
  /** return the enum described by the supplied name
   * @param name of enum to find
   * @return Enum.Colour
   */
  
  method static Colour GetByName(p_Name as char):
    case p_Name:
    	when 'Red':u            then return Colour:Red.
    	when 'Green':u            then return Colour:Green.
    	when 'Blue':u            then return Colour:Blue.
    
    	otherwise undo, throw new Progress.Lang.AppError(substitute('Invalid Colour Enum Name [&1]',p_name),-999).
    end case.
  end method.

  /** return the enum described by the supplied value
   * @param name of enum to find
   * @return Enum.Colour
   */
  
  method static Colour GetByValue(p_Value as integer):
    case p_Value:
    	when 1 then return Colour:Red.
    	when 2 then return Colour:Green.
    	when 3 then return Colour:Blue.
    
    	otherwise undo, throw new Progress.Lang.AppError(substitute('Invalid Colour Enum Value [&1]',p_value),-998).
    end case.
  end method.
  
  
end class.
@BuildInfo(Source="Colour").
@BuildInfo(Class="Enum._Maia.Colour").
@BuildInfo(Date="16/04/2013 06:35:58.455+01:00").
@BuildInfo(MD5Hash="BbZd2wgdjtwov3o3K0xCFw==").