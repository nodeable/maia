"000001" 2 28/03/93 "DKP" "" "Ordered frisbees. We sent Ultimate style, they want Guts. Guts are on backorder, I told them they would receive them by May."
"000002" 21 14/04/93 "HXM" "" "Order of sweat bands were wrong color, and cycle helmets were all a size larger than ordered. They are very upset and threaten to pull their account. I told them I would straighten the situation out and make sure they were happy."
"000003" 56 07/01/93 "DKP" "" "Want us to drop ship for their customers so they don't have to keep such a large inventory. I told them I would discuss it with management and let them know."
"000004" 6 29/01/93 "SLS" "" "Ordered footballs. We sent American footballs, they want soccer balls. They realize it was their error and will pay extra shipping if we can ship as quickly as possible."
"000005" 15 23/04/93 "HXM" "" "Have a shot at a contract with the New Jersey Nets. Need to stage a presentation for Chuck Daly and want us to help them create sweat suit designs. "
"000006" 2 05/05/93 "DKP" "000001" "Received Guts frisbees and are satisfied."
"000007" 37 27/05/93 "GPE" "" "Want to know if we can get Merlin mountain bikes frames. I told them I would call Merlin in Massachusetts and inquire."
"000008" 56 04/03/93 "DKP" "000003" "I told them I spoke to management and they were not convinced that Super Golf would buy as much if they didn't have to warehouse. Dick at Super Golf told me if we could drop ship golf club sets only he could guarantee us a minimum order for the year. I told him I would get back to him."
"000009" 15 06/06/93 "HXM" "000005" "Presentation to Nets went great! They agreed to buy warm-up suits, shorts, knee guards, and sweat bands."
"000010" 6 10/03/93 "SLS" "000004" "Received shipment of soccer balls. Decided to keep footballs as well. They think they can market them effectively. Keep track of progress to see if we should target other U.K. companies for football equipment. "
"000011" 21 10/05/93 "HXM" "000002" "Corrected order and sent a free box of Pearl Izumi gel gloves. They agreed to give us another chance."
"000012" 37 01/06/93 "GPE" "000007" "Merlin is such a high end product, they will deal with them directly. Passed along account info to Dave at Merlin."
"000013" 56 08/04/93 "DKP" "000003" "Contract went out to Super Golf today agreeing to drop ship sets of golf clubs if they guarantee us they will buy as much as they did last year. They will continue to warehouse shoes and balls."
.
PSC
filename=Ref-Call
records=0000000000013
ldbname=sports
timestamp=2013/06/22-14:05:47
numformat=44,46
dateformat=dmy-1950
map=NO-MAP
cpstream=ISO8859-1
.
0000002477
