class sports._Maia.Salesrep inherits sports._Maia._base:
  method public override void init():
    super:init().
		Association(HasMany("Customer"):Keys("SalesRep","SalesRepID"):UsingPropertyName("Customers"):CascadeNullify()).
		Association(HasMany("Order"):Keys("SalesRep","SalesRepID"):UsingPropertyName("Orders"):CascadeNullify()).
  end method.
end class.