class sports._Maia.Customer inherits sports._Maia._base:
  method public override void init():
    super:init().

		Rename("Sales-Rep"):To("SalesRepID").
		Rename("State"):To("StateID").
		
		Validation("Name"):HasValue():Length(5,50).
		
		Association(HasMany("Order"):Keys("CustNum"):UsingPropertyName("Orders"):CascadeDelete()).
    Association(HasMany("Invoice"):Keys("CustNum"):UsingPropertyName("Invoices"):CascadeDelete()).

		Association(BelongsTo("State"):Keys("StateID","State"):CascadeNullify()).
		Association(BelongsTo("Salesrep"):Keys("SalesRepID","SalesRep"):CascadeNullify()).
  end method.
end class.