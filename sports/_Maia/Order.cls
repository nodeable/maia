class sports._Maia.Order inherits sports._Maia._base:
  method public override void init():
    super:init().
		Rename("Sales-Rep"):To("SalesRepID").
		Association(BelongsTo("Customer"):Keys("CustNum"):CascadeDelete()).
		Association(HasMany("OrderLine"):Keys("Ordernum"):UsingPropertyName("OrderLines"):CascadeDelete()).
		Association(BelongsTo("Salesrep"):Keys("SalesRepID","SalesRep"):CascadeNullify()).
  end method.
end class.