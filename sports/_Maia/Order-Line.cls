class sports._Maia.Order-Line inherits sports._Maia._base:
  method public override void init():
    super:init().
		Association(BelongsTo("Order"):Keys("Ordernum"):CascadeDelete()).
		Association(BelongsTo("Item"):Keys("Itemnum"):CascadeNullify()).
  end method.
end class.