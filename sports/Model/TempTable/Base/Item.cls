/*
Copyright (C) 2011 by Julian Lyndon-Smith (julian+mit@dotr.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS for A PARTICULAR PURPOSE and NONINFRINGEMENT. IN no EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE for ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR otherwise, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/


using Progress.Lang.*.
using sports.Model.*.
using Progress.Data.*.

routine-level on error undo, throw.

class sports.Model.TempTable.Base.Item:
  def protected temp-table TT_Item no-undo serialize-name "Item" 
  
  		field CatDescription                        as character  serialize-name 'CatDescription'
  		field ItemName                              as character  serialize-name 'ItemName'
  
  		field Price                                 as decimal  serialize-name 'Price'
  
  		field Allocated                             as integer  serialize-name 'Allocated'
  		field CatPage                               as integer  serialize-name 'CatPage'
  		field ItemNum                               as integer  serialize-name 'ItemNum'
  		field OnHand                                as integer  serialize-name 'OnHand'
  		field OnOrder                               as integer  serialize-name 'OnOrder'
  		field ReOrder                               as integer  serialize-name 'ReOrder'.
  
  /** take current ActiveRecord and convert to longchar
   *  
   *  gets the current object, converts into a temp-table
   *  and exports the data */ 
  
  method public longchar ExportData(p_Format as char,p_Mode as char,p_Item as sports.Model.Item):
    
    def var lv_Data as longchar no-undo.
    
    if p_Mode eq "single" or not valid-handle(p_Item:query) then 
    do:
      CreateRecord(p_Item).
    end.
    
    else
    do:
      p_Item:first().
      
      do while p_Item:available:
        CreateRecord(p_Item).
        p_Item:Next().
      end.
    end.

    temp-table TT_Item:write-json("longchar",lv_Data,yes).

    return lv_Data.
    
    finally:
      empty temp-table TT_Item.
    end finally.
    
  end method. 
   
  method private void CreateRecord(p_Item as sports.Model.Item):
    create TT_Item.

    assign TT_Item.Allocated      = p_Item:Allocated
           TT_Item.CatDescription = p_Item:CatDescription
           TT_Item.CatPage        = p_Item:CatPage
           TT_Item.ItemName       = p_Item:ItemName
           TT_Item.ItemNum        = p_Item:ItemNum
           TT_Item.OnHand         = p_Item:OnHand
           TT_Item.OnOrder        = p_Item:OnOrder
           TT_Item.Price          = p_Item:Price
           TT_Item.ReOrder        = p_Item:ReOrder.
  end method.
  
end class.
@BuildInfo(Source="Item").
@BuildInfo(Class="sports._Maia.Item").
@BuildInfo(Date="16/04/2013 20:29:09.199+01:00").
@BuildInfo(MD5Hash="2igaNokQrk2+ozDtndxmjg==").