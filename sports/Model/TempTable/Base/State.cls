/*
Copyright (C) 2011 by Julian Lyndon-Smith (julian+mit@dotr.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS for A PARTICULAR PURPOSE and NONINFRINGEMENT. IN no EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE for ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR otherwise, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/


using Progress.Lang.*.
using sports.Model.*.
using Progress.Data.*.

routine-level on error undo, throw.

class sports.Model.TempTable.Base.State:
  def protected temp-table TT_State no-undo serialize-name "State" 
  
  		field Region                                as character  serialize-name 'Region'
  		field State                                 as character  serialize-name 'State'
  		field StateName                             as character  serialize-name 'StateName'.
  
  /** take current ActiveRecord and convert to longchar
   *  
   *  gets the current object, converts into a temp-table
   *  and exports the data */ 
  
  method public longchar ExportData(p_Format as char,p_Mode as char,p_State as sports.Model.State):
    
    def var lv_Data as longchar no-undo.
    
    if p_Mode eq "single" or not valid-handle(p_State:query) then 
    do:
      CreateRecord(p_State).
    end.
    
    else
    do:
      p_State:first().
      
      do while p_State:available:
        CreateRecord(p_State).
        p_State:Next().
      end.
    end.

    temp-table TT_State:write-json("longchar",lv_Data,yes).

    return lv_Data.
    
    finally:
      empty temp-table TT_State.
    end finally.
    
  end method. 
   
  method private void CreateRecord(p_State as sports.Model.State):
    create TT_State.

    assign TT_State.Region    = p_State:Region
           TT_State.State     = p_State:State
           TT_State.StateName = p_State:StateName.
  end method.
  
end class.
@BuildInfo(Source="State").
@BuildInfo(Class="sports._Maia.State").
@BuildInfo(Date="15/04/2013 16:01:41.285+01:00").
@BuildInfo(MD5Hash="9cizNPy3WL2cyt83xNjblQ==").