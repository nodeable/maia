/*
Copyright (C) 2011 by Julian Lyndon-Smith (julian+mit@dotr.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS for A PARTICULAR PURPOSE and NONINFRINGEMENT. IN no EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE for ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR otherwise, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/


using Progress.Lang.*.
using sports.Model.*.
using Progress.Data.*.

routine-level on error undo, throw.

class sports.Model.TempTable.Base.RefCall:
  def protected temp-table TT_Ref-Call no-undo serialize-name "RefCall" 
  
  		field CallNum                               as character  serialize-name 'CallNum'
  		field Parent                                as character  serialize-name 'Parent'
  		field SalesRep                              as character  serialize-name 'SalesRep'
  		field Txt                                   as character  serialize-name 'Txt'
  
  		field CallDate                              as date  serialize-name 'CallDate'
  
  		field CustNum                               as integer  serialize-name 'CustNum'.
  
  /** take current ActiveRecord and convert to longchar
   *  
   *  gets the current object, converts into a temp-table
   *  and exports the data */ 
  
  method public longchar ExportData(p_Format as char,p_Mode as char,p_RefCall as sports.Model.RefCall):
    
    def var lv_Data as longchar no-undo.
    
    if p_Mode eq "single" or not valid-handle(p_RefCall:query) then 
    do:
      CreateRecord(p_RefCall).
    end.
    
    else
    do:
      p_RefCall:first().
      
      do while p_RefCall:available:
        CreateRecord(p_RefCall).
        p_RefCall:Next().
      end.
    end.

    temp-table TT_Ref-Call:write-json("longchar",lv_Data,yes).

    return lv_Data.
    
    finally:
      empty temp-table TT_Ref-Call.
    end finally.
    
  end method. 
   
  method private void CreateRecord(p_RefCall as sports.Model.RefCall):
    create TT_Ref-Call.

    assign TT_Ref-Call.CallDate = p_RefCall:CallDate
           TT_Ref-Call.CallNum  = p_RefCall:CallNum
           TT_Ref-Call.CustNum  = p_RefCall:CustNum
           TT_Ref-Call.Parent   = p_RefCall:Parent
           TT_Ref-Call.SalesRep = p_RefCall:SalesRep
           TT_Ref-Call.Txt      = p_RefCall:Txt.
  end method.
  
end class.
@BuildInfo(Source="RefCall").
@BuildInfo(Class="sports._Maia.Ref-Call").
@BuildInfo(Date="15/04/2013 16:07:46.088+01:00").
@BuildInfo(MD5Hash="Qi73WSK7yQzGGqHHMHNd5w==").