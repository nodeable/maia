/*
Copyright (C) 2011 by Julian Lyndon-Smith (julian+mit@dotr.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS for A PARTICULAR PURPOSE and NONINFRINGEMENT. IN no EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE for ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR otherwise, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/


using Progress.Lang.*.
using sports.Model.*.
using Progress.Data.*.

routine-level on error undo, throw.

class sports.Model.TempTable.Base.Order:
  def protected temp-table TT_Order no-undo serialize-name "Order" 
  
  		field Carrier                               as character  serialize-name 'Carrier'
  		field Instructions                          as character  serialize-name 'Instructions'
  		field PO                                    as character  serialize-name 'PO'
  		field SalesRepID                            as character  serialize-name 'SalesRepID'
  		field Terms                                 as character  serialize-name 'Terms'
  
  		field OrderDate                             as date  serialize-name 'OrderDate'
  		field PromiseDate                           as date  serialize-name 'PromiseDate'
  		field ShipDate                              as date  serialize-name 'ShipDate'
  
  		field CustNum                               as integer  serialize-name 'CustNum'
  		field OrderNum                              as integer  serialize-name 'OrderNum'.
  
  /** take current ActiveRecord and convert to longchar
   *  
   *  gets the current object, converts into a temp-table
   *  and exports the data */ 
  
  method public longchar ExportData(p_Format as char,p_Mode as char,p_Order as sports.Model.Order):
    
    def var lv_Data as longchar no-undo.
    
    if p_Mode eq "single" or not valid-handle(p_Order:query) then 
    do:
      CreateRecord(p_Order).
    end.
    
    else
    do:
      p_Order:first().
      
      do while p_Order:available:
        CreateRecord(p_Order).
        p_Order:Next().
      end.
    end.

    temp-table TT_Order:write-json("longchar",lv_Data,yes).

    return lv_Data.
    
    finally:
      empty temp-table TT_Order.
    end finally.
    
  end method. 
   
  method private void CreateRecord(p_Order as sports.Model.Order):
    create TT_Order.

    assign TT_Order.Carrier      = p_Order:Carrier
           TT_Order.CustNum      = p_Order:CustNum
           TT_Order.Instructions = p_Order:Instructions
           TT_Order.OrderDate    = p_Order:OrderDate
           TT_Order.OrderNum     = p_Order:OrderNum
           TT_Order.PO           = p_Order:PO
           TT_Order.PromiseDate  = p_Order:PromiseDate
           TT_Order.SalesRepID   = p_Order:SalesRepID
           TT_Order.ShipDate     = p_Order:ShipDate
           TT_Order.Terms        = p_Order:Terms.
  end method.
  
end class.
@BuildInfo(Source="Order").
@BuildInfo(Class="sports._Maia.Order").
@BuildInfo(Date="11/06/2013 14:29:56.793+01:00").
@BuildInfo(MD5Hash="ggcsG+J92hFV/w+8nQwi9A==").