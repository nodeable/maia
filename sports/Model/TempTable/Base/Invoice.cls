/*
Copyright (C) 2011 by Julian Lyndon-Smith (julian+mit@dotr.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS for A PARTICULAR PURPOSE and NONINFRINGEMENT. IN no EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE for ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR otherwise, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/


using Progress.Lang.*.
using sports.Model.*.
using Progress.Data.*.

routine-level on error undo, throw.

class sports.Model.TempTable.Base.Invoice:
  def protected temp-table TT_Invoice no-undo serialize-name "Invoice" 
  
  		field InvoiceDate                           as date  serialize-name 'InvoiceDate'
  
  		field Adjustment                            as decimal  serialize-name 'Adjustment'
  		field Amount                                as decimal  serialize-name 'Amount'
  		field ShipCharge                            as decimal  serialize-name 'ShipCharge'
  		field TotalPaid                             as decimal  serialize-name 'TotalPaid'
  
  		field CustNum                               as integer  serialize-name 'CustNum'
  		field InvoiceNum                            as integer  serialize-name 'InvoiceNum'
  		field OrderNum                              as integer  serialize-name 'OrderNum'.
  
  /** take current ActiveRecord and convert to longchar
   *  
   *  gets the current object, converts into a temp-table
   *  and exports the data */ 
  
  method public longchar ExportData(p_Format as char,p_Mode as char,p_Invoice as sports.Model.Invoice):
    
    def var lv_Data as longchar no-undo.
    
    if p_Mode eq "single" or not valid-handle(p_Invoice:query) then 
    do:
      CreateRecord(p_Invoice).
    end.
    
    else
    do:
      p_Invoice:first().
      
      do while p_Invoice:available:
        CreateRecord(p_Invoice).
        p_Invoice:Next().
      end.
    end.

    temp-table TT_Invoice:write-json("longchar",lv_Data,yes).

    return lv_Data.
    
    finally:
      empty temp-table TT_Invoice.
    end finally.
    
  end method. 
   
  method private void CreateRecord(p_Invoice as sports.Model.Invoice):
    create TT_Invoice.

    assign TT_Invoice.Adjustment  = p_Invoice:Adjustment
           TT_Invoice.Amount      = p_Invoice:Amount
           TT_Invoice.CustNum     = p_Invoice:CustNum
           TT_Invoice.InvoiceDate = p_Invoice:InvoiceDate
           TT_Invoice.InvoiceNum  = p_Invoice:InvoiceNum
           TT_Invoice.OrderNum    = p_Invoice:OrderNum
           TT_Invoice.ShipCharge  = p_Invoice:ShipCharge
           TT_Invoice.TotalPaid   = p_Invoice:TotalPaid.
  end method.
  
end class.
@BuildInfo(Source="Invoice").
@BuildInfo(Class="sports._Maia.Invoice").
@BuildInfo(Date="15/04/2013 15:59:23.946+01:00").
@BuildInfo(MD5Hash="xvjttoOs8U2J3TevAGqnUQ==").