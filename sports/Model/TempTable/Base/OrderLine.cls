/*
Copyright (C) 2011 by Julian Lyndon-Smith (julian+mit@dotr.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS for A PARTICULAR PURPOSE and NONINFRINGEMENT. IN no EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE for ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR otherwise, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/


using Progress.Lang.*.
using sports.Model.*.
using Progress.Data.*.

routine-level on error undo, throw.

class sports.Model.TempTable.Base.OrderLine:
  def protected temp-table TT_Order-Line no-undo serialize-name "OrderLine" 
  
  		field ExtendedPrice                         as decimal  serialize-name 'ExtendedPrice'
  		field Price                                 as decimal  serialize-name 'Price'
  
  		field Discount                              as integer  serialize-name 'Discount'
  		field ItemNum                               as integer  serialize-name 'ItemNum'
  		field LineNum                               as integer  serialize-name 'LineNum'
  		field OrderNum                              as integer  serialize-name 'OrderNum'
  		field Qty                                   as integer  serialize-name 'Qty'
  
  		field Backorder                             as logical  serialize-name 'Backorder'.
  
  /** take current ActiveRecord and convert to longchar
   *  
   *  gets the current object, converts into a temp-table
   *  and exports the data */ 
  
  method public longchar ExportData(p_Format as char,p_Mode as char,p_OrderLine as sports.Model.OrderLine):
    
    def var lv_Data as longchar no-undo.
    
    if p_Mode eq "single" or not valid-handle(p_OrderLine:query) then 
    do:
      CreateRecord(p_OrderLine).
    end.
    
    else
    do:
      p_OrderLine:first().
      
      do while p_OrderLine:available:
        CreateRecord(p_OrderLine).
        p_OrderLine:Next().
      end.
    end.

    temp-table TT_Order-Line:write-json("longchar",lv_Data,yes).

    return lv_Data.
    
    finally:
      empty temp-table TT_Order-Line.
    end finally.
    
  end method. 
   
  method private void CreateRecord(p_OrderLine as sports.Model.OrderLine):
    create TT_Order-Line.

    assign TT_Order-Line.Backorder     = p_OrderLine:Backorder
           TT_Order-Line.Discount      = p_OrderLine:Discount
           TT_Order-Line.ExtendedPrice = p_OrderLine:ExtendedPrice
           TT_Order-Line.ItemNum       = p_OrderLine:ItemNum
           TT_Order-Line.LineNum       = p_OrderLine:LineNum
           TT_Order-Line.OrderNum      = p_OrderLine:OrderNum
           TT_Order-Line.Price         = p_OrderLine:Price
           TT_Order-Line.Qty           = p_OrderLine:Qty.
  end method.
  
end class.
@BuildInfo(Source="OrderLine").
@BuildInfo(Class="sports._Maia.Order-Line").
@BuildInfo(Date="16/04/2013 20:29:09.731+01:00").
@BuildInfo(MD5Hash="sK3K9hTbw+gOIz8A2baa+Q==").