/*
Copyright (C) 2011 by Julian Lyndon-Smith (julian+mit@dotr.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS for A PARTICULAR PURPOSE and NONINFRINGEMENT. IN no EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE for ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR otherwise, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/


using Progress.Lang.*.
using sports.Model.*.
using Progress.Data.*.

routine-level on error undo, throw.

class sports.Model.TempTable.Base.Customer:
  def protected temp-table TT_Customer no-undo serialize-name "Customer" 
  
  		field Address                               as character  serialize-name 'Address'
  		field Address2                              as character  serialize-name 'Address2'
  		field City                                  as character  serialize-name 'City'
  		field Comments                              as character  serialize-name 'Comments'
  		field Contact                               as character  serialize-name 'Contact'
  		field Country                               as character  serialize-name 'Country'
  		field Name                                  as character  serialize-name 'Name'
  		field Phone                                 as character  serialize-name 'Phone'
  		field PostalCode                            as character  serialize-name 'PostalCode'
  		field SalesRepID                            as character  serialize-name 'SalesRepID'
  		field StateID                               as character  serialize-name 'StateID'
  		field Terms                                 as character  serialize-name 'Terms'
  
  		field Balance                               as decimal  serialize-name 'Balance'
  		field CreditLimit                           as decimal  serialize-name 'CreditLimit'
  
  		field CustNum                               as integer  serialize-name 'CustNum'
  		field Discount                              as integer  serialize-name 'Discount'.
  
  /** take current ActiveRecord and convert to longchar
   *  
   *  gets the current object, converts into a temp-table
   *  and exports the data */ 
  
  method public longchar ExportData(p_Format as char,p_Mode as char,p_Customer as sports.Model.Customer):
    
    def var lv_Data as longchar no-undo.
    
    if p_Mode eq "single" or not valid-handle(p_Customer:query) then 
    do:
      CreateRecord(p_Customer).
    end.
    
    else
    do:
      p_Customer:first().
      
      do while p_Customer:available:
        CreateRecord(p_Customer).
        p_Customer:Next().
      end.
    end.

    temp-table TT_Customer:write-json("longchar",lv_Data,yes).

    return lv_Data.
    
    finally:
      empty temp-table TT_Customer.
    end finally.
    
  end method. 
   
  method private void CreateRecord(p_Customer as sports.Model.Customer):
    create TT_Customer.

    assign TT_Customer.Address     = p_Customer:Address
           TT_Customer.Address2    = p_Customer:Address2
           TT_Customer.Balance     = p_Customer:Balance
           TT_Customer.City        = p_Customer:City
           TT_Customer.Comments    = p_Customer:Comments
           TT_Customer.Contact     = p_Customer:Contact
           TT_Customer.Country     = p_Customer:Country
           TT_Customer.CreditLimit = p_Customer:CreditLimit
           TT_Customer.CustNum     = p_Customer:CustNum
           TT_Customer.Discount    = p_Customer:Discount
           TT_Customer.Name        = p_Customer:Name
           TT_Customer.Phone       = p_Customer:Phone
           TT_Customer.PostalCode  = p_Customer:PostalCode
           TT_Customer.SalesRepID  = p_Customer:SalesRepID
           TT_Customer.StateID     = p_Customer:StateID
           TT_Customer.Terms       = p_Customer:Terms.
  end method.
  
end class.
@BuildInfo(Source="Customer").
@BuildInfo(Class="sports._Maia.Customer").
@BuildInfo(Date="11/06/2013 14:29:56.486+01:00").
@BuildInfo(MD5Hash="MF85mQTLpXhVBe2948K8Ww==").