/*
Copyright (C) 2011 by Julian Lyndon-Smith (julian+mit@dotr.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS for A PARTICULAR PURPOSE and NONINFRINGEMENT. IN no EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE for ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR otherwise, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/


using Progress.Lang.*.
using sports.Model.*.
using Progress.Data.*.

routine-level on error undo, throw.

class sports.Model.TempTable.Base.LocalDefault:
  def protected temp-table TT_Local-Default no-undo serialize-name "LocalDefault" 
  
  		field Country                               as character  serialize-name 'Country'
  		field CurrencySymbol                        as character  serialize-name 'CurrencySymbol'
  		field DateFormat                            as character  serialize-name 'DateFormat'
  		field PostalFormat                          as character  serialize-name 'PostalFormat'
  		field PostalLabel                           as character  serialize-name 'PostalLabel'
  		field Region1Label                          as character  serialize-name 'Region1Label'
  		field Region2Label                          as character  serialize-name 'Region2Label'
  		field TelFormat                             as character  serialize-name 'TelFormat'.
  
  /** take current ActiveRecord and convert to longchar
   *  
   *  gets the current object, converts into a temp-table
   *  and exports the data */ 
  
  method public longchar ExportData(p_Format as char,p_Mode as char,p_LocalDefault as sports.Model.LocalDefault):
    
    def var lv_Data as longchar no-undo.
    
    if p_Mode eq "single" or not valid-handle(p_LocalDefault:query) then 
    do:
      CreateRecord(p_LocalDefault).
    end.
    
    else
    do:
      p_LocalDefault:first().
      
      do while p_LocalDefault:available:
        CreateRecord(p_LocalDefault).
        p_LocalDefault:Next().
      end.
    end.

    temp-table TT_Local-Default:write-json("longchar",lv_Data,yes).

    return lv_Data.
    
    finally:
      empty temp-table TT_Local-Default.
    end finally.
    
  end method. 
   
  method private void CreateRecord(p_LocalDefault as sports.Model.LocalDefault):
    create TT_Local-Default.

    assign TT_Local-Default.Country        = p_LocalDefault:Country
           TT_Local-Default.CurrencySymbol = p_LocalDefault:CurrencySymbol
           TT_Local-Default.DateFormat     = p_LocalDefault:DateFormat
           TT_Local-Default.PostalFormat   = p_LocalDefault:PostalFormat
           TT_Local-Default.PostalLabel    = p_LocalDefault:PostalLabel
           TT_Local-Default.Region1Label   = p_LocalDefault:Region1Label
           TT_Local-Default.Region2Label   = p_LocalDefault:Region2Label
           TT_Local-Default.TelFormat      = p_LocalDefault:TelFormat.
  end method.
  
end class.
@BuildInfo(Source="LocalDefault").
@BuildInfo(Class="sports._Maia.Local-Default").
@BuildInfo(Date="15/04/2013 15:59:28.269+01:00").
@BuildInfo(MD5Hash="SB+IUQpZETt1XxZ3syyKFw==").