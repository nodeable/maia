/*
Copyright (C) 2011 by Julian Lyndon-Smith (julian+mit@dotr.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS for A PARTICULAR PURPOSE and NONINFRINGEMENT. IN no EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE for ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR otherwise, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/


using Progress.Lang.*.
using sports.Model.*.
using Progress.Data.*.

routine-level on error undo, throw.

class sports.Model.Base.Customer abstract :
  def private property DataProvider as dotr.ActiveRecord.Interface.IDataProvider no-undo get . private set.
  
  def private property AvailableFlag as logical no-undo get . set .

  def public property buffer as handle no-undo 
    get():
      return DataProvider:buffer.
    end get . private set.

  def public property query as handle no-undo 
    get():
      return DataProvider:query.
    end get . private set.
  
  def public property rowid as rowid  get. set. /* must be undo, as any error on creation must be reset to ? */
  
  def public property PrivateData  as char no-undo get. set.
  def public property ErrorMessage as char no-undo get. set.
  
  def public property DataFileName as char no-undo get. protected set.
  def public property UpdateType   as char no-undo get. private   set.
  
  def public property SendStompUpdate as logical no-undo init yes get . set .
  def public property InTestMode      as logical no-undo          get . set .
  def public property Preselect			  as logical no-undo          get . set .
  
  def public property Available as logical no-undo
    get():
      return rowid ne ?.
    end get . protected set.
    
  def public property NumRecords as int no-undo
    get():
      return this-object:DataProvider:query:num-results.
    end get . protected set.

  def public property Address    as character no-undo  get .  set .  
  def public property Address2   as character no-undo  get .  set .  
  def public property City       as character no-undo  get .  set .  
  def public property Comments   as character no-undo  get .  set .  
  def public property Contact    as character no-undo  get .  set .  
  def public property Country    as character no-undo init "USA" get .  set .  
  def public property Name       as character no-undo  get .  set .  
  def public property Phone      as character no-undo  get .  set .  
  def public property PostalCode as character no-undo  get .  set .  
  def public property SalesRepID as character no-undo  get .  set .  
  def public property StateID    as character no-undo  get .  set .  
  def public property Terms      as character no-undo init "Net30" get .  set .  
  
  def public property Balance     as decimal no-undo init 0 get .  set .  
  def public property CreditLimit as decimal no-undo init 1500 get .  set .  
  
  def public property CustNum  as integer no-undo init 0 get .  set .  
  def public property Discount as integer no-undo init 0 get .  set .  
  
  def protected property HasValidSalesrep as logical no-undo protected get . private set .
  
  def protected property HasValidState as logical no-undo protected get . private set .
  
  def protected property HasValidInvoices as logical no-undo protected get . private set .
  
  def protected property HasValidOrders as logical no-undo protected get . private set .
  
  def public property Salesrep as sports.Model.Salesrep no-undo
    get():
      if not HasValidSalesrep then 
      do:
        assign Salesrep         = (new sports.Model.Salesrep()):FindUsingSalesRep(this-object:SalesRepID)
               HasValidSalesrep = yes.
      end.
      return Salesrep.
    end get . private set .
  
  def public property State as sports.Model.State no-undo
    get():
      if not HasValidState then 
      do:
        assign State         = (new sports.Model.State()):FindUsingState(this-object:StateID)
               HasValidState = yes.
      end.
      return State.
    end get . private set .
  
  def public property Invoices as sports.Model.Invoice no-undo
    get():
      if not HasValidInvoices then 
      do:
        assign Invoices         = (new sports.Model.Invoice()):FindUsingCustNum(this-object:CustNum)
               HasValidInvoices = yes.
      end.
      return Invoices.
    end get . private set .
  
  def public property Orders as sports.Model.Order no-undo
    get():
      if not HasValidOrders then 
      do:
        assign Orders         = (new sports.Model.Order()):FindUsingCustNum(this-object:CustNum)
               HasValidOrders = yes.
      end.
      return Orders.
    end get . private set .
  
  constructor Customer():
    StartUp().
  end constructor.
  
  destructor Customer():
    if valid-object(this-object:DataProvider) then delete object this-object:DataProvider.
  
    
  end destructor.
  
  /** bind a progress data binding source to the query
   * @param binding source to use
   * @return return value
   */

  method public void Bind(p_BindingSource as Progress.Data.BindingSource):
    p_BindingSource:handle = this-object:DataProvider:query.
  end method.  

  /** Creates the buffer handle and buffer fields 
   */

  method protected void StartUp():
    if valid-handle(dotr.ActiveRecord.Partition:AppServer) and dotr.ActiveRecord.Partition:AppServer ne session:handle  
       then this-object:DataProvider = new dotr.ActiveRecord.ServiceProvider("Customer").
       else this-object:DataProvider = new dotr.ActiveRecord.DataAccess("Customer").    
  end method.
  
  
    /** gets the Customer matching the unique ID 
     *  @param p_CustomerID : the id to find
     */
  
    method public sports.Model.Customer find(p_CustomerID as integer):
      this-object:DataProvider:FetchUnique(substitute("where _Customer.Cust-Num eq &1",quoter(p_CustomerID))).
      
      this-object:Get().
      
      if not this-object:available then assign this-object:CustNum = p_CustomerID.
      return cast(this-object,sports.Model.Customer). 
    end.
    
    /** gets the Customer using the rowid 
     *  @param p_CustomerRowID : the rowid to find
     */
  
    method public sports.Model.Customer find(p_CustomerRowID as rowid):
      this-object:DataProvider:FetchUnique(p_CustomerRowID).
      return this-object:Get().  
    end.
   

  /** stub method to allow for override
   */

  method public void AfterGet():
  end method.

 /** stub method to allow for override
   */

  method public void BeforeGet():
  end method.

 /** stub method to allow for override
  */

  method public void BeforeCreate():
  end method.
  
 /** stub method to allow for override
  */

  method public void AfterCreate():
  end method.

 /** stub method to allow for override
  */

  method public void BeforeUpdate():
  end method.
  
 /** stub method to allow for override
  */

  method public void AfterUpdate():
  end method.
  
 /** stub method to allow for override
  */

  method public void BeforeRemove():
  end method.
  
 /** stub method to allow for override
  */

  method public void AfterRemove():
  end method.

 /** stub method to allow for override
  */
  method public void NotFound():
  end method.

  /** stub method to allow for override
   */

  method public void Imported():
    this-object:save().
  end method.

  /** stub method to allow for override
  */
  method public void AfterTransaction():
  end method.
  
  /** assigns the buffer fields to the object
   *  @return sports.Model.Customer  
   */

  method protected sports.Model.Customer get():
    BeforeGet().
    assign this-object:rowid         = ?
           this-object:UpdateType    = "find".
        
    if not this-object:buffer:available then
    do: 
      NotFound().
      return cast(this-object,sports.Model.Customer). 
    end.

    assign this-object:Address      = buffer::Address
           this-object:Address2     = buffer::Address2
           this-object:Balance      = buffer::Balance
           this-object:City         = buffer::City
           this-object:Comments     = buffer::Comments
           this-object:Contact      = buffer::Contact
           this-object:Country      = buffer::Country
           this-object:CreditLimit = buffer::Credit-Limit
           this-object:CustNum     = buffer::Cust-Num
           this-object:Discount     = buffer::Discount
           this-object:Name         = buffer::Name
           this-object:Phone        = buffer::Phone
           this-object:PostalCode  = buffer::Postal-Code
           this-object:SalesRepID    = buffer::Sales-Rep
           this-object:StateID        = buffer::State
           this-object:Terms        = buffer::Terms
           this-object:rowid        = buffer:rowid.
    
    if HasValidSalesrep then 
    do:
      delete object Salesrep no-error.
      assign HasValidSalesrep = no.
    end.   
    
    if HasValidState then 
    do:
      delete object State no-error.
      assign HasValidState = no.
    end.   
    
    if HasValidInvoices then 
    do:
      delete object Invoices no-error.
      assign HasValidInvoices = no.
    end.   
    
    if HasValidOrders then 
    do:
      delete object Orders no-error.
      assign HasValidOrders = no.
    end.   
           
    AfterGet().
    
    return cast(this-object,sports.Model.Customer). 
  end.
  
  /** open a query for all records in the <<Table>> table
   */
  method public sports.Model.Customer all():
    return this-object:FindWhere("").
  end method.

  /** opens a query for records matching the p_Where clause
   *  @param p_Where : the where statement to use  
   *  @return sports.Model.Customer  
   */
  
  method public sports.Model.Customer FindWhere (p_Where as char):
    return FindWhere(p_Where,"").
  end method.

  /** opens a query for records matching the p_Where clause
   *  @param p_Where : the where statement to use  
   *  @param p_Sort : the sort statement to use  
   *  @return sports.Model.Customer  
   */
  
  method public sports.Model.Customer FindWhere (p_Where as char,p_Sort as char):
    def var lv_Query as char no-undo.
    
    assign lv_Query = substitute("&3 each _Customer &1 no-lock &2",
                                                if trim(p_Where) begins "where " 
                                                   then p_Where
                                                   else if p_Where ne "" then substitute("where &1",p_Where)
                                                   else "",
                                                p_Sort,
                                                if this-object:preselect then "preselect" else "for").
    this-object:DataProvider:FetchData(lv_query).
    
    return this-object:First().                                                        
  end method.
  
	/** get next record, return available status
		* @return record available 
		*/  
  method public logical NextAvailable():
    if this-object:AvailableFlag then this-object:Next().
    assign this-object:AvailableFlag = yes.
    return this-object:available.
  end method.
  
  /** opens a query for records matching the p_Where clause
   *  @param p_Where : the where statement to use  
   *  @return sports.Model.Customer  
   */
  
  method public sports.Model.Customer preselect (p_Where as char):
    return preselect(p_Where,"").
  end method.

  /** opens a query for records matching the p_Where clause
   *  @param p_Where : the where statement to use  
   *  @param p_Sort : the sort statement to use  
   *  @return sports.Model.Customer  
   */
  
  method public sports.Model.Customer preselect (p_Where as char,p_Sort as char):
    def var lv_Query as char no-undo.

    assign lv_query = substitute("preselect each _Customer &1 no-lock &2",
                                                if trim(p_Where) begins "where " 
                                                   then p_Where
                                                   else if p_Where ne "" then substitute("where &1",p_Where)
                                                   else "",
                                                   p_Sort).

    this-object:DataProvider:FetchData(lv_Query).
    
    return this-object:First().                                                        
  end method.
  
  /** gets the first Customer of the query
   *  @return sports.Model.Customer  
   */

  method public sports.Model.Customer first():
    if this-object:DataProvider:query:is-open then this-object:DataProvider:query:get-first().
    assign this-object:AvailableFlag = no.
    return this-object:Get().
  end method.
    
  /** gets the last Customer of the query
   *  @return sports.Model.Customer  
   */

  method public sports.Model.Customer last():
    if this-object:DataProvider:query:is-open then this-object:DataProvider:query:get-last().
    return this-object:Get().
  end method.

  /** gets the next Customer of the query
   *  @return sports.Model.Customer  
   */

  method public sports.Model.Customer next():
    if this-object:DataProvider:query:is-open then this-object:DataProvider:query:get-next().
    return this-object:Get().
  end method.

  /** gets the prev Customer of the query
   *  @return sports.Model.Customer  
   */

  method public sports.Model.Customer prev():
    if this-object:DataProvider:query:is-open then this-object:DataProvider:query:get-prev().
    return this-object:Get().
  end method.

  /** saves the object attributes to the database
   *  @return sports.Model.Customer  
   */
  
  method public sports.Model.Customer save():
    WriteRecord(if this-object:rowid eq ? then "CREATE" else "UPDATE").
    return cast(this-object,sports.Model.Customer). 
  end method.

  /** removes the object from the database
   */
 
  method public void Remove():
    if not this-object:available then return.
    WriteRecord("Remove").
    return. 
  end method. 

  /** writes the approriate changes to the database
   *  @param p_Mode : create (creates a new <<table>>
   *                : update (saves the changes to the database)
   *                : remove (removes the record from the database)
   */
    
  method protected final void WriteRecord(p_Mode as char):
    def var lv_new as logical no-undo.
    
    def var hUpdateBuffer as handle no-undo.
    
    create buffer hUpdateBuffer for table this-object:buffer 
                                    buffer-name this-object:buffer:name.
    
    assign lv_new                 = this-object:rowid eq ?
           this-object:UpdateType = p_mode.
    
    
    /** try to lock the record, give up after 5 seconds */
    repeat while true stop-after 5 transaction on error undo, throw
                                               on stop undo, leave:

      if p_mode eq "CREATE" then 
      do:
        BeforeCreate(). /** override this method if you want to do stuff before the create statement */ 

        hUpdateBuffer:buffer-create().
        
        
        
        
        assign this-object:rowid = hUpdateBuffer:rowid.
               
        AfterCreate().  /** override this method if you want to do stuff after the create statement */
      end.
      
      else 
      do:
        hUpdateBuffer:find-by-rowid(this-object:rowid,exclusive-lock,no-wait) no-error.
   
        if hUpdateBuffer:locked then next.
        if not hUpdateBuffer:avail then undo, throw new AppError(substitute("Could not find Customer [&1]",this-object:rowid),0).
      end.
      
      case p_Mode:
        when "Update" or when "Create" then
        do:
          BeforeUpdate().  /** override this method if you want to do stuff before the update statement */
          Validate().
          assign hUpdateBuffer::Address      = this-object:Address
                 hUpdateBuffer::Address2     = this-object:Address2
                 hUpdateBuffer::Balance      = this-object:Balance
                 hUpdateBuffer::City         = this-object:City
                 hUpdateBuffer::Comments     = this-object:Comments
                 hUpdateBuffer::Contact      = this-object:Contact
                 hUpdateBuffer::Country      = this-object:Country
                 hUpdateBuffer::Credit-Limit = this-object:CreditLimit
                 hUpdateBuffer::Cust-Num     = this-object:CustNum
                 hUpdateBuffer::Discount     = this-object:Discount
                 hUpdateBuffer::Name         = this-object:Name
                 hUpdateBuffer::Phone        = this-object:Phone
                 hUpdateBuffer::Postal-Code  = this-object:PostalCode
                 hUpdateBuffer::Sales-Rep    = this-object:SalesRepID
                 hUpdateBuffer::State        = this-object:StateID
                 hUpdateBuffer::Terms        = this-object:Terms.
          AfterUpdate().  /** override this method if you want to do stuff after the update statement */
        end.
        
        when "Remove" then
        do:
          BeforeRemove().  /** override this method if you want to do stuff before the remove statement */
          hUpdateBuffer:buffer-delete().
          assign this-object:rowid = ?.
          /** clear all sports.Model.Invoice with a foreign key matching this object */ 
          
          Invoices  = (new sports.Model.Invoice()):FindUsingCustNum(this-object:CustNum).
            
          do while Invoices:Available:
            Invoices:RemoveCustomer().
            Invoices:next().
          end.
          
          /** clear all sports.Model.Order with a foreign key matching this object */ 
          
          Orders  = (new sports.Model.Order()):FindUsingCustNum(this-object:CustNum).
            
          do while Orders:Available:
            Orders:RemoveCustomer().
            Orders:next().
          end.
          AfterRemove().  /** override this method if you want to do stuff after the remove statement */
        end.
        
      end case.
      
      
      hUpdateBuffer:buffer-release().
      
      leave.
      
      catch e as Progress.Lang.Error :
        case e:GetMessageNum(1):
          when 132 then undo, throw new AppError(e:getMessage(1),132).
          otherwise     undo, throw new AppError(e:GetMessage(1),0).
        end case.
      end catch.
    end.
    
    AfterTransaction(). /** override this method if you want to do stuff after the (sub)transaction has ended */
    
    this-object:DataProvider:SaveData(). /** does nothing if local, sends to appserver if not */
    
    return.
    
    finally:
      delete object hUpdateBuffer.
    end finally.
    
  end method.
  
  /** Export ActiveRecord as Json */ 
  method public longchar Export():
    return ExportData("json","single").
  end method.

  /** Export ActiveRecord as Json */ 
  method public longchar ExportQuery():
    return ExportData("json","all").
  end method.
  
  /** take current ActiveRecord and convert to longchar
   *  
   *  gets the current object, converts into a temp-table
   *  and exports the data */ 
  
  method private longchar ExportData(p_format as char,p_Mode as char):
    
    return (new sports.Model.TempTable.Customer()):ExportData(p_format,p_mode,cast(this-object,sports.Model.Customer)).
    
  end method. 
  
  /** take a json string and assign properties to Customer
   *  
   */ 
  
  method public void Import(p_json as longchar):
    this-object:import("json",p_json).
  end method.
  
  /** take a json string and assign properties to Customer
   *  
   */ 
  
  method public void Import(p_format as char,p_json as longchar):
    def var tttablehandle as handle no-undo.
    def var tablehandle   as handle no-undo.
    def var tablequery    as handle no-undo.
    def var fieldhandle   as handle no-undo.
    
    def var lv_i as int no-undo.
    
    create temp-table tttablehandle.
    
    tttablehandle:read-json("longchar",p_Json,"empty").
    
    assign tablehandle = tttablehandle:default-buffer-handle.
    
    create query tablequery.
    
    tablequery:add-buffer (tablehandle).

    tablequery:query-prepare(substitute("for each &1 no-lock",tablehandle:name)).
    tablequery:query-open().
    tablequery:get-first().
    
    do while not tablequery:query-off-end on error undo, throw:
      this-object:Find(integer(tablehandle::CustNum)).
      
      do lv_i = 1 to tablehandle:num-fields:
        assign fieldhandle = tablehandle:buffer-field(lv_i).
        
        case fieldhandle:name:
          when 'Address'     then assign this-object:Address     = fieldhandle:buffer-value.
          when 'Address2'    then assign this-object:Address2    = fieldhandle:buffer-value.
          when 'Balance'     then assign this-object:Balance     = decimal(fieldhandle:buffer-value).
          when 'City'        then assign this-object:City        = fieldhandle:buffer-value.
          when 'Comments'    then assign this-object:Comments    = fieldhandle:buffer-value.
          when 'Contact'     then assign this-object:Contact     = fieldhandle:buffer-value.
          when 'Country'     then assign this-object:Country     = fieldhandle:buffer-value.
          when 'CreditLimit' then assign this-object:CreditLimit = decimal(fieldhandle:buffer-value).
          when 'CustNum'     then assign this-object:CustNum     = integer(fieldhandle:buffer-value).
          when 'Discount'    then assign this-object:Discount    = integer(fieldhandle:buffer-value).
          when 'Name'        then assign this-object:Name        = fieldhandle:buffer-value.
          when 'Phone'       then assign this-object:Phone       = fieldhandle:buffer-value.
          when 'PostalCode'  then assign this-object:PostalCode  = fieldhandle:buffer-value.
          when 'SalesRepID'  then assign this-object:SalesRepID  = fieldhandle:buffer-value.
          when 'StateID'     then assign this-object:StateID     = fieldhandle:buffer-value.
          when 'Terms'       then assign this-object:Terms       = fieldhandle:buffer-value.
        end case.
        
      end.
      
      this-object:Imported().
      
      finally:
        tablequery:get-next().
      end finally.
    end.
    
    finally:
      delete object tablequery.
      delete object tttablehandle.
    end finally.
    
  end method.
  
	/** Load db into model
		* @param 
		* @return 
		*/     
		
  method public void AppServerLoad():
    this-object:all().
  end method.

  /** Load db into model
    * @param 
    * @return 
    */     
    
  method public void AppServerSave():
  end method.
  		
  
  /** gets a Customer using FindContainingComments
   *
   * @param  p_Comments - character
   * @return sports.Model.Customer
   */
  method public sports.Model.Customer FindContainingComments( p_Comments as character):
    return FindContainingComments( p_Comments,"").
  end method.
  
  method public sports.Model.Customer FindContainingComments( p_Comments as character,p_Sort as char):
    FindWhere(substitute("where _Customer.Comments contains &1 ",quoter(p_Comments)),p_Sort).
    
    if not this-object:available then
    do:
      assign this-object:Comments = p_Comments.
    end.
    
    return cast(this-object,sports.Model.Customer).
  end method.
  
  /** gets a Customer using FindUsingCountry
   *
   * @param  p_Country - character
   * @return sports.Model.Customer
   */
  method public sports.Model.Customer FindUsingCountry( p_Country as character):
    return FindUsingCountry( p_Country,"").
  end method.
  
  method public sports.Model.Customer FindUsingCountry( p_Country as character,p_Sort as char):
    FindWhere(substitute("where _Customer.Country eq &1 ",quoter(p_Country)),p_Sort).
    
    if not this-object:available then
    do:
      assign this-object:Country = p_Country.
    end.
    
    return cast(this-object,sports.Model.Customer).
  end method.
  
  /** gets a Customer using FindUsingCountryPostalCode
   *
   * @param  p_Country - character, p_PostalCode - character
   * @return sports.Model.Customer
   */
  method public sports.Model.Customer FindUsingCountryPostalCode( p_Country as character, p_PostalCode as character):
    return FindUsingCountryPostalCode( p_Country, p_PostalCode,"").
  end method.
  
  method public sports.Model.Customer FindUsingCountryPostalCode( p_Country as character, p_PostalCode as character,p_Sort as char):
    FindWhere(substitute("where _Customer.Country eq &1  and _Customer.Postal-Code eq &2 ",quoter(p_Country),quoter(p_PostalCode)),p_Sort).
    
    if not this-object:available then
    do:
      assign this-object:Country    = p_Country
             this-object:PostalCode = p_PostalCode.
    end.
    
    return cast(this-object,sports.Model.Customer).
  end method.
  
  /** gets a Customer using FindUsingCustNum
   *
   * @param  p_CustNum - integer
   * @return sports.Model.Customer
   */
  method public sports.Model.Customer FindUsingCustNum( p_CustNum as integer):
    return FindUsingCustNum( p_CustNum,"").
  end method.
  
  method public sports.Model.Customer FindUsingCustNum( p_CustNum as integer,p_Sort as char):
    FindWhere(substitute("where _Customer.Cust-Num eq &1 ",quoter(p_CustNum)),p_Sort).
    
    if not this-object:available then
    do:
      assign this-object:CustNum = p_CustNum.
    end.
    
    return cast(this-object,sports.Model.Customer).
  end method.
  
  /** gets a Customer using FindUsingName
   *
   * @param  p_Name - character
   * @return sports.Model.Customer
   */
  method public sports.Model.Customer FindUsingName( p_Name as character):
    return FindUsingName( p_Name,"").
  end method.
  
  method public sports.Model.Customer FindUsingName( p_Name as character,p_Sort as char):
    FindWhere(substitute("where _Customer.Name eq &1 ",quoter(p_Name)),p_Sort).
    
    if not this-object:available then
    do:
      assign this-object:Name = p_Name.
    end.
    
    return cast(this-object,sports.Model.Customer).
  end method.
  
  /** gets a Customer using FindUsingSalesRepID
   *
   * @param  p_SalesRepID - character
   * @return sports.Model.Customer
   */
  method public sports.Model.Customer FindUsingSalesRepID( p_SalesRepID as character):
    return FindUsingSalesRepID( p_SalesRepID,"").
  end method.
  
  method public sports.Model.Customer FindUsingSalesRepID( p_SalesRepID as character,p_Sort as char):
    FindWhere(substitute("where _Customer.Sales-Rep eq &1 ",quoter(p_SalesRepID)),p_Sort).
    
    if not this-object:available then
    do:
      assign this-object:SalesRepID = p_SalesRepID.
    end.
    
    return cast(this-object,sports.Model.Customer).
  end method.
  
  /** gets a Customer using FindUsingStateID
   *
   * @param  p_StateID - character
   * @return sports.Model.Customer
   */
  method public sports.Model.Customer FindUsingStateID( p_StateID as character):
    return FindUsingStateID( p_StateID,"").
  end method.
  
  method public sports.Model.Customer FindUsingStateID( p_StateID as character,p_Sort as char):
    FindWhere(substitute("where _Customer.State eq &1 ",quoter(p_StateID)),p_Sort).
    
    if not this-object:available then
    do:
      assign this-object:StateID = p_StateID.
    end.
    
    return cast(this-object,sports.Model.Customer).
  end method.
  
  method public void Validate():
  
  	if length(this-object:Name) gt 50 then undo, throw new AppError("Name must be less than 50 characters",0).
  	if length(this-object:Name) lt 5 then undo, throw new AppError("Name must be at least 5 characters ",0).
  	if this-object:Name eq "" then undo, throw new AppError("Name cannot be blank",0).
  	if this-object:Name eq ? then undo, throw new AppError("Name cannot be unknown",0).
  
  end method.
  
  /** nullifies foreign key
   *
   */
  
  method public void RemoveSalesrep():
    assign this-object:SalesRepID = ?
           this-object:HasValidSalesrep = no.
    
    this-object:save().
  end method.
  
  /** nullifies foreign key
   *
   */
  
  method public void RemoveState():
    assign this-object:StateID = ?
           this-object:HasValidState = no.
    
    this-object:save().
  end method.

end class.
@BuildInfo(Source="Customer").
@BuildInfo(Class="sports._Maia.Customer").
@BuildInfo(Date="10/09/2013 10:56:04.124+01:00").
@BuildInfo(MD5Hash="dhr3fPwk7rIOmjCRZrz6CQ==").