/*
Copyright (C) 2011 by Julian Lyndon-Smith (julian+mit@dotr.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS for A PARTICULAR PURPOSE and NONINFRINGEMENT. IN no EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE for ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR otherwise, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/


using Progress.Lang.*.
using sports.Model.*.
using Progress.Data.*.

routine-level on error undo, throw.

class sports.Model.Base.RefCall abstract :
  def private property DataProvider as dotr.ActiveRecord.Interface.IDataProvider no-undo get . private set.
  
  def private property AvailableFlag as logical no-undo get . set .

  def public property buffer as handle no-undo 
    get():
      return DataProvider:buffer.
    end get . private set.

  def public property query as handle no-undo 
    get():
      return DataProvider:query.
    end get . private set.
  
  def public property rowid as rowid  get. set. /* must be undo, as any error on creation must be reset to ? */
  
  def public property PrivateData  as char no-undo get. set.
  def public property ErrorMessage as char no-undo get. set.
  
  def public property DataFileName as char no-undo get. protected set.
  def public property UpdateType   as char no-undo get. private   set.
  
  def public property SendStompUpdate as logical no-undo init yes get . set .
  def public property InTestMode      as logical no-undo          get . set .
  def public property Preselect			  as logical no-undo          get . set .
  
  def public property Available as logical no-undo
    get():
      return rowid ne ?.
    end get . protected set.
    
  def public property NumRecords as int no-undo
    get():
      return this-object:DataProvider:query:num-results.
    end get . protected set.

  def public property CallNum  as character no-undo  get .  set .  
  def public property Parent   as character no-undo  get .  set .  
  def public property SalesRep as character no-undo  get .  set .  
  def public property Txt      as character no-undo  get .  set .  
  
  def public property CallDate as date no-undo  get .  set .  
  
  def public property CustNum as integer no-undo  get .  set .  
  
  
  
  constructor RefCall():
    StartUp().
  end constructor.
  
  destructor RefCall():
    if valid-object(this-object:DataProvider) then delete object this-object:DataProvider.
  
    
  end destructor.
  
  /** bind a progress data binding source to the query
   * @param binding source to use
   * @return return value
   */

  method public void Bind(p_BindingSource as Progress.Data.BindingSource):
    p_BindingSource:handle = this-object:DataProvider:query.
  end method.  

  /** Creates the buffer handle and buffer fields 
   */

  method protected void StartUp():
    if valid-handle(dotr.ActiveRecord.Partition:AppServer) and dotr.ActiveRecord.Partition:AppServer ne session:handle  
       then this-object:DataProvider = new dotr.ActiveRecord.ServiceProvider("Ref-Call").
       else this-object:DataProvider = new dotr.ActiveRecord.DataAccess("Ref-Call").    
  end method.
  
  
    /** gets the RefCall matching the unique ID 
     *  @param p_RefCallID : the id to find
     */
  
    method public sports.Model.RefCall find(p_RefCallID as character):
      this-object:DataProvider:FetchUnique(substitute("where _Ref-Call.Call-Num eq &1",quoter(p_RefCallID))).
      
      this-object:Get().
      
      if not this-object:available then assign this-object:CallNum = p_RefCallID.
      return cast(this-object,sports.Model.RefCall). 
    end.
    
    /** gets the RefCall using the rowid 
     *  @param p_RefCallRowID : the rowid to find
     */
  
    method public sports.Model.RefCall find(p_RefCallRowID as rowid):
      this-object:DataProvider:FetchUnique(p_RefCallRowID).
      return this-object:Get().  
    end.
   

  /** stub method to allow for override
   */

  method public void AfterGet():
  end method.

 /** stub method to allow for override
   */

  method public void BeforeGet():
  end method.

 /** stub method to allow for override
  */

  method public void BeforeCreate():
  end method.
  
 /** stub method to allow for override
  */

  method public void AfterCreate():
  end method.

 /** stub method to allow for override
  */

  method public void BeforeUpdate():
  end method.
  
 /** stub method to allow for override
  */

  method public void AfterUpdate():
  end method.
  
 /** stub method to allow for override
  */

  method public void BeforeRemove():
  end method.
  
 /** stub method to allow for override
  */

  method public void AfterRemove():
  end method.

 /** stub method to allow for override
  */
  method public void NotFound():
  end method.

  /** stub method to allow for override
   */

  method public void Imported():
    this-object:save().
  end method.

  /** stub method to allow for override
  */
  method public void AfterTransaction():
  end method.
  
  /** assigns the buffer fields to the object
   *  @return sports.Model.RefCall  
   */

  method protected sports.Model.RefCall get():
    BeforeGet().
    assign this-object:rowid         = ?
           this-object:UpdateType    = "find".
        
    if not this-object:buffer:available then
    do: 
      NotFound().
      return cast(this-object,sports.Model.RefCall). 
    end.

    assign this-object:CallDate = buffer::Call-Date
           this-object:CallNum  = buffer::Call-Num
           this-object:CustNum  = buffer::Cust-Num
           this-object:Parent    = buffer::Parent
           this-object:SalesRep = buffer::Sales-Rep
           this-object:Txt       = buffer::Txt
           this-object:rowid     = buffer:rowid.
    
    
           
    AfterGet().
    
    return cast(this-object,sports.Model.RefCall). 
  end.
  
  /** open a query for all records in the <<Table>> table
   */
  method public sports.Model.RefCall all():
    return this-object:FindWhere("").
  end method.

  /** opens a query for records matching the p_Where clause
   *  @param p_Where : the where statement to use  
   *  @return sports.Model.RefCall  
   */
  
  method public sports.Model.RefCall FindWhere (p_Where as char):
    return FindWhere(p_Where,"").
  end method.

  /** opens a query for records matching the p_Where clause
   *  @param p_Where : the where statement to use  
   *  @param p_Sort : the sort statement to use  
   *  @return sports.Model.RefCall  
   */
  
  method public sports.Model.RefCall FindWhere (p_Where as char,p_Sort as char):
    def var lv_Query as char no-undo.
    
    assign lv_Query = substitute("&3 each _Ref-Call &1 no-lock &2",
                                                if trim(p_Where) begins "where " 
                                                   then p_Where
                                                   else if p_Where ne "" then substitute("where &1",p_Where)
                                                   else "",
                                                p_Sort,
                                                if this-object:preselect then "preselect" else "for").
    this-object:DataProvider:FetchData(lv_query).
    
    return this-object:First().                                                        
  end method.
  
	/** get next record, return available status
		* @return record available 
		*/  
  method public logical NextAvailable():
    if this-object:AvailableFlag then this-object:Next().
    assign this-object:AvailableFlag = yes.
    return this-object:available.
  end method.
  
  /** opens a query for records matching the p_Where clause
   *  @param p_Where : the where statement to use  
   *  @return sports.Model.RefCall  
   */
  
  method public sports.Model.RefCall preselect (p_Where as char):
    return preselect(p_Where,"").
  end method.

  /** opens a query for records matching the p_Where clause
   *  @param p_Where : the where statement to use  
   *  @param p_Sort : the sort statement to use  
   *  @return sports.Model.RefCall  
   */
  
  method public sports.Model.RefCall preselect (p_Where as char,p_Sort as char):
    def var lv_Query as char no-undo.

    assign lv_query = substitute("preselect each _Ref-Call &1 no-lock &2",
                                                if trim(p_Where) begins "where " 
                                                   then p_Where
                                                   else if p_Where ne "" then substitute("where &1",p_Where)
                                                   else "",
                                                   p_Sort).

    this-object:DataProvider:FetchData(lv_Query).
    
    return this-object:First().                                                        
  end method.
  
  /** gets the first RefCall of the query
   *  @return sports.Model.RefCall  
   */

  method public sports.Model.RefCall first():
    if this-object:DataProvider:query:is-open then this-object:DataProvider:query:get-first().
    assign this-object:AvailableFlag = no.
    return this-object:Get().
  end method.
    
  /** gets the last RefCall of the query
   *  @return sports.Model.RefCall  
   */

  method public sports.Model.RefCall last():
    if this-object:DataProvider:query:is-open then this-object:DataProvider:query:get-last().
    return this-object:Get().
  end method.

  /** gets the next RefCall of the query
   *  @return sports.Model.RefCall  
   */

  method public sports.Model.RefCall next():
    if this-object:DataProvider:query:is-open then this-object:DataProvider:query:get-next().
    return this-object:Get().
  end method.

  /** gets the prev RefCall of the query
   *  @return sports.Model.RefCall  
   */

  method public sports.Model.RefCall prev():
    if this-object:DataProvider:query:is-open then this-object:DataProvider:query:get-prev().
    return this-object:Get().
  end method.

  /** saves the object attributes to the database
   *  @return sports.Model.RefCall  
   */
  
  method public sports.Model.RefCall save():
    WriteRecord(if this-object:rowid eq ? then "CREATE" else "UPDATE").
    return cast(this-object,sports.Model.RefCall). 
  end method.

  /** removes the object from the database
   */
 
  method public void Remove():
    if not this-object:available then return.
    WriteRecord("Remove").
    return. 
  end method. 

  /** writes the approriate changes to the database
   *  @param p_Mode : create (creates a new <<table>>
   *                : update (saves the changes to the database)
   *                : remove (removes the record from the database)
   */
    
  method protected final void WriteRecord(p_Mode as char):
    def var lv_new as logical no-undo.
    
    def var hUpdateBuffer as handle no-undo.
    
    create buffer hUpdateBuffer for table this-object:buffer 
                                    buffer-name this-object:buffer:name.
    
    assign lv_new                 = this-object:rowid eq ?
           this-object:UpdateType = p_mode.
    
    
    /** try to lock the record, give up after 5 seconds */
    repeat while true stop-after 5 transaction on error undo, throw
                                               on stop undo, leave:

      if p_mode eq "CREATE" then 
      do:
        BeforeCreate(). /** override this method if you want to do stuff before the create statement */ 

        hUpdateBuffer:buffer-create().
        
        
        
        
        assign this-object:rowid = hUpdateBuffer:rowid.
               
        AfterCreate().  /** override this method if you want to do stuff after the create statement */
      end.
      
      else 
      do:
        hUpdateBuffer:find-by-rowid(this-object:rowid,exclusive-lock,no-wait) no-error.
   
        if hUpdateBuffer:locked then next.
        if not hUpdateBuffer:avail then undo, throw new AppError(substitute("Could not find Ref-Call [&1]",this-object:rowid),0).
      end.
      
      case p_Mode:
        when "Update" or when "Create" then
        do:
          BeforeUpdate().  /** override this method if you want to do stuff before the update statement */
          Validate().
          assign hUpdateBuffer::Call-Date = this-object:CallDate
                 hUpdateBuffer::Call-Num  = this-object:CallNum
                 hUpdateBuffer::Cust-Num  = this-object:CustNum
                 hUpdateBuffer::Parent    = this-object:Parent
                 hUpdateBuffer::Sales-Rep = this-object:SalesRep
                 hUpdateBuffer::Txt       = this-object:Txt.
          AfterUpdate().  /** override this method if you want to do stuff after the update statement */
        end.
        
        when "Remove" then
        do:
          BeforeRemove().  /** override this method if you want to do stuff before the remove statement */
          hUpdateBuffer:buffer-delete().
          assign this-object:rowid = ?.
          
          AfterRemove().  /** override this method if you want to do stuff after the remove statement */
        end.
        
      end case.
      
      
      hUpdateBuffer:buffer-release().
      
      leave.
      
      catch e as Progress.Lang.Error :
        case e:GetMessageNum(1):
          when 132 then undo, throw new AppError(e:getMessage(1),132).
          otherwise     undo, throw new AppError(e:GetMessage(1),0).
        end case.
      end catch.
    end.
    
    AfterTransaction(). /** override this method if you want to do stuff after the (sub)transaction has ended */
    
    this-object:DataProvider:SaveData(). /** does nothing if local, sends to appserver if not */
    
    return.
    
    finally:
      delete object hUpdateBuffer.
    end finally.
    
  end method.
  
  /** Export ActiveRecord as Json */ 
  method public longchar Export():
    return ExportData("json","single").
  end method.

  /** Export ActiveRecord as Json */ 
  method public longchar ExportQuery():
    return ExportData("json","all").
  end method.
  
  /** take current ActiveRecord and convert to longchar
   *  
   *  gets the current object, converts into a temp-table
   *  and exports the data */ 
  
  method private longchar ExportData(p_format as char,p_Mode as char):
    
    return (new sports.Model.TempTable.RefCall()):ExportData(p_format,p_mode,cast(this-object,sports.Model.RefCall)).
    
  end method. 
  
  /** take a json string and assign properties to RefCall
   *  
   */ 
  
  method public void Import(p_json as longchar):
    this-object:import("json",p_json).
  end method.
  
  /** take a json string and assign properties to RefCall
   *  
   */ 
  
  method public void Import(p_format as char,p_json as longchar):
    def var tttablehandle as handle no-undo.
    def var tablehandle   as handle no-undo.
    def var tablequery    as handle no-undo.
    def var fieldhandle   as handle no-undo.
    
    def var lv_i as int no-undo.
    
    create temp-table tttablehandle.
    
    tttablehandle:read-json("longchar",p_Json,"empty").
    
    assign tablehandle = tttablehandle:default-buffer-handle.
    
    create query tablequery.
    
    tablequery:add-buffer (tablehandle).

    tablequery:query-prepare(substitute("for each &1 no-lock",tablehandle:name)).
    tablequery:query-open().
    tablequery:get-first().
    
    do while not tablequery:query-off-end on error undo, throw:
      this-object:Find(tablehandle::CallNum).
      
      do lv_i = 1 to tablehandle:num-fields:
        assign fieldhandle = tablehandle:buffer-field(lv_i).
        
        case fieldhandle:name:
          when 'CallDate' then assign this-object:CallDate = date(fieldhandle:buffer-value).
          when 'CallNum'  then assign this-object:CallNum  = fieldhandle:buffer-value.
          when 'CustNum'  then assign this-object:CustNum  = integer(fieldhandle:buffer-value).
          when 'Parent'   then assign this-object:Parent   = fieldhandle:buffer-value.
          when 'SalesRep' then assign this-object:SalesRep = fieldhandle:buffer-value.
          when 'Txt'      then assign this-object:Txt      = fieldhandle:buffer-value.
        end case.
        
      end.
      
      this-object:Imported().
      
      finally:
        tablequery:get-next().
      end finally.
    end.
    
    finally:
      delete object tablequery.
      delete object tttablehandle.
    end finally.
    
  end method.
  
	/** Load db into model
		* @param 
		* @return 
		*/     
		
  method public void AppServerLoad():
    this-object:all().
  end method.

  /** Load db into model
    * @param 
    * @return 
    */     
    
  method public void AppServerSave():
  end method.
  		
  
  /** gets a RefCall using FindContainingTxt
   *
   * @param  p_Txt - character
   * @return sports.Model.RefCall
   */
  method public sports.Model.RefCall FindContainingTxt( p_Txt as character):
    return FindContainingTxt( p_Txt,"").
  end method.
  
  method public sports.Model.RefCall FindContainingTxt( p_Txt as character,p_Sort as char):
    FindWhere(substitute("where _Ref-Call.Txt contains &1 ",quoter(p_Txt)),p_Sort).
    
    if not this-object:available then
    do:
      assign this-object:Txt = p_Txt.
    end.
    
    return cast(this-object,sports.Model.RefCall).
  end method.
  
  /** gets a RefCall using FindUsingCallNum
   *
   * @param  p_CallNum - character
   * @return sports.Model.RefCall
   */
  method public sports.Model.RefCall FindUsingCallNum( p_CallNum as character):
    return FindUsingCallNum( p_CallNum,"").
  end method.
  
  method public sports.Model.RefCall FindUsingCallNum( p_CallNum as character,p_Sort as char):
    FindWhere(substitute("where _Ref-Call.Call-Num eq &1 ",quoter(p_CallNum)),p_Sort).
    
    if not this-object:available then
    do:
      assign this-object:CallNum = p_CallNum.
    end.
    
    return cast(this-object,sports.Model.RefCall).
  end method.
  
  /** gets a RefCall using FindUsingCustNum
   *
   * @param  p_CustNum - integer
   * @return sports.Model.RefCall
   */
  method public sports.Model.RefCall FindUsingCustNum( p_CustNum as integer):
    return FindUsingCustNum( p_CustNum,"").
  end method.
  
  method public sports.Model.RefCall FindUsingCustNum( p_CustNum as integer,p_Sort as char):
    FindWhere(substitute("where _Ref-Call.Cust-Num eq &1 ",quoter(p_CustNum)),p_Sort).
    
    if not this-object:available then
    do:
      assign this-object:CustNum = p_CustNum.
    end.
    
    return cast(this-object,sports.Model.RefCall).
  end method.
  
  /** gets a RefCall using FindUsingCustNumCallNum
   *
   * @param  p_CustNum - integer, p_CallNum - character
   * @return sports.Model.RefCall
   */
  method public sports.Model.RefCall FindUsingCustNumCallNum( p_CustNum as integer, p_CallNum as character):
    return FindUsingCustNumCallNum( p_CustNum, p_CallNum,"").
  end method.
  
  method public sports.Model.RefCall FindUsingCustNumCallNum( p_CustNum as integer, p_CallNum as character,p_Sort as char):
    FindWhere(substitute("where _Ref-Call.Cust-Num eq &1  and _Ref-Call.Call-Num eq &2 ",quoter(p_CustNum),quoter(p_CallNum)),p_Sort).
    
    if not this-object:available then
    do:
      assign this-object:CustNum = p_CustNum
             this-object:CallNum = p_CallNum.
    end.
    
    return cast(this-object,sports.Model.RefCall).
  end method.
  
  /** gets a RefCall using FindUsingParent
   *
   * @param  p_Parent - character
   * @return sports.Model.RefCall
   */
  method public sports.Model.RefCall FindUsingParent( p_Parent as character):
    return FindUsingParent( p_Parent,"").
  end method.
  
  method public sports.Model.RefCall FindUsingParent( p_Parent as character,p_Sort as char):
    FindWhere(substitute("where _Ref-Call.Parent eq &1 ",quoter(p_Parent)),p_Sort).
    
    if not this-object:available then
    do:
      assign this-object:Parent = p_Parent.
    end.
    
    return cast(this-object,sports.Model.RefCall).
  end method.
  
  /** gets a RefCall using FindUsingParentCallNum
   *
   * @param  p_Parent - character, p_CallNum - character
   * @return sports.Model.RefCall
   */
  method public sports.Model.RefCall FindUsingParentCallNum( p_Parent as character, p_CallNum as character):
    return FindUsingParentCallNum( p_Parent, p_CallNum,"").
  end method.
  
  method public sports.Model.RefCall FindUsingParentCallNum( p_Parent as character, p_CallNum as character,p_Sort as char):
    FindWhere(substitute("where _Ref-Call.Parent eq &1  and _Ref-Call.Call-Num eq &2 ",quoter(p_Parent),quoter(p_CallNum)),p_Sort).
    
    if not this-object:available then
    do:
      assign this-object:Parent  = p_Parent
             this-object:CallNum = p_CallNum.
    end.
    
    return cast(this-object,sports.Model.RefCall).
  end method.
  
  method public void Validate():
  
  	ValidateCustNumCallNum().
  	ValidateParentCallNum().
  end method.
  /** ensure that CustNumCallNum not used elsewhere 
     * @param none
     * @return void 
     */
    
    method public void ValidateCustNumCallNum():
      def var test as sports.Model.RefCall no-undo.
      
      if this-object:CustNum eq ? then return.
  if this-object:CallNum eq ? then return.
      
      test = new sports.Model.RefCall().
      
      /** try to find another record with CustNumCallNum */
      test:FindUsingCustNumCallNum( this-object:CustNum, this-object:CallNum).
      
      if test:available and this-object:CallNum ne test:CallNum  then undo, throw new AppError("CustNumCallNum already in use",0). 
    end method.
  /** ensure that ParentCallNum not used elsewhere 
     * @param none
     * @return void 
     */
    
    method public void ValidateParentCallNum():
      def var test as sports.Model.RefCall no-undo.
      
      if this-object:Parent eq ? then return.
  if this-object:CallNum eq ? then return.
      
      test = new sports.Model.RefCall().
      
      /** try to find another record with ParentCallNum */
      test:FindUsingParentCallNum( this-object:Parent, this-object:CallNum).
      
      if test:available and this-object:CallNum ne test:CallNum  then undo, throw new AppError("ParentCallNum already in use",0). 
    end method.
  

end class.
@BuildInfo(Source="RefCall").
@BuildInfo(Class="sports._Maia.Ref-Call").
@BuildInfo(Date="11/06/2013 14:29:56.826+01:00").
@BuildInfo(MD5Hash="h6BsaYkAXpm/7gpWLQOgaQ==").