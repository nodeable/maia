/*
Copyright (C) 2011 by Julian Lyndon-Smith (julian+mit@dotr.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS for A PARTICULAR PURPOSE and NONINFRINGEMENT. IN no EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE for ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR otherwise, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/


using Progress.Lang.*.
using sports.Model.*.
using Progress.Data.*.

routine-level on error undo, throw.

class sports.Model.Base.OrderLine abstract :
  def private property DataProvider as dotr.ActiveRecord.Interface.IDataProvider no-undo get . private set.
  
  def private property AvailableFlag as logical no-undo get . set .

  def public property buffer as handle no-undo 
    get():
      return DataProvider:buffer.
    end get . private set.

  def public property query as handle no-undo 
    get():
      return DataProvider:query.
    end get . private set.
  
  def public property rowid as rowid  get. set. /* must be undo, as any error on creation must be reset to ? */
  
  def public property PrivateData  as char no-undo get. set.
  def public property ErrorMessage as char no-undo get. set.
  
  def public property DataFileName as char no-undo get. protected set.
  def public property UpdateType   as char no-undo get. private   set.
  
  def public property SendStompUpdate as logical no-undo init yes get . set .
  def public property InTestMode      as logical no-undo          get . set .
  def public property Preselect			  as logical no-undo          get . set .
  
  def public property Available as logical no-undo
    get():
      return rowid ne ?.
    end get . protected set.
    
  def public property NumRecords as int no-undo
    get():
      return this-object:DataProvider:query:num-results.
    end get . protected set.

  def public property ExtendedPrice as decimal no-undo  get .  set .  
  def public property Price         as decimal no-undo  get .  set .  
  
  def public property Discount as integer no-undo  get .  set .  
  def public property ItemNum  as integer no-undo  get .  set .  
  def public property LineNum  as integer no-undo  get .  set .  
  def public property OrderNum as integer no-undo  get .  set .  
  def public property Qty      as integer no-undo  get .  set .  
  
  def public property Backorder as logical no-undo  get .  set .  
  
  def protected property HasValidItem as logical no-undo protected get . private set .
  
  def protected property HasValidOrder as logical no-undo protected get . private set .
  
  def public property Item as sports.Model.Item no-undo
    get():
      if not HasValidItem then 
      do:
        assign Item         = (new sports.Model.Item()):FindUsingItemnum(this-object:ItemNum)
               HasValidItem = yes.
      end.
      return Item.
    end get . private set .
  
  def public property Order as sports.Model.Order no-undo
    get():
      if not HasValidOrder then 
      do:
        assign Order         = (new sports.Model.Order()):FindUsingOrdernum(this-object:OrderNum)
               HasValidOrder = yes.
      end.
      return Order.
    end get . private set .
  
  constructor OrderLine():
    StartUp().
  end constructor.
  
  destructor OrderLine():
    if valid-object(this-object:DataProvider) then delete object this-object:DataProvider.
  
    
  end destructor.
  
  /** bind a progress data binding source to the query
   * @param binding source to use
   * @return return value
   */

  method public void Bind(p_BindingSource as Progress.Data.BindingSource):
    p_BindingSource:handle = this-object:DataProvider:query.
  end method.  

  /** Creates the buffer handle and buffer fields 
   */

  method protected void StartUp():
    if valid-handle(dotr.ActiveRecord.Partition:AppServer) and dotr.ActiveRecord.Partition:AppServer ne session:handle  
       then this-object:DataProvider = new dotr.ActiveRecord.ServiceProvider("Order-Line").
       else this-object:DataProvider = new dotr.ActiveRecord.DataAccess("Order-Line").    
  end method.
  
  
  
    /** gets the OrderLine matching the unique ID 
     *  @param p_OrderLineID : the id to find
     */
  
    method public sports.Model.OrderLine find( p_OrderNum as integer, p_LineNum as integer):
      this-object:DataProvider:FetchUnique(substitute("where _Order-Line.Order-num eq &1  and _Order-Line.Line-num eq &2 ",quoter(p_OrderNum),quoter(p_LineNum))).
      this-object:Get().
      
      if not this-object:available then
      do:
       assign this-object:OrderNum = p_OrderNum
             this-object:LineNum  = p_LineNum.
      end.
      
      return cast(this-object,sports.Model.OrderLine).
    end.
   
    /** gets the OrderLine using the rowid 
     *  @param p_OrderLineRowID : the rowid to find
     */
  
    method public sports.Model.OrderLine find(p_OrderLineRowID as rowid):
      this-object:DataProvider:FetchUnique(p_OrderLineRowID).
      return this-object:Get().  
    end.
   

  /** stub method to allow for override
   */

  method public void AfterGet():
  end method.

 /** stub method to allow for override
   */

  method public void BeforeGet():
  end method.

 /** stub method to allow for override
  */

  method public void BeforeCreate():
  end method.
  
 /** stub method to allow for override
  */

  method public void AfterCreate():
  end method.

 /** stub method to allow for override
  */

  method public void BeforeUpdate():
  end method.
  
 /** stub method to allow for override
  */

  method public void AfterUpdate():
  end method.
  
 /** stub method to allow for override
  */

  method public void BeforeRemove():
  end method.
  
 /** stub method to allow for override
  */

  method public void AfterRemove():
  end method.

 /** stub method to allow for override
  */
  method public void NotFound():
  end method.

  /** stub method to allow for override
   */

  method public void Imported():
    this-object:save().
  end method.

  /** stub method to allow for override
  */
  method public void AfterTransaction():
  end method.
  
  /** assigns the buffer fields to the object
   *  @return sports.Model.OrderLine  
   */

  method protected sports.Model.OrderLine get():
    BeforeGet().
    assign this-object:rowid         = ?
           this-object:UpdateType    = "find".
        
    if not this-object:buffer:available then
    do: 
      NotFound().
      return cast(this-object,sports.Model.OrderLine). 
    end.

    assign this-object:Backorder      = buffer::Backorder
           this-object:Discount       = buffer::Discount
           this-object:ExtendedPrice = buffer::Extended-Price
           this-object:ItemNum       = buffer::Item-num
           this-object:LineNum       = buffer::Line-num
           this-object:OrderNum      = buffer::Order-num
           this-object:Price          = buffer::Price
           this-object:Qty            = buffer::Qty
           this-object:rowid          = buffer:rowid.
    
    if HasValidItem then 
    do:
      delete object Item no-error.
      assign HasValidItem = no.
    end.   
    
    if HasValidOrder then 
    do:
      delete object Order no-error.
      assign HasValidOrder = no.
    end.   
           
    AfterGet().
    
    return cast(this-object,sports.Model.OrderLine). 
  end.
  
  /** open a query for all records in the <<Table>> table
   */
  method public sports.Model.OrderLine all():
    return this-object:FindWhere("").
  end method.

  /** opens a query for records matching the p_Where clause
   *  @param p_Where : the where statement to use  
   *  @return sports.Model.OrderLine  
   */
  
  method public sports.Model.OrderLine FindWhere (p_Where as char):
    return FindWhere(p_Where,"").
  end method.

  /** opens a query for records matching the p_Where clause
   *  @param p_Where : the where statement to use  
   *  @param p_Sort : the sort statement to use  
   *  @return sports.Model.OrderLine  
   */
  
  method public sports.Model.OrderLine FindWhere (p_Where as char,p_Sort as char):
    def var lv_Query as char no-undo.
    
    assign lv_Query = substitute("&3 each _Order-Line &1 no-lock &2",
                                                if trim(p_Where) begins "where " 
                                                   then p_Where
                                                   else if p_Where ne "" then substitute("where &1",p_Where)
                                                   else "",
                                                p_Sort,
                                                if this-object:preselect then "preselect" else "for").
    this-object:DataProvider:FetchData(lv_query).
    
    return this-object:First().                                                        
  end method.
  
	/** get next record, return available status
		* @return record available 
		*/  
  method public logical NextAvailable():
    if this-object:AvailableFlag then this-object:Next().
    assign this-object:AvailableFlag = yes.
    return this-object:available.
  end method.
  
  /** opens a query for records matching the p_Where clause
   *  @param p_Where : the where statement to use  
   *  @return sports.Model.OrderLine  
   */
  
  method public sports.Model.OrderLine preselect (p_Where as char):
    return preselect(p_Where,"").
  end method.

  /** opens a query for records matching the p_Where clause
   *  @param p_Where : the where statement to use  
   *  @param p_Sort : the sort statement to use  
   *  @return sports.Model.OrderLine  
   */
  
  method public sports.Model.OrderLine preselect (p_Where as char,p_Sort as char):
    def var lv_Query as char no-undo.

    assign lv_query = substitute("preselect each _Order-Line &1 no-lock &2",
                                                if trim(p_Where) begins "where " 
                                                   then p_Where
                                                   else if p_Where ne "" then substitute("where &1",p_Where)
                                                   else "",
                                                   p_Sort).

    this-object:DataProvider:FetchData(lv_Query).
    
    return this-object:First().                                                        
  end method.
  
  /** gets the first OrderLine of the query
   *  @return sports.Model.OrderLine  
   */

  method public sports.Model.OrderLine first():
    if this-object:DataProvider:query:is-open then this-object:DataProvider:query:get-first().
    assign this-object:AvailableFlag = no.
    return this-object:Get().
  end method.
    
  /** gets the last OrderLine of the query
   *  @return sports.Model.OrderLine  
   */

  method public sports.Model.OrderLine last():
    if this-object:DataProvider:query:is-open then this-object:DataProvider:query:get-last().
    return this-object:Get().
  end method.

  /** gets the next OrderLine of the query
   *  @return sports.Model.OrderLine  
   */

  method public sports.Model.OrderLine next():
    if this-object:DataProvider:query:is-open then this-object:DataProvider:query:get-next().
    return this-object:Get().
  end method.

  /** gets the prev OrderLine of the query
   *  @return sports.Model.OrderLine  
   */

  method public sports.Model.OrderLine prev():
    if this-object:DataProvider:query:is-open then this-object:DataProvider:query:get-prev().
    return this-object:Get().
  end method.

  /** saves the object attributes to the database
   *  @return sports.Model.OrderLine  
   */
  
  method public sports.Model.OrderLine save():
    WriteRecord(if this-object:rowid eq ? then "CREATE" else "UPDATE").
    return cast(this-object,sports.Model.OrderLine). 
  end method.

  /** removes the object from the database
   */
 
  method public void Remove():
    if not this-object:available then return.
    WriteRecord("Remove").
    return. 
  end method. 

  /** writes the approriate changes to the database
   *  @param p_Mode : create (creates a new <<table>>
   *                : update (saves the changes to the database)
   *                : remove (removes the record from the database)
   */
    
  method protected final void WriteRecord(p_Mode as char):
    def var lv_new as logical no-undo.
    
    def var hUpdateBuffer as handle no-undo.
    
    create buffer hUpdateBuffer for table this-object:buffer 
                                    buffer-name this-object:buffer:name.
    
    assign lv_new                 = this-object:rowid eq ?
           this-object:UpdateType = p_mode.
    
    
    /** try to lock the record, give up after 5 seconds */
    repeat while true stop-after 5 transaction on error undo, throw
                                               on stop undo, leave:

      if p_mode eq "CREATE" then 
      do:
        BeforeCreate(). /** override this method if you want to do stuff before the create statement */ 

        hUpdateBuffer:buffer-create().
        
        
        
        
        assign this-object:rowid = hUpdateBuffer:rowid.
               
        AfterCreate().  /** override this method if you want to do stuff after the create statement */
      end.
      
      else 
      do:
        hUpdateBuffer:find-by-rowid(this-object:rowid,exclusive-lock,no-wait) no-error.
   
        if hUpdateBuffer:locked then next.
        if not hUpdateBuffer:avail then undo, throw new AppError(substitute("Could not find Order-Line [&1]",this-object:rowid),0).
      end.
      
      case p_Mode:
        when "Update" or when "Create" then
        do:
          BeforeUpdate().  /** override this method if you want to do stuff before the update statement */
          Validate().
          assign hUpdateBuffer::Backorder      = this-object:Backorder
                 hUpdateBuffer::Discount       = this-object:Discount
                 hUpdateBuffer::Extended-Price = this-object:ExtendedPrice
                 hUpdateBuffer::Item-num       = this-object:ItemNum
                 hUpdateBuffer::Line-num       = this-object:LineNum
                 hUpdateBuffer::Order-num      = this-object:OrderNum
                 hUpdateBuffer::Price          = this-object:Price
                 hUpdateBuffer::Qty            = this-object:Qty.
          AfterUpdate().  /** override this method if you want to do stuff after the update statement */
        end.
        
        when "Remove" then
        do:
          BeforeRemove().  /** override this method if you want to do stuff before the remove statement */
          hUpdateBuffer:buffer-delete().
          assign this-object:rowid = ?.
          
          AfterRemove().  /** override this method if you want to do stuff after the remove statement */
        end.
        
      end case.
      
      
      hUpdateBuffer:buffer-release().
      
      leave.
      
      catch e as Progress.Lang.Error :
        case e:GetMessageNum(1):
          when 132 then undo, throw new AppError(e:getMessage(1),132).
          otherwise     undo, throw new AppError(e:GetMessage(1),0).
        end case.
      end catch.
    end.
    
    AfterTransaction(). /** override this method if you want to do stuff after the (sub)transaction has ended */
    
    this-object:DataProvider:SaveData(). /** does nothing if local, sends to appserver if not */
    
    return.
    
    finally:
      delete object hUpdateBuffer.
    end finally.
    
  end method.
  
  /** Export ActiveRecord as Json */ 
  method public longchar Export():
    return ExportData("json","single").
  end method.

  /** Export ActiveRecord as Json */ 
  method public longchar ExportQuery():
    return ExportData("json","all").
  end method.
  
  /** take current ActiveRecord and convert to longchar
   *  
   *  gets the current object, converts into a temp-table
   *  and exports the data */ 
  
  method private longchar ExportData(p_format as char,p_Mode as char):
    
    return (new sports.Model.TempTable.OrderLine()):ExportData(p_format,p_mode,cast(this-object,sports.Model.OrderLine)).
    
  end method. 
  
  /** take a json string and assign properties to OrderLine
   *  
   */ 
  
  method public void Import(p_json as longchar):
    this-object:import("json",p_json).
  end method.
  
  /** take a json string and assign properties to OrderLine
   *  
   */ 
  
  method public void Import(p_format as char,p_json as longchar):
    def var tttablehandle as handle no-undo.
    def var tablehandle   as handle no-undo.
    def var tablequery    as handle no-undo.
    def var fieldhandle   as handle no-undo.
    
    def var lv_i as int no-undo.
    
    create temp-table tttablehandle.
    
    tttablehandle:read-json("longchar",p_Json,"empty").
    
    assign tablehandle = tttablehandle:default-buffer-handle.
    
    create query tablequery.
    
    tablequery:add-buffer (tablehandle).

    tablequery:query-prepare(substitute("for each &1 no-lock",tablehandle:name)).
    tablequery:query-open().
    tablequery:get-first().
    
    do while not tablequery:query-off-end on error undo, throw:
      this-object:Find(integer(tablehandle::OrderNum),integer(tablehandle::LineNum)).
      
      do lv_i = 1 to tablehandle:num-fields:
        assign fieldhandle = tablehandle:buffer-field(lv_i).
        
        case fieldhandle:name:
          when 'Backorder'     then assign this-object:Backorder     = logical(fieldhandle:buffer-value).
          when 'Discount'      then assign this-object:Discount      = integer(fieldhandle:buffer-value).
          when 'ExtendedPrice' then assign this-object:ExtendedPrice = decimal(fieldhandle:buffer-value).
          when 'ItemNum'       then assign this-object:ItemNum       = integer(fieldhandle:buffer-value).
          when 'LineNum'       then assign this-object:LineNum       = integer(fieldhandle:buffer-value).
          when 'OrderNum'      then assign this-object:OrderNum      = integer(fieldhandle:buffer-value).
          when 'Price'         then assign this-object:Price         = decimal(fieldhandle:buffer-value).
          when 'Qty'           then assign this-object:Qty           = integer(fieldhandle:buffer-value).
        end case.
        
      end.
      
      this-object:Imported().
      
      finally:
        tablequery:get-next().
      end finally.
    end.
    
    finally:
      delete object tablequery.
      delete object tttablehandle.
    end finally.
    
  end method.
  
	/** Load db into model
		* @param 
		* @return 
		*/     
		
  method public void AppServerLoad():
    this-object:all().
  end method.

  /** Load db into model
    * @param 
    * @return 
    */     
    
  method public void AppServerSave():
  end method.
  		
  
  /** gets a OrderLine using FindUsingItemNum
   *
   * @param  p_ItemNum - integer
   * @return sports.Model.OrderLine
   */
  method public sports.Model.OrderLine FindUsingItemNum( p_ItemNum as integer):
    return FindUsingItemNum( p_ItemNum,"").
  end method.
  
  method public sports.Model.OrderLine FindUsingItemNum( p_ItemNum as integer,p_Sort as char):
    FindWhere(substitute("where _Order-Line.Item-num eq &1 ",quoter(p_ItemNum)),p_Sort).
    
    if not this-object:available then
    do:
      assign this-object:ItemNum = p_ItemNum.
    end.
    
    return cast(this-object,sports.Model.OrderLine).
  end method.
  
  /** gets a OrderLine using FindUsingOrderNum
   *
   * @param  p_OrderNum - integer
   * @return sports.Model.OrderLine
   */
  method public sports.Model.OrderLine FindUsingOrderNum( p_OrderNum as integer):
    return FindUsingOrderNum( p_OrderNum,"").
  end method.
  
  method public sports.Model.OrderLine FindUsingOrderNum( p_OrderNum as integer,p_Sort as char):
    FindWhere(substitute("where _Order-Line.Order-num eq &1 ",quoter(p_OrderNum)),p_Sort).
    
    if not this-object:available then
    do:
      assign this-object:OrderNum = p_OrderNum.
    end.
    
    return cast(this-object,sports.Model.OrderLine).
  end method.
  
  /** gets a OrderLine using FindUsingOrderNumLineNum
   *
   * @param  p_OrderNum - integer, p_LineNum - integer
   * @return sports.Model.OrderLine
   */
  method public sports.Model.OrderLine FindUsingOrderNumLineNum( p_OrderNum as integer, p_LineNum as integer):
    return FindUsingOrderNumLineNum( p_OrderNum, p_LineNum,"").
  end method.
  
  method public sports.Model.OrderLine FindUsingOrderNumLineNum( p_OrderNum as integer, p_LineNum as integer,p_Sort as char):
    FindWhere(substitute("where _Order-Line.Order-num eq &1  and _Order-Line.Line-num eq &2 ",quoter(p_OrderNum),quoter(p_LineNum)),p_Sort).
    
    if not this-object:available then
    do:
      assign this-object:OrderNum = p_OrderNum
             this-object:LineNum  = p_LineNum.
    end.
    
    return cast(this-object,sports.Model.OrderLine).
  end method.
  
  method public void Validate():
  
  end method.
  
  /** nullifies foreign key
   *
   */
  
  method public void RemoveItem():
    assign this-object:ItemNum = ?
           this-object:HasValidItem = no.
    
    this-object:save().
  end method.
  
  /** remove record on foreign key  deletion
   *
   */
  
  method public void RemoveOrder():
    this-object:Remove().
  end method.

end class.
@BuildInfo(Source="OrderLine").
@BuildInfo(Class="sports._Maia.Order-Line").
@BuildInfo(Date="11/06/2013 14:29:56.697+01:00").
@BuildInfo(MD5Hash="qRh5rk6qVvR8+sVTub41vg==").