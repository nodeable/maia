/*
Copyright (C) 2011 by Julian Lyndon-Smith (julian+mit@dotr.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS for A PARTICULAR PURPOSE and NONINFRINGEMENT. IN no EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE for ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR otherwise, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/


using Progress.Lang.*.
using sports.Model.*.
using Progress.Data.*.

routine-level on error undo, throw.

class sports.Model.Base.State abstract :
  def private property DataProvider as dotr.ActiveRecord.Interface.IDataProvider no-undo get . private set.
  
  def private property AvailableFlag as logical no-undo get . set .

  def public property buffer as handle no-undo 
    get():
      return DataProvider:buffer.
    end get . private set.

  def public property query as handle no-undo 
    get():
      return DataProvider:query.
    end get . private set.
  
  def public property rowid as rowid  get. set. /* must be undo, as any error on creation must be reset to ? */
  
  def public property PrivateData  as char no-undo get. set.
  def public property ErrorMessage as char no-undo get. set.
  
  def public property DataFileName as char no-undo get. protected set.
  def public property UpdateType   as char no-undo get. private   set.
  
  def public property SendStompUpdate as logical no-undo init yes get . set .
  def public property InTestMode      as logical no-undo          get . set .
  def public property Preselect			  as logical no-undo          get . set .
  
  def public property Available as logical no-undo
    get():
      return rowid ne ?.
    end get . protected set.
    
  def public property NumRecords as int no-undo
    get():
      return this-object:DataProvider:query:num-results.
    end get . protected set.

  def public property Region    as character no-undo  get .  set .  
  def public property State     as character no-undo  get .  set .  
  def public property StateName as character no-undo  get .  set .  
  
  def protected property HasValidCustomers as logical no-undo protected get . private set .
  
  def public property Customers as sports.Model.Customer no-undo
    get():
      if not HasValidCustomers then 
      do:
        assign Customers         = (new sports.Model.Customer()):FindUsingStateID(this-object:State)
               HasValidCustomers = yes.
      end.
      return Customers.
    end get . private set .
  
  constructor State():
    StartUp().
  end constructor.
  
  destructor State():
    if valid-object(this-object:DataProvider) then delete object this-object:DataProvider.
  
    
  end destructor.
  
  /** bind a progress data binding source to the query
   * @param binding source to use
   * @return return value
   */

  method public void Bind(p_BindingSource as Progress.Data.BindingSource):
    p_BindingSource:handle = this-object:DataProvider:query.
  end method.  

  /** Creates the buffer handle and buffer fields 
   */

  method protected void StartUp():
    if valid-handle(dotr.ActiveRecord.Partition:AppServer) and dotr.ActiveRecord.Partition:AppServer ne session:handle  
       then this-object:DataProvider = new dotr.ActiveRecord.ServiceProvider("State").
       else this-object:DataProvider = new dotr.ActiveRecord.DataAccess("State").    
  end method.
  
  
    /** gets the State matching the unique ID 
     *  @param p_StateID : the id to find
     */
  
    method public sports.Model.State find(p_StateID as character):
      this-object:DataProvider:FetchUnique(substitute("where _State.State eq &1",quoter(p_StateID))).
      
      this-object:Get().
      
      if not this-object:available then assign this-object:State = p_StateID.
      return cast(this-object,sports.Model.State). 
    end.
    
    /** gets the State using the rowid 
     *  @param p_StateRowID : the rowid to find
     */
  
    method public sports.Model.State find(p_StateRowID as rowid):
      this-object:DataProvider:FetchUnique(p_StateRowID).
      return this-object:Get().  
    end.
   

  /** stub method to allow for override
   */

  method public void AfterGet():
  end method.

 /** stub method to allow for override
   */

  method public void BeforeGet():
  end method.

 /** stub method to allow for override
  */

  method public void BeforeCreate():
  end method.
  
 /** stub method to allow for override
  */

  method public void AfterCreate():
  end method.

 /** stub method to allow for override
  */

  method public void BeforeUpdate():
  end method.
  
 /** stub method to allow for override
  */

  method public void AfterUpdate():
  end method.
  
 /** stub method to allow for override
  */

  method public void BeforeRemove():
  end method.
  
 /** stub method to allow for override
  */

  method public void AfterRemove():
  end method.

 /** stub method to allow for override
  */
  method public void NotFound():
  end method.

  /** stub method to allow for override
   */

  method public void Imported():
    this-object:save().
  end method.

  /** stub method to allow for override
  */
  method public void AfterTransaction():
  end method.
  
  /** assigns the buffer fields to the object
   *  @return sports.Model.State  
   */

  method protected sports.Model.State get():
    BeforeGet().
    assign this-object:rowid         = ?
           this-object:UpdateType    = "find".
        
    if not this-object:buffer:available then
    do: 
      NotFound().
      return cast(this-object,sports.Model.State). 
    end.

    assign this-object:Region     = buffer::Region
           this-object:State      = buffer::State
           this-object:StateName = buffer::State-Name
           this-object:rowid      = buffer:rowid.
    
    if HasValidCustomers then 
    do:
      delete object Customers no-error.
      assign HasValidCustomers = no.
    end.   
           
    AfterGet().
    
    return cast(this-object,sports.Model.State). 
  end.
  
  /** open a query for all records in the <<Table>> table
   */
  method public sports.Model.State all():
    return this-object:FindWhere("").
  end method.

  /** opens a query for records matching the p_Where clause
   *  @param p_Where : the where statement to use  
   *  @return sports.Model.State  
   */
  
  method public sports.Model.State FindWhere (p_Where as char):
    return FindWhere(p_Where,"").
  end method.

  /** opens a query for records matching the p_Where clause
   *  @param p_Where : the where statement to use  
   *  @param p_Sort : the sort statement to use  
   *  @return sports.Model.State  
   */
  
  method public sports.Model.State FindWhere (p_Where as char,p_Sort as char):
    def var lv_Query as char no-undo.
    
    assign lv_Query = substitute("&3 each _State &1 no-lock &2",
                                                if trim(p_Where) begins "where " 
                                                   then p_Where
                                                   else if p_Where ne "" then substitute("where &1",p_Where)
                                                   else "",
                                                p_Sort,
                                                if this-object:preselect then "preselect" else "for").
    this-object:DataProvider:FetchData(lv_query).
    
    return this-object:First().                                                        
  end method.
  
	/** get next record, return available status
		* @return record available 
		*/  
  method public logical NextAvailable():
    if this-object:AvailableFlag then this-object:Next().
    assign this-object:AvailableFlag = yes.
    return this-object:available.
  end method.
  
  /** opens a query for records matching the p_Where clause
   *  @param p_Where : the where statement to use  
   *  @return sports.Model.State  
   */
  
  method public sports.Model.State preselect (p_Where as char):
    return preselect(p_Where,"").
  end method.

  /** opens a query for records matching the p_Where clause
   *  @param p_Where : the where statement to use  
   *  @param p_Sort : the sort statement to use  
   *  @return sports.Model.State  
   */
  
  method public sports.Model.State preselect (p_Where as char,p_Sort as char):
    def var lv_Query as char no-undo.

    assign lv_query = substitute("preselect each _State &1 no-lock &2",
                                                if trim(p_Where) begins "where " 
                                                   then p_Where
                                                   else if p_Where ne "" then substitute("where &1",p_Where)
                                                   else "",
                                                   p_Sort).

    this-object:DataProvider:FetchData(lv_Query).
    
    return this-object:First().                                                        
  end method.
  
  /** gets the first State of the query
   *  @return sports.Model.State  
   */

  method public sports.Model.State first():
    if this-object:DataProvider:query:is-open then this-object:DataProvider:query:get-first().
    assign this-object:AvailableFlag = no.
    return this-object:Get().
  end method.
    
  /** gets the last State of the query
   *  @return sports.Model.State  
   */

  method public sports.Model.State last():
    if this-object:DataProvider:query:is-open then this-object:DataProvider:query:get-last().
    return this-object:Get().
  end method.

  /** gets the next State of the query
   *  @return sports.Model.State  
   */

  method public sports.Model.State next():
    if this-object:DataProvider:query:is-open then this-object:DataProvider:query:get-next().
    return this-object:Get().
  end method.

  /** gets the prev State of the query
   *  @return sports.Model.State  
   */

  method public sports.Model.State prev():
    if this-object:DataProvider:query:is-open then this-object:DataProvider:query:get-prev().
    return this-object:Get().
  end method.

  /** saves the object attributes to the database
   *  @return sports.Model.State  
   */
  
  method public sports.Model.State save():
    WriteRecord(if this-object:rowid eq ? then "CREATE" else "UPDATE").
    return cast(this-object,sports.Model.State). 
  end method.

  /** removes the object from the database
   */
 
  method public void Remove():
    if not this-object:available then return.
    WriteRecord("Remove").
    return. 
  end method. 

  /** writes the approriate changes to the database
   *  @param p_Mode : create (creates a new <<table>>
   *                : update (saves the changes to the database)
   *                : remove (removes the record from the database)
   */
    
  method protected final void WriteRecord(p_Mode as char):
    def var lv_new as logical no-undo.
    
    def var hUpdateBuffer as handle no-undo.
    
    create buffer hUpdateBuffer for table this-object:buffer 
                                    buffer-name this-object:buffer:name.
    
    assign lv_new                 = this-object:rowid eq ?
           this-object:UpdateType = p_mode.
    
    
    /** try to lock the record, give up after 5 seconds */
    repeat while true stop-after 5 transaction on error undo, throw
                                               on stop undo, leave:

      if p_mode eq "CREATE" then 
      do:
        BeforeCreate(). /** override this method if you want to do stuff before the create statement */ 

        hUpdateBuffer:buffer-create().
        
        
        
        
        assign this-object:rowid = hUpdateBuffer:rowid.
               
        AfterCreate().  /** override this method if you want to do stuff after the create statement */
      end.
      
      else 
      do:
        hUpdateBuffer:find-by-rowid(this-object:rowid,exclusive-lock,no-wait) no-error.
   
        if hUpdateBuffer:locked then next.
        if not hUpdateBuffer:avail then undo, throw new AppError(substitute("Could not find State [&1]",this-object:rowid),0).
      end.
      
      case p_Mode:
        when "Update" or when "Create" then
        do:
          BeforeUpdate().  /** override this method if you want to do stuff before the update statement */
          Validate().
          assign hUpdateBuffer::Region     = this-object:Region
                 hUpdateBuffer::State      = this-object:State
                 hUpdateBuffer::State-Name = this-object:StateName.
          AfterUpdate().  /** override this method if you want to do stuff after the update statement */
        end.
        
        when "Remove" then
        do:
          BeforeRemove().  /** override this method if you want to do stuff before the remove statement */
          hUpdateBuffer:buffer-delete().
          assign this-object:rowid = ?.
          /** clear all sports.Model.Customer with a foreign key matching this object */ 
          
          Customers  = (new sports.Model.Customer()):FindUsingStateID(this-object:State).
            
          do while Customers:Available:
            Customers:RemoveState().
            Customers:next().
          end.
          AfterRemove().  /** override this method if you want to do stuff after the remove statement */
        end.
        
      end case.
      
      
      hUpdateBuffer:buffer-release().
      
      leave.
      
      catch e as Progress.Lang.Error :
        case e:GetMessageNum(1):
          when 132 then undo, throw new AppError(e:getMessage(1),132).
          otherwise     undo, throw new AppError(e:GetMessage(1),0).
        end case.
      end catch.
    end.
    
    AfterTransaction(). /** override this method if you want to do stuff after the (sub)transaction has ended */
    
    this-object:DataProvider:SaveData(). /** does nothing if local, sends to appserver if not */
    
    return.
    
    finally:
      delete object hUpdateBuffer.
    end finally.
    
  end method.
  
  /** Export ActiveRecord as Json */ 
  method public longchar Export():
    return ExportData("json","single").
  end method.

  /** Export ActiveRecord as Json */ 
  method public longchar ExportQuery():
    return ExportData("json","all").
  end method.
  
  /** take current ActiveRecord and convert to longchar
   *  
   *  gets the current object, converts into a temp-table
   *  and exports the data */ 
  
  method private longchar ExportData(p_format as char,p_Mode as char):
    
    return (new sports.Model.TempTable.State()):ExportData(p_format,p_mode,cast(this-object,sports.Model.State)).
    
  end method. 
  
  /** take a json string and assign properties to State
   *  
   */ 
  
  method public void Import(p_json as longchar):
    this-object:import("json",p_json).
  end method.
  
  /** take a json string and assign properties to State
   *  
   */ 
  
  method public void Import(p_format as char,p_json as longchar):
    def var tttablehandle as handle no-undo.
    def var tablehandle   as handle no-undo.
    def var tablequery    as handle no-undo.
    def var fieldhandle   as handle no-undo.
    
    def var lv_i as int no-undo.
    
    create temp-table tttablehandle.
    
    tttablehandle:read-json("longchar",p_Json,"empty").
    
    assign tablehandle = tttablehandle:default-buffer-handle.
    
    create query tablequery.
    
    tablequery:add-buffer (tablehandle).

    tablequery:query-prepare(substitute("for each &1 no-lock",tablehandle:name)).
    tablequery:query-open().
    tablequery:get-first().
    
    do while not tablequery:query-off-end on error undo, throw:
      this-object:Find(tablehandle::State).
      
      do lv_i = 1 to tablehandle:num-fields:
        assign fieldhandle = tablehandle:buffer-field(lv_i).
        
        case fieldhandle:name:
          when 'Region'    then assign this-object:Region    = fieldhandle:buffer-value.
          when 'State'     then assign this-object:State     = fieldhandle:buffer-value.
          when 'StateName' then assign this-object:StateName = fieldhandle:buffer-value.
        end case.
        
      end.
      
      this-object:Imported().
      
      finally:
        tablequery:get-next().
      end finally.
    end.
    
    finally:
      delete object tablequery.
      delete object tttablehandle.
    end finally.
    
  end method.
  
	/** Load db into model
		* @param 
		* @return 
		*/     
		
  method public void AppServerLoad():
    this-object:all().
  end method.

  /** Load db into model
    * @param 
    * @return 
    */     
    
  method public void AppServerSave():
  end method.
  		
  
  /** gets a State using FindUsingState
   *
   * @param  p_State - character
   * @return sports.Model.State
   */
  method public sports.Model.State FindUsingState( p_State as character):
    return FindUsingState( p_State,"").
  end method.
  
  method public sports.Model.State FindUsingState( p_State as character,p_Sort as char):
    FindWhere(substitute("where _State.State eq &1 ",quoter(p_State)),p_Sort).
    
    if not this-object:available then
    do:
      assign this-object:State = p_State.
    end.
    
    return cast(this-object,sports.Model.State).
  end method.
  
  method public void Validate():
  
  end method.
  
  

end class.
@BuildInfo(Source="State").
@BuildInfo(Class="sports._Maia.State").
@BuildInfo(Date="11/06/2013 14:29:56.998+01:00").
@BuildInfo(MD5Hash="UvzFgREYyXolmaYG5TFOvA==").