/*
Copyright (C) 2011 by Julian Lyndon-Smith (julian+mit@dotr.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS for A PARTICULAR PURPOSE and NONINFRINGEMENT. IN no EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE for ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR otherwise, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/


using Progress.Lang.*.
using sports.Model.*.
using Progress.Data.*.

routine-level on error undo, throw.

class sports.Model.Base.Order abstract :
  def private property DataProvider as dotr.ActiveRecord.Interface.IDataProvider no-undo get . private set.
  
  def private property AvailableFlag as logical no-undo get . set .

  def public property buffer as handle no-undo 
    get():
      return DataProvider:buffer.
    end get . private set.

  def public property query as handle no-undo 
    get():
      return DataProvider:query.
    end get . private set.
  
  def public property rowid as rowid  get. set. /* must be undo, as any error on creation must be reset to ? */
  
  def public property PrivateData  as char no-undo get. set.
  def public property ErrorMessage as char no-undo get. set.
  
  def public property DataFileName as char no-undo get. protected set.
  def public property UpdateType   as char no-undo get. private   set.
  
  def public property SendStompUpdate as logical no-undo init yes get . set .
  def public property InTestMode      as logical no-undo          get . set .
  def public property Preselect			  as logical no-undo          get . set .
  
  def public property Available as logical no-undo
    get():
      return rowid ne ?.
    end get . protected set.
    
  def public property NumRecords as int no-undo
    get():
      return this-object:DataProvider:query:num-results.
    end get . protected set.

  def public property Carrier      as character no-undo  get .  set .  
  def public property Instructions as character no-undo  get .  set .  
  def public property PO           as character no-undo  get .  set .  
  def public property SalesRepID   as character no-undo  get .  set .  
  def public property Terms        as character no-undo init "Net30" get .  set .  
  
  def public property OrderDate   as date no-undo init TODAY get .  set .  
  def public property PromiseDate as date no-undo init ? get .  set .  
  def public property ShipDate    as date no-undo init ? get .  set .  
  
  def public property CustNum  as integer no-undo init 0 get .  set .  
  def public property OrderNum as integer no-undo init 0 get .  set .  
  
  def protected property HasValidCustomer as logical no-undo protected get . private set .
  
  def protected property HasValidSalesrep as logical no-undo protected get . private set .
  
  def protected property HasValidOrderLines as logical no-undo protected get . private set .
  
  def public property Customer as sports.Model.Customer no-undo
    get():
      if not HasValidCustomer then 
      do:
        assign Customer         = (new sports.Model.Customer()):FindUsingCustNum(this-object:CustNum)
               HasValidCustomer = yes.
      end.
      return Customer.
    end get . private set .
  
  def public property Salesrep as sports.Model.Salesrep no-undo
    get():
      if not HasValidSalesrep then 
      do:
        assign Salesrep         = (new sports.Model.Salesrep()):FindUsingSalesRep(this-object:SalesRepID)
               HasValidSalesrep = yes.
      end.
      return Salesrep.
    end get . private set .
  
  def public property OrderLines as sports.Model.OrderLine no-undo
    get():
      if not HasValidOrderLines then 
      do:
        assign OrderLines         = (new sports.Model.OrderLine()):FindUsingOrdernum(this-object:OrderNum)
               HasValidOrderLines = yes.
      end.
      return OrderLines.
    end get . private set .
  
  constructor Order():
    StartUp().
  end constructor.
  
  destructor Order():
    if valid-object(this-object:DataProvider) then delete object this-object:DataProvider.
  
    
  end destructor.
  
  /** bind a progress data binding source to the query
   * @param binding source to use
   * @return return value
   */

  method public void Bind(p_BindingSource as Progress.Data.BindingSource):
    p_BindingSource:handle = this-object:DataProvider:query.
  end method.  

  /** Creates the buffer handle and buffer fields 
   */

  method protected void StartUp():
    if valid-handle(dotr.ActiveRecord.Partition:AppServer) and dotr.ActiveRecord.Partition:AppServer ne session:handle  
       then this-object:DataProvider = new dotr.ActiveRecord.ServiceProvider("Order").
       else this-object:DataProvider = new dotr.ActiveRecord.DataAccess("Order").    
  end method.
  
  
    /** gets the Order matching the unique ID 
     *  @param p_OrderID : the id to find
     */
  
    method public sports.Model.Order find(p_OrderID as integer):
      this-object:DataProvider:FetchUnique(substitute("where _Order.Order-num eq &1",quoter(p_OrderID))).
      
      this-object:Get().
      
      if not this-object:available then assign this-object:OrderNum = p_OrderID.
      return cast(this-object,sports.Model.Order). 
    end.
    
    /** gets the Order using the rowid 
     *  @param p_OrderRowID : the rowid to find
     */
  
    method public sports.Model.Order find(p_OrderRowID as rowid):
      this-object:DataProvider:FetchUnique(p_OrderRowID).
      return this-object:Get().  
    end.
   

  /** stub method to allow for override
   */

  method public void AfterGet():
  end method.

 /** stub method to allow for override
   */

  method public void BeforeGet():
  end method.

 /** stub method to allow for override
  */

  method public void BeforeCreate():
  end method.
  
 /** stub method to allow for override
  */

  method public void AfterCreate():
  end method.

 /** stub method to allow for override
  */

  method public void BeforeUpdate():
  end method.
  
 /** stub method to allow for override
  */

  method public void AfterUpdate():
  end method.
  
 /** stub method to allow for override
  */

  method public void BeforeRemove():
  end method.
  
 /** stub method to allow for override
  */

  method public void AfterRemove():
  end method.

 /** stub method to allow for override
  */
  method public void NotFound():
  end method.

  /** stub method to allow for override
   */

  method public void Imported():
    this-object:save().
  end method.

  /** stub method to allow for override
  */
  method public void AfterTransaction():
  end method.
  
  /** assigns the buffer fields to the object
   *  @return sports.Model.Order  
   */

  method protected sports.Model.Order get():
    BeforeGet().
    assign this-object:rowid         = ?
           this-object:UpdateType    = "find".
        
    if not this-object:buffer:available then
    do: 
      NotFound().
      return cast(this-object,sports.Model.Order). 
    end.

    assign this-object:Carrier      = buffer::Carrier
           this-object:CustNum     = buffer::Cust-Num
           this-object:Instructions = buffer::Instructions
           this-object:OrderDate   = buffer::Order-Date
           this-object:OrderNum    = buffer::Order-num
           this-object:PO           = buffer::PO
           this-object:PromiseDate = buffer::Promise-Date
           this-object:SalesRepID    = buffer::Sales-Rep
           this-object:ShipDate    = buffer::Ship-Date
           this-object:Terms        = buffer::Terms
           this-object:rowid        = buffer:rowid.
    
    if HasValidCustomer then 
    do:
      delete object Customer no-error.
      assign HasValidCustomer = no.
    end.   
    
    if HasValidSalesrep then 
    do:
      delete object Salesrep no-error.
      assign HasValidSalesrep = no.
    end.   
    
    if HasValidOrderLines then 
    do:
      delete object OrderLines no-error.
      assign HasValidOrderLines = no.
    end.   
           
    AfterGet().
    
    return cast(this-object,sports.Model.Order). 
  end.
  
  /** open a query for all records in the <<Table>> table
   */
  method public sports.Model.Order all():
    return this-object:FindWhere("").
  end method.

  /** opens a query for records matching the p_Where clause
   *  @param p_Where : the where statement to use  
   *  @return sports.Model.Order  
   */
  
  method public sports.Model.Order FindWhere (p_Where as char):
    return FindWhere(p_Where,"").
  end method.

  /** opens a query for records matching the p_Where clause
   *  @param p_Where : the where statement to use  
   *  @param p_Sort : the sort statement to use  
   *  @return sports.Model.Order  
   */
  
  method public sports.Model.Order FindWhere (p_Where as char,p_Sort as char):
    def var lv_Query as char no-undo.
    
    assign lv_Query = substitute("&3 each _Order &1 no-lock &2",
                                                if trim(p_Where) begins "where " 
                                                   then p_Where
                                                   else if p_Where ne "" then substitute("where &1",p_Where)
                                                   else "",
                                                p_Sort,
                                                if this-object:preselect then "preselect" else "for").
    this-object:DataProvider:FetchData(lv_query).
    
    return this-object:First().                                                        
  end method.
  
	/** get next record, return available status
		* @return record available 
		*/  
  method public logical NextAvailable():
    if this-object:AvailableFlag then this-object:Next().
    assign this-object:AvailableFlag = yes.
    return this-object:available.
  end method.
  
  /** opens a query for records matching the p_Where clause
   *  @param p_Where : the where statement to use  
   *  @return sports.Model.Order  
   */
  
  method public sports.Model.Order preselect (p_Where as char):
    return preselect(p_Where,"").
  end method.

  /** opens a query for records matching the p_Where clause
   *  @param p_Where : the where statement to use  
   *  @param p_Sort : the sort statement to use  
   *  @return sports.Model.Order  
   */
  
  method public sports.Model.Order preselect (p_Where as char,p_Sort as char):
    def var lv_Query as char no-undo.

    assign lv_query = substitute("preselect each _Order &1 no-lock &2",
                                                if trim(p_Where) begins "where " 
                                                   then p_Where
                                                   else if p_Where ne "" then substitute("where &1",p_Where)
                                                   else "",
                                                   p_Sort).

    this-object:DataProvider:FetchData(lv_Query).
    
    return this-object:First().                                                        
  end method.
  
  /** gets the first Order of the query
   *  @return sports.Model.Order  
   */

  method public sports.Model.Order first():
    if this-object:DataProvider:query:is-open then this-object:DataProvider:query:get-first().
    assign this-object:AvailableFlag = no.
    return this-object:Get().
  end method.
    
  /** gets the last Order of the query
   *  @return sports.Model.Order  
   */

  method public sports.Model.Order last():
    if this-object:DataProvider:query:is-open then this-object:DataProvider:query:get-last().
    return this-object:Get().
  end method.

  /** gets the next Order of the query
   *  @return sports.Model.Order  
   */

  method public sports.Model.Order next():
    if this-object:DataProvider:query:is-open then this-object:DataProvider:query:get-next().
    return this-object:Get().
  end method.

  /** gets the prev Order of the query
   *  @return sports.Model.Order  
   */

  method public sports.Model.Order prev():
    if this-object:DataProvider:query:is-open then this-object:DataProvider:query:get-prev().
    return this-object:Get().
  end method.

  /** saves the object attributes to the database
   *  @return sports.Model.Order  
   */
  
  method public sports.Model.Order save():
    WriteRecord(if this-object:rowid eq ? then "CREATE" else "UPDATE").
    return cast(this-object,sports.Model.Order). 
  end method.

  /** removes the object from the database
   */
 
  method public void Remove():
    if not this-object:available then return.
    WriteRecord("Remove").
    return. 
  end method. 

  /** writes the approriate changes to the database
   *  @param p_Mode : create (creates a new <<table>>
   *                : update (saves the changes to the database)
   *                : remove (removes the record from the database)
   */
    
  method protected final void WriteRecord(p_Mode as char):
    def var lv_new as logical no-undo.
    
    def var hUpdateBuffer as handle no-undo.
    
    create buffer hUpdateBuffer for table this-object:buffer 
                                    buffer-name this-object:buffer:name.
    
    assign lv_new                 = this-object:rowid eq ?
           this-object:UpdateType = p_mode.
    
    
    /** try to lock the record, give up after 5 seconds */
    repeat while true stop-after 5 transaction on error undo, throw
                                               on stop undo, leave:

      if p_mode eq "CREATE" then 
      do:
        BeforeCreate(). /** override this method if you want to do stuff before the create statement */ 

        hUpdateBuffer:buffer-create().
        
        
        
        
        assign this-object:rowid = hUpdateBuffer:rowid.
               
        AfterCreate().  /** override this method if you want to do stuff after the create statement */
      end.
      
      else 
      do:
        hUpdateBuffer:find-by-rowid(this-object:rowid,exclusive-lock,no-wait) no-error.
   
        if hUpdateBuffer:locked then next.
        if not hUpdateBuffer:avail then undo, throw new AppError(substitute("Could not find Order [&1]",this-object:rowid),0).
      end.
      
      case p_Mode:
        when "Update" or when "Create" then
        do:
          BeforeUpdate().  /** override this method if you want to do stuff before the update statement */
          Validate().
          assign hUpdateBuffer::Carrier      = this-object:Carrier
                 hUpdateBuffer::Cust-Num     = this-object:CustNum
                 hUpdateBuffer::Instructions = this-object:Instructions
                 hUpdateBuffer::Order-Date   = this-object:OrderDate
                 hUpdateBuffer::Order-num    = this-object:OrderNum
                 hUpdateBuffer::PO           = this-object:PO
                 hUpdateBuffer::Promise-Date = this-object:PromiseDate
                 hUpdateBuffer::Sales-Rep    = this-object:SalesRepID
                 hUpdateBuffer::Ship-Date    = this-object:ShipDate
                 hUpdateBuffer::Terms        = this-object:Terms.
          AfterUpdate().  /** override this method if you want to do stuff after the update statement */
        end.
        
        when "Remove" then
        do:
          BeforeRemove().  /** override this method if you want to do stuff before the remove statement */
          hUpdateBuffer:buffer-delete().
          assign this-object:rowid = ?.
          /** clear all sports.Model.OrderLine with a foreign key matching this object */ 
          
          OrderLines  = (new sports.Model.OrderLine()):FindUsingOrdernum(this-object:OrderNum).
            
          do while OrderLines:Available:
            OrderLines:RemoveOrder().
            OrderLines:next().
          end.
          AfterRemove().  /** override this method if you want to do stuff after the remove statement */
        end.
        
      end case.
      
      
      hUpdateBuffer:buffer-release().
      
      leave.
      
      catch e as Progress.Lang.Error :
        case e:GetMessageNum(1):
          when 132 then undo, throw new AppError(e:getMessage(1),132).
          otherwise     undo, throw new AppError(e:GetMessage(1),0).
        end case.
      end catch.
    end.
    
    AfterTransaction(). /** override this method if you want to do stuff after the (sub)transaction has ended */
    
    this-object:DataProvider:SaveData(). /** does nothing if local, sends to appserver if not */
    
    return.
    
    finally:
      delete object hUpdateBuffer.
    end finally.
    
  end method.
  
  /** Export ActiveRecord as Json */ 
  method public longchar Export():
    return ExportData("json","single").
  end method.

  /** Export ActiveRecord as Json */ 
  method public longchar ExportQuery():
    return ExportData("json","all").
  end method.
  
  /** take current ActiveRecord and convert to longchar
   *  
   *  gets the current object, converts into a temp-table
   *  and exports the data */ 
  
  method private longchar ExportData(p_format as char,p_Mode as char):
    
    return (new sports.Model.TempTable.Order()):ExportData(p_format,p_mode,cast(this-object,sports.Model.Order)).
    
  end method. 
  
  /** take a json string and assign properties to Order
   *  
   */ 
  
  method public void Import(p_json as longchar):
    this-object:import("json",p_json).
  end method.
  
  /** take a json string and assign properties to Order
   *  
   */ 
  
  method public void Import(p_format as char,p_json as longchar):
    def var tttablehandle as handle no-undo.
    def var tablehandle   as handle no-undo.
    def var tablequery    as handle no-undo.
    def var fieldhandle   as handle no-undo.
    
    def var lv_i as int no-undo.
    
    create temp-table tttablehandle.
    
    tttablehandle:read-json("longchar",p_Json,"empty").
    
    assign tablehandle = tttablehandle:default-buffer-handle.
    
    create query tablequery.
    
    tablequery:add-buffer (tablehandle).

    tablequery:query-prepare(substitute("for each &1 no-lock",tablehandle:name)).
    tablequery:query-open().
    tablequery:get-first().
    
    do while not tablequery:query-off-end on error undo, throw:
      this-object:Find(integer(tablehandle::OrderNum)).
      
      do lv_i = 1 to tablehandle:num-fields:
        assign fieldhandle = tablehandle:buffer-field(lv_i).
        
        case fieldhandle:name:
          when 'Carrier'      then assign this-object:Carrier      = fieldhandle:buffer-value.
          when 'CustNum'      then assign this-object:CustNum      = integer(fieldhandle:buffer-value).
          when 'Instructions' then assign this-object:Instructions = fieldhandle:buffer-value.
          when 'OrderDate'    then assign this-object:OrderDate    = date(fieldhandle:buffer-value).
          when 'OrderNum'     then assign this-object:OrderNum     = integer(fieldhandle:buffer-value).
          when 'PO'           then assign this-object:PO           = fieldhandle:buffer-value.
          when 'PromiseDate'  then assign this-object:PromiseDate  = date(fieldhandle:buffer-value).
          when 'SalesRepID'   then assign this-object:SalesRepID   = fieldhandle:buffer-value.
          when 'ShipDate'     then assign this-object:ShipDate     = date(fieldhandle:buffer-value).
          when 'Terms'        then assign this-object:Terms        = fieldhandle:buffer-value.
        end case.
        
      end.
      
      this-object:Imported().
      
      finally:
        tablequery:get-next().
      end finally.
    end.
    
    finally:
      delete object tablequery.
      delete object tttablehandle.
    end finally.
    
  end method.
  
	/** Load db into model
		* @param 
		* @return 
		*/     
		
  method public void AppServerLoad():
    this-object:all().
  end method.

  /** Load db into model
    * @param 
    * @return 
    */     
    
  method public void AppServerSave():
  end method.
  		
  
  /** gets a Order using FindUsingCustNum
   *
   * @param  p_CustNum - integer
   * @return sports.Model.Order
   */
  method public sports.Model.Order FindUsingCustNum( p_CustNum as integer):
    return FindUsingCustNum( p_CustNum,"").
  end method.
  
  method public sports.Model.Order FindUsingCustNum( p_CustNum as integer,p_Sort as char):
    FindWhere(substitute("where _Order.Cust-Num eq &1 ",quoter(p_CustNum)),p_Sort).
    
    if not this-object:available then
    do:
      assign this-object:CustNum = p_CustNum.
    end.
    
    return cast(this-object,sports.Model.Order).
  end method.
  
  /** gets a Order using FindUsingCustNumOrderNum
   *
   * @param  p_CustNum - integer, p_OrderNum - integer
   * @return sports.Model.Order
   */
  method public sports.Model.Order FindUsingCustNumOrderNum( p_CustNum as integer, p_OrderNum as integer):
    return FindUsingCustNumOrderNum( p_CustNum, p_OrderNum,"").
  end method.
  
  method public sports.Model.Order FindUsingCustNumOrderNum( p_CustNum as integer, p_OrderNum as integer,p_Sort as char):
    FindWhere(substitute("where _Order.Cust-Num eq &1  and _Order.Order-num eq &2 ",quoter(p_CustNum),quoter(p_OrderNum)),p_Sort).
    
    if not this-object:available then
    do:
      assign this-object:CustNum  = p_CustNum
             this-object:OrderNum = p_OrderNum.
    end.
    
    return cast(this-object,sports.Model.Order).
  end method.
  
  /** gets a Order using FindUsingOrderDate
   *
   * @param  p_OrderDate - date
   * @return sports.Model.Order
   */
  method public sports.Model.Order FindUsingOrderDate( p_OrderDate as date):
    return FindUsingOrderDate( p_OrderDate,"").
  end method.
  
  method public sports.Model.Order FindUsingOrderDate( p_OrderDate as date,p_Sort as char):
    FindWhere(substitute("where _Order.Order-Date eq &1 ",quoter(p_OrderDate)),p_Sort).
    
    if not this-object:available then
    do:
      assign this-object:OrderDate = p_OrderDate.
    end.
    
    return cast(this-object,sports.Model.Order).
  end method.
  
  /** gets a Order using FindUsingOrderNum
   *
   * @param  p_OrderNum - integer
   * @return sports.Model.Order
   */
  method public sports.Model.Order FindUsingOrderNum( p_OrderNum as integer):
    return FindUsingOrderNum( p_OrderNum,"").
  end method.
  
  method public sports.Model.Order FindUsingOrderNum( p_OrderNum as integer,p_Sort as char):
    FindWhere(substitute("where _Order.Order-num eq &1 ",quoter(p_OrderNum)),p_Sort).
    
    if not this-object:available then
    do:
      assign this-object:OrderNum = p_OrderNum.
    end.
    
    return cast(this-object,sports.Model.Order).
  end method.
  
  /** gets a Order using FindUsingSalesRepID
   *
   * @param  p_SalesRepID - character
   * @return sports.Model.Order
   */
  method public sports.Model.Order FindUsingSalesRepID( p_SalesRepID as character):
    return FindUsingSalesRepID( p_SalesRepID,"").
  end method.
  
  method public sports.Model.Order FindUsingSalesRepID( p_SalesRepID as character,p_Sort as char):
    FindWhere(substitute("where _Order.Sales-Rep eq &1 ",quoter(p_SalesRepID)),p_Sort).
    
    if not this-object:available then
    do:
      assign this-object:SalesRepID = p_SalesRepID.
    end.
    
    return cast(this-object,sports.Model.Order).
  end method.
  
  method public void Validate():
  
  	ValidateCustNumOrderNum().
  end method.
  /** ensure that CustNumOrderNum not used elsewhere 
     * @param none
     * @return void 
     */
    
    method public void ValidateCustNumOrderNum():
      def var test as sports.Model.Order no-undo.
      
      test = new sports.Model.Order().
      
      
      /** try to find another record with CustNumOrderNum */
      test:FindUsingCustNumOrderNum( this-object:CustNum, this-object:OrderNum).
      
      if test:available and this-object:OrderNum ne test:OrderNum  then undo, throw new AppError("CustNumOrderNum already in use",0). 
    end method.
  /** remove record on foreign key  deletion
   *
   */
  
  method public void RemoveCustomer():
    this-object:Remove().
  end method.
  
  /** nullifies foreign key
   *
   */
  
  method public void RemoveSalesrep():
    assign this-object:SalesRepID = ?
           this-object:HasValidSalesrep = no.
    
    this-object:save().
  end method.

end class.
@BuildInfo(Source="Order").
@BuildInfo(Class="sports._Maia.Order").
@BuildInfo(Date="10/09/2013 10:56:48.184+01:00").
@BuildInfo(MD5Hash="TG8bJ0npNKOZvnxmAaBZhA==").