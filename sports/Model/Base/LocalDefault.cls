/*
Copyright (C) 2011 by Julian Lyndon-Smith (julian+mit@dotr.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS for A PARTICULAR PURPOSE and NONINFRINGEMENT. IN no EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE for ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR otherwise, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/


using Progress.Lang.*.
using sports.Model.*.
using Progress.Data.*.

routine-level on error undo, throw.

class sports.Model.Base.LocalDefault abstract :
  def private property DataProvider as dotr.ActiveRecord.Interface.IDataProvider no-undo get . private set.
  
  def private property AvailableFlag as logical no-undo get . set .

  def public property buffer as handle no-undo 
    get():
      return DataProvider:buffer.
    end get . private set.

  def public property query as handle no-undo 
    get():
      return DataProvider:query.
    end get . private set.
  
  def public property rowid as rowid  get. set. /* must be undo, as any error on creation must be reset to ? */
  
  def public property PrivateData  as char no-undo get. set.
  def public property ErrorMessage as char no-undo get. set.
  
  def public property DataFileName as char no-undo get. protected set.
  def public property UpdateType   as char no-undo get. private   set.
  
  def public property SendStompUpdate as logical no-undo init yes get . set .
  def public property InTestMode      as logical no-undo          get . set .
  def public property Preselect			  as logical no-undo          get . set .
  
  def public property Available as logical no-undo
    get():
      return rowid ne ?.
    end get . protected set.
    
  def public property NumRecords as int no-undo
    get():
      return this-object:DataProvider:query:num-results.
    end get . protected set.

  def public property Country        as character no-undo  get .  set .  
  def public property CurrencySymbol as character no-undo  get .  set .  
  def public property DateFormat     as character no-undo  get .  set .  
  def public property PostalFormat   as character no-undo  get .  set .  
  def public property PostalLabel    as character no-undo  get .  set .  
  def public property Region1Label   as character no-undo  get .  set .  
  def public property Region2Label   as character no-undo  get .  set .  
  def public property TelFormat      as character no-undo  get .  set .  
  
  
  
  constructor LocalDefault():
    StartUp().
  end constructor.
  
  destructor LocalDefault():
    if valid-object(this-object:DataProvider) then delete object this-object:DataProvider.
  
    
  end destructor.
  
  /** bind a progress data binding source to the query
   * @param binding source to use
   * @return return value
   */

  method public void Bind(p_BindingSource as Progress.Data.BindingSource):
    p_BindingSource:handle = this-object:DataProvider:query.
  end method.  

  /** Creates the buffer handle and buffer fields 
   */

  method protected void StartUp():
    if valid-handle(dotr.ActiveRecord.Partition:AppServer) and dotr.ActiveRecord.Partition:AppServer ne session:handle  
       then this-object:DataProvider = new dotr.ActiveRecord.ServiceProvider("Local-Default").
       else this-object:DataProvider = new dotr.ActiveRecord.DataAccess("Local-Default").    
  end method.
  
  
    /** gets the LocalDefault using the rowid 
     *  @param p_LocalDefaultRowID : the rowid to find
     */
  
    method public sports.Model.LocalDefault find(p_LocalDefaultRowID as rowid):
      this-object:DataProvider:FetchUnique(p_LocalDefaultRowID).
      return this-object:Get().  
    end.
   

  /** stub method to allow for override
   */

  method public void AfterGet():
  end method.

 /** stub method to allow for override
   */

  method public void BeforeGet():
  end method.

 /** stub method to allow for override
  */

  method public void BeforeCreate():
  end method.
  
 /** stub method to allow for override
  */

  method public void AfterCreate():
  end method.

 /** stub method to allow for override
  */

  method public void BeforeUpdate():
  end method.
  
 /** stub method to allow for override
  */

  method public void AfterUpdate():
  end method.
  
 /** stub method to allow for override
  */

  method public void BeforeRemove():
  end method.
  
 /** stub method to allow for override
  */

  method public void AfterRemove():
  end method.

 /** stub method to allow for override
  */
  method public void NotFound():
  end method.

  /** stub method to allow for override
   */

  method public void Imported():
    this-object:save().
  end method.

  /** stub method to allow for override
  */
  method public void AfterTransaction():
  end method.
  
  /** assigns the buffer fields to the object
   *  @return sports.Model.LocalDefault  
   */

  method protected sports.Model.LocalDefault get():
    BeforeGet().
    assign this-object:rowid         = ?
           this-object:UpdateType    = "find".
        
    if not this-object:buffer:available then
    do: 
      NotFound().
      return cast(this-object,sports.Model.LocalDefault). 
    end.

    assign this-object:Country         = buffer::Country
           this-object:CurrencySymbol = buffer::Currency-Symbol
           this-object:DateFormat     = buffer::Date-Format
           this-object:PostalFormat   = buffer::Postal-Format
           this-object:PostalLabel    = buffer::Postal-Label
           this-object:Region1Label   = buffer::Region1-Label
           this-object:Region2Label   = buffer::Region2-Label
           this-object:TelFormat      = buffer::Tel-Format
           this-object:rowid           = buffer:rowid.
    
    
           
    AfterGet().
    
    return cast(this-object,sports.Model.LocalDefault). 
  end.
  
  /** open a query for all records in the <<Table>> table
   */
  method public sports.Model.LocalDefault all():
    return this-object:FindWhere("").
  end method.

  /** opens a query for records matching the p_Where clause
   *  @param p_Where : the where statement to use  
   *  @return sports.Model.LocalDefault  
   */
  
  method public sports.Model.LocalDefault FindWhere (p_Where as char):
    return FindWhere(p_Where,"").
  end method.

  /** opens a query for records matching the p_Where clause
   *  @param p_Where : the where statement to use  
   *  @param p_Sort : the sort statement to use  
   *  @return sports.Model.LocalDefault  
   */
  
  method public sports.Model.LocalDefault FindWhere (p_Where as char,p_Sort as char):
    def var lv_Query as char no-undo.
    
    assign lv_Query = substitute("&3 each _Local-Default &1 no-lock &2",
                                                if trim(p_Where) begins "where " 
                                                   then p_Where
                                                   else if p_Where ne "" then substitute("where &1",p_Where)
                                                   else "",
                                                p_Sort,
                                                if this-object:preselect then "preselect" else "for").
    this-object:DataProvider:FetchData(lv_query).
    
    return this-object:First().                                                        
  end method.
  
	/** get next record, return available status
		* @return record available 
		*/  
  method public logical NextAvailable():
    if this-object:AvailableFlag then this-object:Next().
    assign this-object:AvailableFlag = yes.
    return this-object:available.
  end method.
  
  /** opens a query for records matching the p_Where clause
   *  @param p_Where : the where statement to use  
   *  @return sports.Model.LocalDefault  
   */
  
  method public sports.Model.LocalDefault preselect (p_Where as char):
    return preselect(p_Where,"").
  end method.

  /** opens a query for records matching the p_Where clause
   *  @param p_Where : the where statement to use  
   *  @param p_Sort : the sort statement to use  
   *  @return sports.Model.LocalDefault  
   */
  
  method public sports.Model.LocalDefault preselect (p_Where as char,p_Sort as char):
    def var lv_Query as char no-undo.

    assign lv_query = substitute("preselect each _Local-Default &1 no-lock &2",
                                                if trim(p_Where) begins "where " 
                                                   then p_Where
                                                   else if p_Where ne "" then substitute("where &1",p_Where)
                                                   else "",
                                                   p_Sort).

    this-object:DataProvider:FetchData(lv_Query).
    
    return this-object:First().                                                        
  end method.
  
  /** gets the first LocalDefault of the query
   *  @return sports.Model.LocalDefault  
   */

  method public sports.Model.LocalDefault first():
    if this-object:DataProvider:query:is-open then this-object:DataProvider:query:get-first().
    assign this-object:AvailableFlag = no.
    return this-object:Get().
  end method.
    
  /** gets the last LocalDefault of the query
   *  @return sports.Model.LocalDefault  
   */

  method public sports.Model.LocalDefault last():
    if this-object:DataProvider:query:is-open then this-object:DataProvider:query:get-last().
    return this-object:Get().
  end method.

  /** gets the next LocalDefault of the query
   *  @return sports.Model.LocalDefault  
   */

  method public sports.Model.LocalDefault next():
    if this-object:DataProvider:query:is-open then this-object:DataProvider:query:get-next().
    return this-object:Get().
  end method.

  /** gets the prev LocalDefault of the query
   *  @return sports.Model.LocalDefault  
   */

  method public sports.Model.LocalDefault prev():
    if this-object:DataProvider:query:is-open then this-object:DataProvider:query:get-prev().
    return this-object:Get().
  end method.

  /** saves the object attributes to the database
   *  @return sports.Model.LocalDefault  
   */
  
  method public sports.Model.LocalDefault save():
    WriteRecord(if this-object:rowid eq ? then "CREATE" else "UPDATE").
    return cast(this-object,sports.Model.LocalDefault). 
  end method.

  /** removes the object from the database
   */
 
  method public void Remove():
    if not this-object:available then return.
    WriteRecord("Remove").
    return. 
  end method. 

  /** writes the approriate changes to the database
   *  @param p_Mode : create (creates a new <<table>>
   *                : update (saves the changes to the database)
   *                : remove (removes the record from the database)
   */
    
  method protected final void WriteRecord(p_Mode as char):
    def var lv_new as logical no-undo.
    
    def var hUpdateBuffer as handle no-undo.
    
    create buffer hUpdateBuffer for table this-object:buffer 
                                    buffer-name this-object:buffer:name.
    
    assign lv_new                 = this-object:rowid eq ?
           this-object:UpdateType = p_mode.
    
    
    /** try to lock the record, give up after 5 seconds */
    repeat while true stop-after 5 transaction on error undo, throw
                                               on stop undo, leave:

      if p_mode eq "CREATE" then 
      do:
        BeforeCreate(). /** override this method if you want to do stuff before the create statement */ 

        hUpdateBuffer:buffer-create().
        
        
        
        
        assign this-object:rowid = hUpdateBuffer:rowid.
               
        AfterCreate().  /** override this method if you want to do stuff after the create statement */
      end.
      
      else 
      do:
        hUpdateBuffer:find-by-rowid(this-object:rowid,exclusive-lock,no-wait) no-error.
   
        if hUpdateBuffer:locked then next.
        if not hUpdateBuffer:avail then undo, throw new AppError(substitute("Could not find Local-Default [&1]",this-object:rowid),0).
      end.
      
      case p_Mode:
        when "Update" or when "Create" then
        do:
          BeforeUpdate().  /** override this method if you want to do stuff before the update statement */
          Validate().
          assign hUpdateBuffer::Country         = this-object:Country
                 hUpdateBuffer::Currency-Symbol = this-object:CurrencySymbol
                 hUpdateBuffer::Date-Format     = this-object:DateFormat
                 hUpdateBuffer::Postal-Format   = this-object:PostalFormat
                 hUpdateBuffer::Postal-Label    = this-object:PostalLabel
                 hUpdateBuffer::Region1-Label   = this-object:Region1Label
                 hUpdateBuffer::Region2-Label   = this-object:Region2Label
                 hUpdateBuffer::Tel-Format      = this-object:TelFormat.
          AfterUpdate().  /** override this method if you want to do stuff after the update statement */
        end.
        
        when "Remove" then
        do:
          BeforeRemove().  /** override this method if you want to do stuff before the remove statement */
          hUpdateBuffer:buffer-delete().
          assign this-object:rowid = ?.
          
          AfterRemove().  /** override this method if you want to do stuff after the remove statement */
        end.
        
      end case.
      
      
      hUpdateBuffer:buffer-release().
      
      leave.
      
      catch e as Progress.Lang.Error :
        case e:GetMessageNum(1):
          when 132 then undo, throw new AppError(e:getMessage(1),132).
          otherwise     undo, throw new AppError(e:GetMessage(1),0).
        end case.
      end catch.
    end.
    
    AfterTransaction(). /** override this method if you want to do stuff after the (sub)transaction has ended */
    
    this-object:DataProvider:SaveData(). /** does nothing if local, sends to appserver if not */
    
    return.
    
    finally:
      delete object hUpdateBuffer.
    end finally.
    
  end method.
  
  /** Export ActiveRecord as Json */ 
  method public longchar Export():
    return ExportData("json","single").
  end method.

  /** Export ActiveRecord as Json */ 
  method public longchar ExportQuery():
    return ExportData("json","all").
  end method.
  
  /** take current ActiveRecord and convert to longchar
   *  
   *  gets the current object, converts into a temp-table
   *  and exports the data */ 
  
  method private longchar ExportData(p_format as char,p_Mode as char):
    
    return (new sports.Model.TempTable.LocalDefault()):ExportData(p_format,p_mode,cast(this-object,sports.Model.LocalDefault)).
    
  end method. 
  
  /** take a json string and assign properties to LocalDefault
   *  
   */ 
  
  method public void Import(p_json as longchar):
    this-object:import("json",p_json).
  end method.
  
  /** take a json string and assign properties to LocalDefault
   *  
   */ 
  
  method public void Import(p_format as char,p_json as longchar):
    def var tttablehandle as handle no-undo.
    def var tablehandle   as handle no-undo.
    def var tablequery    as handle no-undo.
    def var fieldhandle   as handle no-undo.
    
    def var lv_i as int no-undo.
    
    create temp-table tttablehandle.
    
    tttablehandle:read-json("longchar",p_Json,"empty").
    
    assign tablehandle = tttablehandle:default-buffer-handle.
    
    create query tablequery.
    
    tablequery:add-buffer (tablehandle).

    tablequery:query-prepare(substitute("for each &1 no-lock",tablehandle:name)).
    tablequery:query-open().
    tablequery:get-first().
    
    do while not tablequery:query-off-end on error undo, throw:
      next.
      
      do lv_i = 1 to tablehandle:num-fields:
        assign fieldhandle = tablehandle:buffer-field(lv_i).
        
        case fieldhandle:name:
          when 'Country'        then assign this-object:Country        = fieldhandle:buffer-value.
          when 'CurrencySymbol' then assign this-object:CurrencySymbol = fieldhandle:buffer-value.
          when 'DateFormat'     then assign this-object:DateFormat     = fieldhandle:buffer-value.
          when 'PostalFormat'   then assign this-object:PostalFormat   = fieldhandle:buffer-value.
          when 'PostalLabel'    then assign this-object:PostalLabel    = fieldhandle:buffer-value.
          when 'Region1Label'   then assign this-object:Region1Label   = fieldhandle:buffer-value.
          when 'Region2Label'   then assign this-object:Region2Label   = fieldhandle:buffer-value.
          when 'TelFormat'      then assign this-object:TelFormat      = fieldhandle:buffer-value.
        end case.
        
      end.
      
      this-object:Imported().
      
      finally:
        tablequery:get-next().
      end finally.
    end.
    
    finally:
      delete object tablequery.
      delete object tttablehandle.
    end finally.
    
  end method.
  
	/** Load db into model
		* @param 
		* @return 
		*/     
		
  method public void AppServerLoad():
    this-object:all().
  end method.

  /** Load db into model
    * @param 
    * @return 
    */     
    
  method public void AppServerSave():
  end method.
  		
  
  method public void Validate():
  
  end method.
  
  

end class.
@BuildInfo(Source="LocalDefault").
@BuildInfo(Class="sports._Maia.Local-Default").
@BuildInfo(Date="11/06/2013 14:29:56.645+01:00").
@BuildInfo(MD5Hash="HBclTC6EobCP7ARdFKw8kQ==").