/*
Copyright (C) 2011 by Julian Lyndon-Smith (julian+mit@dotr.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS for A PARTICULAR PURPOSE and NONINFRINGEMENT. IN no EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE for ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR otherwise, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/


routine-level on error undo, throw.

def input parameter p_token as char no-undo.
def input parameter p_mode  as char no-undo.
def input parameter p_data as longchar no-undo.

def output parameter op_json as longchar no-undo.

def var Parameters as dotr.ActiveRecord.ParseParameters no-undo.

/** insert code here to get session authentication */
Parameters = (new dotr.ActiveRecord.ParseParameters()):Json(p_data).

run value(p_mode) .

return.

procedure get:
  def var Customer1 as sports.Model.Customer no-undo.

  run findCustomer(output Customer1).

  assign op_json = Customer1:ExportQuery().
  
end procedure.

procedure post:
  (new sports.Model.Customer()):Import(p_data).

  finally:
    assign op_json = '~{"success":"ok"}'.
  end finally.
  
end procedure.

procedure delete:
  def var Customer1 as sports.Model.Customer no-undo.
  
  run findCustomer(output Customer1).

  Customer1:remove().

  finally:
    assign op_json = Customer1:Export().
  end finally.
  
end procedure.

procedure findCustomer:
  def output parameter Customer1 as sports.Model.Customer no-undo.   

  def var lv_find as char no-undo.

  assign lv_find = parameters:data::FindUsing no-error.

  case lv_find:
    
    when "FindContainingComments" then Customer1 = (new sports.Model.Customer()):FindContainingComments( Parameters:data::Comments).
    
    when "FindUsingCountry" then Customer1 = (new sports.Model.Customer()):FindUsingCountry( Parameters:data::Country).
    
    when "FindUsingCountryPostalCode" then Customer1 = (new sports.Model.Customer()):FindUsingCountryPostalCode( Parameters:data::Country, Parameters:data::PostalCode).
    
    when "FindUsingCustNum" then Customer1 = (new sports.Model.Customer()):FindUsingCustNum( integer(Parameters:data::CustNum)).
    
    when "FindUsingName" then Customer1 = (new sports.Model.Customer()):FindUsingName( Parameters:data::Name).
    
    when "FindUsingSalesRepID" then Customer1 = (new sports.Model.Customer()):FindUsingSalesRepID( Parameters:data::SalesRepID).
    
    when "FindUsingStateID" then Customer1 = (new sports.Model.Customer()):FindUsingStateID( Parameters:data::StateID).
    
/*    otherwise Customer1 = (new sports.Model.Customer()):Find(integer(Parameters:data::CustNum)).*/
  end case.

  return.
  
end procedure.

finally:
  delete object Parameters no-error.
end finally.
@BuildInfo(Source="Customer").
@BuildInfo(Class="sports._Maia.Customer").
@BuildInfo(Date="11/06/2013 14:29:56.496+01:00").
@BuildInfo(MD5Hash="5CSFULBjebB0T4aQZTztEA==").