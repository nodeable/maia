/*
 dot.r limited  CONFIDENTIAL
 Unpublished Copyright (c) 1991-2013 dot.r limited Limited, All Rights Reserved.

 NOTICE:  All information contained herein is, and remains the property of dot.r limited Limited. The intellectual and technical concepts contained
 herein are proprietary to dot.r limited Limited and are protected by trade secret or copyright law.
 Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
 from dot.r limited Limited.  Access to the source code contained herein is hereby forbidden to anyone except current dot.r limited Limited employees, managers or contractors who have executed 
 Confidentiality and Non-disclosure agreements explicitly covering such access.

 The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes  
 information that is confidential and/or proprietary, and is a trade secret, of dot.r limited Limited.   
 
 ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE 
 WITHOUT THE EXPRESS WRITTEN CONSENT OF dot.r Limited IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES.  
 THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS  
 TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
*/
using Progress.Lang.*.

routine-level on error undo, throw.

class dotr.ActiveRecord.Partition : 
	
  def static property AppServer as handle no-undo get . set .

end class.