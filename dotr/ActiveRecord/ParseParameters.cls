 
 /*------------------------------------------------------------------------
    File        : ParseParameters
    Purpose     : 
    Syntax      : 
    Description : 
    Author(s)   : jmls
    Created     : Sun Mar 31 12:52:42 BST 2013
    Notes       : 
  ----------------------------------------------------------------------*/

using Progress.Lang.*.

routine-level on error undo, throw.

class dotr.ActiveRecord.ParseParameters:
  
  def public property data as handle no-undo get . private set. 

  method public dotr.ActiveRecord.ParseParameters Json(p_json as longchar):
    def var hJsonDataSet as handle no-undo.
    def var hquery       as handle no-undo.
    
    create temp-table hJsonDataSet.
    
    hJsonDataSet:read-json("longchar",trim(p_json),"empty").
    
    assign this-object:data = hJsonDataSet:default-buffer-handle.
     
    create query hquery.
    
    hquery:add-buffer (this-object:data).

    hquery:query-prepare(substitute("for each &1 no-lock",this-object:data:name)).
    hquery:query-open().
    hquery:get-first().
     
    return this-object.
  end method.
  
end class.