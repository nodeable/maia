/*
 dot.r limited  CONFIDENTIAL
 Unpublished Copyright (c) 1991-2013 dot.r limited Limited, All Rights Reserved.

 NOTICE:  All information contained herein is, and remains the property of dot.r limited Limited. The intellectual and technical concepts contained
 herein are proprietary to dot.r limited Limited and are protected by trade secret or copyright law.
 Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
 from dot.r limited Limited.  Access to the source code contained herein is hereby forbidden to anyone except current dot.r limited Limited employees, managers or contractors who have executed 
 Confidentiality and Non-disclosure agreements explicitly covering such access.

 The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes  
 information that is confidential and/or proprietary, and is a trade secret, of dot.r limited Limited.   
 
 ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE 
 WITHOUT THE EXPRESS WRITTEN CONSENT OF dot.r Limited IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES.  
 THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS  
 TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
*/

using Progress.Lang.*.

routine-level on error undo, throw.

class dotr.ActiveRecord.DataSet: 
  
  def private property FirstTempTable as dotr.ActiveRecord.TempTable no-undo get . private set.
  
  def private property DataSetName as char no-undo get . private set.
  
  def public property handle as handle no-undo get . private set.
  
  constructor DataSet():
  end constructor.

  constructor DataSet(p_Name as char):
    assign this-object:DataSetName = p_Name.
  end constructor.
  
  destructor DataSet():
  end destructor.   

  method public dotr.ActiveRecord.DataSet Create():
    create dataset this-object:handle.
    assign this-object:handle:name = this-object:DataSetName.
    return this-object.
  end method.

  method public dotr.ActiveRecord.DataSet AddTable(p_Table as char):
    def var TempTable as dotr.ActiveRecord.TempTable no-undo.
    
    TempTable = (new dotr.ActiveRecord.TempTable(p_Table,this-object:handle)):Create().
    
    assign FirstTempTable = TempTable when not valid-object(FirstTempTable).
    
    return this-object.
  end method.
  
  method public dotr.ActiveRecord.DataSet Fill(p_query as char):
    FirstTempTable:fill(p_query).
    return this-object.
  end method.
  
  method public void SaveChanges(p_Dataset as handle):
    def var lv_i as int no-undo.
    
    def var lv_buffer as handle no-undo.
    
    do lv_i = 1 to p_DataSet:num-top-buffers:
      lv_buffer = p_DataSet:get-top-buffer(lv_i).
 
      if lv_buffer:parent-relation ne ? then next.
 
      TraverseBuffer(lv_buffer).
    end.    
  end method.
  
  method private void TraverseBuffer(p_buffer as handle):
    def var lv_i as int no-undo.
   
    saveBuffer(p_buffer).

    do lv_i = 1 to p_buffer:num-child-relations:
      traverseBuffer(p_buffer:get-child-relation(lv_i):child-buffer).
    end.     
  end method.
  
  method private void SaveBuffer(p_buffer as handle):
    
    def var lv_BeforeBuffer as handle no-undo.
    def var lv_BeforeQuery  as handle no-undo.
    def var lv_DataSource   as handle no-undo.
    def var lv_DBBuffer     as handle no-undo.
    
    lv_BeforeBuffer = p_Buffer:before-buffer.
    
    if valid-handle(lv_BeforeBuffer) then 
    do on error undo, throw:
   
      create buffer lv_DBBuffer for table substring(p_Buffer:name,2).
   
      create data-source lv_datasource.
      lv_datasource:add-source-buffer(lv_DBbuffer,lv_DBbuffer:keys).
      
      p_Buffer:attach-data-source(lv_datasource).
   
      create query lv_BeforeQuery.
      lv_BeforeQuery:add-buffer(lv_BeforeBuffer).
      lv_BeforeQuery:query-prepare(substitute("for each &1" + lv_BeforeBuffer:name)).
      lv_BeforeQuery:query-open().
      lv_BeforeQuery:get-first().
      
      do while not lv_BeforeQuery:query-off-end:
        lv_BeforeBuffer:save-row-changes().
        if lv_BeforeBuffer:error then assign lv_BeforeBuffer:rejected = true.
        lv_BeforeQuery:get-next().
      end.  
      
      CATCH e AS Progress.Lang.Error :
        message "here" e:getmessage(1) view-as alert-box.
      end catch.

      finally:
        delete object lv_BeforeQuery.
        delete object lv_DataSource.
      end finally.
    end. 
        
  end method.
  
end class.