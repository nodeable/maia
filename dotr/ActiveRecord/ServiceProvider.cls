/*
Copyright (c) 2013, Julian Lyndon-Smith (julian+maia@dotr.com)
http://www.dotr.com
All rights reserved.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction except as noted below, including without limitation
the rights to use,copy, modify, merge, publish, distribute,
and/or sublicense, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

 The Software and/or source code cannot be copied in whole and
 sold without meaningful modification for a profit.

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.

 Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in
 the documentation and/or other materials provided with
 the distribution.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/


using Progress.Lang.*.

routine-level on error undo, throw.

class dotr.ActiveRecord.ServiceProvider implements dotr.ActiveRecord.Interface.IDataProvider:
  def var TTDataSet as handle no-undo.
  def var TableName as char no-undo.
  
  def property Buffer as handle no-undo get . private set.
  def property query  as handle no-undo get . private set.
  
  constructor ServiceProvider(p_Table as char):
    assign TableName = p_Table.
  end constructor.
  
  destructor ServiceProvider():
    delete object TTDataSet no-error.
    delete object this-object:query no-error.
    delete object this-object:buffer no-error.
  end destructor.
  
	/** load data from db
		*/      

  method public void FetchData(p_Query as char):
    delete object this-object:query no-error.
    delete object this-object:buffer no-error.
    
    if not valid-handle(TTDataSet) then  
       run dotr/ActiveRecord/FetchData.p on (dotr.ActiveRecord.Partition:AppServer) (TableName,p_Query,output dataset-handle TTDataSet by-reference).
    
    assign this-object:Buffer                               = TTDataSet:get-buffer-handle[1]
           this-object:Buffer:table-handle:tracking-changes = true.
            
    create query this-object:Query.
    
    this-object:Query:add-buffer (this-object:buffer).
    
    this-object:Query:query-prepare(substitute("for each _&1 no-lock",TableName)).    
    this-object:Query:query-open().
    
    return .
  end method.
  
   /** load single record from db
    */      

  method public void FetchUnique(p_Query as char):
    FetchData(substitute("for each _&1 &2 no-lock",TableName,p_query)).
    this-object:Query:get-first().
  end method.
  
  /** load single record from db
    */      

  method public void FetchUnique(p_rowid as rowid):
    this-object:buffer:find-by-rowid(p_rowid,no-lock) no-error.    
  end method.
  
	/** save data
	  * placeholder, as all saving is done in model in this construct
		*/
  method public void SaveData():
    def var Changes as handle no-undo.
    
    create dataset Changes.
    
    Changes:create-like(TTDataSet).
    Changes:get-changes(TTDataSet).
    
    run dotr/ActiveRecord/SaveData.p on (dotr.ActiveRecord.Partition:AppServer) (TableName,input-output dataset-handle Changes by-reference).
    
    this-object:Buffer:table-handle:tracking-changes = false.
    
    Changes:merge-changes (TTDataSet).
    
    this-object:Buffer:table-handle:tracking-changes = true.
    TTDataSet:get-buffer-handle[1]:synchronize ().
    
    finally:
      delete object Changes no-error.
    end finally.
    
  end method.
  		    
end class.