/*
Copyright (c) 2013, Julian Lyndon-Smith (julian+maia@dotr.com)
http://www.dotr.com
All rights reserved.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction except as noted below, including without limitation
the rights to use,copy, modify, merge, publish, distribute,
and/or sublicense, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

 The Software and/or source code cannot be copied in whole and
 sold without meaningful modification for a profit.

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.

 Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in
 the documentation and/or other materials provided with
 the distribution.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/


using Progress.Lang.*.

routine-level on error undo, throw.

class dotr.ActiveRecord.DataAccess implements dotr.ActiveRecord.Interface.IDataProvider:
  def property Buffer as handle no-undo get . private set.
  def property query  as handle no-undo get . private set.
  
  constructor DataAccess(p_table as char):
    create buffer Buffer for table p_table buffer-name "_" + p_table.
    create query this-object:Query.
    
    this-object:Query:add-buffer (this-object:buffer).
    
  end constructor.
  
  destructor DataAccess():
    if valid-handle(this-object:Query) then delete object this-object:Query.
    if valid-handle(this-object:buffer) then delete object this-object:Buffer.
  end destructor.
  
	/** load data from db
		*/      

  method public void FetchData(p_Query as char):
    this-object:Query:query-prepare(p_Query).    
    this-object:Query:query-open().
  end method.

  /** load single record from db
    */      

  method public void FetchUnique(p_Query as char):
    if this-object:Query:is-open then this-object:Query:query-close().
    this-object:buffer:find-unique(p_Query,no-lock) no-error.    
  end method.
  
  /** load single record from db
    */      

  method public void FetchUnique(p_rowid as rowid):
    if this-object:Query:is-open then this-object:Query:query-close().
    this-object:buffer:find-by-rowid(p_rowid,no-lock) no-error.    
  end method.
  

	/** save data
	  * placeholder, as all saving is done in model in this construct
		*/
  method public void SaveData():
  end method.
  		    
end class.