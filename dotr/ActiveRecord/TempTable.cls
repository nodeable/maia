/*
 dot.r limited  CONFIDENTIAL
 Unpublished Copyright (c) 1991-2013 dot.r limited Limited, All Rights Reserved.

 NOTICE:  All information contained herein is, and remains the property of dot.r limited Limited. The intellectual and technical concepts contained
 herein are proprietary to dot.r limited Limited and are protected by trade secret or copyright law.
 Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
 from dot.r limited Limited.  Access to the source code contained herein is hereby forbidden to anyone except current dot.r limited Limited employees, managers or contractors who have executed 
 Confidentiality and Non-disclosure agreements explicitly covering such access.

 The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes  
 information that is confidential and/or proprietary, and is a trade secret, of dot.r limited Limited.   
 
 ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE CODE 
 WITHOUT THE EXPRESS WRITTEN CONSENT OF dot.r Limited IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES.  
 THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS  
 TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
*/
using Progress.Lang.*.

routine-level on error undo, throw.

class dotr.ActiveRecord.TempTable: 
  def public property handle   as handle no-undo get . private set.
  def public property DBBuffer as handle no-undo get . private set.
  def public property query    as handle no-undo get . private set.
  
  def private property TTbuffer   as handle no-undo get . private set.
  def private property dataset    as handle no-undo get . private set.
  def private property datasource as handle no-undo get . private set.
  
  def private property TableName     as char no-undo get . private set.
  
  constructor TempTable(p_Table as char):
    assign this-object:TableName = p_Table.
  end constructor.

  constructor TempTable(p_Table as char,p_DataSet as handle):
    assign this-object:TableName = p_Table
           this-object:dataset   = p_dataset.
  end constructor.
  
  destructor TempTable():
  end destructor. 

  method public dotr.ActiveRecord.TempTable Create():
    
    create temp-table this-object:handle.
    
    create buffer this-object:DBBuffer for table TableName.

    this-object:handle:create-like(this-object:DBbuffer).
    
    this-object:handle:temp-table-prepare("_" + TableName).
    
    assign this-object:TTbuffer = this-object:handle:default-buffer-handle.

    if valid-handle(this-object:dataset) then 
    do:
      this-object:dataset:add-buffer(this-object:TTbuffer).
      create data-source this-object:datasource.
      this-object:datasource:add-source-buffer(this-object:DBbuffer,this-object:DBbuffer:keys).
      this-object:TTbuffer:attach-data-source(this-object:datasource).
    end.
    
    return this-object.
  end method. 
   
  method public dotr.ActiveRecord.TempTable Fill(p_Query as char):

    assign p_query = replace(p_Query,"_" + TableName,TableName).
    
    create query this-object:query.

    this-object:query:set-buffers (this-object:DBBuffer).

    this-object:query:query-prepare(p_Query).
    
    if valid-handle(this-object:dataset) then
    do:
      this-object:datasource:query = this-object:query.
      this-object:dataset:fill().
      delete object this-object:DBBuffer    no-error.
      delete object this-object:datasource no-error.
      
      return this-object.
    end.
    
    this-object:query:query-open().
    this-object:query:get-first().

    do while not this-object:query:query-off-end:
      TTbuffer:buffer-create().
      TTbuffer:buffer-copy(this-object:DBBuffer).
      this-object:query:get-next().
    end.    
    
    return this-object.
    
    finally:
      delete object this-object:query      no-error. 
    end finally.
    
  end method.
   
end class.