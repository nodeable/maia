/*
Copyright (c) 2013, Julian Lyndon-Smith (julian@dotr.com)
http://www.dotr.com
All rights reserved.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction except as noted below, including without limitation
the rights to use,copy, modify, merge, publish, distribute,
and/or sublicense, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

 The Software and/or source code cannot be copied in whole and
 sold without meaningful modification for a profit.

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.

 Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in
 the documentation and/or other materials provided with
 the distribution.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

using Progress.Lang.*.

routine-level on error undo, throw.

class dotr.Maia.Upgrade.scaffold: 

  method public void generate(p_base as char, p_db as char, p_table as char,p_overwrite as logical):
    def var lv_model as longchar no-undo.
    def var lv_base  as longchar no-undo.
    def var lv_code  as longchar no-undo.
    
    def var lv_db        as char no-undo.
    def var lv_package   as char no-undo.
    def var lv_dir       as char no-undo.
    def var lv_baseClass as char no-undo.

    def var lv_path  as char no-undo.
    def var lv_file  as char no-undo.
    def var lv_class as char no-undo.
  
    def var Slash as char no-undo.
 
    case true:
      when opsys begins "win" or
      when opsys begins "ms"  then assign slash = "~\".
      
      when opsys begins "unix" then assign slash = "/".
      otherwise assign slash = "/".
    end case.
      
    assign p_base = replace(p_base,os-getenv("ECLIPSE_ROOT"),"")
           lv_file = substr(p_base,r-index(p_base,slash) + 1)
           lv_Path = substr(p_base,1,r-index(p_base,slash) - 1).

    assign lv_Dir     = substitute("&1/_Maia",p_Base)
           lv_Package = replace(lv_dir,"/",".")
           lv_Package = replace(lv_Package,"~\",".").
      
    os-create-dir value(lv_dir).
      
    os-create-dir value (substitute("&1/_Maia/_Template",p_Base)).
        
    copy-lob from file search("dotr/Maia/Upgrade/_base.cls") to lv_base.
    
    assign lv_db        = p_db
           lv_base      = replace(lv_base,"/*!*/",substitute('assign this-object:dbname = "&1".',lv_db))
           lv_BaseClass = substitute("&1._base",lv_Package)
           lv_model = substitute("class &1.&2 inherits &3:~n",lv_Package,p_Table,lv_BaseClass) + 
                                 "  method public override void init():~n" +
                                 "    super:init().~n" +
                                 "  end method.~n" +
                                 "end class.". 

    if search(substitute("&1/_Maia/&2.cls",p_Base,p_Table)) ne ? and not p_overwrite then return.
    
    copy-lob from lv_model to file substitute("&1/_Maia/&2.cls",p_Base,p_Table).
        
    assign lv_base = replace(lv_base,"dotr.Maia.Upgrade._base",substitute("&1._base",lv_Package)).
    if search(substitute("&1/_Maia/_base.cls",p_Base)) eq ? then copy-lob from lv_base to file substitute("&1/_Maia/_base.cls",p_Base).
  end method.
end class.