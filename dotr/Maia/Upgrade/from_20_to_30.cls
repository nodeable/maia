/*
Copyright (c) 2013, Julian Lyndon-Smith (julian+maia@dotr.com)
http://www.dotr.com
All rights reserved.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction except as noted below, including without limitation
the rights to use,copy, modify, merge, publish, distribute,
and/or sublicense, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

 The Software and/or source code cannot be copied in whole and
 sold without meaningful modification for a profit.

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.

 Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in
 the documentation and/or other materials provided with
 the distribution.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

using Progress.Lang.*.

routine-level on error undo, throw.

class dotr.Maia.Upgrade.from_20_to_30:
  
define temp-table TTProject no-undo serialize-name "Project" 
      field Description                           as character 
      field Directory                             as character 
      field Name                                  as character 
      field Package                               as character 
      field ProjectGUID                           as character 
      field TemplateDirectory                     as character 
  
      field MagicField                            as logical 
      field UseActiveMQ                           as logical .

   
 def temp-table TTModel no-undo serialize-name "ObjectConfig"
    field MagicFields as logical      
    field ClassName   as char /* name of the class (base name only) [foo] */
    field TableName   as char /* table name of the class (base name only) [foo] */
    field KeyFieldName      as char 
    field KeyProperty as char 
    field KeyFieldDatatype as char 
    field modelName as char
    field modelPackage as char
    field Directory as char
    field Package as char
    field projectGUID as char
    field PropertyEditor as char
    field fkDelete as char
    field LogicalDB as char
    field BuildUnitGUID as char
    field inherits as char
    field BuildObject   as logical init yes /* do we build this class or not */
    field isAbstract    as logical init no 
    field TempTableOnly as logical init no
    
    field CRC           as int
    
    index main is primary unique 
      ProjectGUID
      ClassName. 

def temp-table TTProperty no-undo serialize-name "Property"  
      field AccessMode     as character 
      field AppPackage     as character 
      field Brief          as character 
      field BuildUnitGUID  as character 
      field ClassName      as character 
      field Comment        as character 
      field DataType       as character 
      field Description    as character 
      field FieldName      as character 
      field fkClassName    as character 
      field fkFindUsing    as character 
      field fkModelName    as character 
      field fkPropertyName as character 
      field Fluent         as character 
      field FormLabel      as character 
      field GetCode        as character 
      field GetMode        as character 
      field InitialValue   as character 
      field Name           as character 
      field PropertyGUID   as character 
      field PropertyName   as character 
      field Sequence       as character 
      field SetCode        as character 
      field SetMode        as character 
      field UIName         as character 
      field UIType         as character 
  
      field DisplayInForm   as integer 
      field DisplayInGrid   as integer 
      field DisplayInLookup as integer 
      field Extent          as integer 
      field WidthScale      as integer 
  
      field BuildProperty    as logical 
      field Checked          as logical 
      field fkDelete         as logical 
      field fkUpdate         as logical 
      field GenerateGUID     as logical 
      field IsArrayWordIndex as logical 
      field isForeignKey     as logical 
      field LoadfkProperty   as logical 
      field LoadLazy         as logical 
      field ManualProperty   as logical .

            
  def temp-table TTTemplate no-undo serialize-name "Template" 

      field Builder                               as character 
      field BuildUnitGUID                         as character 
      field Description                           as character 
      field Name                                  as character 
      field TemplateGUID                          as character 
  
      field Overwrite                             as logical .
  
  def temp-table TTAssociation no-undo serialize-name "Association" 
  
      field AssociationGUID  as character 
      field AssociationType  as character 
      field BuildUnitGUID    as character 
      field CascadeMode      as character 
      field FromPropertyGUID as character 
      field PropertyName     as character 
      field ToPropertyGUID   as character .
    
  /** dataset so that we can read and write xml */          
  def dataset dsWorkspace xml-node-name "Workspace" for TTProject, TTModel, TTProperty, TTTemplate, TTAssociation
      data-relation for TTProject, TTModel relation-fields (TTProject.ProjectGUID, TTModel.ProjectGUID) nested
      data-relation for TTModel, TTProperty relation-fields (TTModel.BuildUnitGUID, TTProperty.BuildUnitGUID) nested
      data-relation for TTModel, TTTemplate relation-fields (TTModel.BuildUnitGUID, TTTemplate.BuildUnitGUID) nested.
  
  def public property ConfigFile as char no-undo get . private set.

  constructor from_20_to_30 ():
  end constructor.
  
  method public void Upgrade(p_File as char):
    def var lv_model as longchar no-undo.
    def var lv_base  as longchar no-undo.
    def var lv_code  as longchar no-undo.
    
    def var lv_db        as char no-undo.
    def var lv_package   as char no-undo.
    def var lv_dir       as char no-undo.
    def var lv_hasProp   as char no-undo.
    def var lv_baseClass as char no-undo.
    def var lv_proptype  as char no-undo.
    
    def var AR as dotr.Maia.ActiveRecord no-undo.
    
    def buffer FKProp  for temp-table TTProperty.
    def buffer FKModel for temp-table TTModel.
    
    AR = new dotr.Maia.ActiveRecord().
    
    assign ConfigFile = p_File.

    dataset dsWorkspace:read-xml("file",search(p_File),"empty",?,?,?,?) no-error.
    def var i as int.

    for each TTTemplate :
      if search(substitute("dotr/Maia/Template/&1",TTTemplate.Name)) eq ? then delete TTTemplate.
    end.
     
    for each TTProject no-lock:
      assign lv_db      = ""
             lv_package = ""
             lv_dir     = "".
      
      if TTProject.Name eq "maia" then next.
      
      assign lv_Dir     = substitute("&1/_Maia",TTProject.directory)
             lv_Package = substitute("&1._Maia",TTProject.Package).
      
      os-create-dir value(lv_dir).
      
      os-rename value(substitute("&1/_Maia-Template",TTProject.directory)) value (substitute("&1/_Maia/_Template",TTProject.directory)).
      os-create-dir value (substitute("&1/_Maia/_Template",TTProject.directory)).
        
      copy-lob from file search("dotr/Maia/Upgrade/_base.cls") to lv_base.
    
      for each TTModel where TTModel.projectGUID eq TTProject.ProjectGUID no-lock:
        if TTModel.TableName eq "" then assign TTModel.TableName = TTModel.modelName.
        if TTModel.TableName eq "" then assign TTModel.TableName = TTModel.className.
        
        assign lv_Code = "".
                 
        if not TTModel.TempTableOnly and TTModel.LogicalDB ne "" and TTModel.LogicalDB ne "temp-db" then
        do:
          if lv_db eq "" then
          do:
            assign lv_db        = TTModel.LogicalDB
                   lv_base      = replace(lv_base,"/*!*/",substitute('assign this-object:dbname = "&1".',lv_db)).
          end.
          
          assign lv_BaseClass = substitute("&1._base",lv_Package).
        end.
        
        else assign lv_baseClass = "dotr.Maia.Model".

        case TTModel.PropertyEditor:
          when "enum" then assign lv_model = substitute("class &1.&2 inherits dotr.Maia.Enum:~n",lv_Package,TTModel.tablename).
 
          otherwise assign lv_model = substitute("class &1.&2 inherits &3:~n",lv_Package,TTModel.TableName,lv_BaseClass).
        end case.
             
        for each TTProperty where TTProperty.BuildUnitGUID eq TTModel.BuildUnitGUID no-lock:
          TTProperty.PropertyName eq AR:MagicName(TTProperty.PropertyName).
          
          assign lv_hasProp = "".

          case true:
            when TTModel.PropertyEditor eq "enum" then assign lv_hasProp = substitute('NewProp("&1")',TTProperty.PropertyName,TTModel.inherits).
          
            when TTProperty.ManualProperty then assign lv_hasProp = substitute('NewProp("&1",dotr.Maia.Enum.DataTypeEnum:GetByValue("&2"))',
                                                                               (new dotr.Maia.Helper.Property()):clean(TTProperty.PropertyName,TTProperty.DataType),
                                                                               TTProperty.DataType).
            
            when TTProperty.PropertyName ne AR:MagicName(TTProperty.FieldName) 
                 then assign lv_hasProp  = substitute('Rename("&1","&2")',TTProperty.FieldName,(new dotr.Maia.Helper.Property()):clean(TTProperty.PropertyName,TTProperty.DataType)).
          end case.

          if TTProperty.InitialValue ne "" 
             then assign lv_hasProp = lv_hasProp + substitute('&1InitialValue("&2")',
                                                         if lv_hasProp eq "" then substitute('Get("&1"):',TTProperty.PropertyName)
                                                                             else ":",
                                                         TTProperty.InitialValue).
          
          if TTProperty.GenerateGUID then assign lv_hasProp = lv_hasProp + substitute('&1GenerateGUID()',
                                                                                       if lv_hasProp eq "" then substitute('Get("&1"):',TTProperty.PropertyName)
                                                                                                           else ":").
            
          if TTProperty.Sequence ne "" and
             TTProperty.Sequence ne "-- Not Used --" and
             TTProperty.Sequence ne ? then assign lv_hasProp = lv_hasProp + substitute('&1GenerateSequence("&2")',
                                                                                        if lv_hasProp eq "" then substitute('Get("&1"):',TTProperty.PropertyName)
                                                                                                            else ":"
                                                                                        ,TTProperty.Sequence).
                                  
          if trim(lv_hasProp,":") ne "" then assign lv_code = lv_code + substitute('~t~tProperty(&1).~n',trim(lv_hasProp,":")).
        end.
        
        for each TTAssociation where TTAssociation.BuildUnitGUID eq TTModel.BuildUnitGUID no-lock:
          
          find FKProp where FKProp.PropertyGUID eq TTAssociation.ToPropertyGUID no-lock no-error.
          if not avail FKProp then next.
          
          find FKModel where FKModel.BuildUnitGUID eq FKProp.BuildUnitGUID no-lock no-error.
          if not avail FKModel then next.
          
          find TTProperty where TTProperty.PropertyGUID eq TTAssociation.FromPropertyGUID no-lock no-error.
          if not avail TTProperty then next.
          
          assign lv_hasProp = '~t~tAssociation(&1("&2"):Keys("&3")&6:Cascade&5()).~n'
                 lv_hasProp = substitute(lv_hasProp,replace(TTAssociation.AssociationType," ",""),
                                                    AR:MagicName(FKModel.ClassName),
                                                    AR:MagicName(TTProperty.PropertyName),
                                                    AR:MagicName(FKProp.PropertyName),
                                                    if trim(TTAssociation.CascadeMode) eq ""
                                                               then "Delete"
                                                               else TTAssociation.CascadeMode,
                                                    if AR:MagicName(FKModel.ClassName) eq TTAssociation.PropertyName
                                                       then ""
                                                       else substitute(':UsingPropertyName("&1")',TTAssociation.PropertyName)). 
          assign lv_code = lv_code + lv_hasProp.
        end.
        
        for each TTTemplate where TTTemplate.BuildUnitGUID eq TTModel.BuildUnitGUID no-lock:
          if TTTemplate.Name eq "@ActiveRecord" then next. /** handled by ActiveRecord.cls */
          if TTTemplate.Name eq "enum.i" then next. /** handled by enum.cls */
          
          assign lv_hasProp = substitute('~t~tUsingTemplate("&1").~n',TTTemplate.Name)
                 lv_code    = lv_Code + lv_hasProp.
        end.
         
        if lv_code ne "" then 
        do:
          assign lv_model = lv_model + substitute("  method public override void init():~n    super:init().~n",TTModel.TableName).
          if TTModel.PropertyEditor eq "enum" then assign lv_model = lv_model + substitute('~t~tDataType(dotr.Maia.Enum.DataTypeEnum:GetByValue("&1")).~n',TTModel.inherits).
        end.
                
        assign lv_model = lv_model + lv_code.
        
        if lv_code ne "" then assign  lv_model = lv_model + substitute("  end method.~n").
       
        assign lv_model = lv_model + "end class.".
        
        copy-lob from lv_model to file substitute("&1/_Maia/&2.cls",TTProject.directory,TTModel.TableName).
        
        if TTModel.PropertyEditor ne "enum" then
        do: 
          assign lv_base = replace(lv_base,"dotr.Maia.Upgrade._base",substitute("&1._Maia._base",TTProject.Package)).
          if search(substitute("&1/_Maia/_base.cls",TTProject.directory)) eq ? then copy-lob from lv_base to file substitute("&1/_Maia/_base.cls",TTProject.directory).
        end.
      end.      
    end.
  end method.

end class.