/*
Copyright (c) 2012, Julian Lyndon-Smith (julian+maia@dotr.com)
http://www.dotr.com
All rights reserved.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction except as noted below, including without limitation
the rights to use,copy, modify, merge, publish, distribute,
and/or sublicense, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

 The Software and/or source code cannot be copied in whole and
 sold without meaningful modification for a profit.

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.

 Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in
 the documentation and/or other materials provided with
 the distribution.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

using Progress.Lang.*.
using dotr.Maia.Model.*.

routine-level on error undo, throw.

class dotr.Maia.Model.Base.Directive abstract:
  def public property PrivateData as char no-undo get . set .
  
  def public property InTestMode as logical no-undo get . set .
  
  def public property DirectiveName as character no-undo public get . public set .  
  def public property ParameterData as character no-undo extent  public get . public set .  
  def public property ParameterName as character no-undo extent  public get . public set .  
  
  def public property TrimCode as logical no-undo public get . public set .  
  
  def public property Data as longchar no-undo public get . public set .  
  
  
  
  
  
  constructor Directive():
  end constructor.
end class.
@BuildInfo(Source="Directive").
@BuildInfo(BuildUnitGUID="c15af0cd-bc8f-2696-e211-599ff9803486").
@BuildInfo(Project="Maia").
@BuildInfo(Date="07/04/2013 09:05:16.704+01:00").
@BuildInfo(MD5Hash="e5kyR7EYOB695UHr+ILdvw==").