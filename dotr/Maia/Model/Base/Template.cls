/*
Copyright (c) 2012, Julian Lyndon-Smith (julian+maia@dotr.com)
http://www.dotr.com
All rights reserved.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction except as noted below, including without limitation
the rights to use,copy, modify, merge, publish, distribute,
and/or sublicense, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

 The Software and/or source code cannot be copied in whole and
 sold without meaningful modification for a profit.

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.

 Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in
 the documentation and/or other materials provided with
 the distribution.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

using Progress.Lang.*.
using dotr.Maia.Model.*.
using Progress.Data.*.

routine-level on error undo, throw.

class dotr.Maia.Model.Base.Template abstract :
  

  def public property PrivateData  as char no-undo get. set.
  def public property ErrorMessage as char no-undo get. set.
  
  def public property DataFileName as char no-undo get. protected set.
  def public property UpdateType   as char no-undo get. private   set.
  
  def public property SendStompUpdate as logical no-undo init yes get . set .
  def public property InTestMode      as logical no-undo          get . set .
  def public property Preselect			  as logical no-undo          get . set .
  
  def public property Builder      as character no-undo public get . public set .  
  def public property Description  as character no-undo public get . public set .  
  def public property ModelGUID    as character no-undo public get . public set .  
  def public property Name         as character no-undo public get . public set .  
  def public property TemplateGUID as character no-undo public get . public set .  
  
  def public property Overwrite as logical no-undo public get . public set .  
  
  constructor Template():
  end constructor.

  destructor Template():
  end destructor.
  

end class.
@BuildInfo(Source="Template").
@BuildInfo(BuildUnitGUID="c15af0cd-bc8f-2696-e211-719fb928de80").
@BuildInfo(Project="maia3").
@BuildInfo(Date="07/04/2013 11:56:39.219+01:00").
@BuildInfo(MD5Hash="uUrqN1PpVk6jifkZsAeJmQ==").