/*
Copyright (c) 2013, Julian Lyndon-Smith (julian@dotr.com)
http://www.dotr.com
All rights reserved.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction except as noted below, including without limitation
the rights to use,copy, modify, merge, publish, distribute,
and/or sublicense, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

 The Software and/or source code cannot be copied in whole and
 sold without meaningful modification for a profit.

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.

 Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in
 the documentation and/or other materials provided with
 the distribution.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

using Progress.Lang.*.

routine-level on error undo, throw.

class dotr.Maia.Model.Association :
  def protected property Association1 as dotr.Maia.Model.AssociationBase no-undo get . set . 
  
  constructor Association(p_model as char):
    Association1 = new dotr.Maia.Model.AssociationBase(). 
  
    assign Association1:ToModel = p_model.
  end method.

  method dotr.Maia.Model.Association Keys(p_FindUsingProperties as char):
    return this-object:Keys(p_FindUsingProperties,p_FindUsingProperties).
  end method.

  method dotr.Maia.Model.Association Keys(p_FindUsingProperties as char,p_FindUsingfKProperties as char):
    assign Association1:FindUsingProperties   = p_FindUsingProperties
           Association1:FindUsingName         = substitute("FindUsing&1",replace(p_FindUsingProperties,",",""))
           Association1:FindUsingFkProperties = p_FindUsingfKProperties
           Association1:FindUsingfKName       = substitute("FindUsing&1",replace(p_FindUsingfKProperties,",","")).
           
    return this-object.
  end method.

  method dotr.Maia.Model.Association UsingPropertyName(p_Name as char):
    assign Association1:FromName = p_name.
    return this-object.
  end method.
/*                                                             */
/*  method dotr.Maia.Model.Association UsingID(p_Name as char):*/
/*    assign Association1:Name = p_name.                       */
/*    return this-object.                                      */
/*  end method.                                                */
  
  method dotr.Maia.Model.Association CascadeDelete():
    assign Association1:cascade = "delete".
    return this-object.
  end method.
   
  method dotr.Maia.Model.Association CascadeNullify():
    assign Association1:cascade = "nullify".
    return this-object.
  end method.
end class.