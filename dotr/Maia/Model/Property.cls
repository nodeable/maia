/*
Copyright (c) 2013, Julian Lyndon-Smith (julian@dotr.com)
http://www.dotr.com
All rights reserved.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction except as noted below, including without limitation
the rights to use,copy, modify, merge, publish, distribute,
and/or sublicense, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

 The Software and/or source code cannot be copied in whole and
 sold without meaningful modification for a profit.

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.

 Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in
 the documentation and/or other materials provided with
 the distribution.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

using Progress.Lang.*.

routine-level on error undo, throw.

class dotr.Maia.Model.Property :
  def protected property Property1 as dotr.Maia.Model.PropertyBase no-undo get . set . 
  
  constructor Property(p_Name as char):
    this-object(p_Name,dotr.Maia.Enum.DataTypeEnum:char).
  end method.

  constructor Property(p_Name as char,p_Type as dotr.Maia.Enum.DataTypeEnum) :
    Property1 = new dotr.Maia.Model.PropertyBase(). 
  
    assign Property1:PropertyName = p_Name
           Property1:DataType     = p_Type:Value.
  end method.

  method dotr.Maia.Model.Property AccessMode(p_Mode as char):
    assign Property1:AccessMode   = p_Mode. 
    return this-object.
  end method.

  method dotr.Maia.Model.Property Comment(p_Comment as char):
    assign Property1:Comment   = p_Comment.
    return this-object.
  end method.

  method dotr.Maia.Model.Property InitialValue(p_init as char):
    assign Property1:InitialValue = p_init.
    return this-object.
  end method.

  method dotr.Maia.Model.Property DataType(p_DataType as dotr.Maia.Enum.DataTypeEnum):
    assign Property1:DataType = p_DataType:Value.
    return this-object.
  end method.

  method dotr.Maia.Model.Property Extent(p_Extent as int):
    assign Property1:extent = p_Extent.
    return this-object.
  end method.

  method dotr.Maia.Model.Property DoNotBuild():
    assign Property1:BuildProperty = no.
    return this-object.
  end method.
  
  method dotr.Maia.Model.Property GenerateGUID():
    assign Property1:GenerateGUID = yes.
    return this-object.
  end method.

  method dotr.Maia.Model.Property GenerateSequence(p_Seq as char):
    assign Property1:Sequence = p_Seq.
    return this-object.
  end method.

  method dotr.Maia.Model.Property ViewAs(p_viewas as char):
    assign Property1:ViewAs = p_ViewAs.
    return this-object.
  end method.
  
  method dotr.Maia.Model.Property Label(p_Label as char):
    assign Property1:Label = p_Label.
    return this-object.
  end method.

  method dotr.Maia.Model.Property ReadOnly():
    assign Property1:ReadOnly = yes.
    return this-object.
  end method.
  
  method dotr.Maia.Model.Property ToolTip(p_ToolTip as char):
    assign Property1:ToolTip = p_ToolTip.
    return this-object.
  end method.

  method dotr.Maia.Model.Property Help(p_data as char):
    assign Property1:Help = p_Data.
    return this-object.
  end method.
  
end class.