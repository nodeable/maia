/*
Copyright (c) 2013, Julian Lyndon-Smith (julian@dotr.com)
http://www.dotr.com
All rights reserved.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction except as noted below, including without limitation
the rights to use,copy, modify, merge, publish, distribute,
an	d/or sublicense, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

 The Software and/or source code cannot be copied in whole and
 sold without meaningful modification for a profit.

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.

 Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in
 the documentation and/or other materials provided with
 the distribution.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
using Progress.Lang.*.

routine-level on error undo, throw.

class dotr.Maia.Model.Validation :

	def public property PropertyName  as char no-undo init ? get . private set . 
  def public property StartValue    as char no-undo init ? get . private set . 
  def public property EndValue      as char no-undo init ? get . private set . 
  def public property Includes      as char no-undo init ? get . private set . 
  def public property Excludes      as char no-undo init ? get . private set . 
  
  def public property NotNull       as logical no-undo init ? get . private set . 
  def public property NotBlank      as logical no-undo init ? get . private set .
  
  def public property MinLength as int no-undo init ? get . private set .
  def public property MaxLength as int no-undo init ? get . private set .
  
  def public property NotNullMessage    as char init "<<property>> cannot be unknown" no-undo get . private set . 
  def public property NotBlankMessage   as char init "<<property>> cannot be blank" no-undo get . private set .
  def public property MinLengthMessage  as char init "<<property>> must be at least <<minLength>> characters " no-undo get . private set .
  def public property MaxLengthMessage  as char init "<<property>> must be less than <<maxLength>> characters" no-undo get . private set .
  def public property StartValueMessage as char init "<<property>> must be between <<StartValue>> and <<EndValue>>" no-undo get . private set .
  def public property EndValueMessage   as char init "<<property>> must be between <<StartValue>> and <<EndValue>>" no-undo get . private set .
  def public property IncludesMessage   as char init "<<property>> must include <<Includes>>" no-undo get . private set .
  def public property ExcludesMessage   as char init "<<property>> cannot contain <<Excludes>>" no-undo get . private set .  
  
  def public property OldName as char no-undo get . private set .
	def public property NewName as char no-undo get . private set .

	def public property PropertyRowid as rowid no-undo get . private set .
	
	def private property oBase as dotr.Maia.Base no-undo get . set.
	
	constructor Validation(p_base as dotr.Maia.Base,p_Property as char,p_rowid as rowid):
		assign this-object:PropertyName	 = p_Property
					 this-object:oBase   			 = p_base
					 this-object:PropertyRowid = p_rowid.

	end constructor.

  method dotr.Maia.Model.Validation HasValue():
    NotNull().
    NotBlank().
    return this-object.
  end method.

  method dotr.Maia.Model.Validation HasValue(p_message as char):
    NotNull(p_message).
    NotBlank(p_message).
    return this-object.
  end method.

  method dotr.Maia.Model.Validation NotNull():
    assign this-object:NotNull = yes.
    return this-object.
  end method.

  method dotr.Maia.Model.Validation NotNull(p_message as char):
    NotNull().
    assign this-object:NotNullMessage = p_message.
    return this-object.
  end method.

  method dotr.Maia.Model.Validation NotBlank():
    assign this-object:NotBlank = yes.
    return this-object.
  end method.

  method dotr.Maia.Model.Validation NotBlank(p_message as char):
    NotBlank().
    assign this-object:NotBlankMessage = p_message.
    return this-object.
  end method.

  method dotr.Maia.Model.Validation Length(p_min as int,p_Max as int):
    MinLength(p_min).
    MaxLength(p_max).
    return this-object.
  end method.

  method dotr.Maia.Model.Validation Length(p_min as int,p_Max as int,p_message as char):
    MinLength(p_min,p_message).
    MaxLength(p_max,p_message).
    return this-object.
  end method.

  method dotr.Maia.Model.Validation MinLength(p_min as int):
    assign this-object:MinLength = p_min.
    return this-object.
  end method.

  method dotr.Maia.Model.Validation MinLength(p_min as int,p_message as char):
    MinLength(p_min).
    assign this-object:MinLengthMessage = p_message.
    return this-object.
  end method.

  method dotr.Maia.Model.Validation MaxLength(p_max as int):
    assign this-object:MaxLength = p_max.
    return this-object.
  end method.
  
  method dotr.Maia.Model.Validation MaxLength(p_max as int,p_message as char):
    MaxLength(p_max).
    assign this-object:MaxLengthMessage = p_message.
    return this-object.
  end method.

  method dotr.Maia.Model.Validation Between(p_Start as char, p_End as char):
    MoreThan(p_Start).
    LessThan(p_End).
    return this-object.
  end method.

  method dotr.Maia.Model.Validation Between(p_Start as char, p_End as char,p_message as char):
    MoreThan(p_Start,p_message).
    LessThan(p_End,p_message).
    return this-object.
  end method.

  method dotr.Maia.Model.Validation MoreThan(p_min as char):
    assign this-object:StartValue = p_min.
    return this-object.
  end method.

  method dotr.Maia.Model.Validation MoreThan(p_min as char,p_message as char):
    MoreThan(p_min).
    assign this-object:StartValueMessage = p_message.
    return this-object.
  end method.

  method dotr.Maia.Model.Validation LessThan(p_max as char):
    assign this-object:EndValue = p_max.
    return this-object.
  end method.
  
  method dotr.Maia.Model.Validation LessThan(p_max as char,p_message as char):
    LessThan(p_max).
    assign this-object:EndValueMessage = p_message.
    return this-object.
  end method.

  method dotr.Maia.Model.Validation Includes(p_data as char):
    assign this-object:Includes = p_data.
    return this-object.
  end method.
  
  method dotr.Maia.Model.Validation Includes(p_data as char,p_message as char):
    Includes(p_data).
    assign this-object:IncludesMessage = p_message.
    return this-object.
  end method.
   
  method dotr.Maia.Model.Validation Excludes(p_Data as char):
    assign this-object:Excludes = p_Data.
    return this-object.
  end method.

  method dotr.Maia.Model.Validation Excludes(p_Data as char,p_message as char):
    Excludes(p_Data).
    assign this-object:ExcludesMessage = p_message.
    return this-object.
  end method.
 
end class.