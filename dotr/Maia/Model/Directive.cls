/*
Copyright (c) 2012, Julian Lyndon-Smith (julian+maia@dotr.com)
http://www.dotr.com
All rights reserved.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction except as noted below, including without limitation
the rights to use,copy, modify, merge, publish, distribute,
and/or sublicense, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

 The Software and/or source code cannot be copied in whole and
 sold without meaningful modification for a profit.

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.

 Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in
 the documentation and/or other materials provided with
 the distribution.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

using Progress.Lang.*.
using dotr.Maia.Model.*.

routine-level on error undo, throw.

class dotr.Maia.Model.Directive inherits dotr.Maia.Model.Base.Directive: 
  constructor Directive():
    super().
  end constructor.
/**
 * parses the supplied string
 *
 * @param  p_Data String to parse
 */
  constructor public Directive (p_Data as char):
    super ().
    ParseDirective(p_Data).
  end constructor.

/**
 * parses the supplied string
 *
 * Each option is split into a name / value pair 
 * into a variable extent
 *
 * @param  p_Data String to parse
 * @return void
 */
  method private void ParseDirective(p_Data as char):
    def var lv_ParamData as char no-undo.
    def var lv_Name      as char no-undo.
    
    def var lv_Index as int no-undo.
    def var lv_i     as int no-undo.
    
    /** example #1: DefineOption &BuildDirectory="dotr.Maia.Model"  
                #2: Copyright */               
      
    assign p_Data   = replace(p_Data,"~{&!","")
           p_Data   = replace(p_Data,"!}","")
           p_Data   = trim(p_Data)
           lv_Index = index(p_Data," ")
           lv_name  = substr(p_Data,1,if lv_Index eq 0 then -1 else lv_Index) /* -1 returns the whole string */
           p_Data   = if lv_Index eq 0 then "" /* only a parameter, no value */
                                       else replace(substr(p_Data,lv_Index)," &",chr(1)) /* each parameter begins with " &" (excluding quotes) , so if we
                                                                                             replace those with a special character, we can work out how
                                                                                             many name / value there are */
           p_Data   = trim(p_Data,chr(1)). /* get rid of first CHR(1) */
                                          
    assign DirectiveName = trim(lv_Name).

    if num-entries(p_Data,chr(1)) le 0 then return.
    
    extent(ParameterName) = num-entries(p_Data,chr(1)). /* voila! */
    extent(ParameterData) = extent(ParameterName).
    
    do lv_i = 1 to num-entries(p_Data,chr(1)): /* let's loop through all of the parameters */
      assign lv_ParamData        = trim(entry(lv_i,p_Data,chr(1)))
             ParameterName[lv_i] = trim(entry(1,lv_ParamData,"="),"~" '~t~n&") /* strip any leading or trailing chars (" <space> ' <tab> <newline>) */
             lv_ParamData        = if num-entries(lv_ParamData,"=") gt 1 then substr(lv_ParamData,index(lv_ParamData,"=") + 1)
                                                                         else ""
             lv_ParamData        = trim(lv_ParamData,"~" '~t~n") /* strip any leading or trailing chars (" <space> ' <tab> <newline>) */                                                                                          
             ParameterData[lv_i] = lv_ParamData.

      if ParameterName[lv_i] eq "TrimCode" then assign TrimCode = logical(ParameterData[lv_i]). /* special parameter that needs saving */

    end.
    
    return.

  end method.  
end class.
@BuildInfo(Source="Directive").
@BuildInfo(BuildUnitGUID="c15af0cd-bc8f-2696-e211-599ff9803486").
@BuildInfo(Project="Maia").
@BuildInfo(Date="07/04/2013 09:04:05.845+01:00").
@BuildInfo(MD5Hash="WSxLnCFdOEWs24cXsyCiMA==").