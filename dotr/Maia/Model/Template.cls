/*
Copyright (c) 2012, Julian Lyndon-Smith (julian+maia@dotr.com)
http://www.dotr.com
All rights reserved.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction except as noted below, including without limitation
the rights to use,copy, modify, merge, publish, distribute,
and/or sublicense, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

 The Software and/or source code cannot be copied in whole and
 sold without meaningful modification for a profit.

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.

 Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in
 the documentation and/or other materials provided with
 the distribution.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

using Progress.Lang.*.
using dotr.Maia.Model.*.

routine-level on error undo, throw.

class dotr.Maia.Model.Template inherits dotr.Maia.Model.Base.Template:
   
  constructor Template():
    super().
  end constructor.

  /** reads the template in
   *  after reading, check for "include" directives. If found, insert their contents into the data structure as well.
   * @param p_BaseTemplate : template to read
   * @return longchar : template contents
   */
  
  method public longchar LoadTemplate(p_Class as dotr.Maia.Base,p_Template as char):
    def var lv_Data     as longchar no-undo.
    def var lv_Template as longchar no-undo.
  
    def var Directive1 as Directive no-undo.
    
    def var lv_StartLine  as int no-undo.
    def var lv_Startindex as int no-undo.
    def var lv_Endindex   as int no-undo.
    def var lv_i          as int no-undo.
    
    assign file-info:file-name = p_Template.
         
    copy-lob from file file-info:full-pathname to lv_Data.
    
    /** loop looking for include directives */
    do while true on error undo, throw:
      assign lv_StartIndex = index(lv_Data,"~{&! Includes").
      if lv_StartIndex le 0 then leave.

      assign lv_StartLine = r-index(lv_Data,"~n",lv_StartIndex) + 1
             lv_EndIndex  = index(lv_Data,"!}",lv_StartIndex) + 2.

      /** create new directive */
      Directive1 = new Directive(string(substr(lv_Data,lv_StartIndex,lv_EndIndex - lv_StartIndex))).

      assign substr(lv_Data,lv_StartIndex,(index(lv_Data,"~n",lv_StartIndex) + 1) - lv_StartIndex) = "".

      /** insert included template contents into data */
      do lv_i = 1 to extent(Directive1:ParameterData):
        assign Directive1:Data = Directive1:Data + "~n" + LoadTemplate(p_Class,Directive1:ParameterName[lv_i]). /** assume that any include specifies directory */
      end.

      assign substr(lv_Data,lv_StartIndex,0) = replace(Directive1:Data,"~n","~n" + fill(" ",lv_StartIndex - lv_StartLine)).

      finally:
        delete object Directive1 no-error.
      end finally.
    end.
    
    assign lv_Data = GetOptions(p_Class,lv_Data).
     
    return replace(lv_Data,"~r",""). 
  
  end method.

/** reads all Option directives in a template
   * @param p_Data : template contents
   * @return longchar : modified template
   */
  
  method private longchar GetOptions(p_Class as dotr.Maia.Base,p_Data as longchar):
    
    def var Directive1 as dotr.Maia.Model.Directive no-undo.
    
    def var lv_Startindex as int no-undo.
    def var lv_Endindex   as int no-undo.
    def var lv_i          as int no-undo.
    
    /** loop through all defineoption directives */
    do while true on error undo, throw:
      assign lv_StartIndex = index(p_Data,"~{&! DefineOption").
      if lv_StartIndex le 0 then leave.
     
      assign lv_EndIndex  = index(p_Data,"!}",lv_StartIndex) + 2.

      Directive1 = new dotr.Maia.Model.Directive(string(substr(p_Data,lv_StartIndex,lv_EndIndex - lv_StartIndex))).
     
      assign substr(p_Data,lv_StartIndex,(index(p_Data,"~n",lv_StartIndex) + 1) - lv_StartIndex) = "".
      
      do lv_i = 1 to extent(Directive1:ParameterData):
        p_Class:SetOption(Directive1:ParameterName[lv_i],Directive1:ParameterData[lv_i]).
      end.
      
      finally:
        delete object Directive1 no-error.   
      end finally.                   
    end.
    
    return p_Data.
  
  end method.
          
    
end class.
@BuildInfo(Source="Template").
@BuildInfo(BuildUnitGUID="eb981193-2ed5-3987-e211-fd9ededdb722").
@BuildInfo(Project="Maia").
@BuildInfo(Date="06/04/2013 22:01:25.602+01:00").
@BuildInfo(MD5Hash="imzNQi3qoUGeDsVFMzte7w==").