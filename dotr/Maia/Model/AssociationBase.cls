/*
Copyright (c) 2013, Julian Lyndon-Smith (julian@dotr.com)
http://www.dotr.com
All rights reserved.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction except as noted below, including without limitation
the rights to use,copy, modify, merge, publish, distribute,
and/or sublicense, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

 The Software and/or source code cannot be copied in whole and
 sold without meaningful modification for a profit.

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.

 Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in
 the documentation and/or other materials provided with
 the distribution.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
using Progress.Lang.*.

routine-level on error undo, throw.

class dotr.Maia.Model.AssociationBase  :
  
  def public property ToModel               as char no-undo get . set . 
  def public property FromModel             as char no-undo get . set . 
  def public property type                  as char no-undo get . set . 
  def public property Cascade               as char no-undo get . set .
  def public property FindUsingName         as char no-undo get . set .
  def public property FindUsingFkName       as char no-undo get . set .
  def public property FindUsingProperties   as char no-undo get . set .
  def public property FindUsingFkProperties as char no-undo get . set .
  def public property UsingProperty         as char no-undo get . set .
  
  def public property ToProperty    as char no-undo 
    get():
      return if ToProperty eq "" then (if type begins "has" then FromModel
                                                              else ToModel) 
                                 else ToProperty.
    end get . set .
  
  def public property FromProperty as char no-undo
    get():
      return if FromProperty eq "" then ToProperty else FromProperty.
    end get . set .
  
  def public property FromName     as char no-undo
    get():
      return if FromName eq "" then ToModel 
                               else FromName.
    end get . set .

/*  def public property Name     as char no-undo                       */
/*    get():                                                           */
/*      return if Name eq "" then replace(FindUsingName,"FindUsing","")*/
/*                           else Name.                                */
/*    end get . set .                                                  */

end class.