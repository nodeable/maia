{&! DefineOption &builder="dotr.Maia.Builder.BuildTempTable" !}
{&! DefineOption &FileExtension="cls" !}
{&! DefineOption &BuildIfExists="no" !}
{&! DefineOption &BuildDirectory="Model/TempTable" !}
{&! Copyright !}

using Progress.Lang.*.
using <<AppPackage>>.Model.*.

routine-level on error undo, throw.

class <<AppPackage>>.Model.TempTable.<<ClassName>> inherits <<AppPackage>>.Model.TempTable.Base.<<ClassName>>: 
  constructor <<ClassName>>():
    super().
  end constructor.

end class.
