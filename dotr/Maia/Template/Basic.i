{&! DefineOption &builder="dotr.Maia.Builder.BuildBasic" !}
{&! DefineOption &FileExtension="cls" !}
{&! DefineOption &BuildIfExists="no" !}
{&! DefineOption &BuildDirectory="Model" !}
{&! Copyright !}

using Progress.Lang.*.
using <<AppPackage>>.Model.*.

routine-level on error undo, throw.

class <<AppPackage>>.Model.<<ClassName>> inherits <<AppPackage>>.Model.Base.<<ClassName>>: 
  constructor <<ClassName>>():
    super().
  end constructor.
  
end class.
