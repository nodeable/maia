{&! DefineOption &builder="dotr.Maia.Builder.BuildActiveRecord" !}
{&! DefineOption &FileExtension="cls" !}
{&! DefineOption &BuildDirectory="Model" !}
{&! Copyright !}

using Progress.Lang.*.
using <<AppPackage>>.Model.*.

routine-level on error undo, throw.

class <<AppPackage>>.Model.<<ClassName>> inherits <<AppPackage>>.Model.Base.<<ClassName>>: 
  constructor <<ClassName>>():
    super().
  end constructor.

end class.
