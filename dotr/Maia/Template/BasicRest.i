{&! DefineOption &builder="dotr.Maia.Builder.BuildRest" !}
{&! DefineOption &FileExtension="p" !}
{&! DefineOption &BuildDirectory="Rest" !}
{&! Copyright !}

using Progress.Lang.*.

routine-level on error undo, throw.

{ src/web/method/cgidefs.i  }
{ src/web/method/cgiarray.i }
{ src/web/method/tagmap.i   }
{ src/web/method/webutils.i }

def var lv_data as longchar no-undo.

run dotr/hash42/appserver/<<ClassName>>.p (get-field("token"),REQUEST_METHOD,get-field("data"),output lv_data).

finally:
  export stream WEBSTREAM lv_data.
end finally.
