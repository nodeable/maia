{&! DefineOption &builder="dotr.Maia.Builder.BuildBasicBase" !}
{&! DefineOption &FileExtension="cls" !}
{&! DefineOption &BuildDirectory="Model/Base" !}
{&! Copyright !}

using Progress.Lang.*.
using <<AppPackage>>.Model.*.

routine-level on error undo, throw.

class <<AppPackage>>.Model.Base.<<ClassName>> abstract{&! inherits !}:
  def public property PrivateData as char no-undo get . set .
  
  def public property InTestMode as logical no-undo get . set .
  
  {&! DefineProperties !}
  
  constructor <<ClassName>>():
  end constructor.
end class.
