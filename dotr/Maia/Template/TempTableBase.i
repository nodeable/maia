{&! DefineOption &builder="dotr.Maia.Builder.BuildTempTableBase" !}
{&! DefineOption &FileExtension="cls" !}
{&! DefineOption &BuildDirectory="Model/TempTable/Base" !}
{&! Copyright !}

using Progress.Lang.*.
using <<AppPackage>>.Model.*.
using Progress.Data.*.

routine-level on error undo, throw.

class <<AppPackage>>.Model.TempTable.Base.<<ClassName>>{&! inherits !}:
  {&! DefineTempTable !}
  
  /** take current ActiveRecord and convert to longchar
   *  
   *  gets the current object, converts into a temp-table
   *  and exports the data */ 
  
  method public longchar ExportData(p_Format as char,p_Mode as char,p_<<ClassName>> as <<AppPackage>>.Model.<<ClassName>>):
    
    def var lv_Data as longchar no-undo.
    
    if p_Mode eq "single" or not valid-handle(p_<<ClassName>>:query) then 
    do:
      CreateRecord(p_<<ClassName>>).
    end.
    
    else
    do:
      p_<<ClassName>>:first().
      
      do while p_<<ClassName>>:available:
        CreateRecord(p_<<ClassName>>).
        p_<<ClassName>>:Next().
      end.
    end.

    temp-table TT_<<TableName>>:write-json("longchar",lv_Data,yes).

    return lv_Data.
    
    finally:
      empty temp-table TT_<<TableName>>.
    end finally.
    
  end method. 
   
  method private void CreateRecord(p_<<ClassName>> as <<AppPackage>>.Model.<<ClassName>>):
    create TT_<<TableName>>.

    assign {&! TempTable_AssignToBuffer !}
  end method.
  
end class.