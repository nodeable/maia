{&! DefineOption &builder="dotr.Maia.Builder.BuildEnum" !}
{&! DefineOption &ClassSuffix="Enum" !}
{&! Copyright !}

using Progress.Lang.*.
using <<AppPackage>>.*.

routine-level on error undo, throw.

class <<AppPackage>>.<<ClassName>> :
  def public property name        as char no-undo get . private set .
  def public property description as char no-undo get . private set .
  
  def public property value as <<EnumDataType>> no-undo get . private set .
  
  {&! enumProperties !}

  constructor static <<ClassName>>():
  end method.

  constructor private <<ClassName>>(p_Name as char,p_Value as <<EnumDataType>>):
    this-object(p_Name,p_Value,p_Name).
  end constructor.

  constructor private <<ClassName>>(p_Name as char,p_Value as <<EnumDataType>>,p_Desc as char):
    assign this-object:name        = p_Name
           this-object:value       = p_Value
           this-object:description = p_Desc.
  end constructor.
  
  /** return the enum described by the supplied name
   * @param name of enum to find
   * @return <<AppPackage>>.<<ClassName>>
   */
  
  method static <<ClassName>> GetByName(p_Name as char):
    {&! enumGetByName !}
  end method.

  /** return the enum described by the supplied value
   * @param name of enum to find
   * @return <<AppPackage>>.<<ClassName>>
   */
  
  method static <<ClassName>> GetByValue(p_Value as <<EnumDataType>>):
    {&! enumGetByValue !}
  end method.
  
  
end class.
