{&! DefineOption &builder="dotr.Maia.Builder.BuildActiveRecordBase" !}
{&! DefineOption &FileExtension="cls" !}
{&! Copyright !}

using Progress.Lang.*.
using <<AppPackage>>.Model.*.

routine-level on error undo, throw.

class <<AppPackage>>.Model.Base.<<ClassName>> abstract{&! inherits !}:
  
  {&! DefineProperties !}
  
  constructor <<ClassName>>():
  end constructor.
end class.
