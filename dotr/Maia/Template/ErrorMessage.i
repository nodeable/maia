{&! DefineOption &builder="dotr.Maia.Builder.BuildError" !}
{&! DefineOption &FileExtension="cls" !}
{&! Copyright !}

using Progress.Lang.*.

routine-level on error undo, throw.

class <<AppPackage>>.<<ClassName>> inherits Progress.Lang.AppError:
  constructor <<ClassName>>():
    {&! Error !}
  end constructor.
end class.
