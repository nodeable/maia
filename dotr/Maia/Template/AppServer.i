{&! DefineOption &builder="dotr.Maia.Builder.BuildAppServer" !}
{&! DefineOption &BuildDirectory="Appserver" !}
{&! DefineOption &BuildIfExists="yes" !}
{&! DefineOption &FileExtension="p" !}
{&! Copyright !}

routine-level on error undo, throw.

def input parameter p_token as char no-undo.
def input parameter p_mode  as char no-undo.
def input parameter p_data as longchar no-undo.

def output parameter op_json as longchar no-undo.

def var Parameters as dotr.ActiveRecord.ParseParameters no-undo.

{&! AppServer_Token !}

run value(p_mode) .

return.

procedure get:
  def var <<ClassName>>1 as <<AppPackage>>.Model.<<ClassName>> no-undo.

  run find<<ClassName>>(output <<ClassName>>1).

  assign op_json = <<ClassName>>1:ExportQuery().
  
end procedure.

procedure post:
  (new <<ParentPackage>>.<<ClassName>>()):Import(p_data).

  finally:
    assign op_json = '~{"success":"ok"}'.
  end finally.
  
end procedure.

procedure delete:
  def var <<ClassName>>1 as <<AppPackage>>.Model.<<ClassName>> no-undo.
  
  run find<<ClassName>>(output <<ClassName>>1).

  <<ClassName>>1:remove().

  finally:
    assign op_json = <<ClassName>>1:Export().
  end finally.
  
end procedure.

procedure find<<ClassName>>:
  def output parameter <<ClassName>>1 as <<AppPackage>>.Model.<<ClassName>> no-undo.   

  def var lv_find as char no-undo.

  assign lv_find = parameters:data::FindUsing no-error.

  case lv_find:
    {&! AppServer_FindUsing &source="dotr/Maia/Template/builder/appserver_find.i"!}
/*    {&! Appserver_FindFromJson !}*/
  end case.

  return.
  
end procedure.

finally:
  delete object Parameters no-error.
end finally.
