def public static property &1 as <<ClassName>> no-undo
  get():
    if not valid-object(<<AppPackage>>.<<ClassName>>:&1) then <<AppPackage>>.<<ClassName>>:&1 = new <<AppPackage>>.<<ClassName>>('&1',&2&3&2).
    return <<AppPackage>>.<<ClassName>>:&1.
  end get . private set .
