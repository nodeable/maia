/** ensure that <<getname>> not used elsewhere 
   * @param none
   * @return void 
   */
  
  method public void Validate<<getname>>():
    def var test as <<ParentPackage>>.<<ClassName>> no-undo.
    
    test = new <<ParentPackage>>.<<ClassName>>().
    
    
    /** try to find another record with <<getname>> */
    test:FindUsing<<getname>>(<<ParameterCall>>).
    
    if test:available and <<compare>> then undo, throw new AppError("<<getname>> already in use",0). 
  end method.