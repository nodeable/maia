{&! JavaDocHeader &Method="<<GetName>>" &Description="gets a <<Classname>> using <<GetName>>" 
                 &Parameters="@param <<DocParameterList>>" &ReturnValue="@return <<AppPackage>>.Model.<<ClassName>>" !}
method public <<ParentPackage>>.<<ClassName>> <<GetName>>(<<ParameterList>>):
  return <<GetName>>(<<ParameterCall>>,"").
end method.

method public <<ParentPackage>>.<<ClassName>> <<GetName>>(<<ParameterList>>,p_Sort as char):
  FindWhere(<<findString>>,p_Sort).
  
  if not this-object:available then
  do:
    <<AssignDefaultProperties>>
  end.
  
  return cast(this-object,<<ParentPackage>>.<<ClassName>>).
end method.
