{&! JavaDocHeader &Method="Remove<<Association_FromProperty>>" 
     &Description="nullifies foreign key <<Association_ID>>" !}

method public void Remove<<Association_ToModelBase>>():
  assign <<Association_Nullify>>
         this-object:HasValid<<Association_PropertyName>> = no.
  
  this-object:save().
end method.
