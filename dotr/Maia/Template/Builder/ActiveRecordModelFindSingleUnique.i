  /** gets the <<ClassName>> matching the unique ID 
   *  @param p_<<ClassName>>ID : the id to find
   */

  method public <<ParentPackage>>.<<ClassName>> find(p_<<ClassName>>ID as <<KeyFieldDatatype>>):
    this-object:DataProvider:FetchUnique(substitute("where _<<TableName>>.<<KeyFieldName>> eq &1",quoter(p_<<ClassName>>ID))).
    
    this-object:Get().
    
    if not this-object:available then assign this-object:<<KeyProperty>> = p_<<ClassName>>ID.
    return cast(this-object,<<ParentPackage>>.<<ClassName>>). 
  end.
  