  /** gets the <<ClassName>> using the rowid 
   *  @param p_<<ClassName>>RowID : the rowid to find
   */

  method public <<ParentPackage>>.<<ClassName>> find(p_<<ClassName>>RowID as rowid):
    this-object:DataProvider:FetchUnique(p_<<ClassName>>RowID).
    return this-object:Get().  
  end.
 