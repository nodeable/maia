def public property <<Association_PropertyName>> as <<Association_ToModel>> no-undo
  get():
    if not HasValid<<Association_PropertyName>> then 
    do:
      assign <<Association_PropertyName>>         = (new <<Association_ToModel>>()):<<Association_FindUsingFK>>(<<Association_Parameters>>)
             HasValid<<Association_PropertyName>> = yes.
    end.
    return <<Association_PropertyName>>.
  end get . private set .