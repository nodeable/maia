def public static property Instance as class <<Package>>.<<Classname>> no-undo
  get():
    if not valid-object(Instance) then Instance = new <<Package>>.<<Classname>>().
    return Instance.
  end get. private set.
  