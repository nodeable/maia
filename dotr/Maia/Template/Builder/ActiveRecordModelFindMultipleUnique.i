  /** gets the <<ClassName>> matching the unique ID 
   *  @param p_<<ClassName>>ID : the id to find
   */

  method public <<ParentPackage>>.<<ClassName>> find(<<ParameterList>>):
    this-object:DataProvider:FetchUnique(<<findstring>>).
    this-object:Get().
    
    if not this-object:available then
    do:
     <<AssignDefaultProperties>>
    end.
    
    return cast(this-object,<<ParentPackage>>.<<ClassName>>).
  end.
 