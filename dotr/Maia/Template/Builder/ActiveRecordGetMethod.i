{&! JavaDocHeader &Method="<<GetName>>" &Description="gets a <<Classname>> using <<GetName>>" 
                 &Parameters="@param <<DocParameterList>>" &ReturnValue="@return <<AppPackage>>.Model.<<ClassName>>" !}
method public static <<AppPackage>>.Model.<<ClassName>> <<GetName>>(<<ParameterList>>):
  return <<GetName>>(<<ParameterCall>>,"").
end method.

method public static <<AppPackage>>.Model.<<ClassName>> <<GetName>>(<<ParameterList>>,p_Sort as char):
  return FindWhere(<<findString>>,p_Sort).
end method.
