
/** clear all <<Association_ToModel>> with a foreign key matching this object */ 

<<Association_PropertyName>>  = (new <<Association_ToModel>>()):<<Association_FindUsingFK>>(<<Association_Parameters>>).
  
do while <<Association_PropertyName>>:Available:
  <<Association_PropertyName>>:Remove<<ClassName>>().
  <<Association_PropertyName>>:next().
end.