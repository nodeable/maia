if HasValid<<Association_PropertyName>> then 
do:
  delete object <<Association_PropertyName>> no-error.
  assign HasValid<<Association_PropertyName>> = no.
end.   
