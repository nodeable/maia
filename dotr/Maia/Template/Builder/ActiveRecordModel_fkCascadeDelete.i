{&! JavaDocHeader &Method="Remove<<FromProperty>>" 
     &Description="remove record on foreign key <<Association_ID>> deletion" !}

method public void Remove<<Association_ToModelBase>>():
  this-object:Remove().
end method.
