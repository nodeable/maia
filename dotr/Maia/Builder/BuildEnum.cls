/*
Copyright (c) 2011, Julian Lyndon-Smith (julian+maia@dotr.com)
http://www.dotr.com
All rights reserved.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction except as noted below, including without limitation
the rights to use,copy, modify, merge, publish, distribute,
and/or sublicense, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

 The Software and/or source code cannot be copied in whole and
 sold without meaningful modification for a profit.

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.

 Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in
 the documentation and/or other materials provided with
 the distribution.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

using Progress.Lang.*.
using dotr.Maia.Model.*.

/** Maia enum builder 
 * extends the basic builder to add methods to build enum files from the template provided
 * @author Julian Lyndon-Smith (julian+maia@dotr.com) 
 */

class dotr.Maia.Builder.BuildEnum inherits dotr.Maia.Builder.Basic: 
  constructor BuildEnum (p_Class as dotr.Maia.Model):
    super(p_Class).
    find first TTProperty no-lock no-error. /** removes a bug found in 11.2 where an inheriting class needs to reference the TT or error 12378 happens */
  end constructor.

  /** build list of static enum options
   * @param p_Directive : holds the directive name and all parameters
   */
  
  method public void enumProperties(p_Directive as Directive):
    def var lv_Data   as char no-undo.
    def var lv_Assign as char no-undo.
    
    def var lv_base as longchar no-undo.

    assign lv_base = getSource("Enum_Define_Property.i",p_Directive).
    
    
    
    /** loop through, building the defined statement */
    for each TTProperty no-lock by TTProperty.PropertyName:
      if not TTProperty.BuildProperty then next.

      assign lv_Assign = substitute(lv_Base,
                                    TTProperty.PropertyName,
                                    if TTProperty.DataType begins "char" then "'" else "",
                                    TTProperty.InitialValue
                                    )
             lv_Data   = lv_Data + lv_Assign + "~n". 
    end.
    
    assign p_Directive:Data = lv_Data.
    
    ReplaceOptions(p_Directive).                                                                                                                                                
  end method.

  /** get enum by name
   * @param p_Directive : holds the directive name and all parameters
   */
  
  method public void enumGetByName(p_Directive as Directive):
    def var lv_Data as char no-undo.
    def var lv_New  as char no-undo.
    
    def var lv_MaxLen as int no-undo.
    
    
    
    /** get max size of propertyName */
    assign lv_maxLen = length("PrivateData").
    
    /** get max size of property name. Used to align code properly */
    for each TTProperty no-lock by TTProperty.PropertyName:
      if TTProperty.BuildProperty then assign lv_MaxLen = MAX(lv_MaxLen,TTProperty.SIZE).
    end.
    
    assign lv_Data = "case p_Name:~n".
    
    /** loop through all properties, building the assign statement */
    for each TTProperty no-lock by TTProperty.PropertyName:
      
      if not TTProperty.BuildProperty then next.

       assign lv_New = substitute("~twhen '&1':u @then return <<AppPackage>>.<<ClassName>>:&1.~n",
                                 TTProperty.PropertyName
                                 )
              lv_Data   = lv_Data + replace(lv_New,"@",fill(" ",lv_maxLen - TTProperty.Size)).                                          
    end.

    assign lv_Data = lv_Data + "~n~totherwise undo, throw new Progress.Lang.AppError(substitute('Invalid <<AppPackage>>.<<ClassName>> Enum Name [&1]',p_name),-999).~n" 
                             + "end case.~n".
    
    assign p_Directive:Data = lv_Data.
    
    ReplaceOptions(p_Directive).                                                                                                                                                
                                                                                                                                                            
  end method.

  /** get enum by value
   * @param p_Directive : holds the directive name and all parameters
   */
  
  method public void enumGetByValue(p_Directive as Directive):
    def var lv_Data as char no-undo.
    def var lv_New  as char no-undo.
    
    def var lv_MaxLen as int no-undo.
    
    
    
    /** get max size of propertyName */
    assign lv_maxLen = 0.
    
    /** get max size of property name. Used to align code properly */
    for each TTProperty no-lock by TTProperty.PropertyName:
      if TTProperty.BuildProperty then assign lv_MaxLen = MAX(lv_MaxLen,length(TTProperty.InitialValue)).
    end.
    
    assign lv_Data = "case p_Value:~n".
    
    /** loop through all properties, building the assign statement */
    for each TTProperty no-lock by TTProperty.PropertyName:
      
      if not TTProperty.BuildProperty then next.

       assign lv_New = substitute("~twhen &1&2&1 @then return <<AppPackage>>.<<ClassName>>:&3.~n",
                                 if TTProperty.DataType begins "char" then "'" else "",
                                 TTProperty.InitialValue,
                                 TTProperty.PropertyName
                                 )
              lv_Data   = lv_Data + replace(lv_New,"@",fill(" ",lv_maxLen - length(TTProperty.InitialValue))).                                          
    end.

    assign lv_Data = lv_Data + "~n~totherwise undo, throw new Progress.Lang.AppError(substitute('Invalid <<AppPackage>>.<<ClassName>> Enum Value [&1]',p_value),-998).~n" 
                             + "end case.~n".
    
    assign p_Directive:Data = lv_Data.
    
    ReplaceOptions(p_Directive).                                                                                                                                                
                                                                                                                                                            
  end method.

  /** replace standard options
   * @param p_Directive : holds the directive name and all parameters
   */
  
  method private void ReplaceOptions(p_Directive as Directive):
    assign p_Directive:Data = trim(p_Directive:Data,"~n").
  end method.

end class.