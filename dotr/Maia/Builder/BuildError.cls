/*
Copyright (c) 2011, Julian Lyndon-Smith (julian+maia@dotr.com)
http://www.dotr.com
All rights reserved.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction except as noted below, including without limitation
the rights to use,copy, modify, merge, publish, distribute,
and/or sublicense, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

 The Software and/or source code cannot be copied in whole and
 sold without meaningful modification for a profit.

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.

 Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in
 the documentation and/or other materials provided with
 the distribution.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

using Progress.Lang.*.
using dotr.Maia.Model.*.

/** Maia enum builder 
 * extends the basic builder to add methods to build enum files from the template provided
 * @author Julian Lyndon-Smith (julian+maia@dotr.com) 
 */

class dotr.Maia.Builder.BuildError inherits dotr.Maia.Builder.Basic: 
  constructor BuildError (p_Class as dotr.Maia.Model):
    super(p_Class).
    find first TTProperty no-lock no-error. /** removes a bug found in 11.2 where an inheriting class needs to reference the TT or error 12378 happens */
    assign Package   = substitute("&1",p_Class:Package)
           Directory = substitute("&1",p_Class:BaseDirectory).
  end constructor.

/** build an Error class 
   * @param p_Directive : holds the directive name and all parameters
   */
  
  method public void Error(p_Directive as Directive):
    find first TTProperty no-lock no-error.
    
    if not avail TTProperty then return.
    
    p_Directive:Data = substitute('super ("&1", &2).',
                                  TTProperty.PropertyName,
                                  TTProperty.Fieldname).
  end method.



end class.