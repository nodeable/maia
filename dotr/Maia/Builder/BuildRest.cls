/*
Copyright (c) 2011, Julian Lyndon-Smith (julian+maia@dotr.com)
http://www.dotr.com
All rights reserved.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction except as noted below, including without limitation
the rights to use,copy, modify, merge, publish, distribute,
and/or sublicense, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

 The Software and/or source code cannot be copied in whole and
 sold without meaningful modification for a profit.

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.

 Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in
 the documentation and/or other materials provided with
 the distribution.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

using Progress.Lang.*.
using dotr.Maia.Model.*.

/** Maia crud builder 
 * extends the basic builder to add methods to build crud files from the crud.i template provided
 * @author Julian Lyndon-Smith (julian+maia@dotr.com) 
 */

class dotr.Maia.Builder.BuildRest inherits dotr.Maia.Builder.BuildActiveRecordBase: 
  constructor BuildRest (p_Class as dotr.Maia.ActiveRecord):
    super(p_Class). /** directories and packages set in BuildBasicBase, which is inherited by BuildActiveRecordBase */
    find first TTProperty no-lock no-error. /** removes a bug found in 11.2 where an inheriting class needs to reference the TT or error 12378 happens */
        
    assign Package   =  substitute("&1.rest",p_Class:Package)
           Directory = substitute("&1/rest",p_Class:BaseDirectory). 
           
    os-create-dir value(Directory). /** create model directory first */
     
  end constructor.
  
	/** build helpers for all find methods
		* @param 
		* @return 
		*/  
  method public void GetMethods(p_Directive as Directive):
 
  end method.  
  
  /** build helper for POST method
    * @param 
    * @return 
    */  
method public void Post(p_Directive as Directive):
    def var lv_Data   as char no-undo.
    def var lv_Assign as char no-undo.
    
    def var lv_maxLen       as int no-undo.
    def var lv_i             as int no-undo.
    def var lv_tokSize       as int no-undo init 2400. /** default value of 3000 less 20% if not specified as -tok n */
    def var lv_StatementSize as int no-undo.

    
        
    /** see if -tok was specified - if so, grab the value */
    assign lv_tokSize = int(entry(1,substr(session:startup-parameters,index(session:startup-parameters,"-tok") + 5))) * 0.8
           when index("-tok",session:startup-parameters) gt 0.
     
    /** get max size of property name. Used to align code properly */
    for each TTProperty no-lock :
      /** if not temp-table, then a manual property has no db relation , so skip it*/
      if TTProperty.ManualProperty then next.
      
      /** can't store objects in a buffer, so skip em */
      if num-entries(TTProperty.Datatype,".") gt 1 then next.
      
      if TTProperty.BuildProperty then assign lv_MaxLen = MAX(lv_MaxLen,if TTProperty.extent gt 0 then length(TTProperty.PropertyName) + 2 + length(string(TTProperty.extent))
                                                                                                else length(TTProperty.PropertyName)).
    end.
    
    /** now let's loop through, building the assign statement */
    for each TTProperty no-lock break by TTProperty.PropertyName:
      
      if not TTProperty.BuildProperty then next.

      /** a manual property has no db relation , so skip it*/
      if TTProperty.ManualProperty then next.
      if TTProperty.FieldName eq "" then next.
      
      /** can't store objects in a buffer, so skip em */
      if num-entries(TTProperty.Datatype,".") gt 1 then next.
      
      /** if field is an array, and in a word index then we must split the assignment */
      if TTProperty.extent gt 0 and TTProperty.IsArrayWordIndex then
      do lv_i = 1 to TTProperty.extent:
        assign lv_Assign = substitute("if get-field('&1_&3')@ ne '' then assign <<ClassName>>1:&1[&3]@ = &4get-field('&1_&3')&5.~n",TTProperty.PropertyName,
                                                                                          TTProperty.FieldName,
                                                                                          lv_i,
                                                                                          if TTProperty.DataType begins "char" then "" else string(TTProperty.DataType) + "(",
                                                                                          if TTProperty.DataType begins "char" then "" else ")")
                                                                                          
               lv_Data   = lv_Data + replace(lv_Assign,"@",fill(" ",lv_maxLen - length(TTProperty.PropertyName) - 
                                             (if TTProperty.extent lt 9 then 3
                                                                       else if TTProperty.extent lt 99 then 4
                                                                       else 5))).
      end.
      
      else /** create a single assignment */
      do:
        assign lv_Assign = substitute("if get-field('&1')@ ne '' then assign <<ClassName>>1:&1@ = &3get-field('&1')&4.~n",TTProperty.PropertyName,
                                                                          TTProperty.FieldName,
                                                                          if TTProperty.DataType begins "char" then "" else string(TTProperty.DataType) + "(",
                                                                          if TTProperty.DataType begins "char" then "" else ")"
                                                                          )
               lv_Assign = replace(lv_Assign,"@",fill(" ",lv_maxLen - length(TTProperty.PropertyName)))
               lv_Data   = lv_Data + lv_Assign.
      end.
      
      assign lv_StatementSize = lv_StatementSize + length(lv_Assign).
      
      /** each assign line has approx 8 tokens in it. Make sure that this assign statement does not go above the -tok setting. 
       *  if so, break the statement up into multiple assign statements */
       
      if lv_StatementSize / 8 ge lv_tokSize  then
      do:
         assign lv_Data = lv_Data + substitute(". /* split as statement [&1] approaching -tok [&2] */~n",lv_StatementSize,lv_tokSize)
                lv_StatementSize = 0.
                
        if not last(TTProperty.PropertyName) then assign lv_Data = lv_Data + "assign ".
                        
      end.
    end.
    
    assign p_Directive:Data = trim(lv_Data,"~n").
                                                                                                                                                      
  end method.

end class.