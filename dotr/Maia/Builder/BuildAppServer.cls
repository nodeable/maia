/*
Copyright (c) 2011, Julian Lyndon-Smith (julian+maia@dotr.com)
http://www.dotr.com
All rights reserved.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction except as noted below, including without limitation
the rights to use,copy, modify, merge, publish, distribute,
and/or sublicense, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

 The Software and/or source code cannot be copied in whole and
 sold without meaningful modification for a profit.

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.

 Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in
 the documentation and/or other materials provided with
 the distribution.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

using Progress.Lang.*.
using dotr.Maia.Model.*.

/** Maia crud builder 
 * extends the basic builder to add methods to build crud files from the crud.i template provided
 * @author Julian Lyndon-Smith (julian+maia@dotr.com) 
 */

class dotr.Maia.Builder.BuildAppServer inherits dotr.Maia.Builder.BuildActiveRecordBase: 
  constructor BuildAppServer (p_Class as dotr.Maia.ActiveRecord):
    super(p_Class). /** directories and packages set in BuildBasicBase, which is inherited by BuildActiveRecordBase */
    find first TTProperty no-lock no-error. /** removes a bug found in 11.2 where an inheriting class needs to reference the TT or error 12378 happens */
        
    assign Package   =  substitute("&1.AppServer",p_Class:Package)
           Directory = substitute("&1/AppServer",p_Class:BaseDirectory). 
           
    os-create-dir value(Directory). /** create model directory first */
     
  end constructor.

  /** build helpers for all find methods
    * @param 
    * @return 
    */  
method public void AppServer_Token(p_Directive as Directive):
  p_Directive:Data = GetFileContents(p_Directive,"Builder/appserver_token.i").
end method.
  
	/** build helpers for all find methods
		* @param 
		* @return 
		*/  
method public void AppServer_FindUsing(p_Directive as Directive):
    def var lv_Header as longchar no-undo.

    def var lv_Index as char no-undo.
    def var lv_data  as char no-undo.
    def var lv_param as char no-undo.
    def var lv_opt   as char no-undo.
    
    def var lv_i as int no-undo.
    
    assign lv_header = GetSource(p_Directive).
    
    for each TTFindUsing no-lock:
      assign lv_param = "".
      
      do lv_i =  1 to num-entries(TTFindUsing.parameterCall):
        
        assign lv_opt = trim(replace(entry(lv_i,TTFindUsing.parameterCall),"p_","")).
        
        find TTproperty where TTProperty.PropertyName eq lv_opt no-lock no-error.
        
        assign lv_opt = if not TTProperty.DataType begins "char" 
                           then substitute("&1(Parameters:data::&2)",TTProperty.DataType,lv_opt)
                           else substitute("Parameters:data::&1",lv_opt).
             
        assign lv_param = lv_param + ", " + lv_opt.
      end.
      
      assign lv_Index = replace(lv_header,"<<GetName>>",TTFindUsing.GetName)
             lv_index = replace(lv_index,"<<DocParameterList>>",TTFindUsing.DocParameterList)
             lv_index = replace(lv_index,"<<FindString>>",TTFindUsing.FindString[1] + TTFindUsing.FindString[2])
             lv_index = replace(lv_index,"<<ParameterList>>",TTFindUsing.ParameterList)
             lv_index = replace(lv_index,"<<ParameterCall>>",trim(lv_Param,",")).
             
             lv_data = lv_Data + "~n" + lv_index.
    end.

    assign p_Directive:Data = lv_data.
    
  end method.

  method public void Appserver_FindFromJson(p_Directive as Directive):
    def var lv_data  as longchar no-undo.
    def var lv_param as longchar no-undo.
    
    def var lv_i as int no-undo.
    
    def var Model1 as dotr.Maia.ActiveRecord no-undo.
    
    Model1 = cast(Model,dotr.Maia.ActiveRecord).
    
    case Model1:KeyFieldFind:
      when "rowid" then assign p_Directive:Data = "".
      
      otherwise
      do:
        do lv_i = 1 to extent(Model1:KeyProperty):
          assign lv_Param = substitute("Parameters:data::&1",Model1:KeyProperty[lv_i]).
          
          find TTproperty where TTProperty.PropertyName eq Model1:KeyProperty[lv_i] no-lock no-error.
          
          if not TTProperty.DataType begins "char" then
             assign lv_Param = substitute("&1(&2)",TTProperty.DataType,lv_Param).
            
            
          assign lv_data = lv_Data + "," + lv_Param.
        end.
        
        assign p_Directive:Data = substitute("otherwise <<ClassName>>1 = (new <<ParentPackage>>.<<ClassName>>()):Find(&1).",trim(lv_data,",")).
      end.
    end case.
  end method.  

end class.