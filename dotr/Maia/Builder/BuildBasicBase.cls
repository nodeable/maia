/*
Copyright (c) 2011, Julian Lyndon-Smith (julian+maia@dotr.com)
http://www.dotr.com
All rights reserved.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction except as noted below, including without limitation
the rights to use,copy, modify, merge, publish, distribute,
and/or sublicense, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

 The Software and/or source code cannot be copied in whole and
 sold without meaningful modification for a profit.

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.

 Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in
 the documentation and/or other materials provided with
 the distribution.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

using Progress.Lang.*.
using dotr.Maia.Model.*.

/** Maia crud builder 
 * extends the basic builder to add methods to build crud files from the crud.i template provided
 * @author Julian Lyndon-Smith (julian+maia@dotr.com) 
 */

class dotr.Maia.Builder.BuildBasicBase inherits dotr.Maia.Builder.Basic: 
  
  constructor BuildBasicBase(p_class as dotr.Maia.Model):
    super(p_class).
    find first TTProperty no-lock no-error. /** removes a bug found in 11.2 where an inheriting class needs to reference the TT or error 12378 happens */
    
    assign Package   =  substitute("&1.Model.Base",p_class:Package)
           Directory = substitute("&1/Model",p_Class:BaseDirectory). 
           
    os-create-dir value(Directory). /** create model directory first */

    assign Directory = substitute("&1/Model/Base",p_Class:BaseDirectory).
           
    os-create-dir value(Directory).
    
    p_class:SetDefaultOption("ModelPackage",substitute("&1.Model",p_class:Package)).
    p_class:SetDefaultOption("ParentPackage",substitute("&1.Model",p_class:Package)).
    
  end constructor.

  /** generate next-value statements
   * loop through all properties configured with a sequence and generate a next-value statement
   * only set the property if it is currently unknown or 0
   * @param p_Directive : holds the directive name and all parameters
   */

  method public void NextValue(p_Directive as Directive):
    
    def var lv_Data as char no-undo.
    
    for each TTProperty where TTProperty.Sequence gt "" no-lock:
       
      /** is there a sequence defined for this property */
      if TTProperty.Sequence eq ? or TTProperty.Sequence eq "-- Not Used --" then next.
       
      assign lv_Data = lv_Data + substitute("~nif this-object:&1 eq 0 or this-object:&1 eq ? then assign this-object:&1  = next-value(&2,<<DB>>).",
                                             TTProperty.PropertyName,
                                             TTProperty.Sequence).
    end.
    assign p_Directive:Data = trim(lv_Data,"~n").                                                                                                                                                  
  end method.
  
  /** generate guid statements
   * loop through all properties configured with a generateGUID flag and generate a next-value statement
   * only set the property if it is currently unknown or ""
   * @param p_Directive : holds the directive name and all parameters
   */

  method public  void GenerateGUID(p_Directive as Directive):
    
    def var lv_Data as char no-undo.
    
    for each TTProperty no-lock:
      if TTProperty.GenerateGUID then 
          assign lv_Data = lv_Data + substitute("~nif this-object:&1 eq '' or this-object:&1 eq ? then assign this-object:&1  = guid(generate-uuid).",
                                                TTProperty.PropertyName).
    end.
    assign p_Directive:Data = trim(lv_Data,"~n").
  end method.
  
  /** sends a message to ActiveMQ when record is updated
   *
   *  @param p_Directive : holds the directive name and all parameters
   */

  method public void SendUpdateMessage(p_Directive as Directive):
    assign p_Directive:Data = if GetParameter(p_Directive,"UseActiveMQ") eq "true" or Model:SendStompUpdate
                                 then getSource("ActiveRecordSendMessageRowid.i",p_Directive)
                                 else "".
           p_Directive:Data = "".                      
  end method.  


end class.