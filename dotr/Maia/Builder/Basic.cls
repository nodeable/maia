/*
Copyright (c) 2011, Julian Lyndon-Smith (julian+maia@dotr.com)
http://www.dotr.com
All rights reserved.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction except as noted below, including without limitation
the rights to use,copy, modify, merge, publish, distribute,
and/or sublicense, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

 The Software and/or source code cannot be copied in whole and
 sold without meaningful modification for a profit.

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.

 Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in
 the documentation and/or other materials provided with
 the distribution.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

using Progress.Lang.*.
using dotr.Maia.Model.*.

/** Maia Basic builder 
 * Core methods to build source file from the template provided
 * @author Julian Lyndon-Smith (julian+maia@dotr.com) 
 */

class dotr.Maia.Builder.Basic abstract:

  
  {dotr/maia/temptable/Property.i &AccessMode="protected" &REFERENCE-ONLY="reference-only"}
  
/** temptable to hold the meta-data for an object. 
 * Meta-data from each object (from the Maia Config directory) can 
 * hold information from a database (created using dotr/Maia/BuildTableMVC.w) 
 * and then enhanced with custom properties 
 */

  def protected property Model   as dotr.Maia.Model no-undo get . private set .
  
  def protected var DatatypeSize as  int  extent 10 no-undo.
  
  def public property BuilderAvailable as logical init yes no-undo get . protected set .
  
  def public property Directory as char no-undo get . protected set .
  def public property Package   as char no-undo get . protected set .
  
  /** p_Model is the instance of the Maia Config/foo.xml file */
  constructor Basic (p_Model as dotr.Maia.Model):
    def var lv_i as int no-undo.
    
    def var lv_path as char no-undo.
    
    assign Directory  = "dotr/sports/model/base"
           Package    = "dotr.Sports.Base"
           Model      = p_Model.

    Model:GetProperties(output table TTProperty bind).
    
    find first TTProperty no-lock no-error. /** removes a bug found in 11.2 where an inheriting class needs to reference the TT or error 12378 happens */
  end constructor.

 /** get source template contents
  *
  *  @param p_Directive : holds the directive name and all parameters
  */

  method protected longchar getSource(p_Directive as Directive):
    return getSource("",p_Directive).
  end method.

 /** get source template contents
  *
  *  @param p_template  : holds the template to use
  *  @param p_Directive : holds the directive name and all parameters
  */

  method protected longchar getSource(p_template as char, p_Directive as Directive):

    def var lv_Header as longchar no-undo.
    
    /** check for custom builder file */
    assign file-info:file-name = if GetParameter(p_Directive,"Source") eq "" then substitute("dotr/Maia/Template/Builder/&1",p_template)
                                                                             else GetParameter(p_Directive,"Source").                                  
    
    if file-info:full-pathname eq ? then assign lv_header = substitute("/** template &1 not found */",p_template). 
    else copy-lob from file file-info:full-pathname to lv_Header.
    
    return lv_Header.
  end method.

  /** Dummy placeholder for start of build event
   * @param p_Data : Contents of template
   * @return longchar: altered data
   */
   
  method public void StartBuild():
  end method.

  /** Dummy placeholder for end of build event
   * @param p_Data : Contents of modified template
   * @return longchar: altered data
   */

  method public void EndBuild():
  end method.
  
  /** Example of how to write a build method. Not a very useful method ;)
   * @param p_Directive : holds the directive name and all parameters
   */
  method public void Using(p_Directive as Directive):
    def var lv_i as int no-undo.
    def var lv_j as int no-undo.

    /** loop through all parameters of directive */    
    do lv_i = 1 to extent(p_Directive:ParameterData):
      do lv_j = 1 to num-entries(p_Directive:ParameterData[lv_i]):
        assign p_Directive:Data = p_Directive:Data + substitute("using &1~n",entry(lv_j,p_Directive:ParameterData[lv_i])).
      end.
    end.
    
  end method.
  
  /** get the file contents of selected file 
   *  @param p_Directive Directive
   *  @param p_File default file to use
   */
  method protected longchar GetFileContents(p_Directive as Directive,p_File as char):
    def var lv_Header as longchar no-undo.
    
    def var lv_File as char no-undo.
    
    assign lv_File             = if GetParameter(p_Directive,"Source") eq "" then p_File else GetParameter(p_Directive,"Source")
           file-info:file-name = search(substitute("&1/&2",Model:TemplateDirectory,lv_File)).
           
    if file-info:full-pathname eq ? then assign file-info:file-name = search(substitute("dotr/Maia/Template/&1",lv_File)).                                                 
    
    if file-info:full-pathname eq ? then return "".

    copy-lob from file file-info:full-pathname to lv_Header.
    
    return lv_header.

  end method.

  /** Example of how to write a build method. Not a very useful method ;)
   * Gives example of how to use parameter if supplied, or default to standard option
   * @param p_Directive : holds the directive name and all parameters
   */

  method public void MethodHeader(p_Directive as Directive):
    def var lv_i as int no-undo.

    def var lv_Header as longchar no-undo.
    
    assign lv_Header = GetFileContents(p_Directive,"builder/MethodHeader.i").
    
    /** loop through all parameters of directive
     * all options are used to substitute <<option>> tags in the supplied parameters */    
    do lv_i = 1 to extent(p_Directive:ParameterData):
      case p_Directive:ParameterName[lv_i]:
        otherwise assign lv_Header = replace(lv_Header, substitute("<<&1>>",p_Directive:ParameterName[lv_i]),p_Directive:ParameterData[lv_i]). 
      end case.
    end.
    
    assign p_Directive:Data = lv_Header. 
    
  end method.

  /** Example of how to write a build method. Not a very useful method ;)
   * Gives example of how to use parameter if supplied, or default to standard option
   * @param p_Directive : holds the directive name and all parameters
   */

  method public void Inherits(p_Directive as Directive):
    if Model:inherits ne "" then assign p_Directive:Data = substitute(" inherits &1",Model:inherits).
    
    else assign p_Directive:TrimCode = no. /** don't wipe the line if this code does not exist */
                                                  
  end method.


  /** Generate JavaDoc method header comment
   * @param p_Directive : holds the directive name and all parameters
   */

  method public void JavaDocHeader(p_Directive as Directive):
    def var lv_i as int no-undo.
    
    def var lv_Data as char no-undo.
    
    def var lv_Header as longchar no-undo.
    
    assign file-info:file-name = if GetParameter(p_Directive,"Source") eq "" then "dotr/Maia/Template/builder/JavaDocHeader.i"
                                                                             else GetParameter(p_Directive,"Source").                                  
      
    copy-lob from file file-info:full-pathname to lv_Header.
    
    /** loop through all parameters of directive
     * all options are used to substitute <<option>> tags in the supplied parameters 
     * the \n tag is used to insert  new line in the comment      
     * @param p_Directive : holds the directive name and all parameters
     */
    do lv_i = 1 to extent(p_Directive:ParameterData):
      assign lv_Data = p_Directive:ParameterData[lv_i]
             lv_Data = replace(lv_Data,"~\n","~n * ").
             
      case p_Directive:ParameterName[lv_i]:
        otherwise assign lv_Header = replace(lv_Header, substitute("<<&1>>",p_Directive:ParameterName[lv_i]),lv_Data). 
      end case.
    end.
    
    assign lv_header = replace(lv_header," * <<LongDescription>>~n","") /** no longdescription supplied */ 
           lv_header = replace(lv_header," * <<Parameters>>~n","") /** no parameters supplied */ 
           lv_header = replace(lv_header," * <<ReturnValue>>~n","") /** no ReturnValue supplied */ 
           p_Directive:Data = lv_Header. 
    
  end method.

  /** insert custom code
   * calls the getCustomCode method in the Model class to insert custom code into the template
   * @param p_Directive : holds the directive name and all parameters
   */

  method public void CustomCode(p_Directive as Directive):
    def var CustomCode1 as dotr.Maia.Helper.CustomCode no-undo.
    
    def var lv_i as int no-undo.
    
    def var lv_Data as longchar no-undo.

    CustomCode1 = new dotr.Maia.Helper.CustomCode().
    
    /** loop through all parameters of directive     */
    do lv_i = 1 to extent(p_Directive:ParameterData):
      lv_Data = CustomCode1:GetCustomCode(Model:BuildFile,p_Directive:ParameterData[lv_i]).

      if lv_Data eq "" then next.

      assign p_Directive:Data = p_Directive:Data + "~n" + lv_Data.
    end.
 
    assign p_Directive:Data = trim(p_Directive:Data,"~n"). 
 
  end method.
  
 /** reads all Option directives in a template
   * @param p_Data : template contents
   * @return longchar : modified template
   */
  
  method private longchar GetOptionsFromTemplate(p_Data as longchar):
    def var Directive1 as dotr.Maia.Model.Directive no-undo.
    
    def var lv_Startindex as int no-undo.
    def var lv_Endindex   as int no-undo.
    def var lv_i          as int no-undo.
        
    /** loop through all defineoption directives */
    do while true on error undo, throw:
      assign lv_StartIndex = index(p_Data,"~{&! DefineOption").
      if lv_StartIndex le 0 then leave.
     
      assign lv_EndIndex  = index(p_Data,"!}",lv_StartIndex) + 2.

      Directive1 = new dotr.Maia.Model.Directive(string(substr(p_Data,lv_StartIndex,lv_EndIndex - lv_StartIndex))).
     
      assign substr(p_Data,lv_StartIndex,(index(p_Data,"~n",lv_StartIndex) + 1) - lv_StartIndex) = "".
      
      do lv_i = 1 to extent(Directive1:ParameterData):
        Model:SetDefaultOption(Directive1:ParameterName[lv_i],Directive1:ParameterData[lv_i]).
      end.
      
      finally:
        delete object Directive1 no-error.   
      end finally.                   
    end.
    
    return p_Data.
  
  end method.
          

  /** build the "def property" statement for each of the properties
   * @param p_Directive : holds the directive name and all parameters
   */

  method public void DefineProperties(p_Directive as Directive):
    def var lv_i      as int no-undo.
    def var lv_MaxLen as int  no-undo.
      
    def var lv_DataType    as char no-undo.
    def var lv_PropertyDef as char no-undo init "def &5 property &1 as &2 no-undo &3&6 &8 &4&7 &9".
    def var lv_Property    as char no-undo.
    def var lv_Comments    as char no-undo.
    def var lv_lazy        as char no-undo.
    def var lv_GetMode     as char no-undo.
    def var lv_SetMode     as char no-undo.
    def var lv_Shadow      as char no-undo.
    def var lv_Set         as char no-undo.
    
    /** loop through all properties again
     * this time, generating the actual code. Spacers are placed to align the code */
          
    for each TTProperty no-lock break by TTProperty.DataType by TTProperty.PropertyName:
      if first-of(TTProperty.DataType) then assign lv_MaxLen = 0.
      
      /** don't build invalid properties */
      if not TTProperty.BuildProperty then next.
      
      assign lv_Comments = ""
             lv_GetMode  = TTProperty.GetMode
             lv_SetMode  = TTProperty.SetMode
             lv_MaxLen   = Max(lv_MaxLen,length(TTProperty.PropertyName)).
       
      if TTProperty.Comment ne "" or TTProperty.Brief ne "" then
      do:
      
        assign lv_Comments = substitute("  /** &1",TTProperty.Brief).
        
        do lv_i = 1 to num-entries(TTProperty.Comment,"~n"):
          assign lv_Comments = substitute("&1~n   * &2",entry(lv_i,lv_Comments,"~n")).
        end.
        
        assign lv_Comments = lv_Comments + " */".
      end.
      
      case TTProperty.AccessMode:
        when "protected" then assign lv_GetMode = "protected" when lv_GetMode eq "public"
                                     lv_SetMode = "protected" when lv_SetMode eq "public".
        
        when "private"   then assign lv_GetMode = "private" when lv_GetMode ne "private"
                                     lv_SetMode = "private" when lv_SetMode ne "private".
      end case.                              
      
      
      assign lv_Lazy = if TTProperty.GetCode ne "" then "~n~t&1 get():~n~t~t&2~n~tend get ." 
                                                       else "&1 get .".
      
      assign lv_Set           = lc(if TTProperty.SetCode ne "" then "~n~t&1 set(p_Value as &3):~n~t~t&2~n~tend set ." 
                                                              else "&1 set .")
             lv_Set           = substitute(lv_Set,
                                           lc(lv_SetMode),
                                           TTProperty.SetCode,
                                           lc(TTProperty.datatype))
                                                                                                      
             lv_Property      = substitute(lv_PropertyDef,Pad(TTProperty.PropertyName), 
                                                          if num-entries(TTProperty.datatype,".") gt 1 then TTProperty.datatype else if TTProperty.datatype = "clob" then "longchar" else lc(TTProperty.datatype), 
                                                          if TTProperty.Extent gt 0 or TTProperty.extent eq ? then substitute("extent &1 &2",
                                                          																																		if TTProperty.extent eq ? 
                                                          																																				then "" 
                                                          																																				else string(TTProperty.extent),
                                                          																																		"&1") 
                                    																																					 else "&1",
                                                          if TTProperty.Comment ne "" then substitute("/* &1 */",TTProperty.Comment) else "",
                                                          lc(TTProperty.AccessMode),
                                                          lc(substitute(lv_Lazy,
                                                                        lv_GetMode,
                                                                        TTProperty.GetCode)),
                                                          if TTProperty.LoadLazy then "~n" else "",
                                                          lv_Set,
                                                          if TTProperty.Brief ne "" then substitute("/** &1 */",TTProperty.Brief) else "")
                                                          
						 lv_Property      = substitute(lv_Property,if TTProperty.InitialValue eq "" 
						 																							then ""
						 																							else substitute("init &1&2&1",
						 																															if TTProperty.DataType eq "character"
						 																																 then '"'
						 																																 else "",
						 																															TTProperty.InitialValue))
             lv_Property      = "~n" + lv_Property when TTProperty.GetCode ne "" or TTProperty.SetCode ne ""  
             p_Directive:Data = p_Directive:Data + "~n" + lv_Property.

      /** insert blank line for each break of data type */
      if last-of(TTProperty.DataType) then 
      do:
        assign p_Directive:Data = Align(p_Directive:Data,lv_MaxLen) 
               p_Directive:Data = p_Directive:Data + "~n".
      end.
      
    end.

    assign p_Directive:Data = left-trim(p_Directive:Data,"~n").
  end method.
  
  /** Insert copyright message in each file
   * @param p_Directive : holds the directive name and all parameters
   */
  method public void Copyright(p_Directive as Directive):
    assign p_Directive:Data = GetFileContents(p_Directive,"Builder/Copyright.i").
  end method.

  /** Insert version control ID tag
   * @param p_Directive : holds the directive name and all parameters
   */
  
  method public void ID(p_Directive as Directive):
    assign p_Directive:Data = "/* $Id$ */".
  end method.
  
  /** create "instance" statements
   * used to create pseudo-singletons. (Singletons without the static defintions 
   * @param p_Directive : holds the directive name and all parameters
   */
  method public void DefineInstance(p_Directive as Directive):
    def var lv_Data as longchar no-undo.
    
    assign file-info:file-name = if GetParameter(p_Directive,"Source") eq "" then "dotr/Maia/Template/builder/instance.i" 
                                                                             else GetParameter(p_Directive,"Source").
    
    copy-lob from file file-info:full-pathname to lv_Data.
    
    assign p_Directive:Data = lv_Data. 
  end method.

  /** Example of how to write a build method. Not a very useful method ;)
   * @param p_Directive : holds the directive name and all parameters
   */

  method public void DefineDefaultconstructor(p_Directive as Directive):
    assign p_Directive:Data = "constructor <<ClassName>>():~nend constructor.".    
  end method.

  /** gets the value of a directive parameter
   * @param p_Directive : holds the directive name and all parameters
   * @return char : Value of parameter. Blank if not found
   */
   
  method protected char GetParameter(p_Directive as Directive,p_Label as char):
    def var lv_i as int no-undo.
    
    do lv_i = 1 to extent(p_Directive:ParameterData):
      if p_Directive:ParameterName[lv_i] eq p_Label then return p_Directive:ParameterData[lv_i].
    end.
    
    return "".
  end method.  

  /** determine if constructor must use startup  
   * @param p_Directive : holds the directive name and all parameters
   */

  method public void Basic_Startup(p_Directive as Directive):
    assign p_Directive:Data = "StartUp().".
    return.
  end method.
  
  /** build temp-table serialisation 
   * @param p_Directive : holds the directive name and all parameters
   */

  method public void DefineTempTable(p_Directive as Directive):
    
     
    def var lv_i      as int no-undo.

    def var lv_MaxLen as  int  no-undo .
      
    def var lv_DataType    as char no-undo.
    def var lv_PropertyDef as char no-undo init "~t~tfield &1@ as &2 &3 serialize-name '&4'".
    def var lv_Property    as char no-undo.
    
    def var lv_Data as longchar no-undo.

    /** don't generate if option is not set */
/*    if not Model:TempTableOnly then return.*/

    assign lv_Data  = 'def protected temp-table TT_<<TableName>> no-undo serialize-name "<<ClassName>>" ~n'.

    /** loop through all properties again
     * this time, generating the actual code. Spacers are placed to align the code */
          
    for each TTProperty no-lock break by TTProperty.DataType by TTProperty.PropertyName:
      if num-entries(TTProperty.DataType,".") gt 1 then next. /** skip objects */
      
      if first-of(TTProperty.DataType) then assign lv_MaxLen = 0.
      
      /** don't build invalid properties */
      if TTProperty.PropertyName eq "" then next.
      if not TTProperty.BuildProperty then next.
      if TTProperty.Extent eq ? then next.
      
      assign lv_Property      = substitute(lv_PropertyDef,Pad(TTProperty.PropertyName), 
                                                          if num-entries(TTProperty.datatype,".") gt 1 then TTProperty.datatype 
                                                                                                      else if TTProperty.datatype eq "longchar"
                                                                                                              then "clob"
                                                                                                              else lc(TTProperty.datatype), 
                                                          if TTProperty.Extent gt 0 or TTProperty.extent eq ? then substitute("extent &1",if TTProperty.extent eq ? then "" else string(TTProperty.extent)) else "",
                                                          TTProperty.PropertyName)
             lv_Data = lv_Data + "~n" + lv_Property.

      /** insert blank line for each break of data type */
      if last-of(TTProperty.DataType) then assign lv_Data = Align(lv_Data,lv_MaxLen) + "~n".
    end.
    
    assign p_Directive:Data = trim(lv_Data ,"~n") + ".".
  end method.
  
  /** pad a char field out so that we can prepare for alignment
   * @param data to pad
   * @return padded data
   */
  
  method protected  char Pad(p_Data as char):
    return p_Data + fill("@",36 - length(p_Data)).
  end method.

  /** pad a char field out so that we can prepare for alignment
   * @param data to pad
   * @return padded data
   */
  
  method protected longchar Align(p_Data as longchar,p_MaxLen as int):
    def var lv_Data as longchar no-undo.
    
    assign lv_Data = replace(p_Data,fill("@",36 - p_MaxLen),"") 
           lv_Data = replace(lv_Data,"@"," ").
           
    return lv_Data.           
  end method.
  
end class.
