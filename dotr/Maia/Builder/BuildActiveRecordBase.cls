/*
Copyright (c) 2011, Julian Lyndon-Smith (julian+maia@dotr.com)
http://www.dotr.com
All rights reserved.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction except as noted below, including without limitation
the rights to use,copy, modify, merge, publish, distribute,
and/or sublicense, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

 The Software and/or source code cannot be copied in whole and
 sold without meaningful modification for a profit.

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.

 Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in
 the documentation and/or other materials provided with
 the distribution.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

using Progress.Lang.*.
using dotr.Maia.Model.*.

/** Maia crud builder 
 * extends the basic builder to add methods to build crud files from the crud.i template provided
 * @author Julian Lyndon-Smith (julian+maia@dotr.com) 
 */

class dotr.Maia.Builder.BuildActiveRecordBase inherits dotr.Maia.Builder.BuildBasicBase:
  
  {dotr/maia/temptable/Association.i &AccessMode="protected" &REFERENCE-ONLY="reference-only"}
  {dotr/maia/temptable/FindUsing.i   &AccessMode="protected" &REFERENCE-ONLY="reference-only"}
  {dotr/maia/temptable/Validation.i  &AccessMode="protected" &REFERENCE-ONLY="reference-only"}
  
  constructor BuildActiveRecordBase (p_Class as dotr.Maia.ActiveRecord):
    super(p_Class).
    find first TTProperty no-lock no-error. /** removes a bug found in 11.2 where an inheriting class needs to reference the TT or error 12378 happens */
    
    p_Class:BuildFindUsing().
    
    p_class:GetAssociations(output table TTAssociation bind).
    p_class:GetFindUsing(output table TTFindUsing bind).
    p_class:GetValidation(output table TTValidation bind).
    
  end constructor.

  /** build the "def property" statement for each of the properties
   * @param p_Directive : holds the directive name and all parameters
   */

  method public void DefineAssociationProperties(p_Directive as Directive):
    def var lv_fkGetCode   as longchar no-undo.
    def var lv_ShadowCode  as longchar no-undo.
    def var lv_fkGet       as longchar no-undo.
    def var lv_Shadow      as longchar no-undo.
    
    assign lv_fkGet  = GetFileContents(p_Directive,"builder/ActiveRecordModel_fk_Get.i")        
           lv_Shadow = GetFileContents(p_Directive,"builder/ActiveRecordModel_fk_GetShadow.i"). 

    for each TTAssociation no-lock:
      assign lv_fkGetCode  = lv_fkGetCode  + MergeAssociation(rowid(TTAssociation),lv_fkGet) + "~n~n"
             lv_ShadowCode = lv_ShadowCode + MergeAssociation(rowid(TTAssociation),lv_Shadow) + "~n".
    end.

    assign p_Directive:Data = p_Directive:Data + "~n" + trim(lv_Shadowcode) + "~n~n" + trim(lv_fkGetCode) . 
           p_Directive:Data = left-trim(p_Directive:Data,"~n").
  end method.
   /** generate assign to buffer statement
   *
   *  loop through all properties for an object, and build the appropriate assign to buffer statement
   *  @param p_Directive : holds the directive name and all parameters
   */

  method public void ActiveRecordModel_AssignToBuffer(p_Directive as Directive):
    def var lv_Data   as char no-undo.
    def var lv_Assign as char no-undo.
    
    def var lv_maxLen  			as int no-undo.
    def var lv_i       			 as int no-undo.
    def var lv_tokSize 			 as int no-undo init 2400. /** default value of 3000 less 20% if not specified as -tok n */
    def var lv_StatementSize as int no-undo.
    
    /** see if -tok was specified - if so, grab the value */
    assign lv_tokSize = int(entry(1,substr(session:startup-parameters,index(session:startup-parameters,"-tok") + 5))) * 0.8
    			 when index("-tok",session:startup-parameters) gt 0.
     
    /** get max size of property name. Used to align code properly */
    for each TTProperty no-lock :
      /** if not temp-table, then a manual property has no db relation , so skip it*/
      if TTProperty.ManualProperty then next.
      
      /** can't store objects in a buffer, so skip em */
      if num-entries(TTProperty.Datatype,".") gt 1 then next.
      
      /** if read only, skip */
      if TTProperty.ReadOnly then next.
      
      if TTProperty.BuildProperty then assign lv_MaxLen = MAX(lv_MaxLen,if TTProperty.extent gt 0 then length(TTProperty.FieldName) + 2 + length(string(TTProperty.extent))
                                                                                                else length(TTProperty.FieldName)).
    end.
    
    /** now let's loop through, building the assign statement */
    for each TTProperty no-lock break by TTProperty.PropertyName:
      
      if not TTProperty.BuildProperty then next.

      /** a manual property has no db relation , so skip it*/
      if TTProperty.ManualProperty then next.
      if TTProperty.FieldName eq "" then next.
      
      /** if read only, skip */
      if TTProperty.ReadOnly then next.
      
      /** can't store objects in a buffer, so skip em */
      if num-entries(TTProperty.Datatype,".") gt 1 then next.
      
      /** if field is an array, and in a word index then we must split the assignment */
      if TTProperty.extent gt 0 and TTProperty.IsArrayWordIndex then
      do lv_i = 1 to TTProperty.extent:
        assign lv_Assign = substitute("hUpdateBuffer::&2[&3]@ = this-object:&1[&3]~n",TTProperty.PropertyName,
                                                                                          TTProperty.FieldName,
                                                                                          lv_i)
               lv_Data   = lv_Data + replace(lv_Assign,"@",fill(" ",lv_maxLen - length(TTProperty.FieldName) - 
                                             (if TTProperty.extent lt 9 then 3
                                                                       else if TTProperty.extent lt 99 then 4
                                                                       else 5))).
      end.
      
      else /** create a single assignment */
      do:
        assign lv_Assign = substitute("hUpdateBuffer::&2@ = this-object:&1~n",TTProperty.PropertyName,
                                                                          TTProperty.FieldName)
							 lv_Assign = replace(lv_Assign,"@",fill(" ",lv_maxLen - length(TTProperty.FieldName)))
               lv_Data   = lv_Data + lv_Assign.
      end.
      
      assign lv_StatementSize = lv_StatementSize + length(lv_Assign).
      
      /** each assign line has approx 8 tokens in it. Make sure that this assign statement does not go above the -tok setting. 
       *  if so, break the statement up into multiple assign statements */
       
      if lv_StatementSize / 8 ge lv_tokSize  then
      do:
         assign lv_Data = lv_Data + substitute(". /* split as statement [&1] approaching -tok [&2] */~n",lv_StatementSize,lv_tokSize)
			          lv_StatementSize = 0.
                
				if not last(TTProperty.PropertyName) then assign lv_Data = lv_Data + "assign ".
				                
      end.
    end.
    
    assign p_Directive:Data = trim(lv_Data,"~n") + ".".
    
                                                                                                                                                      
  end method.

  /** create statement to assign database fields to properties
   * @param p_Directive : holds the directive name and all parameters
   */
  
  method public void ActiveRecordModel_AssignFromBuffer(p_Directive as Directive):
    
    
    def var lv_Data     as char no-undo.
    def var lv_Assign   as char no-undo.
    
    def var lv_maxLen 			 as int no-undo.
    def var lv_tokSize 			 as int no-undo init 2400. /** default value of 3000 less 20% if not specified as -tok n */
    def var lv_StatementSize as int no-undo.
    
    /** see if -tok was specified - if so, grab the value */
    assign lv_tokSize = int(entry(1,substr(session:startup-parameters,index(session:startup-parameters,"-tok") + 5))) * 0.8
    			 when index("-tok",session:startup-parameters) gt 0.
    
    /** get max size of property name. Used to align code properly */
    for each TTProperty no-lock :
    
      /** a manual property has no db relation , so skip it*/
      if TTProperty.ManualProperty then next.
      
      /** can't store objects in a buffer, so skip em */
      if num-entries(TTProperty.Datatype,".") gt 1 then next.
      
      if TTProperty.BuildProperty then assign lv_MaxLen = MAX(lv_MaxLen,length(TTProperty.fieldname)).
    end.
    
    /** now let's loop through, building the assign statement */
    for each TTProperty no-lock break by TTProperty.PropertyName:
      if not TTProperty.BuildProperty then next.

      /** a manual property has no db relation , so skip it*/
      if TTProperty.ManualProperty then next.
      
      /** can't store objects in a buffer, so skip em */
      if num-entries(TTProperty.Datatype,".") gt 1 then next.
      
      if TTProperty.FieldName eq "" then next.
      
      assign lv_Assign = substitute("this-object:&1@ = buffer::&2~n",TTProperty.PropertyName,
                                                                              TTProperty.FieldName)
						 lv_Assign = replace(lv_Assign,"@",fill(" ",lv_maxLen - length(TTProperty.fieldname)))                                                                               
             lv_Data   = lv_Data + lv_Assign. 

      assign lv_StatementSize = lv_StatementSize + length(lv_Assign).
      
      /** each assign line has approx 8 tokens in it. Make sure that this assign statement does not go above the -tok setting - 20%. 
       *  if so, break the statement up into multiple assign statements */
       
      if lv_StatementSize / 8 ge lv_tokSize  then
      do:
         assign lv_Data = lv_Data + substitute(". /* split as statement [&1] approaching -tok [&2] */~n",lv_StatementSize,lv_tokSize)
			          lv_StatementSize = 0.
                
				if not last(TTProperty.PropertyName) then assign lv_Data = lv_Data + "assign ".
				                
      end.

    end.
    
    assign lv_Assign = "this-object:rowid@ = buffer:rowid~n"
           lv_Data   = lv_Data + replace(lv_Assign,"@",fill(" ",lv_maxLen - length("rowid"))). 

    assign lv_Data = trim(lv_Data,"~n") + ".".
    
    assign p_Directive:Data = trim(lv_Data + "~n" ,"~n").
                                                                                                                                                      
  end method.
  
  method private longchar MergeAssociation(p_rowid as rowid,p_Data as longchar):
    def var lv_toModel as char no-undo.
    def var lv_nullify as char no-undo.
    
    def var lv_i as int no-undo.
    
    assign lv_ToModel = TTAssociation.ToModel.
    
    if num-entries(lv_toModel,".") eq 1
       then assign lv_toModel = substitute("&1.&2",Model:GetOptionValue("ParentPackage"),lv_toModel).
       
    find TTAssociation where rowid(TTAssociation) eq p_rowid no-lock.
    find TTFindUsing where TTFindUsing.GetName eq TTAssociation.FindUsingName no-lock no-error.
    
    if not avail TTFindUsing then
    do:
      message "can't find " TTAssociation.FindUsingName view-as alert-box.
      return "".
    end.
    
    do lv_i = 1 to num-entries(TTFindUsing.ParameterCall):
      assign lv_nullify = lv_nullify + substitute("this-object:&1@ = ?~n        ",trim(replace(entry(lv_i,TTFindUsing.ParameterCall),"p_",""))). 
    end.
    
    assign p_data = replace(p_Data,"<<Association_PropertyName>>",TTAssociation.PropertyName)
/*           p_data = replace(p_Data,"<<Association_FromProperty>>",TTAssociation.FromProperty)*/
           p_data = replace(p_Data,"<<Association_Parameters>>",trim(replace(TTFindUsing.ParameterCall,"p_","this-object:")))
           
           p_data = replace(p_Data,"<<Association_ToFindUsing>>",TTAssociation.FindUsingName)
           p_data = replace(p_Data,"<<Association_Nullify>>",TTFindUsing.Nullify)
           p_data = replace(p_Data,"<<Association_ID>>",TTAssociation.name)
           p_data = replace(p_Data,"<<Association_FindUsingFK>>",TTAssociation.FindUsing_fkName)
           
           p_data = replace(p_Data,"<<Association_ToModel>>",lv_ToModel)
           p_data = replace(p_Data,"<<Association_ToModelBase>>",TTAssociation.ToModel).
    
    return p_Data.     
  end method.
  
  /** create statement to assign database fields to properties
   * @param p_Directive : holds the directive name and all parameters
   */
  
  method public void ActiveRecordModel_fk_ClearAfterGet(p_Directive as Directive):
    def var lv_Data as longchar no-undo.
    def var lv_base as longchar no-undo.
    
    assign lv_base = GetFileContents(p_Directive,"builder/ActiveRecordModel_fk_ClearAfterGet.i").
    
    for each TTAssociation no-lock:
      assign lv_Data = lv_Data + MergeAssociation(rowid(TTAssociation),lv_base) + "~n".
    end. 

    assign p_Directive:Data = trim(lv_Data,"~n").

  end method.
  

 /** creates destructor statements for each fk  
  *
  *  loops through all objects with a fk of this object, and creates the appropriate destructor statement 
  *  @param p_Directive : holds the directive name and all parameters
  */

  method public void ActiveRecordModel_Destructor(p_Directive as Directive):  /** when a fk object is deleted, need to either remove or update related object (update orderline when order deleted) */
  end method.

 /** updates fk on deletion of fk 
  *
  *  loops through all objects with a fk of this object, and sets the fk property to "" 
  *  @param p_Directive : holds the directive name and all parameters
  */

  method public void ActiveRecordModel_fkCascadeDelete(p_Directive as Directive):  /** when a fk object is deleted, need to either remove or update related object (update orderline when order deleted) */
    def var lv_Delete  as longchar no-undo.
    def var lv_Nullify as longchar no-undo.
    def var lv_Data    as longchar no-undo.
    def var lv_code    as longchar no-undo.
    
    assign lv_delete  = GetSource("ActiveRecordModel_fkCascadeDelete.i",p_Directive)
           lv_Nullify = GetSource("ActiveRecordModel_fkCascadeNullify.i",p_Directive).
  
    for each TTAssociation no-lock break by TTAssociation.ToModel:
      if not TTAssociation.Type begins "belongs" then next.
      
      if not first-of(TTAssociation.ToModel) then next.
      
      assign lv_Data  = if  TTAssociation.CascadeMode eq "delete" then lv_Delete else lv_Nullify
             lv_Code  = lv_Code + MergeAssociation(rowid(TTAssociation),lv_Data) + "~n".
    end.
    
    assign p_Directive:Data = trim(lv_Code,"~n").

  end method.

 /** updates fk on deletion of fk 
  *
  *  loops through all objects with a fk of this object, and sets the fk property to "" 
  *  @param p_Directive : holds the directive name and all parameters
  */

  method public void ActiveRecordModel_fkDeleted(p_Directive as Directive):  /** when a fk object is deleted, need to either remove or update related object (update orderline when order deleted) */
  
    def var lv_Data    as longchar no-undo.
    def var lv_code    as longchar no-undo.
  
    assign lv_data  = GetSource("ActiveRecordModel_fkDeleted.i",p_Directive).
    
    for each TTAssociation no-lock:
      if not TTAssociation.Type begins "has" then next.
      assign lv_Code = lv_Code + MergeAssociation(rowid(TTAssociation),lv_Data) + "~n".
    end.
  
    assign p_Directive:Data = trim(lv_Code,"~n").

  end method.

  method private longchar GetFindUsing(p_Data as longchar):
    return GetFindUsing(p_Data,no).
  end method.
  
  method private longchar GetFindUsing(p_Data as longchar,p_unique as logical):
    def var lv_Index          as char no-undo.
    
    def var lv_i as int no-undo.
    
    def var lv_data   as longchar no-undo.
    def var lv_assign as longchar no-undo.
    
    for each TTFindUsing no-lock:
      
      assign lv_assign = "assign ".
      
      if p_unique then 
      do:
        if not TTFindUsing.IsUnique then next.
        if num-entries(TTFindUsing.ParameterCall) eq 1 then next.
      end.
      
      do lv_i = 1 to extent(TTFindUsing.Property):
        if TTFindUsing.Property[lv_i] eq "" then leave.
        assign lv_assign = lv_assign + substitute("this-object:&1@ = p_&1~n           ",TTFindUsing.Property[lv_i])
               lv_assign = replace(lv_Assign,"@",fill(" ",TTFindUsing.MaxProperty - length(TTFindUsing.Property[lv_i]))). 
      end.
      
      assign lv_assign = trim(lv_assign)
             lv_assign = trim(lv_assign,"~n") + ".".
      
      assign lv_Index = replace(p_Data,"<<GetName>>",TTFindUsing.GetName)
             lv_index = replace(lv_index,"<<DocParameterList>>",TTFindUsing.DocParameterList)
             lv_index = replace(lv_index,"<<FindString>>",TTFindUsing.FindString[1] + TTFindUsing.FindString[2])
             lv_index = replace(lv_index,"<<ParameterList>>",TTFindUsing.ParameterList)
             lv_index = replace(lv_index,"<<ParameterCall>>",TTFindUsing.parameterCall)
             lv_index = replace(lv_index,"<<AssignDefaultProperties>>",lv_assign)
             
             lv_data = lv_Data + "~n" + lv_index.
  
      if p_unique then leave.
    end.
    
    return lv_Data.                
  end method.
  
   /** build the methods for each of the get statements in the model
   * @param p_Directive : holds the directive name and all parameters
   */
  
  method public void ActiveRecordHelper_GetMethods(p_Directive as Directive):
    assign p_Directive:Data = p_Directive:Data + GetFindUsing(GetSource(p_Directive)).
  end method.
 
 /** build the validation for each of the unique get statements in the model
   * @param p_Directive : holds the directive name and all parameters
   */
  
  method public void ActiveRecordGetValidation(p_Directive as Directive):
    def var oValidation as dotr.Maia.Model.Validation no-undo.

    def var lv_Name      as char no-undo.
    def var lv_Compare   as char no-undo.
    def var lv_prop      as char no-undo.
    def var lv_Statement as char no-undo.
    
    def var lv_Validation as longchar no-undo.
    def var lv_data       as longchar no-undo.
    def var lv_Validate   as longchar no-undo.
    def var lv_Names      as longchar no-undo.
    def var lv_unknown    as longchar no-undo.
    
    def var lv_i as int no-undo.
    
    if cast(Model,dotr.Maia.ActiveRecord):keyIndex ne "rowid" then 
    do:
      find first TTFindUsing where TTFindUsing.IndexName eq cast(Model,dotr.Maia.ActiveRecord):keyIndex no-lock.

      do lv_i = 1 to num-entries(TTFindUsing.parameterCall):
        assign lv_prop = replace(trim(entry(lv_i,TTFindUsing.parameterCall)),"p_","")
               lv_Compare = lv_Compare + substitute("|this-object:&1 ne test:&1 ",lv_Prop).
      end.
      
      assign lv_Compare = (if num-entries(TTFindUsing.parameterCall) gt 1 then "(" else "") +
                          replace(trim(lv_compare,"|"),"|"," or ") +
                          (if num-entries(TTFindUsing.parameterCall) gt 1 then ")" else "").
       
      for each TTFindUsing where TTFindUsing.IsUnique eq yes:
        
        if TTFindUsing.IndexName eq cast(Model,dotr.Maia.ActiveRecord):keyIndex then next.
        
        assign lv_Data    = GetSource(p_Directive)
               lv_name    = replace(TTFindUsing.GetName,"FindUsing","")
               lv_Names   = lv_Names + substitute("~tValidate&1().~n",lv_Name)
               lv_Unknown = "".
              
        do lv_i = 1 to num-entries(TTFindUsing.parameterCall):
          assign lv_Unknown = lv_UnKnown + substitute("if this-object:&1 eq ? then return.~n",trim(replace(entry(lv_i,TTFindUsing.parameterCall),"p_",""))).
        end.
                      
        assign lv_Data = replace(lv_Data,"<<GetName>>",lv_Name)
               lv_Data = replace(lv_Data,"<<DocParameterList>>",TTFindUsing.DocParameterList)
               lv_Data = replace(lv_Data,"<<FindString>>",TTFindUsing.FindString[1] + TTFindUsing.FindString[2])
               lv_Data = replace(lv_Data,"<<ParameterCall>>",replace(TTFindUsing.parameterCall,"p_","this-object:"))
               lv_Data = replace(lv_Data,"<<compare>>",lv_Compare)
               lv_Data = replace(lv_Data,"<<TestForUnknown>>",trim(lv_Unknown,"~n"))
               lv_Validate = lv_Validate + "~n" + lv_Data.
      end.
    end.

    for each TTValidation no-lock:
      assign lv_Statement = "".
      
      oValidation = cast(TTValidation.oObject,dotr.Maia.Model.Validation).
      
      if oValidation:EndValue ne ? then
      do:
        assign lv_Statement = lv_Statement + substitute('~tif this-object:&1 gt &3 then undo, throw new AppError("&2",0).~n',oValidation:PropertyName
                                                                                                                            ,oValidation:EndValueMessage
                                                                                                                            ,oValidation:EndValue).
      end.
      
      if oValidation:Excludes ne ? then
      do:
        assign lv_Statement = lv_Statement + substitute('~tif lookup("&3",this-object:&1) ne 0 then undo, throw new AppError("&2",0).~n',oValidation:PropertyName
                                                                                                                                        ,oValidation:ExcludesMessage
                                                                                                                                        ,oValidation:Excludes).
      end.
      
      if oValidation:Includes ne ? then
      do:
        assign lv_Statement = lv_Statement + substitute('~tif lookup("&3",this-object:&1) eq 0 then undo, throw new AppError("&2",0).~n',oValidation:PropertyName
                                                                                                                                        ,oValidation:IncludesMessage
                                                                                                                                        ,oValidation:Includes).
      end.
      
      if oValidation:MaxLength ne ? then
      do:
        assign lv_Statement = lv_Statement + substitute('~tif length(this-object:&1) gt &3 then undo, throw new AppError("&2",0).~n',oValidation:PropertyName
                                                                                                                                    ,oValidation:MaxLengthMessage
                                                                                                                                    ,oValidation:MaxLength).
      end.
      
      if oValidation:MinLength ne ? then
      do:
        assign lv_Statement = lv_Statement + substitute('~tif length(this-object:&1) lt &3 then undo, throw new AppError("&2",0).~n',oValidation:PropertyName
                                                                                                                                    ,oValidation:MinLengthMessage
                                                                                                                                    ,oValidation:MinLength).
      end.
      
      if oValidation:NotBlank ne ? then
      do:
        find TTProperty where TTProperty.PropertyName eq oValidation:PropertyName no-lock no-error.
        if TTProperty.DataType begins "char" then assign lv_Statement = lv_Statement + substitute('~tif this-object:&1 eq "" then undo, throw new AppError("&2",0).~n',oValidation:PropertyName,oValidation:NotBlankMessage).
      end.
      
      if oValidation:NotNull ne ? then
      do:
        assign lv_Statement = lv_Statement + substitute('~tif this-object:&1 eq ? then undo, throw new AppError("&2",0).~n',oValidation:PropertyName,oValidation:NotNullMessage).
      end.
      
      if oValidation:StartValue ne ? then
      do:
        assign lv_Statement = lv_Statement + substitute('~tif this-object:&1 lt &3 then undo, throw new AppError("&2",0).~n',oValidation:PropertyName
                                                                                                                            ,oValidation:StartValueMessage
                                                                                                                            ,oValidation:StartValue).
      end.
      
      assign lv_Statement = replace(lv_Statement,"<<Property>>",oValidation:PropertyName).
      
      if oValidation:MinLength  ne ? then assign lv_Statement = replace(lv_Statement,"<<minlength>>",string(oValidation:MinLength)).
      if oValidation:MaxLength  ne ? then assign lv_Statement = replace(lv_Statement,"<<maxlength>>",string(oValidation:MaxLength)).
      if oValidation:StartValue ne ? then assign lv_Statement = replace(lv_Statement,"<<startvalue>>",oValidation:StartValue).
      if oValidation:EndValue   ne ? then assign lv_Statement = replace(lv_Statement,"<<endvalue>>",oValidation:EndValue).
      if oValidation:Includes   ne ? then assign lv_Statement = replace(lv_Statement,"<<includes>>",oValidation:Includes).
      if oValidation:Excludes   ne ? then assign lv_Statement = replace(lv_Statement,"<<excludes>>",oValidation:Excludes).
                   
      assign lv_Validation = lv_Validation + "~n" + lv_Statement.
    end.
    
    assign p_Directive:Data = "method public void Validate():~n" + lv_Validation + "~n" + lv_Names + "end method.~n" + trim(lv_Validate,"~n").                
  end method.
  
  /** build the methods for each of the possible find statements in the model
   * @param p_Directive : holds the directive name and all parameters
   */
  
  method public void ActiveRecordModelFind(p_Directive as Directive):
    def var lv_Data as longchar no-undo.

    case cast(Model,dotr.Maia.ActiveRecord):KeyFieldFind:
      when "single" then assign lv_Data = GetSource("ActiveRecordModelFindSingleUnique.i",p_Directive)
                                lv_Data = lv_Data + "~n" + GetSource("ActiveRecordModelFindRowid.i",p_Directive).
      when "rowid" then assign lv_Data = GetSource("ActiveRecordModelFindRowid.i",p_Directive).
      
      when "multiple" then
        do:
          assign lv_data = GetSource("ActiveRecordModelFindMultipleUnique.i",p_Directive)
                 lv_Data = GetFindUsing(lv_Data,yes)
                 lv_Data = lv_Data + "~n" + GetSource("ActiveRecordModelFindRowid.i",p_Directive).
        end.
    end case.
        
    assign p_Directive:Data = p_Directive:Data + "~n" + lv_Data.
    
  end method.

  method public void ActiveRecordBase_FindFromJson(p_Directive as Directive):
    def var lv_data as longchar no-undo.
    def var lv_param as longchar no-undo.
    
    def var lv_i as int no-undo.
    
    def var Model1 as dotr.Maia.ActiveRecord no-undo.
    
    Model1 = cast(Model,dotr.Maia.ActiveRecord).
     
    case Model1:KeyFieldFind:
      when "rowid" then assign p_Directive:Data = "next.".
      
      otherwise
      do:
        do lv_i = 1 to extent(Model1:KeyProperty):
          
          assign lv_Param = substitute("tablehandle::&1",Model1:KeyProperty[lv_i]).
          
          find TTproperty where TTProperty.PropertyName eq Model1:KeyProperty[lv_i] no-lock no-error.
          
          if not TTProperty.DataType begins "char" then
             assign lv_Param = substitute("&1(&2)",TTProperty.DataType,lv_Param).
            
          assign lv_data = lv_Data + "," + lv_Param.
        end.
        
        assign p_Directive:Data = substitute("this-object:Find(&1).",trim(lv_data,",")).
      end.
    end case.
  end method.
  
  
    
  method public void ActiveRecordBase_ImportFromJson(p_Directive as Directive):
    
    
    def var lv_Data   as char no-undo.
    def var lv_Assign as char no-undo.
    
    def var lv_maxLen       as int no-undo.
    def var lv_i             as int no-undo.
    
    /** get max size of property name. Used to align code properly */
    for each TTProperty no-lock :

      /** if not temp-table, then a manual property has no db relation , so skip it*/
      if TTProperty.ManualProperty then next.
      
      /** can't store objects in a buffer, so skip em */
      if num-entries(TTProperty.Datatype,".") gt 1 then next.
      
      if TTProperty.BuildProperty then assign lv_MaxLen = MAX(lv_MaxLen,if TTProperty.extent gt 0 then length(TTProperty.PropertyName) + 2 + length(string(TTProperty.extent))
                                                                                                else length(TTProperty.PropertyName)).
    end.
    
    /** now let's loop through, building the assign statement */
    for each TTProperty no-lock break by TTProperty.PropertyName:
      
      if not TTProperty.BuildProperty then next.

      /** a manual property has no db relation , so skip it*/
      if TTProperty.ManualProperty then next.
      if TTProperty.FieldName eq "" then next.
      if TTProperty.DataType eq "raw" then next.
      
      /** can't store objects in a buffer, so skip em */
      if num-entries(TTProperty.Datatype,".") gt 1 then next.
      
      /** if field is an array, and in a word index then we must split the assignment */
      if TTProperty.extent gt 0 and TTProperty.IsArrayWordIndex then
      do lv_i = 1 to TTProperty.extent:
        assign lv_Assign = substitute("when '&1_&3'@ then then assign this-object:&1[&3]@ = &4fieldhandle:buffer-value(&3)&5.~n",
                                                                                          TTProperty.PropertyName,
                                                                                          TTProperty.FieldName,
                                                                                          lv_i,
                                                                                          if TTProperty.DataType begins "char" then "" else string(TTProperty.DataType) + "(",
                                                                                          if TTProperty.DataType begins "char" then "" else ")")
                                                                                          
               lv_Data   = lv_Data + replace(lv_Assign,"@",fill(" ",lv_maxLen - length(TTProperty.PropertyName) - 
                                             (if TTProperty.extent lt 9 then 3
                                                                       else if TTProperty.extent lt 99 then 4
                                                                       else 5))).
      end.
      
      else /** create a single assignment */
      do:
        /* If the property is a clob, then we cannot simply assign the buffer-value, but have to use copy-lob instead */
        if TTProperty.DataType = "clob" then 
          assign lv_Assign = substitute("when '&1'@ then copy-lob from fieldhandle:buffer-value to this-object:&1@.~n",TTProperty.PropertyName,TTProperty.FieldName).
        else
          assign lv_Assign = substitute("when '&1'@ then assign this-object:&1@ = &3fieldhandle:buffer-value&4.~n",TTProperty.PropertyName,
                                                                            TTProperty.FieldName,
                                                                            if TTProperty.DataType begins "char" then "" else string(TTProperty.DataType) + "(",
                                                                            if TTProperty.DataType begins "char" then "" else ")"
                                                                            ). 
        assign 
               lv_Assign = replace(lv_Assign,"@",fill(" ",lv_maxLen - length(TTProperty.PropertyName)))
               lv_Data   = lv_Data + lv_Assign.
      end.
      
    end.
    
    assign p_Directive:Data = trim(lv_Data,"~n").
                                                                                                                                                      
  end method.    
end class.