using dotr.Maia.testing.UnitTest.*.

def frame b with 1 col .

def var TestHarness1 as UnitTestHarness.

def var lv_Data as char no-undo.

TestHarness1 = UnitTestHarness:Instance.

TestHarness1:StartTest:SUBSCRIBE("StartTest"). /* hook into start of each test within a unit */ 
TestHarness1:StartUnit:SUBSCRIBE("StartUnit"). /* hook into start of each unit */

TestHarness1:EndTest:SUBSCRIBE("EndTest").     /* hook into end of each test within a unit */
TestHarness1:EndUnit:SUBSCRIBE("EndUnit").   /* hook into end of each unit */

/* add the following tests, in this order */
TestHarness1:AddTestUnit("Maia").
/* now run the tests */

TestHarness1:StartTestSession() no-error.

/* to show all tests, use TestHarness1:ExportjSON(?)
   all passed tests, use TestHarness1:ExportjSON(YES) */
   
assign lv_Data = substitute("Number of Tests:&1~nNumber of Assertions:&2~nPassed:&3~nFailed:&4~nStarted:&5~nEnded:&6~n~n&7",
                            TestHarness1:NumberOfTests,
                            TestHarness1:NumberOfAssertions,
                            TestHarness1:PassedTests,
                            TestHarness1:FailedTests,
                            TestHarness1:StartTime,
                            TestHarness1:EndTime,
                            if TestHarness1:PassedTests eq TestHarness1:NumberOfTests 
                               then ""
                               else string(TestHarness1:ExportjSON(no))
                            ).

output TO "CLIPBOARD".
put unformatted lv_Data.
output CLOSE.

/* display the results (only failed tests) */
message lv_Data  
        view-as alert-box.

/* clean up session */
TestHarness1:EndTestSession().

/* and quit */

quit.

procedure StartTest:
  def input parameter p_Unit as char no-undo.
  def input parameter p_Test as char no-undo.
end procedure.

procedure EndTest:
  def input parameter p_Unit as char no-undo.
  def input parameter p_Test as char no-undo.
end procedure.

procedure StartUnit:
  def input parameter p_Unit as char no-undo.
end procedure.

procedure EndUnit:
  def input parameter p_Unit as char no-undo.
end procedure.
