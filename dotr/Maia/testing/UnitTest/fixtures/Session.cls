using Progress.Lang.*.
using dotr.Maia.testing.UnitTest.fixtures.*.

routine-level on error undo, throw.

class dotr.Maia.testing.UnitTest.fixtures.Session inherits dotr.Maia.testing.UnitTest.fixtures.base: 
  def public static property Instance as Session no-undo 
    get():
      if not valid-object(Instance) then Instance = new Session().
      return Instance.
    end get. private set .

  method override public void Before():

  end method.
  
  method public override void After():
    
  end method.

end class.