using Progress.Lang.*.
using dotr.Maia.testing.UnitTest.*.
using dotr.Maia.testing.UnitTest.fixtures.*.

/* ==========================================
  namespace: testing.UnitTest

  class:  UnitTestHarness
  This class is the central hub for creating and
  performing all unit tests
  
  ===========================================*/

class dotr.Maia.testing.UnitTest.UnitTestHarness:
  def public static property Instance as class dotr.Maia.testing.UnitTest.UnitTestHarness no-undo 
    get():
      if not valid-object(Instance) then Instance = new dotr.Maia.testing.UnitTest.UnitTestHarness().
      return Instance.
    end get. private set .
   
  def public property FailedTests        as int no-undo get . private set . /* <Variable: Number of Failed Tests */  
  def public property PassedTests        as int no-undo get . private set .
  def public property NumberOfTests      as int no-undo get . private set .
  def public property TimeTaken          as int no-undo get . private set .
  def public property NumberOfAssertions as int no-undo get . private set .
  
  def public property StartTime as datetime no-undo get . private set .
  def public property EndTime   as datetime no-undo get . private set .

  def public property Passed as class TestPass no-undo extent get . private set .
  def public property Failed as class TestFail no-undo extent get . private set .

/* ==========================================================================
   Event: StartTest
    Published at start of each test of the unit
  
    Parameters: 
      UnitName : CHAR - Name of the current unit
      TestName : CHAR - Name of the current test
   ==========================================================================*/ 
  def public event StartTest signature void (p_Name as char,p_test as char).
  def public event EndTest   signature void (p_Name as char,p_test as char).
  def public event StartUnit signature void (p_Name as char).
  def public event EndUnit   signature void (p_Name as char).

  def var CurrentTestNumber as int no-undo.
  
  def temp-table TTResult no-undo 
    serialize-name "TestResults"
     
    field TestNumber as int

    field Unit as char
    field Test as char

    field Passed as logical
    field Assertions as int

    field Result as char

    field Order as int
    
    index Number is primary
      TestNumber
      
    index Passed
      Passed
      TestNumber.

  def temp-table TTExport no-undo like TTResult.
  
  def temp-table TTTest no-undo 
    serialize-name "Test"
     
    field TestNumber as int

    field Unit as char
    field Test as char
    
    field MustPass as logical

    index Number is primary
      TestNumber.
      
  constructor public UnitTestHarness ():
    super ().
    session:error-stack-trace = yes.
    
  end constructor.

/* ==========================================================================
   Method: AddTestUnit
   
   Adds a test to the session

   Parameters:
      p_Data : CHAR - list of unit tests to perform

   Returns:
      VOID
   ==========================================================================*/ 
  method public void AddTestUnit(p_Data as char):
    def var lv_Data as longchar no-undo.
    
    lv_Data = p_Data.
    
    AddTestUnit(lv_Data,no).
     
  end method.

/* ==========================================================================
   Method: AddTestUnit
   
   Adds a test to the session

   Parameters:
      p_Data : CHAR - list of unit tests to perform
      p_Pass : LOGICAL - Test must pass

   Returns:
      VOID
   ==========================================================================*/ 
  method public void AddTestUnit(p_Data as char,p_Pass as logical):
    def var lv_Data as longchar no-undo.
    
    lv_Data = p_Data.
    
    AddTestUnit(lv_Data,p_Pass).
     
  end method.

/* ==========================================================================
   Method: AddTestUnit
   
   Adds a test to the session

   Parameters:
      p_Data : LONGCHAR - list of unit tests to perform

   Returns:
      VOID
   ==========================================================================*/ 
  method public void AddTestUnit(p_Data as longchar):
    AddTestUnit(p_Data,no).
  end method.

/* ==========================================================================
   Method: AddTestUnit
   
   Adds a test to the session

   Parameters:
      p_Data : LONGCHAR - list of unit tests to perform
      p_Pass : LOGICAL - Test must pass, or complete test case stops

   Returns:
      VOID
   ==========================================================================*/ 
  method public void AddTestUnit(p_Data as longchar,p_MustPass as logical):
    def var lv_i      as int no-undo.
  
    do lv_i = 1 to num-entries(p_Data,"~n"):
      create TTTest.
      assign TTTest.TestNumber = CurrentTestNumber
             TTTest.MustPass   = p_MustPass
             TTTest.Unit       = entry(lv_i,p_Data,"~n"). 
    end.
    
  end method.

/* ==========================================================================
   Method: EndTestSession
   
   Ends a test session

   Parameters:
      NONE

   Returns:
      VOID
   ==========================================================================*/ 
  method public void EndTestSession():

  end method.

/* ==========================================================================
   Method: StartTestSession
   
   Starts a new test session

   Parameters:
      NONE

   Returns:
      VOID
   ==========================================================================*/ 
  method public void StartTestSession():
    TestSession() .
  end method.
  
/* ==========================================================================
   Method: TestSession
   
   Starts a new test session

   Parameters:
      NONE

   Returns:
      VOID
   ==========================================================================*/ 
  method private void TestSession():
    
    def var lv_Passed as int no-undo.
    def var lv_Failed as int no-undo.
    def var lv_line   as int no-undo.
    
    def var lv_Buffer as handle no-undo.
    
    def var lv_Data  as char no-undo.
    def var lv_Name  as char no-undo.
    def var lv_Error as char no-undo.
    def var lv_fail  as char no-undo.
    
    assign StartTime         = now
           lv_Buffer         = temp-table TTResult:DEFAULT-BUFFER-HANDLE
           CurrentTestNumber = 0.
    
    empty temp-table TTResult.
    
    extent(Failed) = ?.        
    extent(Passed) = ?.

    session:debug-alert = yes.
        
    dotr.Maia.testing.UnitTest.fixtures.Session:Instance:Before().

    testloop:
    for each TTTest no-lock on error undo, throw:
      StartTestUnit(TTTest.Unit).
      
      if TTTest.MustPass then
      for each TTResult where TTResult.Unit eq TTTest.Unit no-lock:
        if not TTResult.Passed then leave testloop.
      end.
       
      delete TTTest.
    end.

    for each TTResult no-lock:
      if TTResult.Passed then assign lv_Passed = lv_Passed + 1.
      else assign lv_Failed = lv_Failed + 1. 
    end.   
    
    if lv_Failed gt 0 then extent(Failed) = lv_Failed .        
    if lv_Passed gt 0 then extent(Passed) = lv_Passed.

    assign lv_Passed = 0
           lv_Failed = 0.

    for each TTResult no-lock:
      if TTResult.Passed then assign lv_Passed         = lv_Passed + 1
                                     Passed[lv_Passed] = new TestPass(lv_Buffer).
                                     
      else assign lv_Failed         = lv_Failed + 1
                  Failed[lv_Failed] = new TestFail(lv_Buffer).
    end.   

 /* catch AppErrors */
    catch f as Progress.Lang.AppError :
      
    end catch.

    /* catch progress errors */
    
    catch g as Progress.Lang.SysError :
      assign lv_Data  = entry(num-entries(g:CallStack,"~n") ,g:CallStack,"~n")
             lv_Line  = INT(SUBSTR(lv_Data,r-index(lv_Data,":") + 1))
             lv_fail = substitute("ERROR: [&1:&2->&3] <<&4>>",
                                  entry(1,lv_data," "),
                                  entry(2,lv_data," "),
                                  INT(SUBSTR(lv_Data,r-index(lv_Data,":") + 1)),
                                  if return-value eq "" or return-value eq ? then g:GetMessage(1) else return-value
                                  ).
    
      TestFailed("Session","StartTestSession",0,lv_Fail,0).    
    end catch.
    
    /* catch progress errors */    
    catch e as Progress.Lang.Error :
      assign lv_Data  = entry(num-entries(e:CallStack,"~n") ,e:CallStack,"~n")
             lv_Line  = INT(SUBSTR(lv_Data,r-index(lv_Data,":") + 1))
             lv_fail = substitute("ERROR: [&1:&2->&3] <<&4>>",
                                  entry(1,lv_data," "),
                                  entry(2,lv_data," "),
                                  INT(SUBSTR(lv_Data,r-index(lv_Data,":") + 1)),
                                  if return-value eq "" or return-value eq ? then e:GetMessage(1) else return-value
                                  ).
    
      TestFailed("Session","StartTestSession",0,lv_Fail,0).    
    end catch.
               
    finally:
      dotr.Maia.testing.UnitTest.fixtures.session:Instance:After().
      assign EndTime   = now
             TimeTaken = EndTime - StartTime.
    end finally.           
  end method.

/* ==========================================================================
   Method: StartTestUnit
   
   Runs all tests in the supplied test unit. 
   
   The test unit is compiled, and all methods starting with "test" are classified as a 
   test. These methods are then run, in the order that they are found.

   Parameters:
      p_Unit : CHAR - the test unit to run

   Returns:
      VOID
   ==========================================================================*/ 

  method private void StartTestUnit(p_TestUnit as char):
    
    def var lv_Data as longchar no-undo.
    
    def var lv_SearchData as char no-undo init "METHOD PUBLIC,,,,,test" .
    def var lv_Method     as char no-undo.
    def var lv_xref       as char no-undo.
    def var lv_Source     as char no-undo.
    def var lv_Class      as char no-undo.
    def var lv_Base       as char no-undo.
    def var lv_Name       as char no-undo.
    def var lv_Error      as char no-undo.
    def var lv_fail       as char no-undo.
    
    def var lv_i         as int no-undo init 1 .
    def var lv_SearchLen as int no-undo.

    def var UnitTest1 as class dotr.Maia.testing.UnitTest.unit.base no-undo.
    def var Fixture1  as class dotr.Maia.testing.UnitTest.Fixture   no-undo.
  
    Fixture1 = new Fixture(p_TestUnit) no-error.
    
    assign lv_SearchLen = length(lv_SearchData)
           lv_Class     = substitute("dotr.Maia.testing.UnitTest.unit.&1",p_TestUnit)
           lv_Base      = p_TestUnit
           lv_Source    = replace(lv_Class,".","/")
           lv_Xref      = substitute("&1.xref",lv_Source)
           lv_Source    = substitute("&1.cls",lv_Source).
    
    StartUnit:Publish(p_TestUnit).
    
    assign file-info:file-name = lv_Source.
     
    if search(file-info:file-name) eq ? then 
    do:
     TestFailed(p_TestUnit,"[Before:Search]",0,substitute("failed to find unit test [&1]",lv_Class),0).
     return.      
    end.    
    
    compile VALUE(lv_Source) save = no xref VALUE(lv_xref) no-error.
    
    if error-status:num-messages gt 0 and error-status:get-number(1) ne 6430 then  
    do:
     TestFailed(p_TestUnit,"[Before:Compile]",0,substitute("[&1]: failed to compile [&2]",lv_Class,error-status:get-message(1)),0).
     return.      
    end.
    
    copy-lob from file lv_xRef to lv_Data.
    
    os-delete VALUE(lv_xref).
    
    UnitTest1 = DYNAMIC-NEW lv_Class().
    
    if valid-object(Fixture1) then Fixture1:Before() .
    
    do while lv_i gt 0:
      assign lv_i = index(lv_Data,lv_SearchData,lv_i).
      
      if lv_i eq 0 then leave.
      
      assign lv_i      = lv_i + lv_SearchLen
             lv_Method = entry(1,substring(lv_Data,lv_i,32)).
    
      if valid-object(Fixture1) then Fixture1:BeforeTest() .

      TestMethod(UnitTest1,lv_Method).
  
      if valid-object(Fixture1) then Fixture1:AfterTest() .

    end.

    catch g as Progress.Lang.SysError :
      undo, throw g.
    end catch.

    catch e as Progress.Lang.Error :
      if e:GetMessageNum(1) eq 14284 then TestFailed(p_TestUnit,"[before:Missing]",0,substitute("failed to find unit test [&1]",lv_Class),0).
                                                 
      else 
      do:
        assign lv_Data = entry(num-entries(e:CallStack,"~n") ,e:CallStack,"~n").
        message e:getmessage(1) skip e:getmessage(2) skip e:Callstack view-as alert-box.
        TestFailed(p_TestUnit,"[before:Other]",INT(SUBSTR(lv_Data,r-index(lv_Data,":") + 1)),e:GETMESSAGE(1),0).   
      end.
      
      undo, throw e.
    end catch.
    
    finally:
      Fixture1:After() no-error.   
      
      EndUnit:Publish(lv_Base).
      delete object Fixture1  no-error.
      delete object UnitTest1 no-error.
    end finally.
    
  end method.
  
/* ==========================================================================
   Method: TestMethod
   
   Runs all tests in the supplied test unit. 
   
   The test unit is compiled, and all methods starting with "test" are classified as a 
   test. These methods are then run, in the order that they are found.

   Parameters:
      p_Unit   : testing.UnitTest.unit.base - the test unit to run
      p_Method : CHAR - the method to run of the test unit 

   Returns:
      VOID
   ==========================================================================*/ 

  method private void TestMethod(p_Unit as dotr.Maia.testing.UnitTest.unit.base,p_Method as char):

    def var lv_Data  as char no-undo.
    def var lv_Name  as char no-undo.
    def var lv_Error as char no-undo.
    def var lv_fail  as char no-undo.
    
    def var lv_line as int no-undo.
    
    assign lv_name = p_Unit:GetClass():TypeName.

    StartTest:Publish(lv_Name,p_Method).
    
    p_Unit:StartTest().
    
	  dynamic-invoke (p_Unit,substitute("test&1",p_Method)).
    
    TestPassed(lv_Name,p_Method,p_Unit:NumberOfAssertions).

    /* catch unit test errors */
    catch eFailure as failure:
      assign lv_Data = entry(num-entries(eFailure:CallStack,"~n") - 1,eFailure:CallStack,"~n").
      TestFailed(lv_Name,p_Method,INT(SUBSTR(lv_Data,r-index(lv_Data,":") + 1)),eFailure:failure,p_Unit:NumberOfAssertions).   
    end catch.
  
    /* catch Apperrors */
    catch f as Progress.Lang.AppError :
      
      assign lv_Data  = entry(num-entries(f:CallStack,"~n") ,f:CallStack,"~n")
             lv_Error = entry(1,SUBSTR(f:CallStack,index(f:CallStack,p_method)),"~n")
             lv_Line  = INT(SUBSTR(lv_Data,r-index(lv_Data,":") + 1))
             lv_fail = substitute("AppError: [&1:&2->&3] <<&4>>",
                                  entry(1,lv_data," "),
                                  entry(2,lv_data," "),
                                  INT(SUBSTR(lv_Data,r-index(lv_Data,":") + 1)),
                                  if f:ReturnValue eq "" then f:GetMessage(1) else f:ReturnValue
                                  ).
             
      TestFailed(lv_name,p_Method,INT(SUBSTR(lv_error,r-index(lv_error,":") + 1)),lv_Fail,p_Unit:NumberOfAssertions).
    end catch.

    /* catch progress errors */
    
    catch g as Progress.Lang.SysError :
            assign lv_Data  = entry(num-entries(g:CallStack,"~n") ,g:CallStack,"~n")
             lv_Error = entry(1,SUBSTR(g:CallStack,index(g:CallStack,p_method)),"~n")
             lv_Line  = INT(SUBSTR(lv_Data,r-index(lv_Data,":") + 1))
             lv_fail = substitute("SysError: [&1:&2->&3] <<&4>>",
                                  entry(1,lv_data," "),
                                  entry(2,lv_data," "),
                                  INT(SUBSTR(lv_Data,r-index(lv_Data,":") + 1)),
                                  g:GetMessage(1)
                                  ).
      
      TestFailed(lv_name,p_Method,INT(SUBSTR(lv_error,r-index(lv_error,":") + 1)),lv_Fail,p_Unit:NumberOfAssertions).
    end catch.

    catch h as Progress.Lang.ProError :
      TestFailed(lv_name,"[yybefore]",0,substitute("Progress Error: [&1]",h:GetMessage(1)),0).
    end catch.
    
		catch e as Progress.Lang.Error :
		  if e:GetMessageNum(1) eq 15312 then TestFailed(lv_name,p_Method,0,substitute("failed to find method [&1Test] in unit test [test&2]",
		                                                                              p_Method,
		                                                                              p_Unit),p_Unit:NumberOfAssertions).

      if e:GetMessageNum(1) eq 14284 then TestFailed(lv_name,p_Method,0,substitute("failed to find unit test [test&1]",p_Unit),p_Unit:NumberOfAssertions).
		                                             
		  else 
		  do:
        assign lv_Data = entry(num-entries(e:CallStack,"~n") ,e:CallStack,"~n").
        TestFailed(lv_name,p_Method,INT(SUBSTR(lv_Data,r-index(lv_Data,":") + 1)),e:GETMESSAGE(1),p_Unit:NumberOfAssertions).   
      end.
		end catch.
		
		finally:
      p_Unit:EndTest().
      ClearError() no-error.
      assign NumberOfAssertions = NumberOfAssertions + p_Unit:NumberOfAssertions.
      EndTest:Publish(lv_Name,p_Method).
    end finally.
	end method.
	
	method private void clearError():
	  undo, throw new Progress.Lang.AppError ("").
	end method.

/* ==========================================================================
   Method: TestPassed
   
   Record that a test has passed 
   
   Parameters:
      p_Unit : CHAR - the test unit to run
      p_Test : CHAR - the test that passed 

   Returns:
      VOID
   ==========================================================================*/ 
  method private void TestPassed(p_Unit as char,p_Test as char,p_Assertions as int):
    assign PassedTests = PassedTests + 1.
    SaveResult(p_Unit,p_Test,yes,0,"Test Passed",p_Assertions).
  end method.

/* ==========================================================================
   Method: TestFailed
   
   Record that a test has failed 
   
   Parameters:
      p_Unit : CHAR - the test unit to run
      p_Test : CHAR - the test that failed 

   Returns:
      VOID
   ==========================================================================*/ 
  method private void TestFailed(p_Unit as char,p_Test as char,p_Line as int,p_Message as char,p_Assertions as int):
    assign FailedTests = FailedTests + 1. 
    SaveResult(p_Unit,p_Test,no,p_Line,p_Message,p_Assertions).
  end method.
  
/* ==========================================================================
   Method: SaveResult
   
   Record a test result
   
   Creates a TTResult record indicating success or failure of a test 
   
   Parameters:
      p_Unit : CHAR - the test unit to run
      p_Test : CHAR - the test that failed
      p_Result : LOGICAL - the test result
      p_Line : Line where the test failed
      p_message : the test message 

   Returns:
      VOID
   ==========================================================================*/ 
  method private void SaveResult(p_Unit as char,p_test as char,p_Result as logical,p_Line as int,p_Message as char,p_Assertions as int):
    assign NumberOfTests = NumberOfTests + 1. 
    
    create TTResult.
    assign TTResult.TestNumber = NumberOfTests
           TTResult.Unit       = replace(p_Unit,"testing.UnitTest.unit.","")
           TTResult.Test       = p_test
           TTResult.Result     = p_Message
           TTResult.Passed     = p_Result
           TTResult.Assertions = p_Assertions
           TTResult.Order = p_Line.
  end method.

/* ==========================================================================
   Method: ExportXML
   
   Return the test results as an XML string
   
   Parameters:
     NONE

   Returns:
      LONGCHAR
   ==========================================================================*/ 

  method public longchar ExportXML():
    return ExportXML(?).
  end.

/* ==========================================================================
   Method: ExportXML
   
   Return the test results as an XML string
   
   Parameters:
     p_Mode : LOGICAL - True for passed, False for fail, ? for all

   Returns:
      LONGCHAR
   ==========================================================================*/ 

  method public longchar ExportXML(p_Mode as logical):
    def var lv_Data as longchar no-undo.
   
    empty temp-table TTExport.
   
    if p_Mode eq ? then 
    do:
      temp-table TTResult:WRITE-XML("LONGCHAR",lv_Data,yes).
    end.

    else      
    do:
      for each TTResult no-lock where TTResult.Passed eq p_mode:
        create TTExport.
        buffer-copy TTResult to TTExport.
      end.
        
      temp-table TTExport:WRITE-XML("LONGCHAR",lv_Data,yes).
    end.
    
    return lv_Data. 
  end method.

/* ==========================================================================
   Method: ExportJson
   
   Return the test results as an JSON string
   
   Parameters:
     p_Mode : LOGICAL - True for passed, False for fail, ? for all

   Returns:
      LONGCHAR
   ==========================================================================*/ 

  method public longchar ExportJson(p_Mode as logical):
    def var lv_Data as longchar no-undo.
   
    empty temp-table TTExport.
   
    if p_Mode eq ? then 
    do:
      temp-table TTResult:WRITE-JSON("LONGCHAR",lv_Data,yes).
    end.

    else      
    do:
      for each TTResult no-lock where TTResult.Passed eq p_mode:
        create TTExport.
        buffer-copy TTResult to TTExport.
      end.
        
      temp-table TTExport:WRITE-JSON("LONGCHAR",lv_Data,yes).
    end.
    
    return lv_Data. 
  end method.

/* ==========================================================================
   Method: ExportJson
   
   Return the test results as an JSON string
   
   Parameters:
     NONE

   Returns:
      LONGCHAR
   ==========================================================================*/ 

  method public longchar ExportJson():
    return ExportJson(?).
  end method.
  
/* ==========================================================================
   Method: GetUnit
   
   Create a new instance of the test unit if it does not already exist
   
   Parameters:
     p_Unit: The unit to instanstiate

   Returns:
      testing.UnitTest.unit.base - the instance of the existing or new test unit
   ==========================================================================*/ 
  method private dotr.Maia.testing.UnitTest.unit.base GetUnit(p_Unit as char):
    def var lv_Data as char no-undo.
    
    def var TestObject as class dotr.Maia.testing.UnitTest.unit.base no-undo.
    
    TestObject = DYNAMIC-NEW p_Unit ().
    
    return TestObject.
    
    catch e as Progress.Lang.Error :
      if e:GetMessageNum(1) eq 14284 then TestFailed(p_unit,"[before:getunit]",0,substitute("failed to find unit test [&1]",p_Unit),0).
                                                 
      else 
      do:
        assign lv_Data = entry(num-entries(e:CallStack,"~n") ,e:CallStack,"~n").
        TestFailed(p_Unit,"[before:GetUnit2]",INT(SUBSTR(lv_Data,r-index(lv_Data,":") + 1)),e:GETMESSAGE(1),0).   
      end.
      
      undo, throw e.
    end catch.
    
  end method.

end class.

 /*
  Property: FailedTests
    Number of Failed Tests
    - Integer
    - ReadOnly
  
  Property: PassedTests
    Number of Passed Tests
    - Integer
    - ReadOnly

  Property: NumberOfTests
    Number of Tests
    - Integer
    - ReadOnly
    
  Property: TimeTaken
    The time taken to perform all the tests
    - Integer (Milliseconds)
    - ReadOnly
    
  Property: StartTime
    Start time of the tests
    - DateTime
    - ReadOnly

  Property: EndTime
    End time of the tests
    - DateTime
    - ReadOnly

  Property: NumberOfAssertions
    Number of Assertions made
    - Integer
    - ReadOnly
    
  Property: Passed
    Collection of Test Passes 
    - Class TestPass
    - ReadOnly

  Property: Failed
    Collection of Test Failures
    - Class TestFail
    - ReadOnly
    
*/    
  
/* ==========================================================================
   Event: StartTest
    Published at start of each test of the unit
  
    Parameters: 
      UnitName : CHAR - Name of the current unit
      TestName : CHAR - Name of the current test

   Event: EndTest
    Published at End of each test of the unit
  
    Parameters: 
      UnitName : CHAR - Name of the current unit
      TestName : CHAR - Name of the current test

   Event: StartUnit
    Published at start of each unit
  
    Parameters: 
      UnitName : CHAR - Name of the current unit
    
   Event: EndUnit
    Published at end of each unit
  
    Parameters: 
      UnitName : CHAR - Name of the current unit
   ==========================================================================*/ 

   