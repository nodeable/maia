using Progress.Lang.*.

class dotr.Maia.testing.UnitTest.TestResult: 
  def public property TestNumber as int no-undo get . private set.
  def public property Assertions as int no-undo get . private set.

  def public property Test as char no-undo get . private set.
  def public property Unit as char no-undo get . private set.
     
  constructor public TestResult (p_Result as handle):
    super ().
    
    assign TestNumber = p_Result::TestNumber
           Assertions = p_Result::Assertions
           Test       = p_Result::Test
           Unit       = p_Result::Unit.
    
  end constructor.

end class.