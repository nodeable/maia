using Progress.Lang.*.
using dotr.Maia.testing.UnitTest.*.

routine-level on error undo, throw.

/* ==========================================
  namespace: testing.UnitTest

  class:  base
  This class is used as the parent of all Unit Tests
  
  ===========================================*/

class dotr.Maia.testing.UnitTest.unit.base abstract:

 /*
  Property: NumberOfAssertions
    Number of Assertions made
    - Integer
    - ReadOnly
*/  

  def public property NumberOfAssertions as int no-undo get . private set .

  def public property CurrentTest as char no-undo get . private set .

  def public property ClassName  as char no-undo get . private set.
  def public property BaseName   as char no-undo get . private set.

  def public property UnknownChar as char no-undo init ? get . private set.

  def protected property ErrorExpected as logical no-undo init no get . set.
      
	constructor public base (  ):
		super ().
    
    assign ClassName = this-object:GetClass():TypeName
           BaseName  = replace(ClassName,"testing.UnitTest.unit.","").
	end constructor.

/* ==========================================================================
   StartTest
   ==========================================================================*/ 

  method public void StartTest():
    assign NumberOfAssertions = 0.
  end method.

/* ==========================================================================
   EndTest
   ==========================================================================*/ 

  method public void EndTest():
  end method.

/* ==========================================================================
   raiseError
   ==========================================================================*/ 

  method protected void raiseError(p_Message as char):
    undo, throw new  failure(p_Message). 
  end method.

/* ==========================================================================
   ExpectError
   ==========================================================================*/ 

  method protected void ExpectError():
    assign ErrorExpected = yes.
  end method.

/* ==========================================================================
   ExpectPass
   ==========================================================================*/ 

  method protected void ExpectNoError():
    assign ErrorExpected = no.
  end method.

/* ==========================================================================
   assertError
   ==========================================================================*/ 

  method protected void assertError():
    assign NumberOfAssertions = NumberOfAssertions + 1.
    if (return-value eq "" or return-value eq ?) and error-status:num-messages eq 0  then undo, throw new  failure("Failed asserting that [ERROR] ocurred"). 
  end method.

/* ==========================================================================
   assertError
   ==========================================================================*/ 

  method protected void assertError(p_message as char):
    if (return-value eq "" or return-value eq ?) and error-status:num-messages eq 0  then undo, throw new  failure(p_message). 
  end method.

/* ==========================================================================
   assertNoError
   ==========================================================================*/ 

  method protected void assertNoError():
    assign NumberOfAssertions = NumberOfAssertions + 1.
    assign NumberOfAssertions = NumberOfAssertions + 1.
    if (return-value eq "" or return-value eq ?) and error-status:num-messages eq 0  then return.
    undo, throw new  failure("Failed asserting that [NO-ERROR] ocurred").
    
  end method.

/* ==========================================================================
   assertNoError
   ==========================================================================*/ 

  method protected void assertNoError(p_Message as char ):
    assign NumberOfAssertions = NumberOfAssertions + 1.
    if (return-value eq "" or return-value eq ?) and error-status:num-messages eq 0  then return.
    undo, throw new  failure(p_message).
  end method.

/* ==========================================================================
   assertTrue
   ==========================================================================*/ 

  method protected void assertTrue(p_Value as logical):
    assign NumberOfAssertions = NumberOfAssertions + 1.
    if p_Value eq ? or p_Value eq no then undo, throw new  failure(substitute("Failed asserting that logical [&1] is true",p_Value)).
    return.  
  end method.
  
/* ==========================================================================
   assertFalse 
   ==========================================================================*/ 
  
  method protected void assertFalse(p_Value as logical):
    assign NumberOfAssertions = NumberOfAssertions + 1.
    if p_Value eq ? or p_Value eq yes then undo, throw new  failure(substitute("Failed asserting that logical [&1] is false",p_Value)).
    return.
  end method.

/* ==========================================================================
   assertEquals [INTEGER]
   ==========================================================================*/ 

  method protected void assertEquals(p_Value1 as int,p_Value2 as int):
    assign NumberOfAssertions = NumberOfAssertions + 1.
    if p_Value1 ne p_Value2 then undo, throw new  failure(substitute("Failed asserting that two integers [&1] and [&2] are equal",p_Value1,p_Value2)). 
  end method.

/* ==========================================================================
   assertEquals [DECIMAL]
   ==========================================================================*/ 

  method protected void assertEquals(p_Value1 as dec,p_Value2 as dec):
    assign NumberOfAssertions = NumberOfAssertions + 1.
    if p_Value1 ne p_Value2 then undo, throw new  failure(substitute("Failed asserting that two decimals [&1] and [&2] are equal",p_Value1,p_Value2)). 
  end method. 

/* ==========================================================================
   assertEquals [DATE]
   ==========================================================================*/ 

  method protected void assertEquals(p_Value1 as date,p_Value2 as date):
    assign NumberOfAssertions = NumberOfAssertions + 1.
    if p_Value1 ne p_Value2 then undo, throw new  failure(substitute("Failed asserting that two dates [&1] and [&2] are equal",p_Value1,p_Value2)). 
  end method.

/* ==========================================================================
   assertEquals [DATETIME]
   ==========================================================================*/ 
    
  method protected void assertEquals(p_Value1 as datetime,p_Value2 as datetime):
    assign NumberOfAssertions = NumberOfAssertions + 1.
    if p_Value1 ne p_Value2 then undo, throw new  failure(substitute("Failed asserting that two datetimes [&1] and [&2] are equal",p_Value1,p_Value2)). 
  end method.

/* ==========================================================================
   assertEquals [HANDLE]
   ==========================================================================*/ 
  
  method protected void assertEquals(p_Value1 as handle,p_Value2 as handle):
    assign NumberOfAssertions = NumberOfAssertions + 1.
    if p_Value1 ne p_Value2 then undo, throw new  failure(substitute("Failed asserting that two handles [&1] and [&2] are equal",p_Value1,p_Value2)). 
  end method.

/* ==========================================================================
   assertEquals [CHARACTER]
   ==========================================================================*/ 
  
  method protected void assertEquals(p_Value1 as char,p_Value2 as char):
    assign NumberOfAssertions = NumberOfAssertions + 1.
    if p_Value1 ne p_Value2 then undo, throw new  failure(substitute("Failed asserting that two strings [&1] and [&2] are equal",p_Value1,p_Value2)). 
  end method.

/* ==========================================================================
   assertEquals [LOGICAL]
   ==========================================================================*/ 

  method protected void assertEquals(p_Value1 as logical,p_Value2 as logical):
    assign NumberOfAssertions = NumberOfAssertions + 1.
    if p_Value1 ne p_Value2 then undo, throw new  failure(substitute("Failed asserting that two logicals [&1] and [&2] are equal",p_Value1,p_Value2)). 
  end method.

/* ==========================================================================
   assertNotEquals [INTEGER]
   ==========================================================================*/ 
  
  method protected void assertNotEquals(p_Value1 as int,p_Value2 as int):
    assign NumberOfAssertions = NumberOfAssertions + 1.
    if p_Value1 eq p_Value2 then undo, throw new  failure(substitute("Failed asserting that two integers [&1] and [&2] are not equal",p_Value1,p_Value2)). 
  end method.

/* ==========================================================================
   assertNotEquals [LOGICAL]
   ==========================================================================*/ 
  
  method protected void assertNotEquals(p_Value1 as logical,p_Value2 as logical):
    assign NumberOfAssertions = NumberOfAssertions + 1.
    if p_Value1 eq p_Value2 then undo, throw new  failure(substitute("Failed asserting that two logicals [&1] and [&2] are not equal",p_Value1,p_Value2)). 
  end method.

/* ==========================================================================
   assertNotEquals [DECIMAL]
   ==========================================================================*/ 

  method protected void assertNotEquals(p_Value1 as dec,p_Value2 as dec):
    assign NumberOfAssertions = NumberOfAssertions + 1.
    if p_Value1 eq p_Value2 then undo, throw new  failure(substitute("Failed asserting that two decimals [&1] and [&2] are not equal",p_Value1,p_Value2)). 
  end method.

/* ==========================================================================
   assertNotEquals [DATE]
   ==========================================================================*/ 

  method protected void assertNotEquals(p_Value1 as date,p_Value2 as date):
    assign NumberOfAssertions = NumberOfAssertions + 1.
    if p_Value1 eq p_Value2 then undo, throw new  failure(substitute("Failed asserting that two dates [&1] and [&2] are not equal",p_Value1,p_Value2)). 
  end method.

/* ==========================================================================
   assertNotEquals [DATETIME]
   ==========================================================================*/ 
    
  method protected void assertNotEquals(p_Value1 as datetime,p_Value2 as datetime):
    assign NumberOfAssertions = NumberOfAssertions + 1.
    if p_Value1 eq p_Value2 then undo, throw new  failure(substitute("Failed asserting that two datetimes [&1] and [&2] are not equal",p_Value1,p_Value2)). 
  end method.

/* ==========================================================================
   assertNotEquals [HANDLE]
   ==========================================================================*/ 
  
  method protected void assertNotEquals(p_Value1 as handle,p_Value2 as handle):
    assign NumberOfAssertions = NumberOfAssertions + 1.
    if p_Value1 eq p_Value2 then undo, throw new  failure(substitute("Failed asserting that two handles [&1] and [&2] are not equal",p_Value1,p_Value2)). 
  end method.

/* ==========================================================================
   assertNotEquals [CHARACTER]
   ==========================================================================*/ 

  method protected void assertNotEquals(p_Value1 as char ,p_Value2 as char):
    assign NumberOfAssertions = NumberOfAssertions + 1.
    if p_Value1 eq p_Value2 then undo, throw new  failure(substitute("Failed asserting that two strings [&1] and [&2] are not equal",p_Value1,p_Value2)). 
  end method.

/* ==========================================================================
   assertNull [CHARACTER]
   ==========================================================================*/ 

  method protected void assertNull(p_Value1 as char):
    assign NumberOfAssertions = NumberOfAssertions + 1.
    if p_Value1 ne ? then undo, throw new  failure(substitute("Failed asserting that string [&1] is null",p_Value1)). 
  end method.

/* ==========================================================================
   assertNull [DATE]
   ==========================================================================*/ 

  method protected void assertNull(p_Value1 as date):
    assign NumberOfAssertions = NumberOfAssertions + 1.
    if p_Value1 ne ? then undo, throw new  failure(substitute("Failed asserting that date [&1] is null",p_Value1)). 
  end method.

/* ==========================================================================
   assertNull [LOGICAL]
   ==========================================================================*/ 

  method protected void assertNull(p_Value1 as logical):
    assign NumberOfAssertions = NumberOfAssertions + 1.
    if p_Value1 ne ? then undo, throw new  failure(substitute("Failed asserting that logical [&1] is null",p_Value1)). 
  end method.

/* ==========================================================================
   assertNull [INTEGER]
   ==========================================================================*/ 
  
  method protected void assertNull(p_Value1 as int):
    assign NumberOfAssertions = NumberOfAssertions + 1.
    if p_Value1 ne ? then undo, throw new  failure(substitute("Failed asserting that integer [&1] is null",p_Value1)). 
  end method.

/* ==========================================================================
   assertNull [DATETIME]
   ==========================================================================*/ 

  method protected void assertNull(p_Value1 as datetime):
    assign NumberOfAssertions = NumberOfAssertions + 1.
    if p_Value1 ne ? then undo, throw new  failure(substitute("Failed asserting that datetime [&1] is null",p_Value1)). 
  end method.

/* ==========================================================================
   assertNull [DEC]
   ==========================================================================*/ 

  method protected void assertNull(p_Value1 as dec):
    assign NumberOfAssertions = NumberOfAssertions + 1.
    if p_Value1 ne ? then undo, throw new  failure(substitute("Failed asserting that decimal [&1] is null",p_Value1)). 
  end method.

/* ==========================================================================
   assertNull [HANDLE]
   ==========================================================================*/ 

  method protected void assertNull(p_Value1 as handle):
    assign NumberOfAssertions = NumberOfAssertions + 1.
    if p_Value1 ne ? then undo, throw new  failure(substitute("Failed asserting that handle [&1] is null",p_Value1)). 
  end method.

/* ==========================================================================
   assertNotNull [CHARACTER]
   ==========================================================================*/ 

  method protected void assertNotNull(p_Value1 as char):
    assign NumberOfAssertions = NumberOfAssertions + 1.
    if p_Value1 eq ? then undo, throw new  failure(substitute("Failed asserting that string [&1] is not null",p_Value1)). 
  end method.

/* ==========================================================================
   assertNotNull [DATE]
   ==========================================================================*/ 

  method protected void assertNotNull(p_Value1 as date):
    assign NumberOfAssertions = NumberOfAssertions + 1.
    if p_Value1 eq ? then undo, throw new  failure(substitute("Failed asserting that date [&1] is not null",p_Value1)). 
  end method.

/* ==========================================================================
   assertNotNull [LOGICAL]
   ==========================================================================*/ 

  method protected void assertNotNull(p_Value1 as logical):
    assign NumberOfAssertions = NumberOfAssertions + 1.
    if p_Value1 eq ? then undo, throw new  failure(substitute("Failed asserting that logical [&1] is not null",p_Value1)). 
  end method.
  
/* ==========================================================================
   assertNotNull [INTEGER]
   ==========================================================================*/ 
  
  method protected void assertNotNull(p_Value1 as int):
    assign NumberOfAssertions = NumberOfAssertions + 1.
    if p_Value1 eq ? then undo, throw new  failure(substitute("Failed asserting that integer [&1] is not null",p_Value1)). 
  end method.

/* ==========================================================================
   assertNotNull [DATETIME]
   ==========================================================================*/ 

  method protected void assertNotNull(p_Value1 as datetime):
    assign NumberOfAssertions = NumberOfAssertions + 1.
    if p_Value1 eq ? then undo, throw new  failure(substitute("Failed asserting that datetime [&1] is not null",p_Value1)). 
  end method.

/* ==========================================================================
   assertNotNull [DECIMAL]
   ==========================================================================*/ 

  method protected void assertNotNull(p_Value1 as dec):
    assign NumberOfAssertions = NumberOfAssertions + 1.
    if p_Value1 eq ? then undo, throw new  failure(substitute("Failed asserting that decimal [&1] is not null",p_Value1)). 
  end method.

/* ==========================================================================
   assertNotNull [HANDLE]
   ==========================================================================*/ 

  method protected void assertNotNull(p_Value1 as handle):
    assign NumberOfAssertions = NumberOfAssertions + 1.
    if p_Value1 eq ? then undo, throw new  failure(substitute("Failed asserting that handle [&1] is not null",p_Value1)). 
  end method.
  
/* ==========================================================================
   assertDepends
   ==========================================================================*/ 

  method protected void assertDepends(p_Unit as char, p_Method as char):
    assign NumberOfAssertions = NumberOfAssertions + 1.
    def var UnitTest1 as class dotr.Maia.testing.UnitTest.unit.base no-undo.
    
    UnitTest1 = dynamic-new substitute("testing.UnitTest.unit.&1",p_Unit) ().
    
    dynamic-invoke (UnitTest1,substitute("test&1",p_Method)).

    catch eFailure as failure:
       undo, throw new  failure(substitute("Failed asserting that Depends [&1:&2] passed [&3]",p_Unit,p_Method,eFailure:failure)).
    end catch.

    catch e as Progress.Lang.Error :
      if e:GetMessageNum(1) eq 15312 then undo, throw new  failure(substitute("Failed asserting Depends [&1:&2] [Invalid Method]",
                                                                                  p_Unit,
                                                                                  p_Method)).

      if e:GetMessageNum(1) eq 14284 then undo, throw new  failure(substitute("Failed asserting Depends [&1:&2] [Invalid Unit Test]",
                                                                                  p_Unit,
                                                                                  p_Method)).
    end catch.

    finally:
      delete object UnitTest1.	
    end finally.
  end method.
  
/* ==========================================================================
   Method: assertFileExists
   
   Verifies the existance of a file

   Parameters:
      FileName : CHAR - Name of the file to find

   Returns:
      VOID
   ==========================================================================*/ 

  method protected void assertFileExists(p_File as char):
   assign NumberOfAssertions = NumberOfAssertions + 1.
   if search(p_File) eq ? then undo, throw new  failure(substitute("Failed asserting File Exists [&1] ", p_File)).
  end method.
  
/* ==========================================================================
   Method: assertValidObject
   
   Verifies that an object is valid

   Parameters:
      p_Class: Progress.lang.Object - the object to check

   Returns:
      VOID
   ==========================================================================*/ 

  method protected void assertValidObject(p_Object as class Progress.Lang.Object):
    assign NumberOfAssertions = NumberOfAssertions + 1.
    if not valid-object(p_Object) then undo, throw new  failure(substitute("Failed asserting object was valid ")).
  end method.

/* ==========================================================================
   Method: assertNotValidObject
   
   Verifies that an object is valid

   Parameters:
      p_Class: Progress.lang.Object - the object to check

   Returns:
      VOID
   ==========================================================================*/ 

  method protected void assertNotValidObject(p_Class as class Progress.Lang.Object):
    assign NumberOfAssertions = NumberOfAssertions + 1.
    if valid-object(p_Class) then undo, throw new  failure(substitute("Failed asserting object [&1] was invalid ",p_Class:GetClass():TypeName)).
  end method.

/* ==========================================================================
   Method: assertFound
   
   Verifies that an object holds appropriate data (parent record was found)

   Parameters:
      Object: Progress.Lang.Object- Name of the file to find

   Returns:
      VOID
   ==========================================================================*/ 

  method protected void assertFound(p_Found as logical):
    assign NumberOfAssertions = NumberOfAssertions + 1.
    if not p_Found then undo, throw new  failure("Failed asserting object was found ").  
  end method.

/* ==========================================================================
   Method: assertNotFound
   
   Verifies that an object does not hold appropriate data (parent record was not found)

   Parameters:
      Object: Progress.Lang.Object- Name of the file to find

   Returns:
      VOID
   ==========================================================================*/ 

  method protected void assertNotFound(p_Found as logical):
    assign NumberOfAssertions = NumberOfAssertions + 1.
    if p_Found then undo, throw new  failure("Failed asserting object was not found ").  
  end method.

end class.