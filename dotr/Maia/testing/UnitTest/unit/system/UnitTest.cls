using Progress.Lang.*.
using dotr.Maia.testing.UnitTest.unit.base.


routine-level on error undo, throw.

/* ==========================================
  class:  UnitTest
  Series of tests for the UnitTest system
  
  namespace: testing.UnitTest.unit.system
  ===========================================*/
  
class testing.UnitTest.unit.system.UnitTest inherits base:

/* ==========================================================================
   Method: testAssertEquals
   
   Performs various tests to ensure assertEquals is working
   
   Parameters:
      None

   Returns:
      VOID
   ==========================================================================*/ 
  method public void testAssertEquals(): 
    def var lv_DateTime as datetime no-undo.
    
    assign lv_DateTime = now.
    
    assertEquals(1,1).
    assertEquals("T","T").
    assertEquals(yes,yes).
    assertEquals(1.0,1.0).
    assertEquals(lv_DateTime,lv_DateTime).
  end method.

/* ==========================================================================
   Method: testAssertNotEquals
   
   Performs various tests to ensure assertNotEquals is working
   
   Parameters:
      None

   Returns:
      VOID
   ==========================================================================*/ 
  method public void testAssertNotEquals(): 
    def var lv_DateTime as datetime no-undo.
    
    assign lv_DateTime = now.

    assertNotEquals(1,0).
    assertNotEquals("T","Z").
    assertNotEquals(yes,no).
    assertNotEquals(1.0,2.0).
    assertNotEquals(lv_DateTime,add-interval(lv_DateTime,1,"second")).
  end method.

/* ==========================================================================
   Method: testAssertNull
   
   Performs various tests to ensure AssertNull is working
   
   Parameters:
      None

   Returns:
      VOID
   ==========================================================================*/ 
  method public void testAssertNull(): 
    def var lv_char     as char     no-undo init ?.
    def var lv_dec      as dec      no-undo init ?.
    def var lv_int      as int      no-undo init ?.
    def var lv_logical  as logical  no-undo init ?.
    def var lv_date     as date     no-undo init ?.
    def var lv_datetime as datetime no-undo init ?.
        
    assertNull(lv_char).
    assertNull(lv_dec).
    assertNull(lv_int).
    assertNull(lv_logical).
    assertNull(lv_date).
    assertNull(lv_datetime).
  end method.

/* ==========================================================================
   Method: testAssertNotNull
   
   Performs various tests to ensure AssertNotNull is working
   
   Parameters:
      None

   Returns:
      VOID
   ==========================================================================*/ 
  method public void testAssertNotNull(): 
    def var lv_char     as char     no-undo .
    def var lv_dec      as dec      no-undo .
    def var lv_int      as int      no-undo .
    def var lv_logical  as logical  no-undo .
    def var lv_date     as date     no-undo .
    def var lv_datetime as datetime no-undo .
    
    assign lv_DateTime = now
           lv_Date     = today.
    
    assertNotNull(lv_char).
    assertNotNull(lv_dec).
    assertNotNull(lv_int).
    assertNotNull(lv_logical).
    assertNotNull(lv_date).
    assertNotNull(lv_datetime).
  end method.
  
/* ==========================================================================
   Method: testAssertError
   
   Performs various tests to ensure AssertError is working
   
   Parameters:
      None

   Returns:
      VOID
   ==========================================================================*/ 
  method public void testAssertError(): 
    def var lv_datetime as datetime no-undo .
    
    assertNotNull(lv_datetime) no-error.
    assertError().
    
  end method.

/* ==========================================================================
   Method: testAssertNoError
   
   Performs various tests to ensure AssertNoError is working
   
   Parameters:
      None

   Returns:
      VOID
   ==========================================================================*/ 
  method public void testAssertNoError(): 
    def var lv_datetime as datetime no-undo .
    
    assertNull(lv_datetime) no-error.
    assertNoError().
    
  end method.
  
  
end class.