using Progress.Lang.*.
using dotr.Maia.testing.UnitTest.unit.base.


routine-level on error undo, throw.

/* ==========================================
  class:  DatabaseInit
  Series of tests to set up the Database Connections
  
  namespace: testing.UnitTest.unit.system
  ===========================================*/
  
class dotr.Maia.testing.UnitTest.unit.system.DatabaseInit inherits base:

/* ==========================================================================
   Method: testIsConnected
   
   Connects Debt and Acceptance databases

   Parameters:
      None

   Returns:
      VOID
   ==========================================================================*/ 
  method public void testIsConnected():
    AssertTrue(connected("UNITTEST")).
  end method.

end class.