using Progress.Lang.*.

class dotr.Maia.testing.UnitTest.TestFail inherits dotr.maia.testing.UnitTest.TestResult: 
	def public property Order as int no-undo get . private set.

  def public property RESULT as char no-undo get . private set.
 
	constructor public TestFail (p_Result as handle):
	  super (p_Result).
	  
	  assign Order = p_Result::Order
           RESULT     = p_Result::Result.	  
	end constructor.

end class.