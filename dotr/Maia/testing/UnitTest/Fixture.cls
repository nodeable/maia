using Progress.Lang.*.

routine-level on error undo, throw.

class dotr.Maia.testing.UnitTest.Fixture: 
  def var FixtureObject as class Progress.Lang.Object no-undo.

	constructor public Fixture( p_TestUnit as char ):
		super ().
    FixtureObject = dynamic-new substitute("testing.UnitTest.fixtures.&1fixture",p_TestUnit) () .
    
	end constructor.

/* ==========================================================================
   After
   ==========================================================================*/ 
  method public void After():
    dynamic-invoke (FixtureObject,"After") .
  end method.
    
/* ==========================================================================
   Before
   ==========================================================================*/ 
  method public void Before():
  
    dynamic-invoke (FixtureObject,"Before") .
  
  end method.

/* ==========================================================================
   BeforeTest
   ==========================================================================*/ 
  method public void BeforeTest():
    dynamic-invoke (FixtureObject,"BeforeTest") .
  end method.

/* ==========================================================================
   AfterTest
   ==========================================================================*/ 
  method public void AfterTest():
    dynamic-invoke (FixtureObject,"AfterTest") .
  end method.
     
end class.