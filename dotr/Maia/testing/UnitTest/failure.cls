using Progress.Lang.*.
routine-level on error undo, throw.

class dotr.Maia.testing.UnitTest.failure inherits AppError : 
  def public property failure        as char no-undo get . private set.
  
  constructor public failure (p_Failure as char):
    def var lv_Data as char no-undo.
    super(p_Failure,0).
    
    assign lv_Data = if return-value eq ? then error-status:get-message(1) else return-value
           this-object:failure     = substitute("&1 <<&2>>",p_Failure,lv_Data)
           NO-ERROR.
    return .
  end constructor.
end class.