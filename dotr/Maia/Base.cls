/*
Copyright (c) 2013, Julian Lyndon-Smith (julian@dotr.com)
http://www.dotr.com
All rights reserved.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction except as noted below, including without limitation
the rights to use,copy, modify, merge, publish, distribute,
and/or sublicense, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

 The Software and/or source code cannot be copied in whole and
 sold without meaningful modification for a profit.

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.

 Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in
 the documentation and/or other materials provided with
 the distribution.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

using Progress.Lang.*.

routine-level on error undo, throw.

class dotr.Maia.Base abstract : 
  {dotr/maia/temptable/Property.i &AccessMode=protected}
  {dotr/maia/temptable/Template.i &AccessMode=protected}
  {dotr/maia/temptable/BuildOptions.i &AccessMode=protected}
  {dotr/maia/temptable/BuildErrors.i &AccessMode=protected}
  {dotr/maia/temptable/Validation.i &AccessMode=protected}

  def public property BaseCode as longchar no-undo get . set .
  
  def public property inherits          as char no-undo get . protected set .
  def public property Package           as char no-undo get . protected set .
  def public property TemplateDirectory as char no-undo get . protected set .
  
  def public property ClassName         as char no-undo 
    get():
      return MagicName(ClassName).
    end get . protected set .

  def public property BaseDirectory     as char no-undo get . private set .
  def public property BuildDirectory    as char no-undo get . private set .

  def public property HasMagicFields as logical no-undo get . protected set .
  
  def public property BuildFile         as char no-undo 
    get():
      return substitute("&1/&2.&3",this-object:BuildDirectory,
                                   this-object:ClassName,
                                   GetOptionValue("FileExtension")).
    end get . private set .
  
  def public property SendStompUpdate as logical no-undo get . protected set .

  constructor Base():
    assign this-object:BaseDirectory     = this-object:GetClass():TypeName
           this-object:BaseDirectory     = substr(this-object:BaseDirectory,1,r-index(this-object:BaseDirectory,".") - 1)
           this-object:BaseDirectory     = replace(this-object:BaseDirectory,".","/")
           this-object:BaseDirectory     = replace(this-object:BaseDirectory,"/_Maia","")
           this-object:BuildDirectory    = this-object:BaseDirectory
           this-object:Package           = replace(this-object:BaseDirectory,"/",".")
           this-object:ClassName         = this-object:GetClass():TypeName
           this-object:ClassName         = MagicName(entry(num-entries(this-object:ClassName,"."),this-object:ClassName,".")).
           this-object:TemplateDirectory = substitute("&1/_Maia/_Template",this-object:BaseDirectory).

  end constructor.
  
  method public void SetBuildDirectory():
    assign this-object:BuildDirectory = getOptionValue("BuildDirectory")
           this-object:BuildDirectory = substitute("&1&2",this-object:BaseDirectory,
                                                          if this-object:BuildDirectory ne ""
                                                             then substitute("/&1",this-object:BuildDirectory)
                                                             else "").
  end method.
  
  method public void GetProperties(output table for TTProperty bind):
  end method.

  method public void GetTemplates(output table for TTTemplate bind):
  end method.

  method public void init():
  end method.

  method protected void UsingTemplate(p_Template as char):
    def var lv_FullTemplate as char no-undo.
    def var lv_List         as char no-undo.
    def var lv_Name         as char no-undo.
    
    def var lv_i as int no-undo.
    
    def var lv_overwrite as logical no-undo.
    
    assign lv_list = getGroup(p_template).
    
    do lv_i = 1 to num-entries(lv_list):     
      assign lv_Name      = entry(lv_i,lv_list)
             lv_overwrite = yes.
      
      if num-entries(lv_name,":") gt 1 
         then assign lv_Overwrite = logical(entry(2,lv_Name,":"))
                     lv_Name      =  entry(1,lv_Name,":").
                     
      assign lv_FullTemplate = substitute("&1/&2",this-object:TemplateDirectory,lv_Name).
    
      if search(lv_fullTemplate) eq ? then 
      do:
        assign lv_fullTemplate = substitute("dotr/Maia/Template/&1",lv_Name).
        if search(lv_fullTemplate) eq ? then undo, throw new Progress.Lang.AppError(substitute("Cannot find template [&1]",lv_name),0).
      end.

      create TTTemplate.
      
      assign TTTemplate.Name      = lv_Name 
             TTTemplate.Overwrite = lv_overwrite 
             TTTemplate.PathName  = search(lv_fullTemplate).
    end.
     
  end method.

  method public void LoadTemplate(p_Template as char):
    def var Template1  as dotr.Maia.Model.Template  no-undo.
    
    Template1 = new dotr.Maia.Model.Template().
    
    this-object:BaseCode = Template1:LoadTemplate(this-object,p_Template).
    
  end method.

/*  method protected dotr.Maia.Model.Validation Validate(p_Property as char):                                 */
/*    def var oValidate as dotr.Maia.Model.BuildValidation no-undo.                                           */
/*                                                                                                            */
/*    find TTProperty where TTProperty.PropertyName eq p_Property no-error.                                   */
/*                                                                                                            */
/*    if not avail TTProperty then undo, throw new AppError(substitute("Property &1 not found",p_Property),0).*/
/*                                                                                                            */
/*    oValidate = new dotr.Maia.Model.BuildValidation(p_Property).                                            */
/*                                                                                                            */
/*    return cast(oValidate,dotr.Maia.Model.Validation).                                                      */
/*  end method.                                                                                               */
 
  method protected dotr.Maia.Model.Property Property(p_Property as dotr.Maia.Model.Property):
    def var Property1 as dotr.Maia.Model.BuildProperty no-undo.
    
    def var lv_PropName as char no-undo.
      
    Property1 = cast(p_Property,dotr.Maia.Model.BuildProperty).    
    
    assign lv_PropName = Property1:PropertyData:PropertyName.
    
    find TTProperty where TTProperty.PropertyName eq lv_PropName no-error.
    
    if avail TTProperty and Property1:PropertyData:mode eq "add" then undo, throw new AppError(substitute("Property &1 already exists",lv_PropName),0).
    
    if not avail TTProperty then
    do:
      if Property1:PropertyData:mode ne "add" then undo, throw new AppError(substitute("Property &1 not found",lv_PropName),0).
      create TTProperty.
    end.
    
    assign TTProperty.AccessMode     = Property1:PropertyData:AccessMode    
           TTProperty.BuildProperty  = Property1:PropertyData:BuildProperty  
           TTProperty.Comment        = Property1:PropertyData:Comment        
           TTProperty.DataType       = Property1:PropertyData:DataType       
           TTProperty.Extent         = Property1:PropertyData:Extent
           TTProperty.PropertyName   = lv_PropName
           TTProperty.ManualProperty = Property1:PropertyData:ManualProperty
           TTProperty.fieldName      = Property1:PropertyData:FieldName
           TTProperty.InitialValue   = Property1:PropertyData:InitialValue   
           TTProperty.Size           = length(TTProperty.FieldName)
           TTProperty.GenerateGUID   = Property1:PropertyData:GenerateGUID
           TTProperty.ViewAs         = Property1:PropertyData:ViewAs
           TTProperty.FormLabel      = Property1:PropertyData:Label
           TTProperty.ToolTip        = Property1:PropertyData:ToolTip
           TTProperty.HelpField      = Property1:PropertyData:Help
           TTProperty.ReadOnly       = Property1:PropertyData:ReadOnly
           
           TTProperty.Sequence       = Property1:PropertyData:Sequence.
 
    return p_Property.            
  end method.

  method private dotr.Maia.Model.Property Get(p_property as char,p_mode as char):
    def var Property1 as dotr.Maia.Model.BuildProperty no-undo.
    
    Property1 = new dotr.Maia.Model.BuildProperty(p_Property).

    find TTProperty where TTProperty.PropertyName eq p_Property no-error.
    
    if not avail TTProperty then undo, throw new AppError(substitute("Property &1 not found",p_property),0).
    
    assign Property1:PropertyData:AccessMode     = TTProperty.AccessMode
           Property1:PropertyData:BuildProperty  = TTProperty.BuildProperty
           Property1:PropertyData:Comment        = TTProperty.Comment
           Property1:PropertyData:DataType       = TTProperty.DataType
           Property1:PropertyData:Extent         = TTProperty.Extent
           Property1:PropertyData:FieldName      = TTProperty.FieldName
           Property1:PropertyData:ManualProperty = TTProperty.ManualProperty
           Property1:PropertyData:PropertyName   = TTProperty.PropertyName
           Property1:PropertyData:InitialValue   = TTProperty.InitialValue.
           Property1:PropertyData:Mode           = p_mode.
           
    return cast(Property1,dotr.Maia.Model.Property).
  end method. 

  method public dotr.Maia.Model.Property Get(p_property as char):
    return this-object:Get(p_property,"get").
  end method. 

	method private rowid IsValidProperty(p_name as char):
		def buffer TTProperty for TTProperty.
  	
    find first TTProperty where TTProperty.PropertyName eq p_name no-error.

    if not avail TTProperty then find first TTProperty where TTProperty.FieldName eq p_name no-error.

    if not avail TTProperty then undo, throw new AppError(substitute("Property [&1] not found",p_name),0).
    
    return rowid(TTProperty).
    
	end method.
	
	method public void Rename(p_Rename as dotr.Maia.Model.Rename):
		def buffer TTProperty for TTProperty.
		
    find TTProperty where rowid(TTProperty) eq p_Rename:PropertyRowid no-error.

    if not avail TTProperty then undo, throw new AppError(substitute("Property [&1] not found",p_Rename:OldName),0).

    assign TTProperty.PropertyName = (new dotr.Maia.Helper.Property()):clean(p_Rename:NewName,TTProperty.DataType).
	end method.
	
  method protected dotr.Maia.Model.Rename Rename(p_old as char):
  	def var oRename as dotr.Maia.Model.Rename no-undo.
  	
    oRename = new dotr.Maia.Model.Rename(this-object,p_Old,IsValidProperty(p_old)).
    return oRename.
  end method. 
  
  method protected dotr.Maia.Model.Validation Validation(p_property as char):
  	def var PropertyRowid as rowid no-undo.
  	
  	def var oValidation as dotr.Maia.Model.Validation no-undo.
  	
  	assign PropertyRowid = IsValidProperty(p_property).

    find TTValidation where TTValidation.PropertyRowid eq oValidation:PropertyRowid no-error.
    
    if avail TTValidation then assign oValidation = cast(TTValidation.oObject,dotr.Maia.Model.Validation).
    
    else
    do:
    	oValidation = new dotr.Maia.Model.Validation(this-object,p_property,PropertyRowid).	    
    	
    	create TTValidation.
    	assign TTValidation.PropertyRowid = PropertyRowid   
      	     TTValidation.oObject       = oValidation.
    end.
  	
    return oValidation.
  end method.
/*                                                                                                                */
/*  method protected dotr.Maia.Model.Property Rename(p_old as char, p_New as char):                               */
/*                                                                                                                */
/*                                                                                                                */
/*  end method.                                                                                                   */

  method protected dotr.Maia.Model.Property NewProp(p_New as char):
    return NewProp(p_New,dotr.Maia.Enum.DataTypeEnum:char).
  end method.
  
  method protected dotr.Maia.Model.Property NewProp(p_New as char,p_DataType as dotr.Maia.Enum.DataTypeEnum):
    def var Property1 as dotr.Maia.Model.BuildProperty no-undo.
    
    assign p_New = (new dotr.Maia.Helper.Property()):clean(p_New,p_DataType:value).
        
    Property1 = new dotr.Maia.Model.BuildProperty(p_New).
    
    find TTProperty where TTProperty.PropertyName eq p_New no-error.

    if avail TTProperty then undo, throw new AppError(substitute("New Property &1 already exists",p_new),0).
    
    assign Property1:PropertyData:AccessMode     = "public"
           Property1:PropertyData:BuildProperty  = yes
           Property1:PropertyData:Comment        = ""
           Property1:PropertyData:DataType       = p_DataType:value
           Property1:PropertyData:Extent         = 0
           Property1:PropertyData:FieldName      = "[none]"
           Property1:PropertyData:ManualProperty = yes 
           Property1:PropertyData:PropertyName   = p_New
           Property1:PropertyData:InitialValue   = ""
           Property1:PropertyData:Mode           = "add".
         
    return cast(Property1,dotr.Maia.Model.Property).
  end method. 
 
    
  /** return the templates in a group 
   *  @param p_Group Group to expand
   *  @return comma-seperated list of templates 
   */ 
  method private char getGroup(p_Group as char):
    def var lv_data as longchar no-undo.
    
    def var lv_Group as char no-undo.
    def var lv_file  as char no-undo.
    
    def var lv_i as int no-undo.
    
    if not p_Group begins "@" then return p_Group.
    
    assign lv_File = substitute("&1/&2",this-object:TemplateDirectory,p_Group).

    if search(lv_File) eq ? then 
    do:
      assign lv_File = substitute("dotr/Maia/Template/&1",p_Group).
    
      if search(lv_File) eq ? then undo, throw new AppError(substitute("Cannot find template [&1]",lv_File),0).
    end.
    
    assign file-info:file-name = lv_File.
             
    copy-lob from file file-info:full-pathname to lv_Data.
    
    assign lv_Data = replace(lv_data,"~n",",")
           lv_Data = replace(lv_data,"~r","").
    
    return string(trim(lv_Data,",")). 
    
  end method.
  
/** returns the Builder option value
   * @param p_Property : name of option
   * @return char : value of option
   */
  
  method public char GetOptionValue(p_Property as char):
    find BuildOption where BuildOption.OptionName eq p_Property no-lock no-error.
    
    return if avail BuildOption then BuildOption.OptionValue else "".  
  end method.

  /** sets a default builder option value
   * if option already exists, do not overwrite it
   * @param p_Property : name of option to set
   * @param p_Value : default value to set
   */

  method public void SetDefaultOption(p_Property as char,p_Value as char):
    find BuildOption where BuildOption.OptionName eq p_Property no-lock no-error.
    
    if avail BuildOption then return.
    SetOption(p_Property,p_Value).
  end method.
  
  /** sets a builder option value
   * @param p_Property : name of option to set
   * @param p_Value : default value to set
   */

  method public void SetOption(p_Property as char,p_Value as char):
    if p_Property eq "" then return.
    
    find BuildOption where BuildOption.OptionName eq p_Property no-lock no-error.
    
    if not avail BuildOption then create BuildOption.
    
    assign BuildOption.OptionName  = p_Property
           BuildOption.OptionValue = p_Value.
  end method.

  /** loop through all builder options, replacing the text in the template <<OptionName>> with the option value */
  method public void SubstituteOptions():
    def var lv_base as longchar no-undo.
    
    assign lv_base = this-object:BaseCode. 
    
    for each BuildOption no-lock:
      assign lv_Base = replace(lv_Base,substitute("<<&1>>",BuildOption.OptionName),BuildOption.OptionValue).
    end.
    
    assign this-object:BaseCode = right-trim(lv_base,"~n").
             
  end method.
  
  method public void SetBuildData():
    assign this-object:BaseCode  = this-object:BaseCode + substitute('~n@BuildInfo(Source="&1").',this-object:Classname)
           this-object:BaseCode  = this-object:BaseCode + substitute('~n@BuildInfo(Class="&1").',this-object:GetClass():TypeName)
           this-object:BaseCode  = this-object:BaseCode + substitute('~n@BuildInfo(Date="&1").',GetOptionValue("BuildDate"))
           this-object:BaseCode  = this-object:BaseCode + substitute('~n@BuildInfo(MD5Hash="&1").',GetOptionValue("MD5Hash")).
                                                                      
  end method.
  
    /** gets a CustomCode object 
   * custom code can be inserted in 3 ways. 
   * 1) a customcode directive in the actual generated file
   * 2) a customcode entry in the object config file
   * 3) a customcode snippet in the object config directory
   * 
   * @param this-object : Object to get property from
   * @param p_CustomCode : CustomCode to get
   * @return dotr.Maia.Model.CustomCode : the CustomCode
   */

  method public longchar GetCustomCode(p_file as char, p_CustomCode as char ):
    def var lv_i     as int no-undo.
    def var lv_Start as int no-undo.
    def var lv_End   as int no-undo.
    
    def var lv_Data as longchar no-undo.
    
    def var lv_CustomStart as char no-undo.
    def var lv_CustomEnd   as char no-undo.
    
    assign file-info:file-name = p_File.

    /** if the output file exists, load it up, and find all customcode directives */
    if file-info:full-pathname ne ? then  
    do:
      copy-lob from file file-info:full-pathname to lv_Data.
      
      assign lv_CustomStart = replace('@CustomCode(name="Start&1").',"&1",p_CustomCode)
             lv_CustomEnd   = replace('@CustomCode(name="End&1").',"&1",p_CustomCode)
             lv_Start       = index(lv_Data,lv_CustomStart) + length(lv_CustomStart) 
             lv_End         = index(lv_Data,lv_CustomEnd,lv_Start).
      
      if lv_Start ne 0 and lv_End ne 0 then 
      do:
        assign lv_Data = substitute("&1~n&2~n&3",lv_CustomStart,trim(substr(lv_Data,lv_Start,(lv_end - 1) - lv_Start)," ~n_t"),lv_CustomEnd).
        return lv_Data.
      end.             
    end.
    
    return "".
                          
  end method.
  
  /** turn fieldname into property name
   * @param field
   * @return magic name
   */
  
  method public char MagicName(p_field as char):
    def var lv_Indicator as char case-sensitive no-undo.
    def var lv_Property  as char                no-undo.
    
    assign lv_Indicator  = substr(p_Field,1,1).
    
    /** this is for cases where the field name starts with a letter indicating the data type. 
      * for example cCustomer. we don't want a property called foo:cCustomer, so need to strip
      * the leading chars off */ 
      
    if this-object:HasMagicFields and lc(lv_Indicator) eq lv_Indicator then /** assuming that a lower-case letter starting a field name indicates the data type */  
    do:
      case lv_Indicator:
        when "i" or when "c" then assign lv_Property = substr(p_Field,2).
        
        otherwise
        do:
          assign lv_Indicator = substr(p_Field,1,2).
          case lv_Indicator:
            when "de" or when "lo" or when "dt" or when "da" then assign lv_Property = substr(p_Field,3).
            otherwise assign lv_Property = p_Field.
          end case.
        end. 
         
      end case.
      
    end.
  
    else assign lv_Property = p_Field.      
    
    assign lv_Property = stripchar(lv_Property,"-")
           lv_Property = stripchar(lv_Property,"_").
          
    return lv_Property.         
  end method.
  
  /** strips chars from field 
   * @param field
   * @param char to strip
   * @return return value
   */
  method private char stripchar(p_field as char,p_char as char):
    def var lv_i as int no-undo.
    
    assign lv_i = index(p_field,p_char).
    
    do while lv_i gt 0 :
      assign substr(p_field,lv_i,2) = caps(substr(p_field,lv_i + 1,1))
             lv_i = index(p_field,p_char).
    end.
    
    assign substr(p_Field,1,1) = caps(substr(p_Field,1,1)).
    
    return p_Field.
    
  end method.
  
	/** log a build error
		* @param 
		* @return 
		*/      

  method public void LogError(p_AppErr as Progress.Lang.AppError):
    this-object:CreateLog(p_AppErr:getmessage(1),p_AppErr:GetMessageNum(1),"AppError").
  end method.      

  method public void LogError(p_Err as Progress.Lang.Error):
    this-object:CreateLog(p_Err:getmessage(1),p_Err:GetMessageNum(1),"SystemError").
  end method.      
  
  method protected void CreateLog(p_message as char,p_num as int, p_Type as char):
    create TTBuildErrors.
    assign TTBuildErrors.ErrMsg  = p_Message
           TTBuildErrors.ErrNum  = p_num
           TTBuildErrors.ErrType = p_type. 
  end method.
  

end class.