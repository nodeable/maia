  def {&AccessMode} temp-table TTTemplate no-undo {&REFERENCE-ONLY}

      field Name      as char 
      field PathName  as char 
      field BuildDir  as char
      field Overwrite as logical init yes.
  