 DEFINE {&AccessMode} temp-table TTAssociation no-undo serialize-name "Association" {&reference-only}
  
      field Type  as character 
      field ToModel as char 
      field FromModel as char
      field CascadeMode      as character 
      field FindUsingProperties as character 
      field FindUsingName as character
      field FindUsing_fkProperties as character 
      field FindUsing_fkName as character
      field PropertyName     as character 
      field hasIndex   as logical
      field name as char
      
      index type is Primary
        Type
        PropertyName
        
      index ToModel 
        ToModel
        
      index FindUsingName
        FindUsingName
        
      index hasIndex
        hasIndex
        .
  