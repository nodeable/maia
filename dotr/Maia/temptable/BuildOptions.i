  /** temp-table used to store options for the build */  
  def {&AccessMode} temp-table BuildOption no-undo {&reference-only}
    field OptionName  as char
    field OptionValue as char
    
    index OptionName is primary unique
      OptionName.