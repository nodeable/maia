 DEFINE {&AccessMode} temp-table TTFindUsing no-undo {&reference-only}
      field GetName  as character 
      field DocParameterList  as character 
      field FindString    as character extent 5
      field FindUnique as character  
      field ParameterList      as character 
      field ParameterCall as character
      field SubstituteList as char extent 5
      field IsWordIndex as logical
      field IsUnique as logical
      field IndexName as char
      field Property as char extent 25
      field FieldName as char extent 25
      field MaxProperty as int 
      field nullify as char
      index Getname is primary
      
        GetName.
  