
define {&AccessMode} temp-table TTProject no-undo serialize-name "Project" {&REFERENCE-ONLY}
      field Description                           as character 
      field Directory                             as character 
      field Name                                  as character 
      field Package                               as character 
      field ProjectGUID                           as character 
      field TemplateDirectory                     as character 
  
      field MagicField                            as logical 
      field UseActiveMQ                           as logical .
