 DEFINE {&AccessMode} temp-table TTAssociation no-undo serialize-name "Association" {&reference-only}
  
      field Type  as character 
      field ToModel as char 
      field FromModel as char
      field CascadeMode      as character 
      field FromProperty     as character 
      field PropertyName     as character 
      field ToProperty       as character 
      
      index type is Primary
        Type
        
      index ToModel 
        ToModel
        .
  