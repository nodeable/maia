def {&AccessMode} temp-table TTProperty no-undo serialize-name "Property" {&REFERENCE-ONLY}
      field AccessMode     as character 
      field AppPackage     as character 
      field Brief          as character 
      field BuildUnitGUID  as character 
      field Comment        as character 
      field DataType       as character 
      field Description    as character 
      field FieldName      as character 
      field FormLabel      as character 
      field GetCode        as character 
      field GetMode        as character 
      field InitialValue   as character 
      field PropertyGUID   as character 
      field PropertyName   as character 
      field Sequence       as character 
      field SetCode        as character 
      field SetMode        as character 
      field ViewAs         as character
      field HelpField      as character
      field tooltip        as character
      
      field Extent          as integer 
      field size            as integer
  
      field BuildProperty    as logical init yes
      field Checked          as logical 
      field GenerateGUID     as logical 
      field IsArrayWordIndex as logical 
      field LoadLazy         as logical 
      field ManualProperty   as logical
      field ReadOnly         as logical .

