 DEFINE {&AccessMode} temp-table TTBuildErrors no-undo serialize-name "BuildErrors" {&reference-only}
  
      field ErrNum as int 
      field ErrMsg as char 
      field ErrType as char
        .
  