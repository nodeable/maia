  DEFINE temp-table TTModel no-undo serialize-name "Model" {&REFERENCE-ONLY}
    field MagicFields as logical      
    field ClassName   as char /* name of the class (base name only) [foo] */
    field TableName   as char /* table name of the class (base name only) [foo] */
    field KeyFieldName      as char 
    field KeyProperty as char 
    field KeyFieldDatatype as char 
    field modelName as char
    field modelPackage as char
    field Directory as char
    field Package as char
    field projectGUID as char
    field PropertyEditor as char
    field fkDelete as char
    field LogicalDB as char
    field BuildUnitGUID as char
    field inherits as char
    field BuildObject   as logical init yes /* do we build this class or not */
    field isAbstract    as logical init no 
    field TempTableOnly as logical init no
    
    field CRC           as int
    
    index main is primary unique 
      ProjectGUID
      ClassName.
