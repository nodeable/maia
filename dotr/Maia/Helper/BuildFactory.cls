/*
Copyright (c) 2013, Julian Lyndon-Smith (julian@dotr.com)
http://www.dotr.com
All rights reserved.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction except as noted below, including without limitation
the rights to use,copy, modify, merge, publish, distribute,
and/or sublicense, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

 The Software and/or source code cannot be copied in whole and
 sold without meaningful modification for a profit.

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.

 Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in
 the documentation and/or other materials provided with
 the distribution.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
using Progress.Lang.*.

routine-level on error undo, throw.

class dotr.Maia.Helper.BuildFactory: 
  def private var Slash as char no-undo.
 
  constructor BuildFactory():
    case true:
      when opsys begins "win" or
      when opsys begins "ms"  then assign slash = "~\".
      
      when opsys begins "unix" then assign slash = "/".
      otherwise assign slash = "/".
    end case.
  end constructor.   
  
  method public void Build(p_path as char):
    assign file-info:file-name = p_Path.
    
    etime(yes).
    
    if file-info:file-type begins "D" then 
    do:
      if not p_Path matches substitute("*&1_Maia",slash) then undo, throw new Progress.Lang.AppError(substitute("&1 is not a maia build file",p_Path),0).
      GetFiles(p_Path).
    end.
    
    else BuildFile(p_Path).
    
    undo, throw new AppError(substitute("build Complete [&1] seconds elapsed",etime / 1000),-42) .
    
  end method.
  
  method private void BuildFile(p_path as char):
    def var lv_path  as char no-undo.
    def var lv_file  as char no-undo.
    def var lv_class as char no-undo.
  
    def var buildModel   as dotr.Maia.Model               no-undo.
           
    assign p_Path = replace(p_Path,os-getenv("ECLIPSE_ROOT"),"")
           lv_file = substr(p_Path,r-index(p_Path,slash) + 1)
           lv_Path = substr(p_Path,1,r-index(p_Path,slash) - 1).
  
    if not entry(2,lv_file,".") eq "cls" then undo, throw new Progress.Lang.AppError(substitute("&1 is not a class file",lv_file),0). 
    if not lv_Path matches substitute("*&1_Maia",slash) then undo, throw new Progress.Lang.AppError(substitute("&1 is not a Maia directory",lv_Path),0).
    
    if lv_file begins "_" then undo, throw new Progress.Lang.AppError(substitute("&1 is not a valid maia build file",lv_file),0).
       
    assign lv_Class = replace(p_Path,slash,".").
           lv_Class = substr(lv_Class,1,r-index(lv_Class,".") - 1).
  
    BuildModel = dynamic-new lv_Class().
    
    (new dotr.Maia.Helper.BuildManager()):Build(BuildModel).
  end method.

  method private void GetFiles (p_Root as char) :
    def var lv_File     as char no-undo.
   
    /* now loop through all directories in the root (which would include the base directory) */
    input from os-dir(p_Root). 
    repeat :
      import lv_File.
      if not lv_file matches "*.cls" then next.
      if lv_file begins "_" then next.
      
      BuildFile(substitute("&1&2&3",p_root,slash,lv_file)).
    end. 
  
    input close.
    return.
    
  end method.
  
  
end class.