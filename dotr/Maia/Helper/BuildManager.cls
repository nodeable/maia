/*
Copyright (c) 2013, Julian Lyndon-Smith (julian+maia@dotr.com)
http://www.dotr.com
All rights reserved.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction except as noted below, including without limitation
the rights to use,copy, modify, merge, publish, distribute,
and/or sublicense, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

 The Software and/or source code cannot be copied in whole and
 sold without meaningful modification for a profit.

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.

 Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in
 the documentation and/or other materials provided with
 the distribution.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

using Progress.Lang.*.
using dotr.Maia.Model.*.

class dotr.Maia.Helper.BuildManager : 
  {dotr/maia/temptable/Property.i &REFERENCE-ONLY="reference-only"}
  {dotr/maia/temptable/Template.i &REFERENCE-ONLY="reference-only"}
 
  /** build supplied object
   * this is the main component of the ObjectBuilder. All templates are read and parsed here
   * and the appropriate directive invoked
   */
  method public void Build(p_Class as dotr.Maia.Model):
    def var lv_changedCode as longchar no-undo.
    def var lv_102B        as longchar no-undo.
    
    def var lv_Builder      as char no-undo.
    def var lv_File         as char no-undo.
    def var lv_FileMD5Hash  as char no-undo.
    def var lv_OrigMD5Hash  as char no-undo.
    def var lv_Info         as char no-undo. 
    def var lv_TemplateName as char no-undo.
    
    def var lv_Startindex  as int no-undo.
    def var lv_Endindex    as int no-undo.
    def var lv_StartLine   as int no-undo.
    def var lv_i           as int no-undo.
    
    def var Directive1 as dotr.Maia.Model.Directive no-undo.
    def var Builder1   as dotr.Maia.Builder.Basic   no-undo.

    /** run through all templates defined for this object */
 
    p_Class:init().
    
    p_Class:GetTemplates(output table TTTemplate bind).
    
    templateloop:
    for each TTTemplate no-lock on error undo, throw:
       
      assign lv_TemplateName  = TTTemplate.Name.
      
      p_Class:LoadTemplate(TTTemplate.PathName).

      /** decide which builder to use for this template */
      assign lv_Builder = p_Class:GetOptionValue("Builder").
  
      /** instantiate the builder */
      Builder1 = dynamic-new string(lv_Builder) (p_Class).
         
      /** oops */
      if not Builder1:BuilderAvailable then 
      do:
        message "Unable to load builder. This may be caused by having no UI properties defined for a UI builder" lv_Builder view-as alert-box warning.
        next templateloop.
      end.
            
      p_Class:SetDefaultOption("AppPackage",p_Class:Package).

      p_Class:SetDefaultOption("ClassName",p_Class:ClassName).
      p_Class:SetDefaultOption("Builder","dotr.Maia.Builder.Basic").
      p_Class:SetDefaultOption("FileExtension", "cls").

      p_Class:SetDefaultOption("FileName",p_Class:GetOptionValue("ClassName")).

      p_Class:SetDefaultOption("StompServer","localhost").

      p_Class:SetOption("BuildDate",string(now)).
      
      p_Class:SetBuildDirectory().
      
      assign lv_File    = substitute("&1/&2.&3",p_Class:BuildDirectory,p_Class:ClassName,p_Class:GetOptionValue("FileExtension")).

      p_Class:SetDefaultOption("BuildFileName",lv_File).
        
      /** if the output file already exists, check the MD5 Hash value to see if there have been any modifications since it was last built */
      if search(lv_File) ne ? then 
      do:
        /** check to see if we should overwrite this file, as it already exists */
        if not TTTemplate.Overwrite then next templateloop.
        
        assign file-info:file-name = search(lv_File).
    
        copy-lob from file file-info:full-pathname to lv_ChangedCode.
    
        /** try to get original MD5 hash */
        assign lv_info        = '@BuildInfo(MD5Hash="'
               lv_StartIndex  = index(lv_ChangedCode,lv_info)
               lv_OrigMD5Hash = if lv_StartIndex gt 0 then substr(lv_ChangedCode,lv_StartIndex + length(lv_info),24)
                                                      else "" .
        
        assign lv_StartIndex = index(lv_ChangedCode,"@BuildInfo(Source=").
        
        /** remove the buildinfo from md5 calculation */
        if lv_StartIndex gt 0 then 
        do:
          assign substr(lv_ChangedCode,lv_StartIndex,length(lv_ChangedCode) - lv_StartIndex + 1) = ""
                 lv_ChangedCode = right-trim(lv_ChangedCode,"~n").
        end.
                  
        assign lv_FileMD5Hash = base64-encode(md5-digest(lv_ChangedCode,"Maia")).

        if lv_OrigMD5Hash ne "" and lv_OrigMD5Hash ne lv_FileMD5Hash then
        do:
          message substitute("Warning: &1 has an invalid MD5 value.~nThis indicates that the file has been modified",lv_File) skip
                  "If you continue, any changes made to this file will be overwritten and lost" skip
                  "Do you want to continue building this file ?"
                  view-as alert-box question buttons yes-no update lv_Continue as logical.
          if not lv_Continue then next templateloop.
          
          /** make a backup of the modified file */
          copy-lob from lv_ChangedCode to file substitute("&1.&2",file-info:full-pathname,interval(now,1/1/1970,"milliseconds")).
          
         /** force a build */
         lv_OrigMD5Hash = "".

        end.
        
      end.
      
      Builder1:StartBuild().
      
      /** loop through directives in the template 
        * a directive is started by a "~{&!" and ended with a "!}" */  
      do while true on error undo, throw:
        
        assign lv_StartIndex = index(p_Class:BaseCode,"~{&!").
        
        
        /** no more directives to parse */
        if lv_StartIndex le 0 then leave. 
       
        assign lv_StartLine = r-index(p_Class:BaseCode,"~n",lv_StartIndex) + 1
               lv_EndIndex  = index(p_Class:BaseCode,"!}",lv_StartIndex) + 2.
       
        /** create a new dirctive object */
        Directive1 = new Directive(string(substr(p_Class:BaseCode,lv_StartIndex,lv_EndIndex - lv_StartIndex))).
       
        /** invoke the directive method in the builder */
        dynamic-invoke(Builder1,Directive1:DirectiveName,Directive1) no-error.
        
        if Directive1:Data eq ? then message Directive1:DirectiveName view-as alert-box.
        assign Directive1:Data = replace(Directive1:Data,"~r","").
        
        /** trim the code so that we don't insert blank lines, and the supplied code lines up with 
          * the previous statements in the template */
        if Directive1:Data eq "" and Directive1:TrimCode then
        do:
          assign lv_StartLine = MAX(lv_StartLine - 1,1)
                 lv_EndIndex  = index(p_Class:BaseCode,"~n",lv_endIndex)
                 lv_102B      = p_Class:BaseCode.
                  
          assign substr(lv_102B,lv_StartLine,(lv_EndIndex - lv_StartLine) ) = ""
                 p_Class:BaseCode = lv_102B.
        end.
        
        else
        do:
          assign lv_102B          = p_Class:BaseCode
                 substr(lv_102B,lv_StartIndex,(lv_EndIndex - lv_StartIndex)) = ""
                 p_Class:BaseCode = lv_102B.
                 
          if Directive1:Data ne "" then assign lv_102B                         = p_Class:BaseCode
                                               substr(lv_102B,lv_StartIndex,0) = replace(Directive1:Data,"~n","~n" + fill(" ",lv_StartIndex - lv_StartLine))
                                               p_Class:BaseCode                = lv_102B.
        end.  
        
        finally:
          delete object Directive1 no-error.   
        end finally.                   
      end.
      
      p_Class:SubstituteOptions().
      
      /** pass modified template data to endbuild method */
      Builder1:EndBuild().
      
      assign lv_FileMD5Hash = base64-encode(md5-digest(p_Class:BaseCode,"Maia")).

      /** if the template md5 hash is the same as the new md5 hash, then the file hasn't changed after a build
       *  check to see if the configuration still requires a new build . */ 

      /** don't bother building if MD5 is the same, unless ForceBuild is set to yes */
      
      if search(lv_File) ne ? and lv_OrigMD5Hash eq lv_FileMD5Hash and p_Class:GetOptionValue("ForceBuild") ne "yes" then next templateloop.

      if p_Class:BaseCode eq ? or p_Class:BaseCode eq "" then message "oh dear" view-as alert-box. /** something has gone wrong with building the template */
      
      else
      do:
        if search(lv_File) eq ? then /** we need to create a dummy file so file-info:file-name does not crap out */
        do:
          output to value(lv_File).
          output close.
        end.
        
        p_Class:SetOption("MD5Hash",lv_FileMD5Hash).
        
        assign file-info:file-name = lv_File.
                                                  
        p_Class:SetBuildData().
                                                          
        copy-lob p_Class:BaseCode to file file-info:full-pathname.
      end.
      
    end.
    
    return.
      
    finally:
    
    end finally.
                    
  end method.

end class.