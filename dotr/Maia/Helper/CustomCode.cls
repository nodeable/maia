/*
Copyright (c) 2013, Julian Lyndon-Smith (julian+maia@dotr.com)
http://www.dotr.com
All rights reserved.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction except as noted below, including without limitation
the rights to use,copy, modify, merge, publish, distribute,
and/or sublicense, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

 The Software and/or source code cannot be copied in whole and
 sold without meaningful modification for a profit.

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.

 Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in
 the documentation and/or other materials provided with
 the distribution.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

using Progress.Lang.*.

routine-level on error undo, throw.

class dotr.Maia.Helper.CustomCode: 

  method public longchar GetCustomCode(p_SourceFile as char,p_CustomCode as char ):
    def var lv_i     as int no-undo.
    def var lv_Start as int no-undo.
    def var lv_End   as int no-undo.
    
    def var lv_Data as longchar no-undo.
    
    def var lv_CustomStart as char no-undo.
    def var lv_CustomEnd   as char no-undo.
    
    assign file-info:file-name = p_SourceFile.

    /** if the output file exists, load it up, and find all customcode directives */
    if file-info:full-pathname ne ? then  
    do:
      copy-lob from file file-info:full-pathname to lv_Data.
      
      assign lv_CustomStart = replace('@CustomCode(name="Start&1").',"&1",p_CustomCode)
             lv_CustomEnd   = replace('@CustomCode(name="End&1").',"&1",p_CustomCode)
             lv_Start       = index(lv_Data,lv_CustomStart) + length(lv_CustomStart) 
             lv_End         = index(lv_Data,lv_CustomEnd,lv_Start).
      
      if lv_Start ne 0 and lv_End ne 0 then 
      do:
        assign lv_Data = substitute("&1~n&2~n&3",lv_CustomStart,trim(substr(lv_Data,lv_Start,(lv_end - 1) - lv_Start)," ~n_t"),lv_CustomEnd).
        return lv_Data.
      end.             
    end.
    
    return "".
                          
  end method.
end class.