&ANALYZE-SUSPEND _VERSION-NUMBER AB_v10r12 GUI
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Dialog-Frame 
/*
Copyright (c) 2013, Julian Lyndon-Smith (julian@dotr.com)
http://www.dotr.com
All rights reserved.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction except as noted below, including without limitation
the rights to use,copy, modify, merge, publish, distribute,
and/or sublicense, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

 The Software and/or source code cannot be copied in whole and
 sold without meaningful modification for a profit.

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.

 Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in
 the documentation and/or other materials provided with
 the distribution.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
def input parameter p_base as char no-undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Dialog-Box
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME Dialog-Frame

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-36 RECT-37 Combo_DB fillin_Prefix ~
ToggleMagic fillin_Suffix ToggleOverwrite FILL-IN-1 SELECT-1 Btn_OK-2 
&Scoped-Define DISPLAYED-OBJECTS Combo_DB fillin_Prefix ToggleMagic ~
fillin_Suffix ToggleOverwrite FILL-IN-1 SELECT-1 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD OpenQuery Dialog-Frame 
FUNCTION OpenQuery returns logical
  (  ) FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_OK-2 AUTO-GO 
     LABEL "&Generate" 
     SIZE 15 BY 1.14
     BGCOLOR 8 .

DEFINE VARIABLE Combo_DB AS CHARACTER FORMAT "X(256)":U 
     LABEL "&DB" 
     VIEW-AS COMBO-BOX SORT INNER-LINES 5
     DROP-DOWN-LIST
     SIZE 35 BY 1 NO-UNDO.

DEFINE VARIABLE FILL-IN-1 AS CHARACTER FORMAT "X(256)":U INITIAL "*" 
     VIEW-AS FILL-IN 
     SIZE 64.6 BY 1 NO-UNDO.

DEFINE VARIABLE fillin_Prefix AS CHARACTER FORMAT "X(256)":U 
     LABEL "&Prefix" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE fillin_Suffix AS CHARACTER FORMAT "X(256)":U 
     LABEL "&Suffix" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-36
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 69 BY 4.24.

DEFINE RECTANGLE RECT-37
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 69 BY 21.19.

DEFINE VARIABLE SELECT-1 AS CHARACTER 
     VIEW-AS SELECTION-LIST MULTIPLE SORT SCROLLBAR-VERTICAL 
     SIZE 65 BY 18.57 NO-UNDO.

DEFINE VARIABLE ToggleMagic AS LOGICAL INITIAL yes 
     LABEL "Magic Field Handling" 
     VIEW-AS TOGGLE-BOX
     SIZE 24 BY .81 NO-UNDO.

DEFINE VARIABLE ToggleOverwrite AS LOGICAL INITIAL no 
     LABEL "Overwrite existing build classes" 
     VIEW-AS TOGGLE-BOX
     SIZE 33 BY .81 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME Dialog-Frame
     Combo_DB AT ROW 1.95 COL 12 COLON-ALIGNED WIDGET-ID 12
     fillin_Prefix AT ROW 1.95 COL 55 COLON-ALIGNED WIDGET-ID 16
     ToggleMagic AT ROW 3.14 COL 14 WIDGET-ID 40
     fillin_Suffix AT ROW 3.14 COL 55 COLON-ALIGNED WIDGET-ID 14
     ToggleOverwrite AT ROW 4.1 COL 14 WIDGET-ID 44
     FILL-IN-1 AT ROW 7.19 COL 4.4 COLON-ALIGNED NO-LABEL WIDGET-ID 46
     SELECT-1 AT ROW 8.62 COL 6 NO-LABEL WIDGET-ID 2
     Btn_OK-2 AT ROW 28.14 COL 58 WIDGET-ID 8
     " Tables" VIEW-AS TEXT
          SIZE 9 BY .62 AT ROW 6.24 COL 5 WIDGET-ID 38
     RECT-36 AT ROW 1.52 COL 4 WIDGET-ID 32
     RECT-37 AT ROW 6.48 COL 4 WIDGET-ID 36
     SPACE(1.19) SKIP(2.08)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         TITLE "Generate BuildUnit" WIDGET-ID 100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Dialog-Box
   Allow: Basic,Browse,DB-Fields,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX Dialog-Frame
   FRAME-NAME                                                           */
ASSIGN 
       FRAME Dialog-Frame:SCROLLABLE       = FALSE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME Dialog-Frame
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Dialog-Frame Dialog-Frame
ON window-close OF FRAME Dialog-Frame /* Generate BuildUnit */
do:
  apply "END-ERROR":U to self.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK-2 Dialog-Frame
ON choose OF Btn_OK-2 IN FRAME Dialog-Frame /* Generate */
do:
  def var lv_i as int no-undo.
  def var lv_j as int no-undo.
  
  def var lv_table  as char no-undo.
     
  assign Select-1 fillin_prefix fillin_Suffix.

  do lv_i = 1 to num-entries(Select-1):
    assign lv_table  = entry(lv_i,Select-1).
    
    (new dotr.Maia.Upgrade.scaffold()):generate(p_base,Combo_DB:screen-value,lv_table,ToggleOverwrite:checked).
  end.
  
  message "generation done" view-as alert-box information.
  
  apply "close" to frame {&frame-name}.
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Combo_DB
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Combo_DB Dialog-Frame
ON value-changed OF Combo_DB IN FRAME Dialog-Frame /* DB */
do:
  openQuery(). 
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&Scoped-define SELF-NAME FILL-IN-1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL FILL-IN-1 Dialog-Frame
ON value-changed OF FILL-IN-1 IN FRAME Dialog-Frame /* */
do:
  openQuery().  
end.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Dialog-Frame 


/* ***************************  Main Block  *************************** */

/* Parent the dialog-box to the ACTIVE-WINDOW, if there is no parent.   */
if valid-handle(active-window) and frame {&FRAME-NAME}:PARENT eq ?
then frame {&FRAME-NAME}:PARENT = active-window.


/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
do on error   undo MAIN-BLOCK, leave MAIN-BLOCK
   on end-key undo MAIN-BLOCK, leave MAIN-BLOCK:
  
  run StartUp.
  run enable_UI.
  wait-for go of frame {&FRAME-NAME}.
end.
run disable_UI.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Dialog-Frame  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME Dialog-Frame.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Dialog-Frame  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY Combo_DB fillin_Prefix ToggleMagic fillin_Suffix ToggleOverwrite 
          FILL-IN-1 SELECT-1 
      WITH FRAME Dialog-Frame.
  ENABLE RECT-36 RECT-37 Combo_DB fillin_Prefix ToggleMagic fillin_Suffix 
         ToggleOverwrite FILL-IN-1 SELECT-1 Btn_OK-2 
      WITH FRAME Dialog-Frame.
  {&OPEN-BROWSERS-IN-QUERY-Dialog-Frame}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE StartUp Dialog-Frame 
PROCEDURE StartUp :
  
  def var lv_i as int no-undo.
  
  do lv_i = 1 to num-dbs:
    Combo_DB:ADD-LAST(ldbname(lv_i)) in frame {&frame-name}.
  end.

  return.
    
end procedure.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION OpenQuery Dialog-Frame 
FUNCTION OpenQuery returns logical
  (  ):
/*------------------------------------------------------------------------------
 Purpose:
 Notes:
------------------------------------------------------------------------------*/
  def var lv_Query  as handle no-undo.
  def var lv_Buffer as handle no-undo.
  
  delete alias DICTDB.
  create alias DICTDB for database value(Combo_DB:screen-value in frame {&frame-name}).
  
  assign Select-1:list-items    = ""
         Select-1:screen-value = "".
  
  create query lv_Query.
  create buffer lv_Buffer for table substitute("&1._File",Combo_DB:screen-value).
   
  lv_Query:add-buffer(lv_Buffer).
  
  lv_Query:query-prepare(substitute("FOR each &1._File no-lock where &1._File._file-number GT 0 and &1._File._file-number LT 1000 and &1._File._file-name matches '&2' BY &1._File._File-name",
                                    Combo_DB:screen-value,
                                    fill-in-1:screen-value)).
  lv_Query:query-open().
  lv_Query:GET-FIRST().
  
  do while not lv_Query:query-off-end:
    Select-1:ADD-LAST(lv_Buffer::_File-name) in frame {&FRAME-NAME}.
    
    lv_Query:get-next().
  end.
  
  lv_Query:query-close().
  delete object lv_Query.
  delete object lv_buffer.
  
  assign Select-1 = left-trim(Select-1,",").
  Select-1:screen-value = Select-1.
end function.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

