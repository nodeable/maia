/*
Copyright (c) 2012, Julian Lyndon-Smith (julian+maia@dotr.com)
http://www.dotr.com
All rights reserved.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction except as noted below, including without limitation
the rights to use,copy, modify, merge, publish, distribute,
and/or sublicense, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

 The Software and/or source code cannot be copied in whole and
 sold without meaningful modification for a profit.

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.

 Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in
 the documentation and/or other materials provided with
 the distribution.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/

using Progress.Lang.*.
using dotr.Maia.Enum.*.

routine-level on error undo, throw.

class dotr.Maia.Enum.DataTypeEnum :
  def public property name        as char no-undo get . private set .
  def public property description as char no-undo get . private set .
  
  def public property value as character no-undo get . private set .

  def public static property placeholder as DataTypeEnum no-undo
    get():
      if not valid-object(dotr.Maia.Enum.DataTypeEnum:placeholder) then dotr.Maia.Enum.DataTypeEnum:placeholder = new DataTypeEnum('placeholder','placeholder').
      return dotr.Maia.Enum.DataTypeEnum:placeholder.
    end get . private set .
  
  def public static property blob as DataTypeEnum no-undo
    get():
      if not valid-object(dotr.Maia.Enum.DataTypeEnum:blob) then dotr.Maia.Enum.DataTypeEnum:blob = new DataTypeEnum('blob','blob').
      return dotr.Maia.Enum.DataTypeEnum:blob.
    end get . private set .
  
  def public static property char as DataTypeEnum no-undo
    get():
      if not valid-object(dotr.Maia.Enum.DataTypeEnum:char) then dotr.Maia.Enum.DataTypeEnum:char = new DataTypeEnum('char','character').
      return dotr.Maia.Enum.DataTypeEnum:char.
    end get . private set .
  
  def public static property clob as DataTypeEnum no-undo
    get():
      if not valid-object(dotr.Maia.Enum.DataTypeEnum:clob) then dotr.Maia.Enum.DataTypeEnum:clob = new DataTypeEnum('clob','clob').
      return dotr.Maia.Enum.DataTypeEnum:clob.
    end get . private set .
  
  def public static property date as DataTypeEnum no-undo
    get():
      if not valid-object(dotr.Maia.Enum.DataTypeEnum:date) then dotr.Maia.Enum.DataTypeEnum:date = new DataTypeEnum('date','date').
      return dotr.Maia.Enum.DataTypeEnum:date.
    end get . private set .
  
  def public static property datetime as DataTypeEnum no-undo
    get():
      if not valid-object(dotr.Maia.Enum.DataTypeEnum:datetime) then dotr.Maia.Enum.DataTypeEnum:datetime = new DataTypeEnum('datetime','datetime').
      return dotr.Maia.Enum.DataTypeEnum:datetime.
    end get . private set .

  def public static property datetime-tz as DataTypeEnum no-undo
    get():
      if not valid-object(dotr.Maia.Enum.DataTypeEnum:datetime-tz) then dotr.Maia.Enum.DataTypeEnum:datetime-tz = new DataTypeEnum('datetime-tz','datetime-tz').
      return dotr.Maia.Enum.DataTypeEnum:datetime-tz.
    end get . private set .
  
  def public static property dec as DataTypeEnum no-undo
    get():
      if not valid-object(dotr.Maia.Enum.DataTypeEnum:dec) then dotr.Maia.Enum.DataTypeEnum:dec = new DataTypeEnum('decimal','decimal').
      return dotr.Maia.Enum.DataTypeEnum:dec.
    end get . private set .
  
  def public static property int as DataTypeEnum no-undo
    get():
      if not valid-object(dotr.Maia.Enum.DataTypeEnum:int) then dotr.Maia.Enum.DataTypeEnum:int = new DataTypeEnum('int','integer').
      return dotr.Maia.Enum.DataTypeEnum:int.
    end get . private set .
  
  def public static property int64 as DataTypeEnum no-undo
    get():
      if not valid-object(dotr.Maia.Enum.DataTypeEnum:int64) then dotr.Maia.Enum.DataTypeEnum:int64 = new DataTypeEnum('int64','int64').
      return dotr.Maia.Enum.DataTypeEnum:int64.
    end get . private set .
  
  def public static property logical as DataTypeEnum no-undo
    get():
      if not valid-object(dotr.Maia.Enum.DataTypeEnum:logical) then dotr.Maia.Enum.DataTypeEnum:logical = new DataTypeEnum('logical','logical').
      return dotr.Maia.Enum.DataTypeEnum:logical.
    end get . private set .
  
  def public static property longchar as DataTypeEnum no-undo
    get():
      if not valid-object(dotr.Maia.Enum.DataTypeEnum:longchar) then dotr.Maia.Enum.DataTypeEnum:longchar = new DataTypeEnum('longchar','longchar').
      return dotr.Maia.Enum.DataTypeEnum:longchar.
    end get . private set .
  
  def public static property raw as DataTypeEnum no-undo
    get():
      if not valid-object(dotr.Maia.Enum.DataTypeEnum:raw) then dotr.Maia.Enum.DataTypeEnum:raw = new DataTypeEnum('raw','raw').
      return dotr.Maia.Enum.DataTypeEnum:raw.
    end get . private set .
  
  def public static property recid as DataTypeEnum no-undo
    get():
      if not valid-object(dotr.Maia.Enum.DataTypeEnum:recid) then dotr.Maia.Enum.DataTypeEnum:recid = new DataTypeEnum('recid','recid').
      return dotr.Maia.Enum.DataTypeEnum:recid.
    end get . private set .
  
  def public static property rowid as DataTypeEnum no-undo
    get():
      if not valid-object(dotr.Maia.Enum.DataTypeEnum:rowid) then dotr.Maia.Enum.DataTypeEnum:rowid = new DataTypeEnum('rowid','rowid').
      return dotr.Maia.Enum.DataTypeEnum:rowid.
    end get . private set .

  constructor static DataTypeEnum():
  end method.

  constructor private DataTypeEnum(p_Name as char,p_Value as character):
    this-object(p_Name,p_Value,p_Name).
  end constructor.

  constructor private DataTypeEnum(p_Name as char,p_Value as character,p_Desc as char):
    assign this-object:name        = p_Name
           this-object:value       = p_Value
           this-object:description = p_Desc.
  end constructor.
  
  /** return the enum described by the supplied name
   * @param name of enum to find
   * @return dotr.Maia.Enum.DataTypeEnum
   */
  
  method static DataTypeEnum GetByName(p_Name as char):
    case p_Name:
    	when 'placeholder':u then return DataTypeEnum:placeholder.
    	when 'blob':u        then return DataTypeEnum:blob.
    	when 'char':u        then return DataTypeEnum:char.
    	when 'clob':u        then return DataTypeEnum:clob.
    	when 'date':u        then return DataTypeEnum:date.
    	when 'datetime':u    then return DataTypeEnum:datetime.
    	when 'datetime-tz':u    then return DataTypeEnum:datetime-tz.
    	when 'decimal':u         then return DataTypeEnum:dec.
    	when 'int':u         then return DataTypeEnum:int.
    	when 'int64':u       then return DataTypeEnum:int64.
    	when 'logical':u     then return DataTypeEnum:logical.
    	when 'longchar':u    then return DataTypeEnum:longchar.
    	when 'raw':u         then return DataTypeEnum:raw.
    	when 'recid':u       then return DataTypeEnum:recid.
    	when 'rowid':u       then return DataTypeEnum:rowid.
    
    	otherwise undo, throw new Progress.Lang.AppError(substitute('Invalid DataTypeEnum Enum Name [&1]',p_name),-999).
    end case.
  end method.

  /** return the enum described by the supplied value
   * @param name of enum to find
   * @return dotr.Maia.Enum.DataTypeEnum
   */
  
  method static DataTypeEnum GetByValue(p_Value as character):
    case p_Value:
    	when 'placeholder' then return DataTypeEnum:placeholder.
    	when 'blob'      then return DataTypeEnum:blob.
    	when 'character' then return DataTypeEnum:char.
    	when 'clob'      then return DataTypeEnum:clob.
    	when 'date'      then return DataTypeEnum:date.
    	when 'datetime'  then return DataTypeEnum:datetime.
    	when 'datetime-tz'  then return DataTypeEnum:datetime-tz.
    	when 'decimal'   then return DataTypeEnum:dec.
    	when 'integer'   then return DataTypeEnum:int.
    	when 'int64'     then return DataTypeEnum:int64.
    	when 'logical'   then return DataTypeEnum:logical.
    	when 'longchar'  then return DataTypeEnum:longchar.
    	when 'raw'       then return DataTypeEnum:raw.
    	when 'recid'     then return DataTypeEnum:recid.
    	when 'rowid'     then return DataTypeEnum:rowid.
    
    	otherwise undo, throw new Progress.Lang.AppError(substitute('Invalid DataTypeEnum Enum Value [&1]',p_value),-998).
    end case.
  end method.
  
  
end class.
@BuildInfo(Source="DataType").
@BuildInfo(BuildUnitGUID="f40f9ac8-fa40-bf99-e211-15a276c19c4a").
@BuildInfo(Project="Maia3Enum").
@BuildInfo(Date="10/04/2013 20:38:30.961+01:00").
@BuildInfo(MD5Hash="ApvmyvBxz1syl2Hne5z1lQ==").