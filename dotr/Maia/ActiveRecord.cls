/*
Copyright (c) 2013, Julian Lyndon-Smith (julian@dotr.com)
http://www.dotr.com
All rights reserved.

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction except as noted below, including without limitation
the rights to use,copy, modify, merge, publish, distribute,
and/or sublicense, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

 The Software and/or source code cannot be copied in whole and
 sold without meaningful modification for a profit.

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.

 Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in
 the documentation and/or other materials provided with
 the distribution.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
*/
using Progress.Lang.*.

routine-level on error undo, throw.

class dotr.Maia.ActiveRecord inherits dotr.Maia.Model: 
  {dotr/maia/temptable/Association.i &AccessMode=protected}
  {dotr/maia/temptable/FindUsing.i &AccessMode=protected}
 
  def temp-table TTIndex no-undo
    field guid as char
    field name as char
    field IsWordIndex as logical
    field IsUnique as logical
    field numComp as int
    
    index guid is unique
      guid
      
    index name is primary unique
      name.
      
  def temp-table TTIndexField no-undo
    field IndexGUID as char
    field Order as int 
    field FieldName as char
    
    index guidorder is primary unique
      IndexGUID
      Order.
          
  def public property TableName   as char no-undo get . protected set .
  def public property DBName      as char no-undo get . protected set .
  def public property Exclude     as char no-undo get . protected set .
  def public property KeyIndex    as char no-undo get . protected set .
  
  def public property TablePrefix as char no-undo get . 
    protected set(p_prefix as char):
      assign TablePrefix = p_prefix.
      SetClassName() .
    end set.

  def public property TableSeperator as char no-undo get . 
    protected set(p_prefix as char):
      assign TableSeperator = p_prefix.
      SetClassName() .
    end set.

  def public property KeyFieldName as char extent  no-undo get . private set .
  def public property KeyProperty  as char extent  no-undo get . private set .
  def public property KeyFieldType as char extent  no-undo get . private set .
  def public property KeyFieldFind as char         no-undo get . private set .

  constructor ActiveRecord():
    def var lv_Table as char no-undo.
    
    assign lv_Table = this-object:GetClass():TypeName
           lv_Table = entry(num-entries(lv_Table,"."),lv_Table,".").
           
    TableName(lv_table).           
  end constructor.
  
  constructor ActiveRecord(p_table as char):
    TableName(p_table).  
  end constructor.

  method public void TableName(p_table as char):
    assign this-object:TableName = p_table.
  end method.
  
  method private void SetClassName():
    def var lv_Prefix as char no-undo.
    
    assign lv_Prefix = MagicName(this-object:TablePrefix).
    
    case true:
      when this-object:TableSeperator ne "" and num-entries(this-object:TableName,this-object:TableSeperator) gt 1
           then assign this-object:className = MagicName(substr(this-object:tableName,index(this-object:TableName,this-object:TableSeperator) + 1)).
      
      when this-object:TablePrefix ne "" and this-object:TableName begins lv_Prefix  
            then assign this-object:className = substr(this-object:TableName,length(lv_Prefix) + 1).
    end case. 
  end method.
  
  method public void GetAssociations(output table for TTAssociation bind):
  end method.
  
  method public void GetFindUsing(output table for TTFindUsing bind):
  end method.

  method public void GetValidation(output table for TTValidation bind):
  end method.
    
  method override public void init():
    super:init().
    
    UsingTemplate("@ActiveRecord").
    GetDBProperties().

    SetDefaultOption("DB", dbname).
    SetDefaultOption("TableName",TableName).

    catch e as Progress.Lang.Error :
      message "ooops" e:getmessage(1) view-as alert-box.
    end catch.
  end method.
  
  method protected void Exclude(p_fields as char):
    assign Exclude = p_fields.
  end method. 
  
  method protected dotr.Maia.Model.Association BelongsTo(p_model as char):
    return NewAssociation(p_model,"BelongsTo").
  end method. 

  method protected dotr.Maia.Model.Association HasMany(p_model as char):
    return NewAssociation(p_model,"HasMany").
  end method. 

  method private dotr.Maia.Model.Association NewAssociation(p_model as char,p_type as char):
    def var Association as dotr.Maia.Model.BuildAssociation no-undo.
    
    Association = new dotr.Maia.Model.BuildAssociation(p_Model,p_Type).
    
    return cast(Association,dotr.Maia.Model.Association).
  end method. 

  method protected void KeyIndex(p_Key as char):
    find TTIndex where TTIndex.name eq p_Key no-lock no-error.
    
    if not avail TTIndex then undo, throw new AppError(substitute("Invalid index name [&1]",p_Key)).
    
    assign this-object:KeyIndex = p_Key. 
  end method.
    
  method protected void Association(p_association as dotr.Maia.Model.Association):
    def var lv_i as int no-undo.
    
    def var Association1 as dotr.Maia.Model.BuildAssociation no-undo.
    
    Association1 = cast(p_association,dotr.Maia.Model.BuildAssociation).
    
    assign Association1:Association:FromModel = this-object:ClassName.
     
    find TTAssociation where TTAssociation.ToModel       eq Association1:Association:ToModel
                         and TTAssociation.FindUsingName eq Association1:Association:FindUsingName
                         no-error.

    if avail TTAssociation then undo, throw new AppError(substitute("Association already exists to [&1]",Association1:Association:ToModel),0).
    
    create TTAssociation.
    
    assign TTAssociation.ToModel                = Association1:Association:ToModel
           TTAssociation.FindUsingProperties    = Association1:Association:FindUsingProperties
           TTAssociation.FindUsingName          = Association1:Association:FindUsingName
           TTAssociation.FindUsing_fkProperties = Association1:Association:FindUsingFkProperties
           TTAssociation.FindUsing_fkName       = Association1:Association:FindUsingFkName
           TTAssociation.CascadeMode            = Association1:Association:Cascade
           TTAssociation.Type                   = Association1:Association:type
           TTAssociation.PropertyName           = Association1:Association:FromName
           TTAssociation.FromModel              = Association1:Association:FromModel.
    
/*    /** don't need a find using for child associations (HasMany / HasOne ) */*/
/*    if TTAssociation.Type begins "has" then return.                          */

    find TTFindUsing where TTFindUsing.GetName eq TTAssociation.FindUsingName no-lock no-error.
    
    if avail TTFindUsing then return.

    create TTFindUsing.
    
    assign TTFindUsing.IsUnique    = no
           TTFindUsing.IsWordIndex = no
           TTFindUsing.GetName     = "FindUsing".
    
    do lv_i = 1 to num-entries(TTAssociation.FindUsingProperties):
      find TTProperty where TTProperty.PropertyName eq entry(lv_i,TTAssociation.FindUsingProperties) no-lock no-error.
      
      if not avail TTProperty then undo, throw new AppError(substitute("Invalid Property &1 specified in findusing phrase",entry(lv_i,TTAssociation.FindUsingProperties)),0).

      UpdateFindUsing(rowid(TTFindUsing),TTProperty.FieldName,lv_i,lv_i eq num-entries(TTAssociation.FindUsingProperties)).                                      
    end.  
  end method.
    
  /** read database table, getting fields and create properties based on those fields 
   * @param this-object : object to build
   */
  
  method private void GetDBProperties():
    def var lv_index    as int no-undo.
    def var lv_Endindex as int no-undo.
    def var lv_MaxLen   as int no-undo.
    def var lv_i        as int no-undo.
    def var lv_comp     as int no-undo.
    def var lv_TypeLen  as  int  no-undo extent 10 .
    
    def var lv_Data  as longchar no-undo.
    
    def var lv_Field     as char no-undo.
    def var lv_Options   as char no-undo.
    def var lv_Table     as char no-undo.
    
    def var lv_DBBuffer as handle no-undo.
    
    def var FileHandle as handle no-undo.
    def var FieldHandle as handle no-undo.
    def var QueryHandle as handle no-undo.

    def var Property1 as dotr.Maia.Model.BuildProperty no-undo.
    
    assign lv_table = substitute("&1.&2",this-object:DBName,this-object:tableName).
    
    create buffer FileHandle for table substitute("&1._file",this-object:DBName) no-error.
    
    if not valid-handle(FileHandle) then undo, throw new AppError(substitute("Invalid table &1",lv_table),0).
    
    FileHandle:find-unique(substitute("where &1._File._File-name eq '&2'",
                                      this-object:DBName,
                                      this-object:tablename)).

    create buffer FieldHandle for table substitute("&1._field",this-object:DBName) .
    
    create query QueryHandle.
    
    QueryHandle:add-buffer(FieldHandle).
    QueryHandle:query-prepare(substitute("for each &1._field where &1._field._file-recid eq &2 no-lock break by &1._Field._data-type by &1._Field._field-name",
                                          this-object:dbname,
                                          FileHandle:recid)).
    QueryHandle:query-open().
    
    create buffer lv_DBBuffer for table lv_table.
    
    QueryHandle:get-first().
              
    /** loop through all fields */
    do while not QueryHandle:query-off-end on error undo, throw:
    
      /** each datatype is grouped and split by a blank line 
        * we need to identify the longest property name in each datatype
        * so that we can align the code nicely */ 
      if queryhandle:first-of(1) then 
      do:
       assign lv_Index = lv_Index + 1.
      end.

      Property1 = new dotr.Maia.Model.BuildProperty(MagicName(FieldHandle::_Field-name)).
    
      find TTProperty where TTProperty.PropertyName eq Property1:PropertyData:PropertyName no-lock no-error.
      
      if avail TTProperty then assign  Property1:PropertyData:PropertyName = Property1:PropertyData:PropertyName + FieldHandle::_Data-type.
      
      assign Property1:PropertyData:AccessMode     = "public"
             Property1:PropertyData:BuildProperty  = FieldHandle::_Data-type ne "blob"
             Property1:PropertyData:Comment        = ""
             Property1:PropertyData:DataType       = FieldHandle::_Data-type
             Property1:PropertyData:Extent         = FieldHandle::_Extent 
             Property1:PropertyData:FieldName      = FieldHandle::_Field-name
             Property1:PropertyData:ManualProperty = no
             Property1:PropertyData:Mode           = "add"
             Property1:PropertyData:label          = FieldHandle::_Label
             Property1:PropertyData:Help           = FieldHandle::_Help
             Property1:PropertyData:ViewAs         = FieldHandle::_View-As
             Property1:PropertyData:InitialValue   = FieldHandle::_initial.

      assign lv_TypeLen[lv_index] = MAX(lv_TypeLen[lv_index],length(FieldHandle::_Field-name) + 1)
             lv_MaxLen            = MAX(lv_TypeLen[lv_index],lv_MaxLen).
      
      /** if field matches guid or <<table>>GUID or <<table>>_GUID then automatically set it as a generate guid field */
      if Property1:PropertyData:FieldName eq substitute("&1GUID",this-object:ClassName) or Property1:PropertyData:FieldName eq "GUID"         
         then assign Property1:PropertyData:GenerateGUID  = yes.
      
      /** if field matches <<table>>ID or <<table>>_id or i<<table>>ID then automatically set it as a next-value field */

      if Property1:PropertyData:DataType eq "integer" and ((Property1:PropertyData:FieldName eq substitute("&1ID",this-object:ClassName) or Property1:PropertyData:FieldName eq "id")) 
                                          and can-find(DICTDB._sequence where DICTDB._sequence._seq-name eq Property1:PropertyData:PropertyName) 
         then assign Property1:PropertyData:Sequence = Property1:PropertyData:PropertyName.
    
      Property(Property1).
         
      finally:
        queryhandle:get-next().
      end finally.
    end.
    
    BuildIndex(FileHandle,FieldHandle).
    
    do lv_i = 1 to num-entries(this-object:Exclude):
      find TTProperty where TTProperty.FieldName eq entry(lv_i,this-object:Exclude) no-error.
      if avail TTproperty then assign TTProperty.BuildProperty = no.
    end.
      
    catch e as Progress.Lang.Error :
      message "ooops#2" e:getmessage(1) view-as alert-box.
    end catch.

    finally:
      delete object lv_DBBuffer no-error.
      delete object FileHandle  no-error.
      delete object FieldHandle no-error.
      delete object QueryHandle no-error.
    end finally.
      
  end method.
  
  method private void UpdateFindUsing(p_FindUsingRowid as rowid,p_Field as char,p_Seq as int,p_Last as logical):
    def var lv_and     as char no-undo.
    def var lv_prefix  as char no-undo.
    def var lv_nullify as char no-undo.
    
    def var lv_index as int no-undo.
    def var lv_seq   as int no-undo.
    
    def buffer TTPartial for temp-table TTFindUsing.
        
    find TTProperty  where TTProperty.FieldName eq p_Field no-lock no-error.
    find TTFindUsing where rowid(TTFindUsing) eq p_FindUsingRowid no-lock no-error.
    
    assign lv_and    = if p_Seq gt 1 then " and " else ""
           lv_Prefix = if p_Seq gt 1 then "" else ""
           lv_Index  = int(trunc(p_seq / 10,0)) + 1
           lv_seq    = p_seq mod 9.
           
    if lv_seq eq 0 then assign lv_Seq = 9.           
               
    assign TTFindUsing.Property[p_seq]   = TTProperty.PropertyName
           TTFindUsing.FieldName[p_seq]  = TTProperty.FieldName
           TTFindUsing.MaxProperty       = max(TTFindUsing.MaxProperty,length(TTProperty.PropertyName)) 
           TTFindUsing.GetName           = TTFindUsing.GetName           + TTProperty.PropertyName
           TTFindUsing.parameterList     = TTFindUsing.parameterList     + substitute(", p_&1 as &2",TTProperty.PropertyName,lc(TTProperty.dataType))
           TTFindUsing.parameterCall     = TTFindUsing.parameterCall     + substitute(", p_&1",TTProperty.PropertyName)
           TTFindUsing.SubstituteList[lv_Index] = TTFindUsing.SubstituteList[lv_Index] + substitute(",quoter(p_&1)",TTProperty.PropertyName)
           TTFindUsing.FindString[lv_Index]     = TTFindUsing.FindString[lv_Index]     + substitute('&1&3_&4.&5 &6 _maia_##&7 ',
                                                                                                lv_Prefix,
                                                                                                TTProperty.PropertyName,
                                                                                                lv_and,
                                                                                                this-object:TableName,
                                                                                                TTProperty.FieldName,
                                                                                                if TTFindUsing.isWordIndex then "contains" else "eq",
                                                                                                lv_seq
                                                                                                )
             TTFindUsing.FindUnique = TTFindUsing.FindUnique + substitute('&1 &3_&4.&5 eq p_&2',
                                                                                                lv_Prefix,
                                                                                                TTProperty.PropertyName,
                                                                                                lv_and,
                                                                                                this-object:TableName,
                                                                                                TTProperty.FieldName
                                                                                                ).
    assign TTFindUsing.parameterList            = trim(TTFindUsing.parameterList,",")
           TTFindUsing.parameterCall            = trim(TTFindUsing.parameterCall,",")
           TTFindUsing.SubstituteList[lv_Index] = trim(TTFindUsing.SubstituteList[lv_Index],",")
           TTFindUsing.DocParameterList         = replace(TTFindUsing.parameterList," as ", " - ").
  
    if not p_Last then
    do:
      find first TTPartial where TTPartial.GetName eq TTFindUsing.GetName no-lock no-error.
      
      if not avail TTPartial or rowid(TTPartial) eq rowid(TTFindUsing) then
      do:  
        create TTPartial.
        buffer-copy TTFindUsing to TTPartial.
        assign TTPartial.IsUnique = no.
        
        do lv_index = 1 to extent(TTFindUsing.FindString):
          if TTPartial.FindString[lv_index] eq "" then next.

          assign TTPartial.FindString[lv_index] = substitute('&1substitute("&2&3",&4)', 
                                                             if lv_Index gt 1 then " +~n " else "",
                                                             if lv_index eq 1 then "where " else "",
                                                             TTPartial.FindString[lv_index],
                                                             TTPartial.SubstituteList[lv_index])
                 TTPartial.FindString[lv_index] = replace(TTPartial.FindString[lv_index],"_maia_##","&").
        end.                 
      end.
    end.
    
    else
    do:
      do lv_index = 1 to extent(TTFindUsing.Property):
        if TTFindUsing.Property[lv_index] eq "" then leave.
        assign lv_nullify = lv_nullify + substitute("this-object:&1@ = ?~n           ",TTFindUsing.Property[lv_index])
               lv_nullify = replace(lv_nullify,"@",fill(" ",TTFindUsing.MaxProperty - length(TTFindUsing.Property[lv_index]))). 
      end.
      
      assign lv_nullify          = trim(lv_Nullify)
             TTFindUsing.Nullify = trim(lv_Nullify,"~n").
      
      do lv_index = 1 to extent(TTFindUsing.FindString):
        if TTFindUsing.FindString[lv_index] eq "" then next.
  
        assign TTFindUsing.FindString[lv_index] = substitute('&1substitute("&2&3",&4)', 
                                                           if lv_Index gt 1 then " +~n~t~t~t~t~t~t~t~t~t " else "",
                                                           if lv_index eq 1 then "where " else "",
                                                           TTFindUsing.FindString[lv_index],
                                                           TTFindUsing.SubstituteList[lv_index])
               TTFindUsing.FindString[lv_index] = replace(TTFindUsing.FindString[lv_index],"_maia_##","&").
                      
      end.
    end.     
  end method.
      
  method private void BuildIndex(p_File as handle,p_field as handle):
    def var IndexHandle      as handle no-undo.
    def var IndexFieldHandle as handle no-undo.
    def var IndexQueryHandle as handle no-undo.
    
    def var lv_i as int no-undo.
    
    create buffer IndexHandle      for table substitute("&1._Index",this-object:DBName).
    create buffer IndexFieldHandle for table substitute("&1._Index-Field",this-object:DBName).
    
    create query IndexQueryHandle.
    
    IndexQueryHandle:add-buffer(IndexHandle).
    IndexQueryHandle:add-buffer(IndexFieldHandle).
    
    IndexQueryHandle:query-prepare(substitute("for each &1._Index where &1._Index._file-recid eq &2 no-lock,
                                                   each &1._Index-Field where &1._Index-field._index-recid eq recid(&1._Index) break by &1._Index._Index-name",
                                          this-object:DBName,
                                          p_File:recid)).
    IndexQueryHandle:query-open().
    
    IndexQueryHandle:get-first().
    
    do while not IndexQueryHandle:query-off-end on error undo, throw: 
      
      if IndexQueryHandle:first-of(1) then
      do:
        assign lv_i         = 1.
        
        if IndexHandle::_num-comp eq 0 then next.

        create TTIndex.
        assign TTIndex.guid        = guid(generate-uuid)
               TTIndex.IsUnique    = IndexHandle::_Unique
               TTIndex.Name        = IndexHandle::_Index-Name
               TTIndex.numComp     = IndexHandle::_num-comp
               TTIndex.IsWordIndex = if IndexHandle::_WordIDX eq 1 then yes else no.
               
      end.

      p_field:find-unique(substitute("where recid(&1._Field) eq &2",
                                      this-object:DBName,
                                      IndexFieldHandle::_field-recid)).

      create TTIndexField.
      assign TTIndexField.IndexGUID = TTIndex.GUID
             TTIndexField.Order     = IndexFieldHandle::_Index-Seq
             TTIndexField.FieldName = p_Field::_Field-name.
      
      finally:
        IndexQueryHandle:get-next().
      end finally.
      
    end.
    
    catch e as Progress.Lang.Error :
      message e:getmessage(1) skip e:callstack view-as alert-box.
      undo, throw e.
    end catch.
    
    finally:
      delete object IndexHandle      no-error.
      delete object IndexFieldHandle no-error.
      delete object IndexQueryHandle no-error.
    end finally.
    
  end method.
  
  method public void BuildFindUsing():
    def var lv_Seq as int no-undo.
    
    def var lv_findname as char no-undo.
   
    def var lv_i as int no-undo.
    
    indexloop:
    do while this-object:KeyIndex eq "": /** no key supplied, try to work it out */
    
      /** try unique quid or id */
      for each TTIndex where TTIndex.IsUnique eq yes 
                         and TTIndex.numComp eq 1 no-lock,
          first TTIndexField where TTIndexField.IndexGUID eq TTIndex.guid 
                              and TTIndexField.FieldName matches "*ID" no-lock:
       assign this-object:KeyIndex = TTIndex.name.
       leave indexloop. 
      end.

      /** try first unique single index */
      for each TTIndex where TTIndex.IsUnique eq yes 
                         and TTIndex.numComp eq 1 no-lock by TTIndex.Name:
       assign this-object:KeyIndex = TTIndex.name.
       leave indexloop. 
      end.

      /** try first unique multiple index */
      for each TTIndex where TTIndex.IsUnique eq yes 
                         and TTIndex.numComp gt 1 no-lock,
          each TTIndexField where TTIndexField.IndexGUID eq TTIndex.guid no-lock by TTIndex.numComp by TTIndex.Name:
       assign this-object:KeyIndex = TTIndex.name.
       leave indexloop. 
      end.
      
      assign this-object:KeyIndex = "rowid". /** give up and use rowid */
    end.
    
    if extent(KeyFieldName) eq ? then
    do: 
      find TTIndex where TTIndex.name eq this-object:KeyIndex no-lock no-error.  

      if avail TTIndex then 
      do: 
        extent(KeyFieldName) = TTIndex.numComp.
        extent(KeyFieldType) = TTIndex.numComp.
        extent(KeyProperty)  = TTIndex.numComp.
      
        for each TTIndexField where TTIndexField.IndexGUID eq TTIndex.guid no-lock lv_i = 1 to lv_i + 1:
          find TTProperty where TTProperty.FieldName eq TTIndexField.FieldName no-lock no-error.
          if not avail TTProperty then next. 
          
          assign KeyFieldName[lv_i] = TTIndexField.FieldName
                 KeyFieldType[lv_i] = TTProperty.DataType
                 KeyProperty[lv_i]  = TTProperty.PropertyName.
        end.
      
        assign KeyFieldFind = if extent(KeyFieldType) gt 1 
                                 then "multiple"
                                 else "single". 
      end.
      
      else
      do:
        extent(KeyFieldName) = 1.
        extent(KeyFieldType) = 1.
        extent(KeyProperty) = 1.
        
        assign KeyFieldName[1] = "rowid"
               KeyFieldType[1] = "rowid"
               KeyProperty[1]  = "rowid"
               KeyFieldFind    = "rowid". 
      end.
    end.

    SetDefaultOption("KeyFieldDatatype",KeyFieldType[1]).
    SetDefaultOption("KeyFieldName",KeyFieldName[1]).
    SetDefaultOption("KeyProperty",KeyProperty[1]).
      
    for each TTIndex no-lock by TTIndex.Name:
      
      create TTFindUsing.
      
      assign TTFindUsing.IsUnique    = TTIndex.IsUnique
             TTFindUsing.IsWordIndex = TTIndex.IsWordIndex
             TTFindUsing.IndexName   = TTIndex.name
             TTFindUsing.GetName     = if TTIndex.IsWordIndex then "FindContaining"
                                                              else "FindUsing".
    
      for each TTIndexField where TTIndexField.IndexGUID eq TTIndex.guid no-lock break by TTIndexField.IndexGUID lv_Seq = 1 to lv_Seq + 1:
        UpdateFindUsing(rowid(TTFindUsing),TTIndexField.FieldName,lv_Seq,last-of(TTIndexField.IndexGUID)).                                      
      end. 
    end.
    
    for each TTAssociation where TTAssociation.Type begins "belongs" no-lock:

      find TTFindUsing where TTFindUsing.GetName eq TTAssociation.FindUsingName no-lock no-error.
      
      if avail TTFindUsing then next.
         
      create TTFindUsing. 
      assign TTFindUsing.IsUnique    = no
             TTFindUsing.IsWordIndex = no
             TTFindUsing.GetName     = "FindUsing".

      do lv_i = 1 to num-entries(TTAssociation.FindUsingProperties):
        find TTProperty where TTProperty.PropertyName eq entry(lv_i,TTAssociation.FindUsingProperties) no-lock no-error.
        
        if not avail TTProperty then undo, throw new AppError(substitute("Invalid Property &1 specified in findusing phrase",entry(lv_i,TTAssociation.FindUsingProperties)),0).
  
        UpdateFindUsing(rowid(TTFindUsing),TTProperty.FieldName,lv_i,lv_i eq num-entries(TTAssociation.FindUsingProperties)).                                      
      end.  

    end.
    
    /** remove all duplicates : these can be caused by processing a field1field2field3 index
      * which creates findusingField1 findusingField1Field2 and findusingField1Field2Field3
      * and then processing a field1field2 index. This will duplicate findusingField1Field2 
      * it's simpler just to remove the duplicates than it is to try and put logic in :) */

    /** step 1 : remove all TTFindUsing that are duplicates, but not the one defined as the primary key
      * this fixes problems with multiple indexes defined with the same fields, but one is used
      * as the primary or unique index */
      
    find first TTFindUsing no-lock where TTFindUsing.IndexName eq this-object:keyIndex no-error.
    
    if avail TTFindUsing then
    do:
      assign lv_findname = TTFindUsing.GetName.
      
      for each ttfindusing where ttfindusing.GetName eq lv_findName:
        if ttfindusing.IndexName eq this-object:keyIndex then next. /** don't delete primary key */
        delete ttfindusing.
      end.
    end.
    
    /** step 2 : now remove all other duplicates. this fixes problems with multiple indexes defined 
      * with the same fields */
       
    for each TTFindUsing break by TTFindUsing.GetName by TTFindUsing.IsUnique:
      if not last-of(TTFindUsing.IsUnique) then assign TTFindUsing.IsUnique = yes.
      if not last-of(TTFindUsing.GetName) then delete TTFindUsing.
    end.

  end method.

end class.